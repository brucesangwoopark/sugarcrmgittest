<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-21
 * Time: 오전 10:18
 */


$from = $_POST['from'];
$to = $_POST['to'];
$cols = json_decode($_POST['cols']);
$oauth_token = $_POST['oauthtoken'];

$session_list = scandir(session_save_path());
if (!in_array("sess_{$oauth_token}", $session_list)) {
    header('HTTP/1.1 401 Unauthorized');
    echo 'Login Sugar!!';
    return;
}

require_once '../../config.php';

$servername = $sugar_config['dbconfig']['db_host_name'];
$username = $sugar_config['dbconfig']['db_user_name'];
$password = $sugar_config['dbconfig']['db_password'];
$db_name = $sugar_config['dbconfig']['db_name'];

$conn = new mysqli($servername, $username, $password);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Type: application/vnd.ms-excel; charset=utf-8');
header("Content-Disposition: attachment; filename = report_{$from}_{$to}.xls");

$selected_cols = '';
foreach ($cols as $key => $value) {
    switch ($key) {
        case 'user_real_name':
            $selected_cols .= "concat(u.first_name, ' ', u.last_name) as '{$value->name}',";
            break;
        case 'ex_cancel_reason':
            $selected_cols .= "
                CASE
                    WHEN acc.billing_address_country = 'Canada' AND TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) <= 10 THEN 'IBC'
                    WHEN acc.billing_address_country = 'United States' AND TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) <= 3 THEN 'IBC'
                    ELSE ''
                END as '{$value->name}',";
            break;
        case 'period_of_use':
            $selected_cols .= "TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) as '{$value->name}',";
            break;
        default:
            $col_name = $key;
            $pos = strpos($key, '_');
            if ($pos !== false) {
                $col_name = substr_replace($key, '.', $pos, strlen('_'));
            }
            $selected_cols .= "{$col_name} as '{$value->name}',";
    }
}
$selected_cols = substr_replace($selected_cols, '', -1);

$sql = "
    select 
        {$selected_cols}
    from {$db_name}.revenue_line_items rli
    join {$db_name}.revenue_line_items_cstm rlic on rli.id = rlic.id_c
    join {$db_name}.users u on rlic.who_cancelled_c = u.id
    left join {$db_name}.accounts acc on acc.id = rli.account_id
    left join {$db_name}.accounts_cstm accc on acc.id = accc.id_c
    left join {$db_name}.opportunities opp on rli.opportunity_id = opp.id
    where 
        rli.deleted = 0 and 
        left(rlic.when_cancelled_c, 10) >= '{$from}' and 
        left(rlic.when_cancelled_c, 10) <= '{$to}' and 
        rlic.uninstall_c = '1';
";

$result = $conn->query($sql);

$excel_body = "<table border='1'>";

$first_row = true;
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if ($first_row) {
            $table_header = '<tr>';
            foreach ($row as $key => $value) {
                foreach ($cols as $col_name => $col_value) {
                    if ($key === $col_value->name) {
                        $table_header .= "<td style=\"mso-number-format:\@ \">{$key}</td>";
                    }
                }
            }
            $table_header .= '</tr>';
            $excel_body .= $table_header;
            $first_row = false;
        }

        $table_body_row = '<tr>';
        foreach ($row as $key => $value) {
            foreach ($cols as $col_name => $col_value) {
                if ($key === $col_value->name) {
                    $table_body_row .= "<td style=\"mso-number-format:\@ \">{$value}</td>";
                }
            }
        }
        $table_body_row .= '</tr>';
        $excel_body .= $table_body_row;
    }
} else {
    $excel_body .= '<tr>';
    $excel_body .= '<td>0 results</td>';
    $excel_body .= '</tr>';
}

$excel_body .= '</table >';

echo '<meta content="application/vnd.ms-excel; charset=UTF-8" name="Content-type">';
echo $excel_body;

$conn->close();
