<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['abcde_Jobs'] = 'Jobs';
$app_list_strings['moduleListSingular']['abcde_Jobs'] = 'Job';
$app_list_strings['priority_list'][1] = '1';
$app_list_strings['priority_list'][2] = '2';
$app_list_strings['priority_list'][0] = '0';
