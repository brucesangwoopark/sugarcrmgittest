<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Išimti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Redaguotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Jobs Sąrašas',
  'LBL_MODULE_NAME' => 'Jobs',
  'LBL_MODULE_TITLE' => 'Jobs',
  'LBL_MODULE_NAME_SINGULAR' => 'Job',
  'LBL_HOMEPAGE_TITLE' => 'Mano Jobs',
  'LNK_NEW_RECORD' => 'Sukurti Job',
  'LNK_LIST' => 'View Jobs',
  'LNK_IMPORT_ABCDE_JOBS' => 'Import Jobs',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Job',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_ABCDE_JOBS_SUBPANEL_TITLE' => 'Jobs',
  'LBL_NEW_FORM_TITLE' => 'Naujas Job',
  'LNK_IMPORT_VCARD' => 'Import Job vCard',
  'LBL_IMPORT' => 'Import Jobs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Job record by importing a vCard from your file system.',
  'LBL_TECH_SUPKG_SYSTEMUSERS_ID' => 'tech (related System User ID)',
  'LBL_TECH' => 'tech',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'opportunity (related Opportunity ID)',
  'LBL_OPPORTUNITY' => 'opportunity',
  'LBL_LOCATION' => 'location',
  'LBL_COMMENTS' => 'comments',
  'LBL_JOB_PRIORITY' => 'job priority',
  'LBL_JOB_DATE' => 'job date',
  'LBL_REAL_START_TIME' => 'real start time',
  'LBL_REAL_END_TIME' => 'real end time',
  'LBL_SCHEDULED_START_TIME' => 'scheduled start time',
  'LBL_SCHEDULED_END_TIME' => 'scheduled end time',
);