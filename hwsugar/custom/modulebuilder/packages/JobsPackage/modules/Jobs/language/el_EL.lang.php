<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_LIST_FORM_TITLE' => 'Jobs Λίστα',
  'LBL_MODULE_NAME' => 'Jobs',
  'LBL_MODULE_TITLE' => 'Jobs',
  'LBL_MODULE_NAME_SINGULAR' => 'Job',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Jobs',
  'LNK_NEW_RECORD' => 'Δημιουργία Job',
  'LNK_LIST' => 'Προβολή Jobs',
  'LNK_IMPORT_ABCDE_JOBS' => 'Import Jobs',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Job',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_ABCDE_JOBS_SUBPANEL_TITLE' => 'Jobs',
  'LBL_NEW_FORM_TITLE' => 'Νέα Job',
  'LNK_IMPORT_VCARD' => 'Import Job vCard',
  'LBL_IMPORT' => 'Import Jobs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Job record by importing a vCard from your file system.',
  'LBL_TECH_SUPKG_SYSTEMUSERS_ID' => 'tech (related System User ID)',
  'LBL_TECH' => 'tech',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'opportunity (related Opportunity ID)',
  'LBL_OPPORTUNITY' => 'opportunity',
  'LBL_LOCATION' => 'location',
  'LBL_COMMENTS' => 'comments',
  'LBL_JOB_PRIORITY' => 'job priority',
  'LBL_JOB_DATE' => 'job date',
  'LBL_REAL_START_TIME' => 'real start time',
  'LBL_REAL_END_TIME' => 'real end time',
  'LBL_SCHEDULED_START_TIME' => 'scheduled start time',
  'LBL_SCHEDULED_END_TIME' => 'scheduled end time',
);