<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => '团队',
  'LBL_TEAMS' => '团队',
  'LBL_TEAM_ID' => '团队编号',
  'LBL_ASSIGNED_TO_ID' => '分配的用户 ID',
  'LBL_ASSIGNED_TO_NAME' => '指派给',
  'LBL_TAGS_LINK' => '标签',
  'LBL_TAGS' => '标签',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改的日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '修改人 ID',
  'LBL_MODIFIED_NAME' => '修改人姓名',
  'LBL_CREATED' => '创建人',
  'LBL_CREATED_ID' => '创建人 ID',
  'LBL_DOC_OWNER' => '文档所有者',
  'LBL_USER_FAVORITES' => '最受欢迎的用户',
  'LBL_DESCRIPTION' => '说明',
  'LBL_DELETED' => '已删除',
  'LBL_NAME' => '姓名',
  'LBL_CREATED_USER' => '由用户创建',
  'LBL_MODIFIED_USER' => '由用户修改',
  'LBL_LIST_NAME' => '姓名',
  'LBL_EDIT_BUTTON' => '编辑',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '按姓名修改',
  'LBL_LIST_FORM_TITLE' => 'Jobs 列表',
  'LBL_MODULE_NAME' => 'Jobs',
  'LBL_MODULE_TITLE' => 'Jobs',
  'LBL_MODULE_NAME_SINGULAR' => 'Job',
  'LBL_HOMEPAGE_TITLE' => '我的 Jobs',
  'LNK_NEW_RECORD' => '创建 Job',
  'LNK_LIST' => '查看 Jobs',
  'LNK_IMPORT_ABCDE_JOBS' => 'Import Jobs',
  'LBL_SEARCH_FORM_TITLE' => '查找 Job',
  'LBL_HISTORY_SUBPANEL_TITLE' => '查看历史记录',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活动流',
  'LBL_ABCDE_JOBS_SUBPANEL_TITLE' => 'Jobs',
  'LBL_NEW_FORM_TITLE' => '新建 Job',
  'LNK_IMPORT_VCARD' => 'Import Job vCard',
  'LBL_IMPORT' => 'Import Jobs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Job record by importing a vCard from your file system.',
  'LBL_TECH_SUPKG_SYSTEMUSERS_ID' => 'tech (related System User ID)',
  'LBL_TECH' => 'tech',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'opportunity (related Opportunity ID)',
  'LBL_OPPORTUNITY' => 'opportunity',
  'LBL_LOCATION' => 'location',
  'LBL_COMMENTS' => 'comments',
  'LBL_JOB_PRIORITY' => 'job priority',
  'LBL_JOB_DATE' => 'job date',
  'LBL_REAL_START_TIME' => 'real start time',
  'LBL_REAL_END_TIME' => 'real end time',
  'LBL_SCHEDULED_START_TIME' => 'scheduled start time',
  'LBL_SCHEDULED_END_TIME' => 'scheduled end time',
);