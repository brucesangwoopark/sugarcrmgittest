<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_LIST_FORM_TITLE' => 'Lead Line Items List',
  'LBL_MODULE_NAME' => 'Lead Line Items',
  'LBL_MODULE_TITLE' => 'Lead Line Items',
  'LBL_MODULE_NAME_SINGULAR' => 'Lead Line Item',
  'LBL_HOMEPAGE_TITLE' => 'My Lead Line Items',
  'LNK_NEW_RECORD' => 'Create Lead Line Item',
  'LNK_LIST' => 'View Lead Line Items',
  'LNK_IMPORT_LIPKG_LEAD_LINE_ITEMS' => 'Import Lead Line Items Lead Line Items',
  'LBL_SEARCH_FORM_TITLE' => 'Search Lead Line Item',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_LIPKG_LEAD_LINE_ITEMS_SUBPANEL_TITLE' => 'Lead Line Items',
  'LBL_NEW_FORM_TITLE' => 'New Lead Line Item',
  'LNK_IMPORT_VCARD' => 'Import Lead Line Items Lead Line Item vCard',
  'LBL_IMPORT' => 'Import Lead Line Items Lead Line Items',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Lead Line Item record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Currency',
  'LBL_CALCULATEDAMOUNT' => 'CalculatedAmount',
  'LBL_PRODUCT_PRICE' => 'Product Price',
);