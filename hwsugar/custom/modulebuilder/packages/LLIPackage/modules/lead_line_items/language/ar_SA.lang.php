<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_TAGS_LINK' => 'العلامات',
  'LBL_TAGS' => 'العلامات',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_LIST_FORM_TITLE' => 'Lead Line Items القائمة',
  'LBL_MODULE_NAME' => 'Lead Line Items',
  'LBL_MODULE_TITLE' => 'Lead Line Items',
  'LBL_MODULE_NAME_SINGULAR' => 'Lead Line Item',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Lead Line Items',
  'LNK_NEW_RECORD' => 'إنشاء Lead Line Item',
  'LNK_LIST' => 'عرض Lead Line Items',
  'LNK_IMPORT_LIPKG_LEAD_LINE_ITEMS' => 'Import Lead Line Items Lead Line Items',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Lead Line Item',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط الكلي',
  'LBL_LIPKG_LEAD_LINE_ITEMS_SUBPANEL_TITLE' => 'Lead Line Items',
  'LBL_NEW_FORM_TITLE' => 'جديد Lead Line Item',
  'LNK_IMPORT_VCARD' => 'Import Lead Line Items Lead Line Item vCard',
  'LBL_IMPORT' => 'Import Lead Line Items Lead Line Items',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Lead Line Item record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Currency',
  'LBL_CALCULATEDAMOUNT' => 'CalculatedAmount',
  'LBL_PRODUCT_PRICE' => 'Product Price',
);