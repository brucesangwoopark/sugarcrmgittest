<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'קבוצות',
  'LBL_TEAMS' => 'קבוצות',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'מזהה משתמש מוקצה',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'מזהה',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'נערך על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעל המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_LIST_FORM_TITLE' => 'Lead Line Items List',
  'LBL_MODULE_NAME' => 'Lead Line Items',
  'LBL_MODULE_TITLE' => 'Lead Line Items',
  'LBL_MODULE_NAME_SINGULAR' => 'Lead Line Item',
  'LBL_HOMEPAGE_TITLE' => 'שלי Lead Line Items',
  'LNK_NEW_RECORD' => 'צור Lead Line Item',
  'LNK_LIST' => 'View Lead Line Items',
  'LNK_IMPORT_LIPKG_LEAD_LINE_ITEMS' => 'Import Lead Line Items Lead Line Items',
  'LBL_SEARCH_FORM_TITLE' => 'Search Lead Line Item',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_LIPKG_LEAD_LINE_ITEMS_SUBPANEL_TITLE' => 'Lead Line Items',
  'LBL_NEW_FORM_TITLE' => 'חדש Lead Line Item',
  'LNK_IMPORT_VCARD' => 'Import Lead Line Items Lead Line Item vCard',
  'LBL_IMPORT' => 'Import Lead Line Items Lead Line Items',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Lead Line Item record by importing a vCard from your file system.',
  'LBL_CURRENCY' => 'Currency',
  'LBL_CALCULATEDAMOUNT' => 'CalculatedAmount',
  'LBL_PRODUCT_PRICE' => 'Product Price',
);