<?php
// created: 2016-09-28 20:12:07
$dictionary["lipkg_lead_line_items_producttemplates"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'lipkg_lead_line_items_producttemplates' => 
    array (
      'lhs_module' => 'ProductTemplates',
      'lhs_table' => 'product_templates',
      'lhs_key' => 'id',
      'rhs_module' => 'lipkg_lead_line_items',
      'rhs_table' => 'lipkg_lead_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'lipkg_lead_line_items_producttemplates_c',
      'join_key_lhs' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
      'join_key_rhs' => 'lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb',
    ),
  ),
  'table' => 'lipkg_lead_line_items_producttemplates_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'lipkg_lead_line_items_producttemplatesproducttemplates_ida' => 
    array (
      'name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
      'type' => 'id',
    ),
    'lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb' => 
    array (
      'name' => 'lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'lipkg_lead_line_items_producttemplatesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'lipkg_lead_line_items_producttemplates_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'lipkg_lead_line_items_producttemplates_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb',
      ),
    ),
  ),
);