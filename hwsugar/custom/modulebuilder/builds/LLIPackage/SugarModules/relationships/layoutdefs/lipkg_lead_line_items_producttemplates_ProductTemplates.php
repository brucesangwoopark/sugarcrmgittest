<?php
 // created: 2016-09-28 20:12:07
$layout_defs["ProductTemplates"]["subpanel_setup"]['lipkg_lead_line_items_producttemplates'] = array (
  'order' => 100,
  'module' => 'lipkg_lead_line_items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'lipkg_lead_line_items_producttemplates',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
