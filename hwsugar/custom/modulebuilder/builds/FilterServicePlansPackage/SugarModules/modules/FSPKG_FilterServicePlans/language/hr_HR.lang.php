<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID tima',
  'LBL_ASSIGNED_TO_ID' => 'ID dodijeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Dodijeljeno',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum stvaranja',
  'LBL_DATE_MODIFIED' => 'Datum izmjene',
  'LBL_MODIFIED' => 'Izmijenio/la',
  'LBL_MODIFIED_ID' => 'Izmijenio ID',
  'LBL_MODIFIED_NAME' => 'Izmijenilo ime',
  'LBL_CREATED' => 'Stvorio/la',
  'LBL_CREATED_ID' => 'Stvorio ID',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su dodali u Favorite',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Izbrisano',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Stvorio korisnik',
  'LBL_MODIFIED_USER' => 'Izmijenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Uredi',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Izmijenilo ime',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Plans Popis',
  'LBL_MODULE_NAME' => 'Filter Service Plans',
  'LBL_MODULE_TITLE' => 'Filter Service Plans',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Plan',
  'LBL_HOMEPAGE_TITLE' => 'Moja Filter Service Plans',
  'LNK_NEW_RECORD' => 'Stvori Filter Service Plan',
  'LNK_LIST' => 'Prikaži Filter Service Plans',
  'LNK_IMPORT_FSPKG_FILTERSERVICEPLANS' => 'Import Filter Service Plans',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraži Filter Service Plan',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Prikaži povijest',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Pregled aktivnosti',
  'LBL_FSPKG_FILTERSERVICEPLANS_SUBPANEL_TITLE' => 'Filter Service Plans',
  'LBL_NEW_FORM_TITLE' => 'Novo Filter Service Plan',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Plan vCard',
  'LBL_IMPORT' => 'Import Filter Service Plans',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Plan record by importing a vCard from your file system.',
  'LBL_AIRFILTERSERVICEID' => 'Air Filter Service ID',
  'LBL_ISACTIVE' => 'Active?',
  'LBL_ISENROLLED' => 'Enrolled?',
  'LBL_COMMISSIONPAID' => 'Commission Paid?',
);