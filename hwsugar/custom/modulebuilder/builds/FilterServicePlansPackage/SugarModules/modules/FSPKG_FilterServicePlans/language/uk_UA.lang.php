<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Plans Список',
  'LBL_MODULE_NAME' => 'Filter Service Plans',
  'LBL_MODULE_TITLE' => 'Filter Service Plans',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Plan',
  'LBL_HOMEPAGE_TITLE' => 'Мій Filter Service Plans',
  'LNK_NEW_RECORD' => 'Створити Filter Service Plan',
  'LNK_LIST' => 'Переглянути Filter Service Plans',
  'LNK_IMPORT_FSPKG_FILTERSERVICEPLANS' => 'Import Filter Service Plans',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Filter Service Plan',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_FSPKG_FILTERSERVICEPLANS_SUBPANEL_TITLE' => 'Filter Service Plans',
  'LBL_NEW_FORM_TITLE' => 'Новий Filter Service Plan',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Plan vCard',
  'LBL_IMPORT' => 'Import Filter Service Plans',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Plan record by importing a vCard from your file system.',
  'LBL_AIRFILTERSERVICEID' => 'Air Filter Service ID',
  'LBL_ISACTIVE' => 'Active?',
  'LBL_ISENROLLED' => 'Enrolled?',
  'LBL_COMMISSIONPAID' => 'Commission Paid?',
);