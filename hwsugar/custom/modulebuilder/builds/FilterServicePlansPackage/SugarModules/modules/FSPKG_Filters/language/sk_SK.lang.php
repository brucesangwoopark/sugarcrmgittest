<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Priradené používateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Priradené k',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenené používateľom',
  'LBL_MODIFIED_ID' => 'Upravené používateľom s ID',
  'LBL_MODIFIED_NAME' => 'Upravené používateľom s menom',
  'LBL_CREATED' => 'Vytvoril',
  'LBL_CREATED_ID' => 'Vytvorené používateľom s ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Obľúbení používatelia',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upravené používateľom s menom',
  'LBL_LIST_FORM_TITLE' => 'Filters Zoznam',
  'LBL_MODULE_NAME' => 'Filters',
  'LBL_MODULE_TITLE' => 'Filters',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter',
  'LBL_HOMEPAGE_TITLE' => 'Moje Filters',
  'LNK_NEW_RECORD' => 'Vytvoriť Filter',
  'LNK_LIST' => 'Zobraziť Filters',
  'LNK_IMPORT_FSPKG_FILTERS' => 'Import Filters',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Filter',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_FSPKG_FILTERS_SUBPANEL_TITLE' => 'Filters',
  'LBL_NEW_FORM_TITLE' => 'Nové Filter',
  'LNK_IMPORT_VCARD' => 'Import Filter vCard',
  'LBL_IMPORT' => 'Import Filters',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter record by importing a vCard from your file system.',
  'LBL_PRODUCTCATEOGORY' => 'Product Category',
  'LBL_PRODUCTTYPE' => 'Product Type',
  'LBL_PRODUCTCOUNT' => 'Product Count',
  'LBL_BILLTYPECODE' => 'Bill Type Code',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_STANDINGREQUEST' => 'Standing Request',
  'LBL_DURATIONBYMONTH' => 'Duration by Month',
  'LBL_SECONDPRICE' => 'Second Price',
  'LBL_INSTALLATION' => 'Installation',
  'LBL_FILTERMAINTENANCE' => 'Filter Maintenance',
  'LBL_FREQUENCY' => 'Frequency',
  'LBL_ISVIP' => 'VIP?',
  'LBL_FILTERTYPE' => 'Type',
  'LBL_EFFECTIVEFROM' => 'Effective From',
  'LBL_EFFECTIVETO' => 'Effective To',
  'LBL_ISACTIVE' => 'Active?',
);