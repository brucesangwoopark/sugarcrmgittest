<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Prefilter Types Списък',
  'LBL_MODULE_NAME' => 'Prefilter Types',
  'LBL_MODULE_TITLE' => 'Prefilter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Prefilter Type',
  'LBL_HOMEPAGE_TITLE' => 'Мои Prefilter Types',
  'LNK_NEW_RECORD' => 'Създай Prefilter Type',
  'LNK_LIST' => 'Изглед Prefilter Types',
  'LNK_IMPORT_FSPKG_PREFILTERTYPES' => 'Import Prefilter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Prefilter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_FSPKG_PREFILTERTYPES_SUBPANEL_TITLE' => 'Prefilter Types',
  'LBL_NEW_FORM_TITLE' => 'Нов Prefilter Type',
  'LNK_IMPORT_VCARD' => 'Import Prefilter Type vCard',
  'LBL_IMPORT' => 'Import Prefilter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Prefilter Type record by importing a vCard from your file system.',
);