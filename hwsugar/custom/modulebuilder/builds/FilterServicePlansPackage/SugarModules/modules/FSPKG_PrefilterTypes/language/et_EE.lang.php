<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_LIST_FORM_TITLE' => 'Prefilter Types Loend',
  'LBL_MODULE_NAME' => 'Prefilter Types',
  'LBL_MODULE_TITLE' => 'Prefilter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Prefilter Type',
  'LBL_HOMEPAGE_TITLE' => 'Minu Prefilter Types',
  'LNK_NEW_RECORD' => 'Loo Prefilter Type',
  'LNK_LIST' => 'Vaade Prefilter Types',
  'LNK_IMPORT_FSPKG_PREFILTERTYPES' => 'Import Prefilter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Prefilter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_FSPKG_PREFILTERTYPES_SUBPANEL_TITLE' => 'Prefilter Types',
  'LBL_NEW_FORM_TITLE' => 'Uus Prefilter Type',
  'LNK_IMPORT_VCARD' => 'Import Prefilter Type vCard',
  'LBL_IMPORT' => 'Import Prefilter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Prefilter Type record by importing a vCard from your file system.',
);