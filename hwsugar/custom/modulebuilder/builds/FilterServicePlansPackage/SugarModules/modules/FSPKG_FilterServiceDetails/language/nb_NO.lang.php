<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Details Liste',
  'LBL_MODULE_NAME' => 'Filter Service Details',
  'LBL_MODULE_TITLE' => 'Filter Service Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Detail',
  'LBL_HOMEPAGE_TITLE' => 'Min Filter Service Details',
  'LNK_NEW_RECORD' => 'Opprett Filter Service Detail',
  'LNK_LIST' => 'Vis Filter Service Details',
  'LNK_IMPORT_FSPKG_FILTERSERVICEDETAILS' => 'Import Filter Service Details',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Filter Service Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_FSPKG_FILTERSERVICEDETAILS_SUBPANEL_TITLE' => 'Filter Service Details',
  'LBL_NEW_FORM_TITLE' => 'Ny Filter Service Detail',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Detail vCard',
  'LBL_IMPORT' => 'Import Filter Service Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Detail record by importing a vCard from your file system.',
  'LBL_FILTERCHANGEDATE' => 'Filter Change Date',
  'LBL_WATERMETERREAD' => 'Water Meter Read',
  'LBL_UNITS' => 'Units',
  'LBL_STATE' => 'State',
  'LBL_CHANGETYPE' => 'Change Type',
  'LBL_TRACKINGNUMBER' => 'Tracking Number',
  'LBL_APPOINMENTID' => 'Appoinment ID',
);