<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID broj tima',
  'LBL_ASSIGNED_TO_ID' => 'ID broj dodeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Korisnik',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum kreiranja',
  'LBL_DATE_MODIFIED' => 'Datum izmene',
  'LBL_MODIFIED' => 'Promenio',
  'LBL_MODIFIED_ID' => 'ID broj korisnika koji je promenio',
  'LBL_MODIFIED_NAME' => 'Modifikovano prema imenu',
  'LBL_CREATED' => 'Autor',
  'LBL_CREATED_ID' => 'Kreirano prema Id-u',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su označili omiljene',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Obrisan',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Kreirao korisnik',
  'LBL_MODIFIED_USER' => 'Promenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Izmeni',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikovano prema imenu',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Details Lista',
  'LBL_MODULE_NAME' => 'Filter Service Details',
  'LBL_MODULE_TITLE' => 'Filter Service Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Detail',
  'LBL_HOMEPAGE_TITLE' => 'Moja Filter Service Details',
  'LNK_NEW_RECORD' => 'Kreiraj Filter Service Detail',
  'LNK_LIST' => 'Pregled Filter Service Details',
  'LNK_IMPORT_FSPKG_FILTERSERVICEDETAILS' => 'Import Filter Service Details',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraga Filter Service Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Pregled istorije',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivnosti',
  'LBL_FSPKG_FILTERSERVICEDETAILS_SUBPANEL_TITLE' => 'Filter Service Details',
  'LBL_NEW_FORM_TITLE' => 'Novo Filter Service Detail',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Detail vCard',
  'LBL_IMPORT' => 'Import Filter Service Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Detail record by importing a vCard from your file system.',
  'LBL_FILTERCHANGEDATE' => 'Filter Change Date',
  'LBL_WATERMETERREAD' => 'Water Meter Read',
  'LBL_UNITS' => 'Units',
  'LBL_STATE' => 'State',
  'LBL_CHANGETYPE' => 'Change Type',
  'LBL_TRACKINGNUMBER' => 'Tracking Number',
  'LBL_APPOINMENTID' => 'Appoinment ID',
);