<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'ทีม',
  'LBL_TEAMS' => 'ทีม',
  'LBL_TEAM_ID' => 'ID ทีม',
  'LBL_ASSIGNED_TO_ID' => 'ID ผู้ใช้ที่ระบุ',
  'LBL_ASSIGNED_TO_NAME' => 'ระบุให้',
  'LBL_TAGS_LINK' => 'แท็ก',
  'LBL_TAGS' => 'แท็ก',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'วันที่สร้าง',
  'LBL_DATE_MODIFIED' => 'วันที่แก้ไข',
  'LBL_MODIFIED' => 'แก้ไขโดย',
  'LBL_MODIFIED_ID' => 'แก้ไขโดย ID',
  'LBL_MODIFIED_NAME' => 'แก้ไขโดยชื่อ',
  'LBL_CREATED' => 'สร้างโดย',
  'LBL_CREATED_ID' => 'สร้างโดย ID',
  'LBL_DOC_OWNER' => 'เจ้าของเอกสาร',
  'LBL_USER_FAVORITES' => 'ผู้ใช้ที่เพิ่มรายการโปรด',
  'LBL_DESCRIPTION' => 'คำอธิบาย',
  'LBL_DELETED' => 'ลบ',
  'LBL_NAME' => 'ชื่อ',
  'LBL_CREATED_USER' => 'สร้างโดยผู้ใช้',
  'LBL_MODIFIED_USER' => 'แก้ไขโดยผู้ใช้',
  'LBL_LIST_NAME' => 'ชื่อ',
  'LBL_EDIT_BUTTON' => 'แก้ไข',
  'LBL_REMOVE' => 'นำออก',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'แก้ไขโดยชื่อ',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Details รายการ',
  'LBL_MODULE_NAME' => 'Filter Service Details',
  'LBL_MODULE_TITLE' => 'Filter Service Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Detail',
  'LBL_HOMEPAGE_TITLE' => 'ของฉัน Filter Service Details',
  'LNK_NEW_RECORD' => 'สร้าง Filter Service Detail',
  'LNK_LIST' => 'มุมมอง Filter Service Details',
  'LNK_IMPORT_FSPKG_FILTERSERVICEDETAILS' => 'Import Filter Service Details',
  'LBL_SEARCH_FORM_TITLE' => 'ค้นหา Filter Service Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'ดูประวัติ',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'สตรีมกิจกรรม',
  'LBL_FSPKG_FILTERSERVICEDETAILS_SUBPANEL_TITLE' => 'Filter Service Details',
  'LBL_NEW_FORM_TITLE' => 'ใหม่ Filter Service Detail',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Detail vCard',
  'LBL_IMPORT' => 'Import Filter Service Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Detail record by importing a vCard from your file system.',
  'LBL_FILTERCHANGEDATE' => 'Filter Change Date',
  'LBL_WATERMETERREAD' => 'Water Meter Read',
  'LBL_UNITS' => 'Units',
  'LBL_STATE' => 'State',
  'LBL_CHANGETYPE' => 'Change Type',
  'LBL_TRACKINGNUMBER' => 'Tracking Number',
  'LBL_APPOINMENTID' => 'Appoinment ID',
);