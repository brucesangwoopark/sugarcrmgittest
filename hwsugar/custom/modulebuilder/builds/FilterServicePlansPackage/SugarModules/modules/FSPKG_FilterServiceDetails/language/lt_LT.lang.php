<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Priskirto vartotojo ID',
  'LBL_ASSIGNED_TO_NAME' => 'Kam priskirta',
  'LBL_TAGS_LINK' => 'Žymės',
  'LBL_TAGS' => 'Žymės',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukūrimo data',
  'LBL_DATE_MODIFIED' => 'Modifikavimo data',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo ID',
  'LBL_DOC_OWNER' => 'Dokumento savininkas',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašas',
  'LBL_DELETED' => 'Panaikintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūręs vartotojas',
  'LBL_MODIFIED_USER' => 'Modifikavęs vartotojas.',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Pašalinti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Details Sąrašas',
  'LBL_MODULE_NAME' => 'Filter Service Details',
  'LBL_MODULE_TITLE' => 'Filter Service Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Detail',
  'LBL_HOMEPAGE_TITLE' => 'Mano Filter Service Details',
  'LNK_NEW_RECORD' => 'Sukurti Filter Service Detail',
  'LNK_LIST' => 'View Filter Service Details',
  'LNK_IMPORT_FSPKG_FILTERSERVICEDETAILS' => 'Import Filter Service Details',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Filter Service Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_FSPKG_FILTERSERVICEDETAILS_SUBPANEL_TITLE' => 'Filter Service Details',
  'LBL_NEW_FORM_TITLE' => 'Naujas Filter Service Detail',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Detail vCard',
  'LBL_IMPORT' => 'Import Filter Service Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Detail record by importing a vCard from your file system.',
  'LBL_FILTERCHANGEDATE' => 'Filter Change Date',
  'LBL_WATERMETERREAD' => 'Water Meter Read',
  'LBL_UNITS' => 'Units',
  'LBL_STATE' => 'State',
  'LBL_CHANGETYPE' => 'Change Type',
  'LBL_TRACKINGNUMBER' => 'Tracking Number',
  'LBL_APPOINMENTID' => 'Appoinment ID',
);