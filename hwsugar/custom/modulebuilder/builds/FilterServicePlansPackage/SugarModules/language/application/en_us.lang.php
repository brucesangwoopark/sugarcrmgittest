<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FSPKG_PrefilterTypes'] = 'Prefilter Types';
$app_list_strings['moduleList']['FSPKG_Filters'] = 'Filters';
$app_list_strings['moduleList']['FSPKG_FilterServiceDetails'] = 'Filter Service Details';
$app_list_strings['moduleList']['FSPKG_FilterServicePlans'] = 'Filter Service Plans';
$app_list_strings['moduleListSingular']['FSPKG_PrefilterTypes'] = 'Prefilter Type';
$app_list_strings['moduleListSingular']['FSPKG_Filters'] = 'Filter';
$app_list_strings['moduleListSingular']['FSPKG_FilterServiceDetails'] = 'Filter Service Detail';
$app_list_strings['moduleListSingular']['FSPKG_FilterServicePlans'] = 'Filter Service Plan';
$app_list_strings['fspkg_filters_productcateogory']['Whole Home'] = 'Whole Home';
$app_list_strings['fspkg_filters_productcateogory']['Air Filter'] = 'Air Filter';
$app_list_strings['fspkg_filters_producttype']['Service'] = 'Service';
$app_list_strings['fspkg_filters_filtertype']['Filter Change'] = 'Filter Change';
$app_list_strings['fspkg_filter_service_details_units']['Gallons'] = 'Gallons';
$app_list_strings['fspkg_filter_service_details_units']['M3'] = 'M3';
$app_list_strings['fspkg_filter_service_details_state'][1] = '1';
$app_list_strings['fspkg_filter_service_details_state'][2] = '2';
$app_list_strings['fspkg_filter_service_details_state'][3] = '3';
$app_list_strings['fspkg_filter_service_details_state'][4] = '4';
$app_list_strings['fspkg_filter_service_details_state'][5] = '5';
$app_list_strings['fspkg_filter_service_details_state'][0] = '';
