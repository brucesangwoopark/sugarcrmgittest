<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-04
 * Time: 오전 9:28
 */

require_once 'include/api/SugarApiException.php';

class InvalidUninstallationJobException extends SugarApiException
{
    public $httpCode = CHT_INVALID_UNINSTALLATION_JOB_EXCEPTION;
    public $errorLabel = 'invalid_uninstallation_job_request';
    public $messageLabel = 'You are trying to create Uninstallation Job for already uninstalled RLI.<br>Please check the status of current RLI.';
}