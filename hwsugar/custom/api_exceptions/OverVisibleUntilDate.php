<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-09-18
 * Time: 오전 11:47
 */

require_once 'include/api/SugarApiException.php';

class OverVisibleUntilDate extends SugarApiException
{
    public $httpCode = CHT_OVER_VISIBLE_UNTIL_DATE_EXCEPTION;
    public $errorLabel = 'over_visible_until_date';
    public $messageLabel = "This record is not allowed to be shown due to expired 'Visible Until' date";
}