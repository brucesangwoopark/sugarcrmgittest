<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-27
 * Time: 오전 10:24
 */

class AccountRLINotPlaced extends SugarApiException
{
    public $httpCode = CHT_ACCOUNT_RLI_NOT_PLACED;
    public $errorLabel = 'account_rli_not_placed';
    public $messageLabel = 'Account needs to be selected!';
}