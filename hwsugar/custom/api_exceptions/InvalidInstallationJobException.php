<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-04
 * Time: 오전 9:26
 */

require_once 'include/api/SugarApiException.php';

class InvalidInstallationJobException extends SugarApiException
{
    public $httpCode = CHT_INVALID_INSTALLATION_JOB_EXCEPTION;
    public $errorLabel = 'invalid_installation_job_request';
    public $messageLabel = 'You are trying to create Installation Job for already installed RLI.<br>Please check the status of current RLI.';
}