<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-27
 * Time: 오전 10:25
 */

class WrongAccountRLICombination extends SugarApiException
{
    public $httpCode = CHT_WRONG_ACCOUNT_RLI_COMBINATION;
    public $errorLabel = 'wrong_account_rli_combination';
    public $messageLabel = 'Chosen Account and Revenue Line Item are not related.<br>Please select right Account or Revenue Line Item.';
}