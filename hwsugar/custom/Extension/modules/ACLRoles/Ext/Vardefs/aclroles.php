<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-09-21
 * Time: 오전 10:58
 */

$dictionary['ACLRole']['fields']['isvisibleuntilapplied'] = array(
    'name' => 'isvisibleuntilapplied',
    'type' => 'bool',
    'module' => 'ACLRoles',
    'default_value' => false,
);