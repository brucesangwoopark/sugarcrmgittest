<?php

// No longer needed to define logic hook v7.7
//$hook_version = 1;
//$hook_array = Array();
//// order, description, file, class, method
//$hook_array['before_save'] = Array();
   $hook_array['after_save'][] = Array(
   
   //Processing index. For sorting the array.
   99,
   
   //Label. A string value to identify the hook.
   'After Save Accounts Hook',
   
   //The PHP file where the class is located.
   'custom/modules/Accounts/afterSaveHookClass.php',
   
   //The class the method is in.
   'AfterSaveAccountHook',
   
   //The method to call.
   'afterSave'
   
   );

?>