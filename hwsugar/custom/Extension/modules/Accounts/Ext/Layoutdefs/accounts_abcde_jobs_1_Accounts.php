<?php
 // created: 2016-10-24 21:45:23
$layout_defs["Accounts"]["subpanel_setup"]['accounts_abcde_jobs_1'] = array (
  'order' => 100,
  'module' => 'abcde_Jobs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE',
  'get_subpanel_data' => 'accounts_abcde_jobs_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
