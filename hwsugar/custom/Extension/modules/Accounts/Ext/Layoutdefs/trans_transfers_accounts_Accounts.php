<?php
 // created: 2016-12-28 12:31:37
$layout_defs["Accounts"]["subpanel_setup"]['trans_transfers_accounts'] = array (
  'order' => 100,
  'module' => 'trans_Transfers',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TRANS_TRANSFERS_ACCOUNTS_FROM_TRANS_TRANSFERS_TITLE',
  'get_subpanel_data' => 'trans_transfers_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
