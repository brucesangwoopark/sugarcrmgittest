<?php
 // created: 2017-10-26 16:56:34
$layout_defs["Accounts"]["subpanel_setup"]['cases_tickets_accounts'] = array (
  'order' => 100,
  'module' => 'cases_Tickets',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_TICKETS_ACCOUNTS_FROM_CASES_TICKETS_TITLE',
  'get_subpanel_data' => 'cases_tickets_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
