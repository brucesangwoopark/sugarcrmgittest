<?php
// created: 2016-12-28 12:31:37
$dictionary["Account"]["fields"]["trans_transfers_accounts"] = array (
  'name' => 'trans_transfers_accounts',
  'type' => 'link',
  'relationship' => 'trans_transfers_accounts',
  'source' => 'non-db',
  'module' => 'trans_Transfers',
  'bean_name' => false,
  'vname' => 'LBL_TRANS_TRANSFERS_ACCOUNTS_FROM_TRANS_TRANSFERS_TITLE',
  'id_name' => 'trans_transfers_accountstrans_transfers_ida',
);
