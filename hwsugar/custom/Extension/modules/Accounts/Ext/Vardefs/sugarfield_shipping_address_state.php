<?php
 // created: 2016-10-21 18:02:22
$dictionary['Account']['fields']['shipping_address_state']['type']='enum';
$dictionary['Account']['fields']['shipping_address_state']['options']='state_dom';
$dictionary['Account']['fields']['shipping_address_state']['required']=true;
$dictionary['Account']['fields']['shipping_address_state']['audited']=false;
$dictionary['Account']['fields']['shipping_address_state']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_state']['comments']='State for shipping address';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_state']['calculated']=false;

 ?>
