<?php
 // created: 2017-06-22 09:05:41
$dictionary['Account']['fields']['compartment_number_c']['labelValue']='Unit #';
$dictionary['Account']['fields']['compartment_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['compartment_number_c']['enforced']='';
$dictionary['Account']['fields']['compartment_number_c']['dependency']='';

 ?>