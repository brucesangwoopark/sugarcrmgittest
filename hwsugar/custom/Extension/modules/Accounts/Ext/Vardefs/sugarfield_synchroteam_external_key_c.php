<?php
 // created: 2016-11-07 20:20:43
$dictionary['Account']['fields']['synchroteam_external_key_c']['labelValue']='Synchroteam Customer ID';
$dictionary['Account']['fields']['synchroteam_external_key_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['synchroteam_external_key_c']['enforced']='';
$dictionary['Account']['fields']['synchroteam_external_key_c']['dependency']='';

 ?>