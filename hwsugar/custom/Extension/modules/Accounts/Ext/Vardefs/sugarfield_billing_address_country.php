<?php
 // created: 2016-11-14 15:47:34
$dictionary['Account']['fields']['billing_address_country']['type']='enum';
$dictionary['Account']['fields']['billing_address_country']['options']='country_list';
$dictionary['Account']['fields']['billing_address_country']['default']='Canada';
$dictionary['Account']['fields']['billing_address_country']['audited']=false;
$dictionary['Account']['fields']['billing_address_country']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['calculated']=false;
$dictionary['Account']['fields']['billing_address_country']['dependency']=false;

 ?>