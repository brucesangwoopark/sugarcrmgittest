<?php
 // created: 2016-10-17 21:56:29
$dictionary['Account']['fields']['shipping_address_country']['type']='enum';
$dictionary['Account']['fields']['shipping_address_country']['options']='countries_dom';
$dictionary['Account']['fields']['shipping_address_country']['default']='(Do not complete this field)';
$dictionary['Account']['fields']['shipping_address_country']['audited']=false;
$dictionary['Account']['fields']['shipping_address_country']['massupdate']=false;
$dictionary['Account']['fields']['shipping_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['shipping_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['shipping_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Account']['fields']['shipping_address_country']['calculated']=false;

 ?>
