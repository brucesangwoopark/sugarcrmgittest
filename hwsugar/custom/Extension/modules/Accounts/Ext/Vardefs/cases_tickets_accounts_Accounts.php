<?php
// created: 2017-10-26 16:56:34
$dictionary["Account"]["fields"]["cases_tickets_accounts"] = array (
  'name' => 'cases_tickets_accounts',
  'type' => 'link',
  'relationship' => 'cases_tickets_accounts',
  'source' => 'non-db',
  'module' => 'cases_Tickets',
  'bean_name' => 'cases_Tickets',
  'vname' => 'LBL_CASES_TICKETS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'cases_tickets_accountsaccounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
