<?php
 // created: 2016-10-14 20:14:32
$dictionary['Account']['fields']['phone_office']['len']='100';
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['massupdate']=false;
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['phone_office']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';
$dictionary['Account']['fields']['phone_office']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.05',
  'searchable' => true,
);
$dictionary['Account']['fields']['phone_office']['calculated']=false;

 ?>