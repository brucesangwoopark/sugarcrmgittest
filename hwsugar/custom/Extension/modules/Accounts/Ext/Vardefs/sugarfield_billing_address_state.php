<?php
 // created: 2016-11-14 15:49:49
$dictionary['Account']['fields']['billing_address_state']['type']='enum';
$dictionary['Account']['fields']['billing_address_state']['options']='state_list';
$dictionary['Account']['fields']['billing_address_state']['required']=true;
$dictionary['Account']['fields']['billing_address_state']['audited']=false;
$dictionary['Account']['fields']['billing_address_state']['massupdate']=false;
$dictionary['Account']['fields']['billing_address_state']['comments']='State for billing address';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['billing_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['calculated']=false;
$dictionary['Account']['fields']['billing_address_state']['default']='ON';
$dictionary['Account']['fields']['billing_address_state']['len']=100;
$dictionary['Account']['fields']['billing_address_state']['dependency']=false;

 ?>