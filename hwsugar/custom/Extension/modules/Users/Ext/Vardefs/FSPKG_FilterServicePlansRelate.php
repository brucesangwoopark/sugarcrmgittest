<?php

$dictionary['Users']['fields']['fspkg_filterserviceplans'] = array(
    'name'         => 'fspkg_filterserviceplans',
    'type'         => 'link',
    'relationship' => 'users_fspkg_filterserviceplans',
    'module'       => 'FSPKG_FilterServicePlans',
    'bean_name'    => 'FSPKG_FilterServicePlans',
    'source'       => 'non-db',
    'vname'        => 'LBL_FILTERSERVICEPLANS',
);
