<?php
 // created: 2016-11-14 15:51:23
$dictionary['Contact']['fields']['alt_address_state']['type']='enum';
$dictionary['Contact']['fields']['alt_address_state']['options']='state_list';
$dictionary['Contact']['fields']['alt_address_state']['required']=true;
$dictionary['Contact']['fields']['alt_address_state']['audited']=false;
$dictionary['Contact']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_state']['comments']='State for alt address';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_state']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_state']['default']='ON';
$dictionary['Contact']['fields']['alt_address_state']['len']=100;
$dictionary['Contact']['fields']['alt_address_state']['dependency']=false;

 ?>