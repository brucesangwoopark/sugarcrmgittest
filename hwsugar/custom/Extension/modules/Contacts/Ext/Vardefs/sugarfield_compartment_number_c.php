<?php
 // created: 2017-06-30 08:56:45
$dictionary['Contact']['fields']['compartment_number_c']['labelValue']='Unit #';
$dictionary['Contact']['fields']['compartment_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['compartment_number_c']['enforced']='';
$dictionary['Contact']['fields']['compartment_number_c']['dependency']='';

 ?>