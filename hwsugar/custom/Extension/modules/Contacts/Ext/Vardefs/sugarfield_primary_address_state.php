<?php
 // created: 2016-11-14 15:50:56
$dictionary['Contact']['fields']['primary_address_state']['type']='enum';
$dictionary['Contact']['fields']['primary_address_state']['options']='state_list';
$dictionary['Contact']['fields']['primary_address_state']['required']=true;
$dictionary['Contact']['fields']['primary_address_state']['audited']=false;
$dictionary['Contact']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_state']['calculated']=false;
$dictionary['Contact']['fields']['primary_address_state']['default']='ON';
$dictionary['Contact']['fields']['primary_address_state']['len']=100;
$dictionary['Contact']['fields']['primary_address_state']['dependency']=false;

 ?>