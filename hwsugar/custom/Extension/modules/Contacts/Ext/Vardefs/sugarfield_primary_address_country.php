<?php
 // created: 2016-10-17 21:57:42
$dictionary['Contact']['fields']['primary_address_country']['type']='enum';
$dictionary['Contact']['fields']['primary_address_country']['options']='countries_dom';
$dictionary['Contact']['fields']['primary_address_country']['default']='(Do not complete this field)';
$dictionary['Contact']['fields']['primary_address_country']['audited']=false;
$dictionary['Contact']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_country']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Contact']['fields']['primary_address_country']['calculated']=false;

 ?>
