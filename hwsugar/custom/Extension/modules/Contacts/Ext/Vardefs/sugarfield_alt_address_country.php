<?php
 // created: 2016-11-14 15:51:52
$dictionary['Contact']['fields']['alt_address_country']['type']='enum';
$dictionary['Contact']['fields']['alt_address_country']['options']='country_list';
$dictionary['Contact']['fields']['alt_address_country']['default']='Canada';
$dictionary['Contact']['fields']['alt_address_country']['audited']=false;
$dictionary['Contact']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Contact']['fields']['alt_address_country']['comments']='Country for alt address';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Contact']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Contact']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_country']['calculated']=false;
$dictionary['Contact']['fields']['alt_address_country']['dependency']=false;

 ?>