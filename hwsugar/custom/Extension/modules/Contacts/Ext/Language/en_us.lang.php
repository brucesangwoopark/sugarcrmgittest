<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_EXTERNAL_KEY'] = 'ChargeOver external key';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'ChargeOver';
$mod_strings['LBL_RESPONSE_MESSAGE'] = 'ChargeOver response message';
$mod_strings['LBL_RESPONSE_CODE'] = 'ChargeOver response code';
$mod_strings['LBL_COUNTRY'] = 'country';
$mod_strings['LBL_PRIMARY_ADDRESS_COUNTRY'] = 'Primary Address Country:';
$mod_strings['LBL_PRIMARY_ADDRESS_STATE'] = 'Primary Address State:';
$mod_strings['LBL_ALT_ADDRESS_STATE'] = 'Alternate Address State:';
$mod_strings['LBL_ALT_ADDRESS_COUNTRY'] = 'Alternate Address Country:';
$mod_strings['LBL_MOST_RECENT_ADDRESS'] = 'most recent address';
$mod_strings['LBL_COMPARTMENT_NUMBER'] = 'Unit #';
$mod_strings['LBL_VISIBLEUNTILDATE'] = 'visibleuntildate';
