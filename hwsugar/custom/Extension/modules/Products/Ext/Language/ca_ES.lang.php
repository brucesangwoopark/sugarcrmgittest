<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_PRODUCT'] = 'Crear element de la línia pressupostada';
$mod_strings['LBL_MODULE_NAME'] = 'Elements de línies d&#039;oferta';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Element de línia d&#039;oferta';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Crear element de la línia pressupostada';
$mod_strings['LNK_PRODUCT_LIST'] = 'Productes';
$mod_strings['LNK_IMPORT_PRODUCTS'] = 'Importar Productes';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Llista d&#039;elements de línia de pressupost';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cerca d&#039;elements de la línia pressupostada';
$mod_strings['LBL_PRODUCTS_SUBPANEL_TITLE'] = 'Elements de línies d&#039;oferta';
$mod_strings['LBL_RELATED_PRODUCTS'] = 'Elements de línies d&#039;oferta relacionats';
