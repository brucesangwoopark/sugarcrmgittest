<?php
// created: 2016-10-25 13:31:54
$dictionary["abcde_jobs_notes_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'abcde_jobs_notes_1' => 
    array (
      'lhs_module' => 'abcde_Jobs',
      'lhs_table' => 'abcde_jobs',
      'lhs_key' => 'id',
      'rhs_module' => 'Notes',
      'rhs_table' => 'notes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'abcde_jobs_notes_1_c',
      'join_key_lhs' => 'abcde_jobs_notes_1abcde_jobs_ida',
      'join_key_rhs' => 'abcde_jobs_notes_1notes_idb',
    ),
  ),
  'table' => 'abcde_jobs_notes_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'abcde_jobs_notes_1abcde_jobs_ida' => 
    array (
      'name' => 'abcde_jobs_notes_1abcde_jobs_ida',
      'type' => 'id',
    ),
    'abcde_jobs_notes_1notes_idb' => 
    array (
      'name' => 'abcde_jobs_notes_1notes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'abcde_jobs_notes_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'abcde_jobs_notes_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'abcde_jobs_notes_1abcde_jobs_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'abcde_jobs_notes_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'abcde_jobs_notes_1notes_idb',
      ),
    ),
  ),
);