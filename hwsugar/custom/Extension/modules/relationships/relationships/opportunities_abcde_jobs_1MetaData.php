<?php
// created: 2016-10-24 19:51:07
$dictionary["opportunities_abcde_jobs_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'opportunities_abcde_jobs_1' => 
    array (
      'lhs_module' => 'Opportunities',
      'lhs_table' => 'opportunities',
      'lhs_key' => 'id',
      'rhs_module' => 'abcde_Jobs',
      'rhs_table' => 'abcde_jobs',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'opportunities_abcde_jobs_1_c',
      'join_key_lhs' => 'opportunities_abcde_jobs_1opportunities_ida',
      'join_key_rhs' => 'opportunities_abcde_jobs_1abcde_jobs_idb',
    ),
  ),
  'table' => 'opportunities_abcde_jobs_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'opportunities_abcde_jobs_1opportunities_ida' => 
    array (
      'name' => 'opportunities_abcde_jobs_1opportunities_ida',
      'type' => 'id',
    ),
    'opportunities_abcde_jobs_1abcde_jobs_idb' => 
    array (
      'name' => 'opportunities_abcde_jobs_1abcde_jobs_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'opportunities_abcde_jobs_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'opportunities_abcde_jobs_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'opportunities_abcde_jobs_1opportunities_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'opportunities_abcde_jobs_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'opportunities_abcde_jobs_1abcde_jobs_idb',
      ),
    ),
  ),
);