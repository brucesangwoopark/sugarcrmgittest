<?php
// created: 2016-11-14 14:59:12
$dictionary["abcde_jobs_revenuelineitems_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'abcde_jobs_revenuelineitems_1' => 
    array (
      'lhs_module' => 'abcde_Jobs',
      'lhs_table' => 'abcde_jobs',
      'lhs_key' => 'id',
      'rhs_module' => 'RevenueLineItems',
      'rhs_table' => 'revenue_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'abcde_jobs_revenuelineitems_1_c',
      'join_key_lhs' => 'abcde_jobs_revenuelineitems_1abcde_jobs_ida',
      'join_key_rhs' => 'abcde_jobs_revenuelineitems_1revenuelineitems_idb',
    ),
  ),
  'table' => 'abcde_jobs_revenuelineitems_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'abcde_jobs_revenuelineitems_1abcde_jobs_ida' => 
    array (
      'name' => 'abcde_jobs_revenuelineitems_1abcde_jobs_ida',
      'type' => 'id',
    ),
    'abcde_jobs_revenuelineitems_1revenuelineitems_idb' => 
    array (
      'name' => 'abcde_jobs_revenuelineitems_1revenuelineitems_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'abcde_jobs_revenuelineitems_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'abcde_jobs_revenuelineitems_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'abcde_jobs_revenuelineitems_1abcde_jobs_ida',
        1 => 'abcde_jobs_revenuelineitems_1revenuelineitems_idb',
      ),
    ),
  ),
);