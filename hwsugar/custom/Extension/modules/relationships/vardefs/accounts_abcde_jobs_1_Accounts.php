<?php
// created: 2016-10-24 21:45:23
$dictionary["Account"]["fields"]["accounts_abcde_jobs_1"] = array (
  'name' => 'accounts_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'accounts_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'abcde_Jobs',
  'bean_name' => 'abcde_Jobs',
  'vname' => 'LBL_ACCOUNTS_ABCDE_JOBS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_abcde_jobs_1accounts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
