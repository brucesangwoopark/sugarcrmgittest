<?php
// created: 2016-10-24 21:45:23
$dictionary["abcde_Jobs"]["fields"]["accounts_abcde_jobs_1"] = array (
  'name' => 'accounts_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'accounts_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE',
  'id_name' => 'accounts_abcde_jobs_1accounts_ida',
  'link-type' => 'one',
);
$dictionary["abcde_Jobs"]["fields"]["accounts_abcde_jobs_1_name"] = array (
  'name' => 'accounts_abcde_jobs_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ABCDE_JOBS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_abcde_jobs_1accounts_ida',
  'link' => 'accounts_abcde_jobs_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["abcde_Jobs"]["fields"]["accounts_abcde_jobs_1accounts_ida"] = array (
  'name' => 'accounts_abcde_jobs_1accounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE_ID',
  'id_name' => 'accounts_abcde_jobs_1accounts_ida',
  'link' => 'accounts_abcde_jobs_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
