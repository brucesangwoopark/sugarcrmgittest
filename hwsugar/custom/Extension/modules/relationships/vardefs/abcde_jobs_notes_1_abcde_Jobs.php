<?php
// created: 2016-10-25 13:31:54
$dictionary["abcde_Jobs"]["fields"]["abcde_jobs_notes_1"] = array (
  'name' => 'abcde_jobs_notes_1',
  'type' => 'link',
  'relationship' => 'abcde_jobs_notes_1',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_ABCDE_JOBS_NOTES_1_FROM_ABCDE_JOBS_TITLE',
  'id_name' => 'abcde_jobs_notes_1abcde_jobs_ida',
  'link-type' => 'many',
  'side' => 'left',
);
