<?php
// created: 2016-11-14 14:59:12
$dictionary["abcde_Jobs"]["fields"]["abcde_jobs_revenuelineitems_1"] = array (
  'name' => 'abcde_jobs_revenuelineitems_1',
  'type' => 'link',
  'relationship' => 'abcde_jobs_revenuelineitems_1',
  'source' => 'non-db',
  'module' => 'RevenueLineItems',
  'bean_name' => 'RevenueLineItem',
  'vname' => 'LBL_ABCDE_JOBS_REVENUELINEITEMS_1_FROM_REVENUELINEITEMS_TITLE',
  'id_name' => 'abcde_jobs_revenuelineitems_1revenuelineitems_idb',
);
