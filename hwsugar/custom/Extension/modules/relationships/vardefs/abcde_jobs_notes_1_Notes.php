<?php
// created: 2016-10-25 13:31:54
$dictionary["Note"]["fields"]["abcde_jobs_notes_1"] = array (
  'name' => 'abcde_jobs_notes_1',
  'type' => 'link',
  'relationship' => 'abcde_jobs_notes_1',
  'source' => 'non-db',
  'module' => 'abcde_Jobs',
  'bean_name' => 'abcde_Jobs',
  'side' => 'right',
  'vname' => 'LBL_ABCDE_JOBS_NOTES_1_FROM_NOTES_TITLE',
  'id_name' => 'abcde_jobs_notes_1abcde_jobs_ida',
  'link-type' => 'one',
);
$dictionary["Note"]["fields"]["abcde_jobs_notes_1_name"] = array (
  'name' => 'abcde_jobs_notes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ABCDE_JOBS_NOTES_1_FROM_ABCDE_JOBS_TITLE',
  'save' => true,
  'id_name' => 'abcde_jobs_notes_1abcde_jobs_ida',
  'link' => 'abcde_jobs_notes_1',
  'table' => 'abcde_jobs',
  'module' => 'abcde_Jobs',
  'rname' => 'name',
);
$dictionary["Note"]["fields"]["abcde_jobs_notes_1abcde_jobs_ida"] = array (
  'name' => 'abcde_jobs_notes_1abcde_jobs_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_ABCDE_JOBS_NOTES_1_FROM_NOTES_TITLE_ID',
  'id_name' => 'abcde_jobs_notes_1abcde_jobs_ida',
  'link' => 'abcde_jobs_notes_1',
  'table' => 'abcde_jobs',
  'module' => 'abcde_Jobs',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
