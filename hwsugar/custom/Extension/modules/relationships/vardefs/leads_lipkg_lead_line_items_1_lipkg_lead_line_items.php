<?php
// created: 2016-09-23 15:14:30
$dictionary["lipkg_lead_line_items"]["fields"]["leads_lipkg_lead_line_items_1"] = array (
  'name' => 'leads_lipkg_lead_line_items_1',
  'type' => 'link',
  'relationship' => 'leads_lipkg_lead_line_items_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_LEADS_LIPKG_LEAD_LINE_ITEMS_1_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE',
  'id_name' => 'leads_lipkg_lead_line_items_1leads_ida',
  'link-type' => 'one',
);
$dictionary["lipkg_lead_line_items"]["fields"]["leads_lipkg_lead_line_items_1_name"] = array (
  'name' => 'leads_lipkg_lead_line_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_LIPKG_LEAD_LINE_ITEMS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_lipkg_lead_line_items_1leads_ida',
  'link' => 'leads_lipkg_lead_line_items_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["lipkg_lead_line_items"]["fields"]["leads_lipkg_lead_line_items_1leads_ida"] = array (
  'name' => 'leads_lipkg_lead_line_items_1leads_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_LIPKG_LEAD_LINE_ITEMS_1_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE_ID',
  'id_name' => 'leads_lipkg_lead_line_items_1leads_ida',
  'link' => 'leads_lipkg_lead_line_items_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
