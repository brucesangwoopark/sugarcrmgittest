<?php
 // created: 2016-10-25 13:31:54
$layout_defs["abcde_Jobs"]["subpanel_setup"]['abcde_jobs_notes_1'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ABCDE_JOBS_NOTES_1_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'abcde_jobs_notes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
