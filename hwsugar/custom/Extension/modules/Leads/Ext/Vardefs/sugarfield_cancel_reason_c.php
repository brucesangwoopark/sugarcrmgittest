<?php
 // created: 2016-10-13 21:28:05
$dictionary['Lead']['fields']['cancel_reason_c']['labelValue']='cancel reason';
$dictionary['Lead']['fields']['cancel_reason_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['cancel_reason_c']['enforced']='';
$dictionary['Lead']['fields']['cancel_reason_c']['dependency']='equal($customer_status_c,"Cancelled")';

 ?>