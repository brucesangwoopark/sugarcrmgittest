<?php
 // created: 2016-10-21 18:01:59
$dictionary['Lead']['fields']['primary_address_city']['required']=true;
$dictionary['Lead']['fields']['primary_address_city']['audited']=false;
$dictionary['Lead']['fields']['primary_address_city']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_city']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['primary_address_city']['calculated']=false;

 ?>