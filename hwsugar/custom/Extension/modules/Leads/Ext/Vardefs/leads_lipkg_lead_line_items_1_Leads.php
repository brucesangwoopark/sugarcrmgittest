<?php
// created: 2016-09-23 15:14:30
$dictionary["Lead"]["fields"]["leads_lipkg_lead_line_items_1"] = array (
  'name' => 'leads_lipkg_lead_line_items_1',
  'type' => 'link',
  'relationship' => 'leads_lipkg_lead_line_items_1',
  'source' => 'non-db',
  'module' => 'lipkg_lead_line_items',
  'bean_name' => 'lipkg_lead_line_items',
  'vname' => 'LBL_LEADS_LIPKG_LEAD_LINE_ITEMS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_lipkg_lead_line_items_1leads_ida',
  'link-type' => 'many',
  'side' => 'left',
);
