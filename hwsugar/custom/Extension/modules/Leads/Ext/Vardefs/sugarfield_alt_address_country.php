<?php
 // created: 2016-11-14 15:52:47
$dictionary['Lead']['fields']['alt_address_country']['type']='enum';
$dictionary['Lead']['fields']['alt_address_country']['options']='country_list';
$dictionary['Lead']['fields']['alt_address_country']['default']='Canada';
$dictionary['Lead']['fields']['alt_address_country']['audited']=false;
$dictionary['Lead']['fields']['alt_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_country']['comments']='Country for alt address';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_country']['calculated']=false;
$dictionary['Lead']['fields']['alt_address_country']['dependency']=false;

 ?>