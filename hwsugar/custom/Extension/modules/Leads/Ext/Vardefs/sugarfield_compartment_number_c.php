<?php
 // created: 2017-06-22 09:02:46
$dictionary['Lead']['fields']['compartment_number_c']['labelValue']='Unit #';
$dictionary['Lead']['fields']['compartment_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['compartment_number_c']['enforced']='';
$dictionary['Lead']['fields']['compartment_number_c']['dependency']='';

 ?>