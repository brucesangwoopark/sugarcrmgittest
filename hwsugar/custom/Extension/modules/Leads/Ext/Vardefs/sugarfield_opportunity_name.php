<?php
 // created: 2016-10-13 22:33:33
$dictionary['Lead']['fields']['opportunity_name']['required']=false;
$dictionary['Lead']['fields']['opportunity_name']['audited']=false;
$dictionary['Lead']['fields']['opportunity_name']['massupdate']=false;
$dictionary['Lead']['fields']['opportunity_name']['comments']='Opportunity name associated with lead';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['opportunity_name']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['opportunity_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['opportunity_name']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['opportunity_name']['calculated']=false;

 ?>