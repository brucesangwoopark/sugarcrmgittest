<?php
 // created: 2016-10-21 18:02:22
$dictionary['Lead']['fields']['alt_address_state']['type']='enum';
$dictionary['Lead']['fields']['alt_address_state']['options']='state_dom';
$dictionary['Lead']['fields']['alt_address_state']['required']=true;
$dictionary['Lead']['fields']['alt_address_state']['audited']=false;
$dictionary['Lead']['fields']['alt_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['alt_address_state']['comments']='State for alt address';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['alt_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_state']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['alt_address_state']['calculated']=false;

 ?>
