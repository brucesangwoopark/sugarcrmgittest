<?php
 // created: 2017-04-21 10:39:58
$dictionary['Lead']['fields']['primary_address_country']['type']='enum';
$dictionary['Lead']['fields']['primary_address_country']['options']='country_list';
$dictionary['Lead']['fields']['primary_address_country']['default']='';
$dictionary['Lead']['fields']['primary_address_country']['audited']=false;
$dictionary['Lead']['fields']['primary_address_country']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_country']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_country']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_country']['dependency']=false;
$dictionary['Lead']['fields']['primary_address_country']['required']=true;

 ?>