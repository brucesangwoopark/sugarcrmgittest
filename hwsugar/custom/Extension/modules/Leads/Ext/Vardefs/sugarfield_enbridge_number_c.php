<?php
 // created: 2017-02-27 16:26:06
$dictionary['Lead']['fields']['enbridge_number_c']['labelValue']='enbridge number';
$dictionary['Lead']['fields']['enbridge_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Lead']['fields']['enbridge_number_c']['enforced']='';
$dictionary['Lead']['fields']['enbridge_number_c']['dependency']='equal($enbridge_customer_c,"1")';

 ?>