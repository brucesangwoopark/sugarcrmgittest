<?php
 // created: 2017-06-23 08:59:42
$dictionary['Lead']['fields']['primary_address_state']['type']='enum';
$dictionary['Lead']['fields']['primary_address_state']['options']='state_list';
$dictionary['Lead']['fields']['primary_address_state']['required']=true;
$dictionary['Lead']['fields']['primary_address_state']['audited']=false;
$dictionary['Lead']['fields']['primary_address_state']['massupdate']=false;
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['primary_address_state']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['calculated']=false;
$dictionary['Lead']['fields']['primary_address_state']['default']='';
$dictionary['Lead']['fields']['primary_address_state']['len']=100;
$dictionary['Lead']['fields']['primary_address_state']['dependency']=false;

 ?>