<?php
// created: 2017-10-26 16:56:34
$dictionary["Note"]["fields"]["cases_tickets_notes"] = array (
  'name' => 'cases_tickets_notes',
  'type' => 'link',
  'relationship' => 'cases_tickets_notes',
  'source' => 'non-db',
  'module' => 'cases_Tickets',
  'bean_name' => 'cases_Tickets',
  'side' => 'right',
  'vname' => 'LBL_CASES_TICKETS_NOTES_FROM_NOTES_TITLE',
  'id_name' => 'cases_tickets_notescases_tickets_ida',
  'link-type' => 'one',
);
$dictionary["Note"]["fields"]["cases_tickets_notes_name"] = array (
  'name' => 'cases_tickets_notes_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_NOTES_FROM_CASES_TICKETS_TITLE',
  'save' => true,
  'id_name' => 'cases_tickets_notescases_tickets_ida',
  'link' => 'cases_tickets_notes',
  'table' => 'cases_tickets',
  'module' => 'cases_Tickets',
  'rname' => 'name',
);
$dictionary["Note"]["fields"]["cases_tickets_notescases_tickets_ida"] = array (
  'name' => 'cases_tickets_notescases_tickets_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_NOTES_FROM_NOTES_TITLE_ID',
  'id_name' => 'cases_tickets_notescases_tickets_ida',
  'link' => 'cases_tickets_notes',
  'table' => 'cases_tickets',
  'module' => 'cases_Tickets',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
