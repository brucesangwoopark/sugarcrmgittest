<?php
 // created: 2016-11-07 20:33:18
$layout_defs["supkg_SystemUsers"]["subpanel_setup"]['supkg_systemusers_abcde_jobs_1'] = array (
  'order' => 100,
  'module' => 'abcde_Jobs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SUPKG_SYSTEMUSERS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE',
  'get_subpanel_data' => 'supkg_systemusers_abcde_jobs_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
