<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-29
 * Time: 오전 9:35
 */

$viewdefs['supkg_SystemUsers']['base']['filter']['basic']['filters'][] = array(
    'id' => 'filter_supkg_systemusers_template',
    'name' => 'LBL_TECH_ONLY_FILTER',
    'filter_definition' => array(
        array(
            'is_active_c' => array(
                '$in' => array(
                    'y',
                    'Y'
                ),
            ),
        )
    ),
    'editable' => true,
    'is_template' => true,
);