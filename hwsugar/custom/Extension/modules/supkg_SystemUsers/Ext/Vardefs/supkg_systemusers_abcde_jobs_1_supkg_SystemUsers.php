<?php
// created: 2016-11-07 20:33:18
$dictionary["supkg_SystemUsers"]["fields"]["supkg_systemusers_abcde_jobs_1"] = array (
  'name' => 'supkg_systemusers_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'supkg_systemusers_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'abcde_Jobs',
  'bean_name' => 'abcde_Jobs',
  'vname' => 'LBL_SUPKG_SYSTEMUSERS_ABCDE_JOBS_1_FROM_SUPKG_SYSTEMUSERS_TITLE',
  'id_name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
  'link-type' => 'many',
  'side' => 'left',
);
