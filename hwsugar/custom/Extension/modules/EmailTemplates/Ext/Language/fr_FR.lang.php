<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Modèle d&#039;email';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Modèle Email';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nouveau Modèle Email';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Liste des Modèles';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Rechercher un Modèle';
