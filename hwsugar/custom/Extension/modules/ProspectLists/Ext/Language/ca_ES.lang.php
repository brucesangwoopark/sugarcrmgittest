<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_MODULE_NAME'] = 'Llistes d&#039;objectius';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Llista d&#039;objectius';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Nova llista d&#039;objectius';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Llistes d&#039;objectius';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Cerca de llistes d&#039;objectius';
