<?php
// created: 2016-11-07 20:33:18
$dictionary["abcde_Jobs"]["fields"]["supkg_systemusers_abcde_jobs_1"] = array (
  'name' => 'supkg_systemusers_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'supkg_systemusers_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'supkg_SystemUsers',
  'bean_name' => 'supkg_SystemUsers',
  'side' => 'right',
  'vname' => 'LBL_SUPKG_SYSTEMUSERS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE',
  'id_name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
  'link-type' => 'one',
);
$dictionary["abcde_Jobs"]["fields"]["supkg_systemusers_abcde_jobs_1_name"] = array (
  'name' => 'supkg_systemusers_abcde_jobs_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SUPKG_SYSTEMUSERS_ABCDE_JOBS_1_FROM_SUPKG_SYSTEMUSERS_TITLE',
  'save' => true,
  'id_name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
  'link' => 'supkg_systemusers_abcde_jobs_1',
  'table' => 'supkg_systemusers',
  'module' => 'supkg_SystemUsers',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["abcde_Jobs"]["fields"]["supkg_systemusers_abcde_jobs_1supkg_systemusers_ida"] = array (
  'name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_SUPKG_SYSTEMUSERS_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE_ID',
  'id_name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
  'link' => 'supkg_systemusers_abcde_jobs_1',
  'table' => 'supkg_systemusers',
  'module' => 'supkg_SystemUsers',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
