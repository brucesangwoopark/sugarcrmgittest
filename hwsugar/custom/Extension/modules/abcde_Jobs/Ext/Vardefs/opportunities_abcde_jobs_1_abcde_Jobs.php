<?php
// created: 2016-10-24 19:51:07
$dictionary["abcde_Jobs"]["fields"]["opportunities_abcde_jobs_1"] = array (
  'name' => 'opportunities_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'opportunities_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE',
  'id_name' => 'opportunities_abcde_jobs_1opportunities_ida',
  'link-type' => 'one',
);
$dictionary["abcde_Jobs"]["fields"]["opportunities_abcde_jobs_1_name"] = array (
  'name' => 'opportunities_abcde_jobs_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_ABCDE_JOBS_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_abcde_jobs_1opportunities_ida',
  'link' => 'opportunities_abcde_jobs_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["abcde_Jobs"]["fields"]["opportunities_abcde_jobs_1opportunities_ida"] = array (
  'name' => 'opportunities_abcde_jobs_1opportunities_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_ABCDE_JOBS_1_FROM_ABCDE_JOBS_TITLE_ID',
  'id_name' => 'opportunities_abcde_jobs_1opportunities_ida',
  'link' => 'opportunities_abcde_jobs_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
