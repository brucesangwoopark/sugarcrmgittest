<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-23
 * Time: 오후 7:50
 */

$dictionary['abcde_Jobs']['fields']['fspg2_filterservicedetails'] = array (
    'name' => 'fspg2_filterservicedetails',
    'type' => 'link',
    'relationship' => 'abcde_jobs_fspg2_filterservicedetails',
    'module' => 'FSPG2_FilterServiceDetails',
    'bean_name' => 'FSPG2_FilterServiceDetails',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEDETAILS',
);