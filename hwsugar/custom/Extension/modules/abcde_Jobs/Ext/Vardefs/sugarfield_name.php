<?php
 // created: 2016-10-25 14:29:28
$dictionary['abcde_Jobs']['fields']['name']['default']='Do not fill in - will be filled automatically';
$dictionary['abcde_Jobs']['fields']['name']['len']='255';
$dictionary['abcde_Jobs']['fields']['name']['audited']=false;
$dictionary['abcde_Jobs']['fields']['name']['massupdate']=false;
$dictionary['abcde_Jobs']['fields']['name']['unified_search']=false;
$dictionary['abcde_Jobs']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['abcde_Jobs']['fields']['name']['calculated']=false;

 ?>