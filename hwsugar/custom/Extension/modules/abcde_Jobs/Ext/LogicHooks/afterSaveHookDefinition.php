<?php

// No longer needed to define logic hook v7.7
//$hook_version = 1;
//$hook_array = Array();
//// order, description, file, class, method
//$hook_array['before_save'] = Array();
   $hook_array['after_save'][] = Array(
   
   //Processing index. For sorting the array.
   99,
   
   //Label. A string value to identify the hook.
   'After Save Jobs Hook',
   
   //The PHP file where the class is located.
   'custom/modules/abcde_Jobs/afterSaveHookClass.php',
   
   //The class the method is in.
   'JobsAfterSaveHook',
   
   //The method to call.
   'afterSave'
   
   );

?>