<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 10:25
 */

$dictionary['FSPG2_FilterServiceDetails']['fields']['fspg2_filterserviceplans_name'] = array(
    'required' => true,
    'source' => 'non-db',
    'name' => 'fspg2_filterserviceplans_name',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'fspg2_filterserviceplans_id',
    'join_name' => 'fspg2_filterserviceplans',
    'link' => 'fspg2_filterserviceplans',
    'table' => 'fspg2_filterserviceplans',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterServicePlans',
    'readonly' => true,
);


$dictionary['FSPG2_FilterServiceDetails']['fields']['fspg2_filterserviceplans_id'] = array(
    'name' => 'fspg2_filterserviceplans_id',
    'rname' => 'id',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS_ID',
    'type' => 'id',
    'table' => 'fspg2_filterserviceplans',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterServicePlans',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);


$dictionary['FSPG2_FilterServiceDetails']['fields']['fspg2_filterserviceplans'] = array(
    'name' => 'fspg2_filterserviceplans',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_fspg2_filterservicedetails',
    'module' => 'FSPG2_FilterServicePlans',
    'bean_name' => 'FSPG2_FilterServicePlans',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS',
);


$dictionary['FSPG2_FilterServiceDetails']['relationships']['fspg2_filterserviceplans_fspg2_filterservicedetails'] = array(
    'lhs_module' => 'FSPG2_FilterServicePlans',
    'lhs_table' => 'fspg2_filterserviceplans',
    'lhs_key' => 'id',
    'rhs_module' => 'FSPG2_FilterServiceDetails',
    'rhs_table' => 'fspg2_filterservicedetails',
    'rhs_key' => 'fspg2_filterserviceplans_id',
    'relationship_type' => 'one-to-many',
);