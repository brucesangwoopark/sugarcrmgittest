<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-21
 * Time: 오후 3:35
 */


$dictionary['FSPG2_FilterServiceDetails']['fields']['abcde_jobs_name'] = array(
    'required' => false,
    'source' => 'non-db',
    'name' => 'abcde_jobs_name',
    'vname' => 'LBL_ABCDE_JOBS_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'abcde_jobs_id',
    'join_name' => 'abcde_jobs',
    'link' => 'abcde_jobs',
    'table' => 'abcde_jobs',
    'isnull' => true,
    'module' => 'abcde_Jobs',
    'readonly' => true,
);


$dictionary['FSPG2_FilterServiceDetails']['fields']['abcde_jobs_id'] = array(
    'name' => 'abcde_jobs_id',
    'rname' => 'id',
    'vname' => 'LBL_ABCDE_JOBS_ID',
    'type' => 'id',
    'table' => 'abcde_jobs',
    'isnull' => true,
    'module' => 'abcde_Jobs',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);


$dictionary['FSPG2_FilterServiceDetails']['fields']['abcde_jobs'] = array(
    'name' => 'abcde_jobs',
    'type' => 'link',
    'relationship' => 'abcde_jobs_fspg2_filterservicedetails',
    'module' => 'abcde_Jobs',
    'bean_name' => 'abcde_Jobs',
    'source' => 'non-db',
    'vname' => 'LBL_ABCDE_JOBS',
);


$dictionary['FSPG2_FilterServiceDetails']['relationships']['abcde_jobs_fspg2_filterservicedetails'] = array(
    'lhs_module' => 'abcde_Jobs',
    'lhs_table' => 'abcde_jobs',
    'lhs_key' => 'id',
    'rhs_module' => 'FSPG2_FilterServiceDetails',
    'rhs_table' => 'fspg2_filterservicedetails',
    'rhs_key' => 'abcde_jobs_id',
    'relationship_type' => 'one-to-many',
);