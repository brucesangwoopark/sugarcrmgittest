<?php

$mod_strings['LBL_OPS_NOTIFICATION_SETTINGS_LINK_NAME'] = 'Notification Settings';
$mod_strings['LBL_OPS_NOTIFICATION_SETTINGS_LINK_DESCRIPTION'] = 'Email Addresses that should be notified for upgrades and OnDemand changes.';
$mod_strings['LBL_OPS_ONDEMAND_SECTION_HEADER'] = 'Sugar OnDemand';
$mod_strings['LBL_OPS_ONDEMAND_SECTION_DESCRIPTION'] = 'Sugar OnDemand Modules and Settings';
$mod_strings['LBL_OPS_NOTIFICATION_EMAILS'] = 'Send Notifications To:';
$mod_strings['LBL_OPS_NOTIFICATION_EMAIL_SETTINGS'] = 'Email Settings';
$mod_strings['LBL_OPS_BACKUPS'] = 'Download Backups';
$mod_strings['LBL_OPS_BACKUPS_DESCRIPTION'] = 'Download Archived backups of the Sugar OnDemand instance.';
