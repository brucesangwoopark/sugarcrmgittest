<?php
 // created: 2017-04-05 14:54:47
$dictionary['Case']['fields']['priority']['default']='P3';
$dictionary['Case']['fields']['priority']['massupdate']=true;
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['priority']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';
$dictionary['Case']['fields']['priority']['calculated']=false;
$dictionary['Case']['fields']['priority']['dependency']=false;

 ?>