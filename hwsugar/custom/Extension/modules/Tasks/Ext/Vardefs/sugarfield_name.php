<?php
 // created: 2017-01-16 16:31:44
$dictionary['Task']['fields']['name']['default']='';
$dictionary['Task']['fields']['name']['audited']=false;
$dictionary['Task']['fields']['name']['massupdate']=false;
$dictionary['Task']['fields']['name']['duplicate_merge']='enabled';
$dictionary['Task']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['Task']['fields']['name']['merge_filter']='disabled';
$dictionary['Task']['fields']['name']['unified_search']=false;
$dictionary['Task']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.45',
  'searchable' => true,
);
$dictionary['Task']['fields']['name']['calculated']=false;

 ?>