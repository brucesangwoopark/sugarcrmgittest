<?php
 // created: 2016-10-17 21:50:26
$dictionary['lipkg_lead_line_items']['fields']['name']['len']='255';
$dictionary['lipkg_lead_line_items']['fields']['name']['required']=false;
$dictionary['lipkg_lead_line_items']['fields']['name']['audited']=false;
$dictionary['lipkg_lead_line_items']['fields']['name']['massupdate']=false;
$dictionary['lipkg_lead_line_items']['fields']['name']['unified_search']=false;
$dictionary['lipkg_lead_line_items']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['lipkg_lead_line_items']['fields']['name']['calculated']=false;
$dictionary['lipkg_lead_line_items']['fields']['name']['default']='Do not fill in - will be filled automatically';

 ?>