<?php
// created: 2016-09-23 14:47:30
$dictionary["lipkg_lead_line_items"]["fields"]["lipkg_lead_line_items_producttemplates"] = array (
  'name' => 'lipkg_lead_line_items_producttemplates',
  'type' => 'link',
  'relationship' => 'lipkg_lead_line_items_producttemplates',
  'source' => 'non-db',
  'module' => 'ProductTemplates',
  'bean_name' => 'ProductTemplate',
  'side' => 'right',
  'vname' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE',
  'id_name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  'link-type' => 'one',
);
$dictionary["lipkg_lead_line_items"]["fields"]["lipkg_lead_line_items_producttemplates_name"] = array (
  'name' => 'lipkg_lead_line_items_producttemplates_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_PRODUCTTEMPLATES_TITLE',
  'save' => true,
  'id_name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  'link' => 'lipkg_lead_line_items_producttemplates',
  'table' => 'product_templates',
  'module' => 'ProductTemplates',
  'rname' => 'name',
);
$dictionary["lipkg_lead_line_items"]["fields"]["lipkg_lead_line_items_producttemplatesproducttemplates_ida"] = array (
  'name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE_ID',
  'id_name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  'link' => 'lipkg_lead_line_items_producttemplates',
  'table' => 'product_templates',
  'module' => 'ProductTemplates',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
