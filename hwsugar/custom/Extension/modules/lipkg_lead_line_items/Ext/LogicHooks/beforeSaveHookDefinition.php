<?php
   $hook_array['before_save'][] = Array(
   
   //Processing index. For sorting the array.
   99,
   
   //Label. A string value to identify the hook.
   'Before Save LeadLineItems Hook',
   
   //The PHP file where the class is located.
   'custom/modules/lipkg_lead_line_items/beforeSaveHookClass.php',
   
   //The class the method is in.
   'BeforeSaveLeadLineItemsHook',
   
   //The method to call.
   'beforeSave'
   
   );

?>