<?php
 // created: 2017-10-26 16:56:34
$layout_defs["cases_Tickets"]["subpanel_setup"]['cases_tickets_notes'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_TICKETS_NOTES_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'cases_tickets_notes',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
