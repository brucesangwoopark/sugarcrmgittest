<?php
 // created: 2017-10-26 16:56:34
$layout_defs["cases_Tickets"]["subpanel_setup"]['cases_tickets_documents'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'ForContractType',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_TICKETS_DOCUMENTS_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'cases_tickets_documents',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
