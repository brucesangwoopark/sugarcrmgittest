<?php
 // created: 2017-10-26 16:56:35
$layout_defs["cases_Tickets"]["subpanel_setup"]['cases_tickets_documents'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'ForContractType',
  'title_key' => 'LBL_CASES_TICKETS_DOCUMENTS_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'cases_tickets_documents',
);
