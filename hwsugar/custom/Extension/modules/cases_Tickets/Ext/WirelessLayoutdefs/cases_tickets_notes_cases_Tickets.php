<?php
 // created: 2017-10-26 16:56:35
$layout_defs["cases_Tickets"]["subpanel_setup"]['cases_tickets_notes'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CASES_TICKETS_NOTES_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'cases_tickets_notes',
);
