<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_Tickets"]["fields"]["cases_tickets_documents"] = array (
  'name' => 'cases_tickets_documents',
  'type' => 'link',
  'relationship' => 'cases_tickets_documents',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_CASES_TICKETS_DOCUMENTS_FROM_CASES_TICKETS_TITLE',
  'id_name' => 'cases_tickets_documentscases_tickets_ida',
  'link-type' => 'many',
  'side' => 'left',
);
