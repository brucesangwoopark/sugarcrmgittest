<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_Tickets"]["fields"]["cases_tickets_notes"] = array (
  'name' => 'cases_tickets_notes',
  'type' => 'link',
  'relationship' => 'cases_tickets_notes',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'vname' => 'LBL_CASES_TICKETS_NOTES_FROM_CASES_TICKETS_TITLE',
  'id_name' => 'cases_tickets_notescases_tickets_ida',
  'link-type' => 'many',
  'side' => 'left',
);
