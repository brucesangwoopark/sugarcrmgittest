<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_Tickets"]["fields"]["cases_tickets_revenuelineitems"] = array (
  'name' => 'cases_tickets_revenuelineitems',
  'type' => 'link',
  'relationship' => 'cases_tickets_revenuelineitems',
  'source' => 'non-db',
  'module' => 'RevenueLineItems',
  'bean_name' => 'RevenueLineItem',
  'side' => 'right',
  'vname' => 'LBL_CASES_TICKETS_REVENUELINEITEMS_FROM_CASES_TICKETS_TITLE',
  'id_name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
  'link-type' => 'one',
);
$dictionary["cases_Tickets"]["fields"]["cases_tickets_revenuelineitems_name"] = array (
  'name' => 'cases_tickets_revenuelineitems_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_REVENUELINEITEMS_FROM_REVENUELINEITEMS_TITLE',
  'save' => true,
  'id_name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
  'link' => 'cases_tickets_revenuelineitems',
  'table' => 'revenue_line_items',
  'module' => 'RevenueLineItems',
  'rname' => 'name',
);
$dictionary["cases_Tickets"]["fields"]["cases_tickets_revenuelineitemsrevenuelineitems_ida"] = array (
  'name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_REVENUELINEITEMS_FROM_CASES_TICKETS_TITLE_ID',
  'id_name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
  'link' => 'cases_tickets_revenuelineitems',
  'table' => 'revenue_line_items',
  'module' => 'RevenueLineItems',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
