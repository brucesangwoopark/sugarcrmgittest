<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_Tickets"]["fields"]["cases_tickets_accounts"] = array (
  'name' => 'cases_tickets_accounts',
  'type' => 'link',
  'relationship' => 'cases_tickets_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_CASES_TICKETS_ACCOUNTS_FROM_CASES_TICKETS_TITLE',
  'id_name' => 'cases_tickets_accountsaccounts_ida',
  'link-type' => 'one',
);
$dictionary["cases_Tickets"]["fields"]["cases_tickets_accounts_name"] = array (
  'name' => 'cases_tickets_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'cases_tickets_accountsaccounts_ida',
  'link' => 'cases_tickets_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["cases_Tickets"]["fields"]["cases_tickets_accountsaccounts_ida"] = array (
  'name' => 'cases_tickets_accountsaccounts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_TICKETS_ACCOUNTS_FROM_CASES_TICKETS_TITLE_ID',
  'id_name' => 'cases_tickets_accountsaccounts_ida',
  'link' => 'cases_tickets_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
