<?php

// No longer needed to define logic hook v7.7
//$hook_version = 1;
//$hook_array = Array();
//// order, description, file, class, method
//$hook_array['before_save'] = Array();
$hook_array['after_save'][] = Array(
    1,
    'Complete Searchable Field',
    'custom/modules/cases_Tickets/CompleteSearchableField.php',
    'CompleteSearchableField',
    'afterSave'
);


$hook_array['after_save'][] = Array(
    2,
    'Mail Notification',
    'custom/modules/cases_Tickets/NotifyTicketUpdate.php',
    'NotifyTicketUpdate',
    'afterSave'
);