<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_SUBPANEL_FILTERSERVICEDETAILS'] = 'Filter Service Details';
$mod_strings['LBL_FSPG2_FILTERSERVICEPACKAGES_NAME'] = 'Filter Service Packages';
$mod_strings['LBL_REVENUELINEITEM_NAME'] = 'Revenue Line Items';
$mod_strings['LBL_SUBPANEL_REVENUELINEITEMS'] = 'Revenue Line Items';
$mod_strings['LBL_CO_SUB_ID'] = 'Chargeover Subscription ID';
