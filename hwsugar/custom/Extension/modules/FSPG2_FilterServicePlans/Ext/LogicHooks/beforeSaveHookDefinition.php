<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-16
 * Time: 오후 4:05
 */

$hook_array['before_save'][] = Array(
    1,
    'before_save for Filter Service Plans',
    'custom/modules/FSPG2_FilterServicePlans/BeforeSaveFilterServicePlansHook.php',
    'BeforeSaveFilterServicePlansHook',
    'beforeSave'
);