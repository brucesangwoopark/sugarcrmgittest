<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-16
 * Time: 오후 4:05
 */

$hook_array['after_save'][] = Array(
    1,
    'after_save for Filter Service Plans',
    'custom/modules/FSPG2_FilterServicePlans/AfterSaveFilterServicePlansHook.php',
    'AfterSaveFilterServicePlansHook',
    'afterSave'
);