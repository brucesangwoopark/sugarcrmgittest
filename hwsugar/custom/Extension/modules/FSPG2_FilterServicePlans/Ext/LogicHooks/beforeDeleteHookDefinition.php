<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-16
 * Time: 오후 4:05
 */

$hook_array['before_delete'][] = Array(
    1,
    'before_delete for Filter Service Plans',
    'custom/modules/FSPG2_FilterServicePlans/BeforeDeleteFilterServicePlansHook.php',
    'BeforeDeleteFilterServicePlansHook',
    'beforeDelete'
);