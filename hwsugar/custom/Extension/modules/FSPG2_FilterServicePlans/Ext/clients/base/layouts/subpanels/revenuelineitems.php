<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-12
 * Time: 오후 9:48
 */

$viewdefs['FSPG2_FilterServicePlans']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_SUBPANEL_REVENUELINEITEMS',
    'context' => array(
        'link' => 'rli',
    ),
    'override_paneltop_view' => 'panel-top-for-fspg2_filterserviceplans-rli',
);

