<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 10:57
 */

$viewdefs['FSPG2_FilterServicePlans']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_SUBPANEL_FILTERSERVICEDETAILS',
    'context' => array(
        'link' => 'fspg2_filterservicedetails',
    ),
);
