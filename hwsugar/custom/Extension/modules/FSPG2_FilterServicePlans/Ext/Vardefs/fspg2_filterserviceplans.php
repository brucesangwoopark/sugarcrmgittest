<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-21
 * Time: 오후 2:24
 */


$dictionary['FSPG2_FilterServicePlans']['fields']['lastservicedate']['readonly'] = true;
$dictionary['FSPG2_FilterServicePlans']['fields']['name']['readonly'] = true;

$dictionary['FSPG2_FilterServicePlans']['fields']['co_sub_id'] = array(
    'required' => false,
    'name' => 'co_sub_id',
    'vname' => 'LBL_CO_SUB_ID',
    'type' => 'varchar',
    'massupdate' => false,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'full_text_search' =>
        array(
            'enabled' => '0',
            'boost' => '1',
            'searchable' => false,
        ),
    'calculated' => false,
    'len' => '255',
    'size' => '20',
    'readonly' => true,
);