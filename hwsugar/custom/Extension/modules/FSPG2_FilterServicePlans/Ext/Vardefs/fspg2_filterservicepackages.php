<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 10:05
 */

$dictionary['FSPG2_FilterServicePlans']['fields']['fspg2_filterservicepackages_name'] = array(
    'required' => true,
    'source' => 'non-db',
    'name' => 'fspg2_filterservicepackages_name',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPACKAGES_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'fspg2_filterservicepackages_id',
    'join_name' => 'fspg2_filterservicepackages',
    'link' => 'fspg2_filterservicepackages',
    'table' => 'fspg2_filterservicepackages',
    'isnull' => true,
    'module' => 'FSPG2_FilterServicePackages',
    'readonly' => true,
);


$dictionary['FSPG2_FilterServicePlans']['fields']['fspg2_filterservicepackages_id'] = array(
    'name' => 'fspg2_filterservicepackages_id',
    'rname' => 'id',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPAKCAGES_ID',
    'type' => 'id',
    'table' => 'fspg2_filterservicepackages',
    'isnull' => true,
    'module' => 'FSPG2_FilterServicePackages',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);


$dictionary['FSPG2_FilterServicePlans']['fields']['fspg2_filterservicepackages'] = array(
    'name' => 'fspg2_filterservicepackages',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_fspg2_filterservicepakcages',
    'module' => 'FSPG2_FilterServicePackages',
    'bean_name' => 'FSPG2_FilterServicePackages',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPAKCAGES',
);


$dictionary['FSPG2_FilterServicePlans']['relationships']['fspg2_filterserviceplans_fspg2_filterservicepakcages'] = array(
    'lhs_module' => 'FSPG2_FilterServicePackages',
    'lhs_table' => 'fspg2_filterservicepackages',
    'lhs_key' => 'id',
    'rhs_module' => 'FSPG2_FilterServicePlans',
    'rhs_table' => 'fspg2_filterserviceplans',
    'rhs_key' => 'fspg2_filterservicepackages_id',
    'relationship_type' => 'one-to-many',
);