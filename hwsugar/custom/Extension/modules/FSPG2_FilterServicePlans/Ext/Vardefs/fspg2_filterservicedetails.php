<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 10:31
 */

$dictionary['FSPG2_FilterServicePlans']['fields']['fspg2_filterservicedetails'] = array (
    'name' => 'fspg2_filterservicedetails',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_fspg2_filterservicedetails',
    'module' => 'FSPG2_FilterServiceDetails',
    'bean_name' => 'FSPG2_FilterServiceDetails',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEDETAILS',
);