<?php
/**
 * Created by PhpStorm.
 * User: bpark
 * Date: 7/10/2017
 * Time: 3:41 PM
 */

$dictionary['FSPG2_FilterServicePlans']['fields']['rli'] = array (
    'name' => 'rli',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_revenuelineitems',
    'module' => 'RevenueLineItems',
    'bean_name' => 'RevenueLineItems',
    'source' => 'non-db',
    'vname' => 'LBL_REVENUELINEITEMS',
);