<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-11-08
 * Time: 오후 12:00
 */


$viewdefs['RevenueLineItems']['base']['filter']['basic']['filters'][] = array(
    'id' => 'rli_account_filter_template',
    'name' => 'LBL_RLI_ACCOUNT_FILTER_TEMPLATE',
    'filter_definition' => array(
        array(
            'account_name' => array(
                '$in' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);