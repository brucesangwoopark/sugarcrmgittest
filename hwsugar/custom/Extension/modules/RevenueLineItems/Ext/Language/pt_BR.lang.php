<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'Criar item da linha de receita';
$mod_strings['LBL_MODULE_NAME'] = 'Itens da linha de receita';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Item da linha de receita';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'Criar item da linha de receita';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'Visualizar item da linha de receita';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'Importar itens da linha de receita';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Lista de item da linha de receita';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Pesquisar item da linha de receita';
