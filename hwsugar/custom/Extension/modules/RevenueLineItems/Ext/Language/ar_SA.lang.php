<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_REVENUELINEITEM'] = 'إنشاء بند العائد';
$mod_strings['LBL_MODULE_NAME'] = 'بنود العائدات';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'بند العائد';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'إنشاء بند العائد';
$mod_strings['LNK_REVENUELINEITEM_LIST'] = 'عرض بنود العائدات';
$mod_strings['LNK_IMPORT_REVENUELINEITEMS'] = 'استيراد بنود العائدات';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'قائمة بند العائد';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'البحث عن بند العائد';
