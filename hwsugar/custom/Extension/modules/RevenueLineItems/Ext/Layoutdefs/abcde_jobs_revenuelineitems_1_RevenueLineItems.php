<?php
 // created: 2016-11-14 14:59:12
$layout_defs["RevenueLineItems"]["subpanel_setup"]['abcde_jobs_revenuelineitems_1'] = array (
  'order' => 100,
  'module' => 'abcde_Jobs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ABCDE_JOBS_REVENUELINEITEMS_1_FROM_ABCDE_JOBS_TITLE',
  'get_subpanel_data' => 'abcde_jobs_revenuelineitems_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
