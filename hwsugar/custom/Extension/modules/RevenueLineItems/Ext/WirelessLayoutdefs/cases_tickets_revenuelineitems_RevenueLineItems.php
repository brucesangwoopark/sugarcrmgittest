<?php
 // created: 2017-10-26 16:56:35
$layout_defs["RevenueLineItems"]["subpanel_setup"]['cases_tickets_revenuelineitems'] = array (
  'order' => 100,
  'module' => 'cases_Tickets',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_CASES_TICKETS_REVENUELINEITEMS_FROM_CASES_TICKETS_TITLE',
  'get_subpanel_data' => 'cases_tickets_revenuelineitems',
);
