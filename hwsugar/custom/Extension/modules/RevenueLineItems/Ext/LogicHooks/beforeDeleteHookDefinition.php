<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-31
 * Time: 오후 9:13
 */

$hook_array['before_delete'][] = Array(
    1,
    'Before Delete RevenueLineItems Hook',
    'custom/modules/RevenueLineItems/BeforeDeleteRevenueLineItemsHook.php',
    'BeforeDeleteRevenueLineItemsHook',
    'beforeDelete'
);
