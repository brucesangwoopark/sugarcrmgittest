<?php
 // created: 2017-03-13 17:20:06
$dictionary['RevenueLineItem']['fields']['sales_stage']['len']=100;
$dictionary['RevenueLineItem']['fields']['sales_stage']['massupdate']=true;
$dictionary['RevenueLineItem']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['sales_stage']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['sales_stage']['calculated']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['dependency']=false;
$dictionary['RevenueLineItem']['fields']['sales_stage']['default']='Pre Install';
$dictionary['RevenueLineItem']['fields']['sales_stage']['options']='sales_stage_list';

 ?>