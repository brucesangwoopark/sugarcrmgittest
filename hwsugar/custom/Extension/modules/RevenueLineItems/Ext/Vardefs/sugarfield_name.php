<?php
 // created: 2016-11-07 22:30:16
$dictionary['RevenueLineItem']['fields']['name']['default']='Do not fill in - will be filled automatically';
$dictionary['RevenueLineItem']['fields']['name']['massupdate']=false;
$dictionary['RevenueLineItem']['fields']['name']['comments']='Name of the product';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge']='enabled';
$dictionary['RevenueLineItem']['fields']['name']['duplicate_merge_dom_value']='1';
$dictionary['RevenueLineItem']['fields']['name']['merge_filter']='disabled';
$dictionary['RevenueLineItem']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.57',
  'searchable' => true,
);
$dictionary['RevenueLineItem']['fields']['name']['calculated']=false;

 ?>