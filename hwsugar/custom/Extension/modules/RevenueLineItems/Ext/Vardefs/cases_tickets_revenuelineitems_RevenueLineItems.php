<?php
// created: 2017-10-26 16:56:34
$dictionary["RevenueLineItem"]["fields"]["cases_tickets_revenuelineitems"] = array (
  'name' => 'cases_tickets_revenuelineitems',
  'type' => 'link',
  'relationship' => 'cases_tickets_revenuelineitems',
  'source' => 'non-db',
  'module' => 'cases_Tickets',
  'bean_name' => 'cases_Tickets',
  'vname' => 'LBL_CASES_TICKETS_REVENUELINEITEMS_FROM_REVENUELINEITEMS_TITLE',
  'id_name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
  'link-type' => 'many',
  'side' => 'left',
);
