<?php
// created: 2016-11-14 14:59:12
$dictionary["RevenueLineItem"]["fields"]["abcde_jobs_revenuelineitems_1"] = array (
  'name' => 'abcde_jobs_revenuelineitems_1',
  'type' => 'link',
  'relationship' => 'abcde_jobs_revenuelineitems_1',
  'source' => 'non-db',
  'module' => 'abcde_Jobs',
  'bean_name' => 'abcde_Jobs',
  'vname' => 'LBL_ABCDE_JOBS_REVENUELINEITEMS_1_FROM_ABCDE_JOBS_TITLE',
  'id_name' => 'abcde_jobs_revenuelineitems_1abcde_jobs_ida',
);
