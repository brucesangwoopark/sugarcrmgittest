<?php
/**
 * Created by PhpStorm.
 * User: bpark
 * Date: 7/10/2017
 * Time: 3:38 PM
 */


$dictionary['RevenueLineItem']['fields']['fspg2_filterserviceplans_name'] = array(
    'required' => false,
    'source' => 'non-db',
    'name' => 'fspg2_filterserviceplans_name',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'fspg2_filterserviceplans_id',
    'join_name' => 'fspg2_filterserviceplans',
    'link' => 'fspg2_filterserviceplans',
    'table' => 'fspg2_filterserviceplans',
    'isnull' => true,
    'module' => 'FSPG2_FilterServicePlans',
    'readonly' => true
);


$dictionary['RevenueLineItem']['fields']['fspg2_filterserviceplans_id'] = array(
    'name' => 'fspg2_filterserviceplans_id',
    'rname' => 'id',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS_ID',
    'type' => 'id',
    'table' => 'fspg2_filterserviceplans',
    'isnull' => true,
    'module' => 'FSPG2_FilterServicePlans',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);


$dictionary['RevenueLineItem']['fields']['fspg2_filterserviceplans'] = array(
    'name' => 'fspg2_filterserviceplans',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_revenuelineitems',
    'module' => 'RevenueLineItems',
    'bean_name' => 'RevenueLineItems',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS',
);


$dictionary['RevenueLineItem']['relationships']['fspg2_filterserviceplans_revenuelineitems'] = array(
    'lhs_module' => 'FSPG2_FilterServicePlans',
    'lhs_table' => 'fspg2_filterserviceplans',
    'lhs_key' => 'id',
    'rhs_module' => 'RevenueLineItems',
    'rhs_table' => 'revenue_line_items',
    'rhs_key' => 'fspg2_filterserviceplans_id',
    'relationship_type' => 'one-to-many',
);