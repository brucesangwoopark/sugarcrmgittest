<?php
 // created: 2016-12-30 17:30:24
$dictionary['Opportunity']['fields']['transferred_opportunity_id_c']['labelValue']='transferred opportunity id';
$dictionary['Opportunity']['fields']['transferred_opportunity_id_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['transferred_opportunity_id_c']['enforced']='';
$dictionary['Opportunity']['fields']['transferred_opportunity_id_c']['dependency']='';

 ?>