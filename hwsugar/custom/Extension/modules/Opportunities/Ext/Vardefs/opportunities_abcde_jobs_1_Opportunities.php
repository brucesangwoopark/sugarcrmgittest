<?php
// created: 2016-10-24 19:51:07
$dictionary["Opportunity"]["fields"]["opportunities_abcde_jobs_1"] = array (
  'name' => 'opportunities_abcde_jobs_1',
  'type' => 'link',
  'relationship' => 'opportunities_abcde_jobs_1',
  'source' => 'non-db',
  'module' => 'abcde_Jobs',
  'bean_name' => 'abcde_Jobs',
  'vname' => 'LBL_OPPORTUNITIES_ABCDE_JOBS_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_abcde_jobs_1opportunities_ida',
  'link-type' => 'many',
  'side' => 'left',
);
