<?php
 // created: 2017-01-11 20:12:08
$dictionary['Opportunity']['fields']['enbridge_number_c']['labelValue']='enbridge number';
$dictionary['Opportunity']['fields']['enbridge_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['enbridge_number_c']['enforced']='';
$dictionary['Opportunity']['fields']['enbridge_number_c']['dependency']='equal($enbridge_customer_c,"1")';

 ?>