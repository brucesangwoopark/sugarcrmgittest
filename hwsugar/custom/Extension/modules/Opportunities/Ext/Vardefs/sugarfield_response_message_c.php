<?php
 // created: 2016-10-03 16:39:28
$dictionary['Opportunity']['fields']['response_message_c']['labelValue']='ChargeOver response message';
$dictionary['Opportunity']['fields']['response_message_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['Opportunity']['fields']['response_message_c']['enforced']='';
$dictionary['Opportunity']['fields']['response_message_c']['dependency']='';

 ?>