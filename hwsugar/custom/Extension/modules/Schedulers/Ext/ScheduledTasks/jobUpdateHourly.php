<?php

array_push($job_strings, 'jobUpdateHourly');

function jobUpdateHourly()
{
	$GLOBALS['log']->fatal('Beginning Hourly Synchroteam Job Update '.date("Y-m-d H:i:s"));
	//Synchroteam credentials
	//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=STCRED';
	$url = 'http://www.thefilterboss.com/init.php?pid=STCRED';
	$curl = curl_init($url);
	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Content-type: application/json';
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
	curl_setopt($curl, CURLOPT_HTTPGET, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//make REST call
	$response = curl_exec($curl);
	curl_close($curl);
	$jsonResponse = json_decode($response, true);
	$encoding = base64_encode($response);
	
	//find all jobs that have been changed in the last hour
	//Synchro server is 6 hours ahead, 4 hour 45 min ahead is 1.25 hours ago actual time
	$last_hour = date("Y-m-d H:i:s", strtotime('+4 hour 45 min'));
	$json = '{
		  "changedFrom":"'.$last_hour.'"
		}';
	$GLOBALS['log']->fatal('JSON Customer string: '.$json.', '.date("Y-m-d H:i:s"));
	$url = 'https://apis.synchroteam.com/Api/v2/Jobs/Search';
	//open curl for POST
	$curl = curl_init($url);
	$headr = array();
	$headr[] = 'Accept: application/json';
	$headr[] = 'Content-Length: ' . strlen($json);
	$headr[] = 'Content-type: application/json';
	$headr[] = 'Authorization: Basic '.$encoding;
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//make REST call
	$response = curl_exec($curl);
	curl_close($curl);
	$jsonResponse = json_decode($response, true);

	// save the response as a file
    $file_content = print_r($jsonResponse, true);
    $file_name = 'list_' . date('Y-m-d_H;i;s');
    file_put_contents('./from_synchroteam/' . $file_name, $file_content);

	$job_id = array(); //this holds Synchro's job IDs
	$sugar_job_id = array(); //this holds Sugar's native job IDs
	
	//retrieve all the Sugar job IDs based on Synchro's job IDs
	for($i = 0; $i < sizeof($jsonResponse); $i++){
		$job_id[] = $jsonResponse[$i]["num"];
		$query = 'SELECT j.id as sugar_job_id
			FROM abcde_jobs j
			JOIN abcde_jobs_cstm jc ON jc.id_c = j.id
			WHERE jc.external_key_c = "'.$job_id[$i].'"
			AND j.deleted <> "1"';
		$result = $GLOBALS['db']->query($query);
		while($row = $GLOBALS['db']->fetchByAssoc($result)){
			$sugar_job_id[] = $row ['sugar_job_id'];
		}
	}

	for ($i = 0; $i < sizeof($sugar_job_id); $i++){
	   $job = BeanFactory::getBean('abcde_Jobs', $sugar_job_id[$i]);
	   $job->save();
	   //$GLOBALS['log']->fatal('Job id: '.$job_id[$i].', '.date("Y-m-d H:i:s"));
	}
	$GLOBALS['log']->fatal('Ending Hourly Synchroteam Job Update '.date("Y-m-d H:i:s"));	
	return true;

}

?>