<?php

array_push($job_strings, 'TechAvailabilityUpdate');

    function TechAvailabilityUpdate()
    {
      $GLOBALS['log']->fatal('Beginning Tech Availability Update, '.date("Y-m-d H:i:s"));
      
      //Synchroteam credentials
	    $url = 'http://www.thefilterboss.com/init.php?pid=STCRED';
      $curl = curl_init($url);
      $headr = array();
      $headr[] = 'Accept: application/json';
      $headr[] = 'Content-type: application/json';
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
      curl_setopt($curl, CURLOPT_HTTPGET, true);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      //make REST call
      $response = curl_exec($curl);
      curl_close($curl);
      $jsonResponse = json_decode($response, true);
      $encoding = base64_encode($response);
      
      $week_beginning = date("Y-m-d H:i:s", strtotime('Last Monday - 3 week')); //this is the Monday 3 weeks ago
      $week_end = date("Y-m-d H:i:s", strtotime('Last Sunday - 2 week')); //this is the Sunday 2 weeks ago
      
      $json = '{
          "dateFrom":"'.$week_beginning.'",
          "dateTo":"'.$week_end.'"
        }';
      $GLOBALS['log']->fatal('JSON Customer string: '.$json.', '.date("Y-m-d H:i:s"));
      $url = 'https://apis.synchroteam.com/Api/v2/Activities/Search';
      //open curl for POST
      $curl = curl_init($url);
      $headr = array();
      $headr[] = 'Accept: application/json';
      $headr[] = 'Content-Length: ' . strlen($json);
      $headr[] = 'Content-type: application/json';
      $headr[] = 'Authorization: Basic '.$encoding;
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      //make REST call
      $response = curl_exec($curl);
      curl_close($curl);
      $jsonResponse = json_decode($response, true);
    
      $date_start = array();
      $date_end = array();
      $tech_id = array();
      $activity_type = array();
      $notes = array();
      
      for ($i = 0; $i < sizeof($jsonResponse); $i++){
        $date_start[] = $jsonResponse[$i]['dtStart'];
        $date_end[] = $jsonResponse[$i]['dtEnd'];
        $activity_type[] = $jsonResponse[$i]['nmActivity'];
        $notes[] = $jsonResponse[$i]['noteActivity'];
        $tech_id[] = $jsonResponse[$i]['user']['id'];
      }
      
      for ($i = 0; $i < sizeof($jsonResponse); $i++){
        $query = 'INSERT INTO comm_techavailability
                  SET id = UUID(),
                    date_start = "'.$date_start[$i].'",
                    date_end = "'.$date_end[$i].'",
                    activity_type = "'.$activity_type[$i].'",
                    notes = "'.$notes[$i].'",
                    tech_id = "'.$tech_id[$i].'",
                    date_entered = "'.date("Y-m-d H:i:s").'"';
        $GLOBALS['db']->query($query);
      }
      
    
    
    
      $GLOBALS['log']->fatal('Ending Tech Availability Update, '.date("Y-m-d H:i:s")); 
      return true;
    }

?>