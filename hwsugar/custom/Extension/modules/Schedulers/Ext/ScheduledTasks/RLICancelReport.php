<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-21
 * Time: 오후 4:03
 */

array_push($job_strings, 'RLICancelReport');

function RLICancelReport()
{
    $GLOBALS['log']->fatal('Beginning RLICancelReport, '.date("Y-m-d H:i:s"));

    require_once 'custom/modules/RevenueLineItems/clients/base/api/GetCancellation.php';
    $report_api = new GetCancellation();

    $yesterdate = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
    $result = $report_api->getReport(null, array(
        'from' => $yesterdate,
        'to' => $yesterdate
    ));

    $mail_subject = "[{$yesterdate}] RLI Cancellation Report";
    $mail_body = getMailbody($yesterdate, $result['numrow']);
    $GLOBALS['log']->fatal($mail_body);

    $mailer = MailerFactory::getSystemDefaultMailer();
    $mailer->setSubject($mail_subject);
    $mailer->setHtmlBody($mail_body);
    $mailer->addRecipientsTo(new EmailIdentity('bpark@homewater.com', 'Bruce Park'));
    //$mailer->addRecipientsTo(new EmailIdentity('hmukri@homewater.com', 'Huzaifa Mukri'));
    //$mailer->addRecipientsTo(new EmailIdentity('chaznedaroglu@homewater.com', 'Can Haznedaroglu'));
    //$mailer->addRecipientsTo(new EmailIdentity('tburgess@homewater.com', 'Todd Burgess'));
    //$mailer->addRecipientsTo(new EmailIdentity('jduthie@homewater.com', 'Jason Duthie'));
    $mailer->send();

    $GLOBALS['log']->fatal('Ending RLICancelReport, '.date("Y-m-d H:i:s"));

    return true;
}

function getMailbody($date, $numcancel)
{
    global $sugar_config;

    $mail_body = "
<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Title</title>
    <link href=\"https://fonts.googleapis.com/css?family=PT+Sans\" rel=\"stylesheet\">
    <style>
        body {
            font-family: 'PT Sans';
        }

        .center {
            margin: auto;
        }

        table {
            margin: 40px 0px;
            width: 100%;
            border: 1px dashed #000000;
            border-collapse: collapse;
            height: 80px;
        }

        table thead th {
            vertical-align: middle;
            height: 30px;
            background-color: #7892c2;
        }

        .button {
            -moz-box-shadow: 0px 0px 0px 2px #9fb4f2;
            -webkit-box-shadow: 0px 0px 0px 2px #9fb4f2;
            box-shadow: 0px 0px 0px 2px #9fb4f2;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7892c2), color-stop(1, #476e9e));
            background: -moz-linear-gradient(top, #7892c2 5%, #476e9e 100%);
            background: -webkit-linear-gradient(top, #7892c2 5%, #476e9e 100%);
            background: -o-linear-gradient(top, #7892c2 5%, #476e9e 100%);
            background: -ms-linear-gradient(top, #7892c2 5%, #476e9e 100%);
            background: linear-gradient(to bottom, #7892c2 5%, #476e9e 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7892c2', endColorstr='#476e9e', GradientType=0);
            background-color: #7892c2;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 15px;
            border: 1px solid #4e6096;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-size: 20px;
            padding: 8px 20px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #283966;
        }

        .button:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #476e9e), color-stop(1, #7892c2));
            background: -moz-linear-gradient(top, #476e9e 5%, #7892c2 100%);
            background: -webkit-linear-gradient(top, #476e9e 5%, #7892c2 100%);
            background: -o-linear-gradient(top, #476e9e 5%, #7892c2 100%);
            background: -ms-linear-gradient(top, #476e9e 5%, #7892c2 100%);
            background: linear-gradient(to bottom, #476e9e 5%, #7892c2 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#476e9e', endColorstr='#7892c2', GradientType=0);
            background-color: #476e9e;
        }

        .button:active {
            position: relative;
            top: 1px;
        }
    </style>
</head>
<body>
<div class=\"center\" style=\"width: 500px; text-align: center;\">
    <h1>RLI Cancellation Report</h1>
    <table>
        <thead>
        <tr>
            <th>Date</th>
            <th>Cancelled RLIs</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>{$date}</b></td>
            <td><b>{$numcancel}</b></td>
        </tr>
        </tbody>
    </table>
    <div style=\"width: 100%; margin-bottom: 50px;\">
        <a href=\"{$sugar_config['site_url']}/ext_popup/RevenueLineItems/rlicancelreport.html\" target=\"_blank\" class=\"button\" style=\"padding: 5px 40px;\">Show details</a>
    </div>
</div>
</body>
</html>";

    return $mail_body;
}