<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-02
 * Time: 오전 9:31
 */

array_push($job_strings, 'RLIUpdateReport');

function RLIUpdateReport()
{
    require_once 'custom/Extension/modules/Schedulers/Ext/ScheduledTaskExtension/RLIUpdateReport.php';

    $rliupdatedreport = new RLIUpdateReport();
    $rliupdatedreport->run();
}