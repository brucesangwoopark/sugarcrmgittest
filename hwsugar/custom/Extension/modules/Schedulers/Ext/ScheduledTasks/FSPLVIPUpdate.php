<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-30
 * Time: 오후 2:09
 */

array_push($job_strings, 'FSPLVIPUpdate');

function FSPLVIPUpdate()
{
    $GLOBALS['log']->fatal("VIP Filter Service Plans batch is ready to run!!! " . date('Y-m-d H:i:s'));

    $current_date = date('Y-m-d');
    $weekago_date = date('Y-m-d', strtotime("{$current_date} -1 week"));
    //$current_date = '2018-09-01';

    require_once 'include/SugarQuery/SugarQuery.php';

    // get Filter Service Plans
    $sugarQuery = new SugarQuery();
    $sugarQuery->from(BeanFactory::newBean('FSPG2_FilterServicePlans'));
    $sugarQuery->joinTable('fspg2_filterservicepackages', array('alias' => 'fspg'))->on()->equalsField('fspg2_filterserviceplans.fspg2_filterservicepackages_id', 'fspg.id');
    $sugarQuery->where()
        ->queryAnd()
        ->equals('isactive', '1')
        ->lte('nextservicedate', $current_date)
        ->gte('nextservicedate', $weekago_date)
        ->equals('fspg.isvip', '1');
    $query = $sugarQuery->compileSql();
    $fspl_rows = $sugarQuery->execute();

    foreach ($fspl_rows as $fspl_row) {
        $fspl_id = $fspl_row['id'];

        $GLOBALS['log']->fatal("FSPL ID - {$fspl_id} found " . date('Y-m-d H:i:s'));

        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean('RevenueLineItems'));
        $sugarQuery->where()->equals('fspg2_filterserviceplans_id', $fspl_id);
        $query = $sugarQuery->compileSql();
        $rli_rows = $sugarQuery->execute();
        if (count($rli_rows) <= 0) {
            $GLOBALS['log']->fatal("FSPL ID - {$fspl_id} has no RLIs. " . date('Y-m-d H:i:s'));
            continue;
        }

        $rli_row = $rli_rows[0];
        $opportunity_id = $rli_row['opportunity_id'];
        $job_name = "{$fspl_row['name']} - Job";
        $account_id = $rli_row['account_id'];

        $GLOBALS['log']->fatal("FSPL ID - {$fspl_id} => Job creating " . date('Y-m-d H:i:s'));

        $job_bean = BeanFactory::newBean('abcde_Jobs');
        $job_bean->opportunity_id_c = $opportunity_id;
        $job_bean->name = $job_name;
        $job_bean->from_conversion_c = 'true';
        $job_bean->num_items_c = count($rli_rows);
        $job_bean->job_type_c = 'Service';
        $job_bean->service_type_c = 'Prefilter Change';
        $job_bean->description = 'Filter Service Plan';
        $job_bean->save();
        $job_bean->load_relationship('accounts_abcde_jobs_1');
        $job_bean->accounts_abcde_jobs_1->add($account_id);
        $job_bean->load_relationship('abcde_jobs_revenuelineitems_1');
        foreach ($rli_rows as $rli_row) {
            $job_bean->abcde_jobs_revenuelineitems_1->add($rli_row['id']);
        }
        $job_id = $job_bean->save();

        createDetail('[Auto] Filter Service Job created', null, $job_id, $fspl_id, $current_date);
        $fspl_bean = BeanFactory::getBean('FSPG2_FilterServicePlans', $fspl_id);
        $fspl_bean->lastservicedate = $current_date;
        $fspl_bean->save();
    }

    $GLOBALS['log']->fatal("VIP Filter Service Plans batch is done!!! " . date('Y-m-d H:i:s'));
}

function createDetail($name, $description, $job_id, $fspl_id, $filterchangedate)
{
    $fsd = BeanFactory::newBean('FSPG2_FilterServiceDetails');
    $fsd->name = $name;
    $fsd->description = $description;
    $fsd->abcde_jobs_id = $job_id;
    $fsd->fspg2_filterserviceplans_id = $fspl_id;
    $fsd->filterchangedate = $filterchangedate;
    $fsd->save();
}