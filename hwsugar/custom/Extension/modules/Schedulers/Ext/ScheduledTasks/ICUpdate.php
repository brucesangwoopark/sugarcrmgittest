<?php

array_push($job_strings, 'ICUpdate');

    function ICUpdate()
    {
      $GLOBALS['log']->fatal('Beginning IC Update, '.date("Y-m-d H:i:s"));
      
      $id_num_c = array();
      $first_name = array();
      $last_name = array();
      $phone_work = array();
      $office_c = array();
      $is_active_c = array();
      
      $query = 'SELECT ic.ICID as id_num_c,
                  ic.FIRSTNAME as first_name,
                  ic.LASTNAME as last_name,
                  ic.PHONE as phone_work,
                  ic.office as office_c,
                  ic.isActive1 as is_active_c
                FROM hwsugar.comm_ICgooglesheets ic
                WHERE ic.ICID NOT IN(
                  SELECT suc.id_num_c 
                    FROM supkg_systemusers_cstm suc
                    JOIN supkg_systemusers su ON su.id = suc.id_c
                    WHERE suc.usertype_c = "Agent"
                    AND su.deleted = ""
                    AND suc.id_num_c <> ""
                    )
                    AND ic.FIRSTNAME <> ""
                    AND ic.LASTNAME <> ""';
                    
      $result = $GLOBALS['db']->query($query);
      while($row = $GLOBALS['db']->fetchByAssoc($result)){
         $id_num_c[] = $row['id_num_c'];
         $first_name[] = $row['first_name'];
         $last_name[] = $row['last_name'];
         $phone_work[] = $row['phone_work'];
         $office_c[] = $row['office_c'];
         $is_active_c[] = $row['is_active_c'];
      }
      
      for ($i = 0; $i < sizeof($id_num_c); $i++){
        #$GLOBALS['log']->fatal('Creating Agent with ID: '.$id_num_c[$i].' '.date("Y-m-d H:i:s"));
        $system_user = BeanFactory::newBean('supkg_SystemUsers');
        $system_user->id_num_c = $id_num_c[$i];
        $system_user->usertype_c = "Agent";
        $system_user->first_name = $first_name[$i];
        $system_user->last_name = $last_name[$i];
        $system_user->phone_work = $phone_work[$i];
        $system_user->office_c = $office_c[$i];
        $system_user->is_active_c = $is_active_c[$i];
        $system_user->save();
      }
      
      unset($id_num_c);
      unset($first_name);
      unset($last_name);
      unset($phone_work);
      unset($office_c);
      unset($is_active_c);
      
      //
      //Running Peter's Queries
      //
      //#Append new Overrides
      $query = "insert into hwsugar.comm_Overrides (IDs,ICID,position,overrideagentid,startdate)
                Select uuid() uuid,
                nicg.ICID,
                cps.positionID, 
                case when cps.positionID=1 then nicg.ICID
                   when cps.positionID=2 and nicg.CREWMGR <> '' then nicg.CREWMGR
                     when cps.positionID=3 and nicg.ASSTCREW <> '' then nicg.ASSTCREW
                     else cps.ICID
                     end as overrideagentid
                     ,
                case when nicg.DATEACTIVATED ='' then date_add(now(),INTERVAL -8 day)
                when STR_TO_DATE(nicg.DATEACTIVATED ,'%m/%d/%Y') is null then date_add(now(),INTERVAL -8 day)
                else cast(STR_TO_DATE(nicg.DATEACTIVATED ,'%m/%d/%Y') as datetime) end startdate 
                from
                ##List of unique IDs together with googlesheets data that are in supkg table but are not in overrides table
                (Select * from
                #List of unique IDs that are in supkg table but are not in overrides table
                (SELECT ssc.id_num_c FROM hwsugar.supkg_systemusers ss
                join hwsugar.supkg_systemusers_cstm ssc on ssc.id_c=ss.id
                left join hwsugar.comm_Overrides co on co.ICID=ssc.id_num_c
                where co.ICID is null
                and ssc.usertype_c='Agent'
                and ss.deleted=0
                and ssc.id_num_c is not null
                and trim(ssc.id_num_c) <>''
                group by ssc.id_num_c) nic
                join hwsugar.comm_ICgooglesheets cIG on nic.id_num_c=cIG.ICID) nicg
                join hwsugar.comm_positionstandard cps on cps.office=nicg.office";
      $GLOBALS['db']->query($query);
                
      //##UPDATE Crew Leader
      $query = "Insert into hwsugar.comm_Overrides
                Select uuid() uuid,
                nicg.ICID,
                cps.positionID,
                case when cps.positionID=2 and nicg.CREWMGR <> '' then nicg.CREWMGR
                     else cps.ICID
                     end as overrideagentid,
                     #when deciding on where the override begins and where it ends, the end date of the old one should be at the time when update is made aka recordstamp,
                     #but, if we assume that the old startdate is before the recordstamp, we might run into trouble if that's not true because the time difference would be negative, so we take that into account
                     case when nicg.oldstartdate<nicg.recordstamp then nicg.recordstamp
                     else date_add(nicg.oldstartdate, interval 1 second) end startdate,
                     '1993-06-06 09:30:00' enddate
                from
                hwsugar.comm_positionstandard cps join
                (Select cIG.*,co1.startdate as oldstartdate from
                #all ICIDs of agents who's Crew override is different on ICgoogle sheet than in Overrides table
                (select icg.ICID from hwsugar.comm_ICgooglesheets icg
                join hwsugar.comm_Overrides co on co.ICID =icg.ICID
                join hwsugar.comm_positionstandard cps on cps.office=icg.office
                where co.position=2
                and co.overrideagentid <> case when icg.CREWMGR <> '' then icg.CREWMGR else cps.ICID end
                and co.enddate is null
                and cps.positionID=2
                group by icg.ICID) wo
                join hwsugar.comm_ICgooglesheets cIG on wo.ICID=cIG.ICID
                left join hwsugar.comm_Overrides co1 on co1.icid=cIG.icid
                where co1.position=2
                and co1.enddate is null) nicg on cps.office=nicg.office
                where
                cps.positionID=2";
      $GLOBALS['db']->query($query);
                
      $query = "Update hwsugar.comm_Overrides as co2
                inner join (select co.IDs,icg.recordstamp from hwsugar.comm_ICgooglesheets icg
                join hwsugar.comm_Overrides co on co.ICID =icg.ICID
                join hwsugar.comm_positionstandard cps on cps.office=icg.office
                where co.position=2
                and co.overrideagentid <> case when icg.CREWMGR <> '' then icg.CREWMGR else cps.ICID end
                and co.enddate is null
                and cps.positionID=2
                group by co.IDs) d on d.IDs=co2.IDs
                set enddate = case when startdate<d.recordstamp then d.recordstamp else date_add(startdate, interval 1 second) end
                where enddate is null
                and position=2";
      $GLOBALS['db']->query($query);
      
      $query = "Update hwsugar.comm_Overrides as co2
                set enddate=NULL
                where enddate='1993-06-06 09:30:00'";
      $GLOBALS['db']->query($query);
                
      //##UPDATE Assistant Crew
      $query = "Insert into hwsugar.comm_Overrides
                Select uuid() uuid,
                nicg.ICID,
                cps.positionID,
                case when cps.positionID=3 and nicg.ASSTCREW <> '' then nicg.ASSTCREW
                     else cps.ICID
                     end as overrideagentid,
                   case when nicg.oldstartdate<nicg.recordstamp then nicg.recordstamp
                     else date_add(nicg.oldstartdate, interval 1 second) end startdate,
                     '1993-06-06 09:30:00' enddate
                from
                hwsugar.comm_positionstandard cps join
                (Select cIG.*,co1.startdate as oldstartdate from
                #all ICIDs of agents who's Crew override is different on ICgoogle sheet than in Overrides table
                (select icg.ICID from hwsugar.comm_ICgooglesheets icg
                join hwsugar.comm_Overrides co on co.ICID =icg.ICID
                join hwsugar.comm_positionstandard cps on cps.office=icg.office
                where co.position=3
                and co.overrideagentid <> case when icg.ASSTCREW <> '' then icg.ASSTCREW else cps.ICID end
                and co.enddate is null
                and cps.positionID=3
                group by icg.ICID) wo
                join hwsugar.comm_ICgooglesheets cIG on wo.ICID=cIG.ICID
                left join hwsugar.comm_Overrides co1 on co1.icid=cIG.icid
                where co1.position=3
                and co1.enddate is null) nicg on cps.office=nicg.office
                where
                cps.positionID=3";
      $GLOBALS['db']->query($query);          
                
      $query = "Update hwsugar.comm_Overrides as co2
                inner join (select co.IDs,icg.recordstamp from hwsugar.comm_ICgooglesheets icg
                join hwsugar.comm_Overrides co on co.ICID =icg.ICID
                join hwsugar.comm_positionstandard cps on cps.office=icg.office
                where co.position=3
                and co.overrideagentid <> case when icg.ASSTCREW <> '' then icg.ASSTCREW else cps.ICID end
                and co.enddate is null
                and cps.positionID=3
                group by co.IDs) d on d.IDs=co2.IDs
                set enddate = case when startdate<d.recordstamp then d.recordstamp else date_add(startdate, interval 1 second) end
                where enddate is null
                and position=3";
      $GLOBALS['db']->query($query);
                
      $query = "Update hwsugar.comm_Overrides as co2
                set enddate=NULL
                where enddate='1993-06-06 09:30:00'";
      $GLOBALS['db']->query($query);
      //
      //END OF Peter's Queries
      //
      
      $GLOBALS['log']->fatal('Ending IC Update, '.date("Y-m-d H:i:s")); 
      return true;
    }

?>