<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-09-08
 * Time: 오전 11:29
 */

array_push($job_strings, 'AnnualPriceIncrease');

function AnnualPriceIncrease()
{
    require_once 'custom/Extension/modules/Schedulers/Ext/ScheduledTaskExtension/AnnualPriceIncrease.php';

    $priceincrease = new AnnualPriceIncrease();
    $priceincrease->run();
}