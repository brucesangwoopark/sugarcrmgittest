<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-09-07
 * Time: 오후 1:18
 */

class AnnualPriceIncrease
{
    function run()
    {
        $GLOBALS['log']->fatal('RLI annual price increase batch is ready to go!!! ' . date('Y-m-d H:i:s'));

        $report = '';
        $sugar_rlis = $this->getRLIs();
        foreach ($sugar_rlis as $rli) {
            $opp_id = $rli['opps_id'];
            $acc_id = $rli['accs_id'];

            $country_code = $this->getCountry($acc_id);
            $co_package_id = $rli['opps_sub_id'];

            $GLOBALS['log']->fatal("Opp: {$opp_id} on board, CO Sub: {$co_package_id} " . date('Y-m-d H:i:s'));

            $co_lineitems = $this->getCoLineItems($co_package_id, $country_code);
            if ($co_lineitems === null || count($co_lineitems) === 0) {
                $report .= "Opp: {$opp_id} - {$co_package_id}, Country code: {$country_code} | Error or no line items on this subscription\n";
                $GLOBALS['log']->fatal("Opp: {$opp_id}, CO Sub: {$co_package_id} doesn't have line items. " . date('Y-m-d H:i:s'));
                continue;
            }

            $rli_id = $rli['rlis_id'];
            $rli_name = $rli['rlis_name'];
            $rli_price = floatval($rli['rlis_price']);
            $rli_inc_rate = floatval($rli['rlis_priceincreaserate']);
            $co_item_price = floatval($co_lineitems[$rli_id]->tierset->base);
            $co_line_item_id = $co_lineitems[$rli_id]->line_item_id;
            $co_item_name = $co_lineitems[$rli_id]->item_name;

            if (isset($co_lineitems[$rli_id])) {
                $GLOBALS['log']->fatal("RLI ({$rli_id}) and CO Line Item ({$co_line_item_id}) " . date('Y-m-d H:i:s'));

                if ($rli_price === $co_item_price) { // both prices should be exactly the same
                    $new_price = round($rli_price * ($rli_inc_rate / 100 + 1), 2);

                    if ($this->upgradeCoPrice($co_package_id, $country_code, $new_price, $co_line_item_id)) {
                        $this->updateRLIPrice($rli_price, $new_price, $rli_id);

                        $note = "{$co_item_name} ({$co_line_item_id}) - annual price increase \${$rli_price} to \${$new_price}\n";
                        $this->createCoNote($co_package_id, $country_code, $note);
                        $this->createSugarNotes($acc_id, $opp_id, $rli_id, "[Auto] Annual Price Increase - RLI: {$rli_name}", $note);

                        $GLOBALS['log']->fatal("RLI ({$rli_id}) and CO Line Item ({$co_line_item_id}) \${$rli_price} -> \${$new_price} rate: {$rli_inc_rate} " . date('Y-m-d H:i:s'));
                    }
                } else {
                    $report .= "RLI: {$rli_id}, CO Sub: {$co_package_id} - [{$co_item_name}] | Prices are not matched RLI-> \${$rli_price} CO Line item-> \${$co_item_price}\n";
                }
            } else {
                $report .= "RLI: {$rli_id}, CO Sub: {$co_package_id} | This RLI doesn't exist in subscription.\n";
            }
        }

        if ($report !== '') {
            $dir = './priceincrease/';
            if (!is_dir($dir)) {
                mkdir($dir);
            }

            $file_name = 'error_report_' . date('Y-m-d_H;i;s');
            file_put_contents($dir . $file_name, $report);
        }

        $GLOBALS['log']->fatal('RLI annual price increase batch is done!!! ' . date('Y-m-d H:i:s'));
    }

    function createSugarNotes($account_id, $opportunity_id, $rli_id, $name, $description)
    {
        $note = BeanFactory::newBean('Notes');

        $note->name = $name;
        $note->description = $description;
        $note->parent_type = 'Accounts';
        $note->parent_id = $account_id;

        $note->save();

        $note = BeanFactory::newBean('Notes');

        $note->name = $name;
        $note->description = $description;
        $note->parent_type = 'Opportunities';
        $note->parent_id = $opportunity_id;

        $note->save();

        $note = BeanFactory::newBean('Notes');

        $note->name = $name;
        $note->description = $description;
        $note->parent_type = 'RevenueLineItems';
        $note->parent_id = $rli_id;

        $note->save();
    }

    function getRLIs()
    {
        $query = "
            select 
              rlis.name as rlis_name, 
              opps.id as opps_id, 
              opps.name as opps_name, 
              oppsc.external_key_c as opps_sub_id, 
              rlis.account_id as accs_id, 
              rlis.id as rlis_id, 
              rlis.cost_price as rlis_price, 
              rlisc.priceincreaserate_c as rlis_priceincreaserate, 
              rlisc.lastpriceincreasedate_c as rlisc_lastpriceincreasedate
            from revenue_line_items_cstm rlisc
            left join revenue_line_items rlis on rlis.id = rlisc.id_c
            left join opportunities_cstm oppsc on rlis.opportunity_id = oppsc.id_c
            left join opportunities opps on opps.id = oppsc.id_c
            where 
              rlis.deleted = '0' and 
              rlis.sales_stage = 'Installed' and 
              year(rlisc.lastpriceincreasedate_c) < year(now()) and 
              month(rlisc.lastpriceincreasedate_c) = month(now());
            ";

        $rlis_rows = array();
        $result = $GLOBALS['db']->query($query);
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $rlis_rows[] = $row;
        }

        return $rlis_rows;
    }

    function createCoApi($country_code)
    {
        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$country_code]['username'],
            $sugar_config['chargeover'][$country_code]['password']
        );

        return $api;
    }

    function getCountry($account_id)
    {
        $account_bean = BeanFactory::getBean('Accounts', $account_id);

        if ($account_bean->billing_address_country === 'Canada') {
            return 'canada';
        }

        if ($account_bean->billing_address_country === 'United States') {
            return 'us';
        }

        return null;
    }

    function getCoLineItems($subscription_id, $country_code)
    {
        $api = $this->createCoApi($country_code);
        $response = $api->findById(ChargeOverAPI_Object::TYPE_PACKAGE, $subscription_id);
        $GLOBALS['log']->fatal('getCoLineItems CO response => ' . print_r($response, true) . ' ' . date('Y-m-d H:i:s'));

        if (200 <= $response->code && $response->code < 300) {
            $line_items = array();
            foreach ($response->response->line_items as $line_item) {
                $line_items[$line_item->external_key] = $line_item;
            }

            return $line_items;
        }

        return null;
    }

    function upgradeCoPrice($subscription_id, $country_code, $new_price, $line_item_id)
    {
        $api = $this->createCoApi($country_code);
        $req_body = array(
            'line_items' => array(
                array(
                    'line_item_id' => $line_item_id,
                    'tierset' => array(
                        'base' => $new_price,
                        'pricemodel' => 'fla',
                    )
                )
            )
        );

        $response = $api->action(ChargeOverAPI_Object::TYPE_PACKAGE, $subscription_id, 'upgrade', $req_body);
        $GLOBALS['log']->fatal('upgradeCoPrice response => ' . print_r($response, true) . ' ' . date('Y-m-d H:i:s'));
        if (200 <= $response->code && $response->code < 300) {
            return true;
        }

        return false;
    }

    function createCoNote($subscription_id, $country_code, $note_content)
    {
        $api = $this->createCoApi($country_code);
        $note = new ChargeOverAPI_Object_Note(array(
            'note' => $note_content,
            'obj_type' => ChargeOverAPI_Object::TYPE_PACKAGE,
            'obj_id' => $subscription_id
        ));

        $response = $api->create($note);
        $GLOBALS['log']->fatal('createCoNote response => ' . print_r($response, true) . ' ' . date('Y-m-d H:i:s'));
    }

    function updateRLIPrice($old_price, $new_price, $rli_id)
    {
        $current_date = date('Y-m-d');

        $query = "
            update revenue_line_items
            set 
              cost_price = {$new_price}
            where id = '{$rli_id}';
        ";

        $GLOBALS['db']->query($query);

        $query = "
            update revenue_line_items_cstm
            set 
              previousprice_c = {$old_price},
              lastpriceincreasedate_c = '{$current_date}'
            where id_c = '{$rli_id}';
        ";

        $GLOBALS['db']->query($query);
    }
}