<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-02
 * Time: 오전 9:33
 */

class RLIUpdateReport
{
    function run()
    {
        $GLOBALS['log']->fatal('Running RLI Updated Report is ready to go!!! ' . date('Y-m-d H:i:s'));

        $query = '
            insert into rli_update_report
            select a.new_rlis_id, a.what_updated, a.old_value, a.new_value, now()
            from (
            select 
                new_rlis.id new_rlis_id, 
                case when new_rlis.deleted != old_rlis.deleted then \'deleted\'
                when new_rlis.sales_stage != old_rlis.sales_stage then \'sales_stage\'
                when old_rlis.sales_stage is null then \'new\' end as what_updated,
                case when new_rlis.deleted != old_rlis.deleted then old_rlis.deleted
                when new_rlis.sales_stage != old_rlis.sales_stage then old_rlis.sales_stage end as old_value,
                case when new_rlis.deleted != old_rlis.deleted then new_rlis.deleted
                when new_rlis.sales_stage != old_rlis.sales_stage then new_rlis.sales_stage end as new_value
            from revenue_line_items new_rlis
            left join rli_compare old_rlis on new_rlis.id = old_rlis.id) a
            where a.what_updated is not null;
        ';
        $GLOBALS['db']->query($query);
        $GLOBALS['log']->fatal('rli_update_report extended ' . date('Y-m-d H:i:s'));

        $query = '
            delete
            from rli_compare;
        ';
        $GLOBALS['db']->query($query);
        $GLOBALS['log']->fatal('rli_compare deleted ' . date('Y-m-d H:i:s'));

        $query = '
            insert into rli_compare
            select id, sales_stage, deleted
            from revenue_line_items;
        ';
        $GLOBALS['db']->query($query);
        $GLOBALS['log']->fatal('rli_compare reset ' . date('Y-m-d H:i:s'));

        $GLOBALS['log']->fatal('RLI Updated Report is created!!! ' . date('Y-m-d H:i:s'));
    }

}