<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_OLD_ACCOUNT_ID_ACCOUNT_ID'] = 'old account id (related Account ID)';
$mod_strings['LBL_NEW_ACCOUNT_ID_ACCOUNT_ID'] = 'new account id (related Account ID)';
$mod_strings['LBL_TRANSFER_COMPLETE'] = 'transfer complete';
$mod_strings['LBL_NEW_ADDRESS_CITY'] = 'new address city';
$mod_strings['LBL_NEW_ADDRESS_COUNTRY'] = 'new address country';
$mod_strings['LBL_NEW_ADDRESS_POSTALCODE'] = 'new address postalcode';
$mod_strings['LBL_NEW_ADDRESS_STATE'] = 'new address state';
$mod_strings['LBL_NEW_ADDRESS_STREET'] = 'new address street';
$mod_strings['LBL_NEW_PHONE'] = 'new phone';
$mod_strings['LBL_TRANSFER_OWNERSHIP'] = 'transfer ownership';
$mod_strings['LBL_RECORDVIEW_PANEL2'] = 'Customer&#039;s Updated Address';
$mod_strings['LBL_RECORDVIEW_PANEL1'] = 'New Customer';
$mod_strings['LBL_CURRENCY'] = 'LBL_CURRENCY';
$mod_strings['LBL_TRANSFER_FEE'] = 'transfer fee';
$mod_strings['LBL_RENTAL_APPLIED'] = 'rental applied';
$mod_strings['LBL_COMPARTMENT_NUMBER'] = 'Unit #';
$mod_strings['LBL_NEW_ENBRIDGE_NUMBER'] = 'New Enbridge #';
