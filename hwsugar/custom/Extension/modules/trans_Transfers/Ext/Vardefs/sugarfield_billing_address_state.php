<?php
 // created: 2017-02-15 12:29:54
$dictionary['trans_Transfers']['fields']['billing_address_state']['dependency']='equal($transfer_ownership_c,"1")';
$dictionary['trans_Transfers']['fields']['billing_address_state']['type']='enum';
$dictionary['trans_Transfers']['fields']['billing_address_state']['options']='state_list';
$dictionary['trans_Transfers']['fields']['billing_address_state']['default']='ON';
$dictionary['trans_Transfers']['fields']['billing_address_state']['len']=100;
$dictionary['trans_Transfers']['fields']['billing_address_state']['full_text_search']=array (
);

 ?>