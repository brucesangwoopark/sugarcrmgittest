<?php
 // created: 2017-02-15 09:44:37
$dictionary['trans_Transfers']['fields']['new_address_state_c']['labelValue']='new address state';
$dictionary['trans_Transfers']['fields']['new_address_state_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_address_state_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_address_state_c']['dependency']='not(equal($transfer_ownership_c,"1"))';
$dictionary['trans_Transfers']['fields']['new_address_state_c']['type']='enum';
$dictionary['trans_Transfers']['fields']['new_address_state_c']['options']='state_list';
 ?>
