<?php
 // created: 2017-06-30 09:15:54
$dictionary['trans_Transfers']['fields']['compartment_number_c']['labelValue']='Unit #';
$dictionary['trans_Transfers']['fields']['compartment_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['compartment_number_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['compartment_number_c']['dependency']='not(equal($transfer_ownership_c,"1"))';

 ?>