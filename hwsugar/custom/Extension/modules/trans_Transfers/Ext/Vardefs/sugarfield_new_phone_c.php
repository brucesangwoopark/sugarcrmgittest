<?php
 // created: 2017-02-15 09:45:01
$dictionary['trans_Transfers']['fields']['new_phone_c']['labelValue']='new phone';
$dictionary['trans_Transfers']['fields']['new_phone_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_phone_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_phone_c']['dependency']='not(equal($transfer_ownership_c,"1"))';

 ?>