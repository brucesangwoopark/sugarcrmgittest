<?php
 // created: 2017-01-16 16:31:57
$dictionary['trans_Transfers']['fields']['name']['default']='Do not fill in - will be filled automatically';
$dictionary['trans_Transfers']['fields']['name']['len']='255';
$dictionary['trans_Transfers']['fields']['name']['audited']=false;
$dictionary['trans_Transfers']['fields']['name']['massupdate']=false;
$dictionary['trans_Transfers']['fields']['name']['unified_search']=false;
$dictionary['trans_Transfers']['fields']['name']['full_text_search']=array (
  'enabled' => true,
  'boost' => '1.55',
  'searchable' => true,
);
$dictionary['trans_Transfers']['fields']['name']['calculated']=false;

 ?>