<?php
 // created: 2017-02-15 12:29:29
$dictionary['trans_Transfers']['fields']['billing_address_country']['dependency']='equal($transfer_ownership_c,"1")';
$dictionary['trans_Transfers']['fields']['billing_address_country']['type']='enum';
$dictionary['trans_Transfers']['fields']['billing_address_country']['options']='country_list';
$dictionary['trans_Transfers']['fields']['billing_address_country']['default']='Canada';
$dictionary['trans_Transfers']['fields']['billing_address_country']['len']=100;
$dictionary['trans_Transfers']['fields']['billing_address_country']['full_text_search']=array (
);

 ?>