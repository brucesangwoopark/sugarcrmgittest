<?php
 // created: 2017-07-05 09:11:06
$dictionary['trans_Transfers']['fields']['new_enbridge_number_c']['labelValue']='New Enbridge #';
$dictionary['trans_Transfers']['fields']['new_enbridge_number_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_enbridge_number_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_enbridge_number_c']['dependency']='not(equal($transfer_ownership_c,"1"))';

 ?>