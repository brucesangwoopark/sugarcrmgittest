<?php
 // created: 2017-02-15 09:43:59
$dictionary['trans_Transfers']['fields']['new_address_city_c']['labelValue']='new address city';
$dictionary['trans_Transfers']['fields']['new_address_city_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_address_city_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_address_city_c']['dependency']='not(equal($transfer_ownership_c,"1"))';

 ?>