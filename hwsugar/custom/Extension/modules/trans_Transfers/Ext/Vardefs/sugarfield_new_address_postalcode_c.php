<?php
 // created: 2017-02-15 09:44:20
$dictionary['trans_Transfers']['fields']['new_address_postalcode_c']['labelValue']='new address postalcode';
$dictionary['trans_Transfers']['fields']['new_address_postalcode_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_address_postalcode_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_address_postalcode_c']['dependency']='not(equal($transfer_ownership_c,"1"))';

 ?>