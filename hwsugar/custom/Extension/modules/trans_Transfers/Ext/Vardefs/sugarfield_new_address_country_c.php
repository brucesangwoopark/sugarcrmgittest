<?php
 // created: 2017-02-15 09:44:10
$dictionary['trans_Transfers']['fields']['new_address_country_c']['labelValue']='new address country';
$dictionary['trans_Transfers']['fields']['new_address_country_c']['full_text_search']=array (
  'enabled' => '0',
  'boost' => '1',
  'searchable' => false,
);
$dictionary['trans_Transfers']['fields']['new_address_country_c']['enforced']='';
$dictionary['trans_Transfers']['fields']['new_address_country_c']['dependency']='not(equal($transfer_ownership_c,"1"))';
$dictionary['trans_Transfers']['fields']['new_address_country_c']['type']='enum';
$dictionary['trans_Transfers']['fields']['new_address_country_c']['options']='country_list';
 ?>
