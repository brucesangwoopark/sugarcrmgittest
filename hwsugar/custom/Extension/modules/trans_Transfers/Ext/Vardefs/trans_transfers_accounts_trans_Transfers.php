<?php
// created: 2016-12-28 12:31:37
$dictionary["trans_Transfers"]["fields"]["trans_transfers_accounts"] = array (
  'name' => 'trans_transfers_accounts',
  'type' => 'link',
  'relationship' => 'trans_transfers_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_TRANS_TRANSFERS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'trans_transfers_accountsaccounts_idb',
);
