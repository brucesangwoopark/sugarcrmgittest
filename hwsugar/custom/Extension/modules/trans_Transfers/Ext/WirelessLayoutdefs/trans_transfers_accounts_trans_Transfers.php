<?php
 // created: 2016-12-28 12:31:37
$layout_defs["trans_Transfers"]["subpanel_setup"]['trans_transfers_accounts'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_TRANS_TRANSFERS_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'trans_transfers_accounts',
);
