<?php
 // created: 2016-09-23 14:47:30
$layout_defs["ProductTemplates"]["subpanel_setup"]['lipkg_lead_line_items_producttemplates'] = array (
  'order' => 100,
  'module' => 'lipkg_lead_line_items',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_LIPKG_LEAD_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'lipkg_lead_line_items_producttemplates',
);
