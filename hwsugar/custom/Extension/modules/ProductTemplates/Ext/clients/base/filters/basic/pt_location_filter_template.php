<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-11-02
 * Time: 오후 11:01
 */

$viewdefs['ProductTemplates']['base']['filter']['basic']['filters'][] = array(
    'id' => 'pt_location_filter_template',
    'name' => 'LBL_PT_LOCATION_FILTER_TEMPLATE',
    'filter_definition' => array(
        array(
            'country_c' => array(
                '$in' => '',
            ),
        ),
        array(
            'active_c' => array(
                '$equals' => '',
            ),
        ),
    ),
    'editable' => true,
    'is_template' => true,
);