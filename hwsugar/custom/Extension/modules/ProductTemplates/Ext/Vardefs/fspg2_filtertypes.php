<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-12
 * Time: 오후 10:44
 */

$dictionary['ProductTemplate']['fields']['fspg2_filtertype_name'] = array(
    'required' => true,
    'source' => 'non-db',
    'name' => 'fspg2_filtertype_name',
    'vname' => 'LBL_FSPG2_FILTERTYPE_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'fspg2_filtertype_id',
    'join_name' => 'fspg2_filtertype',
    'link' => 'fspg2_filtertype',
    'table' => 'fspg2_filtertypes',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterTypes',
);

$dictionary['ProductTemplate']['fields']['fspg2_filtertype_id'] = array(
    'name' => 'fspg2_filtertype_id',
    'rname' => 'id',
    'vname' => 'LBL_FILTERTYPE_ID',
    'type' => 'id',
    'table' => 'fspg2_filtertypes',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterTypes',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);

$dictionary['ProductTemplate']['fields']['fspg2_filtertype'] = array(
    'name' => 'fspg2_filtertype',
    'type' => 'link',
    'relationship' => 'fspg2_filtertypes_producttemplates',
    'module' => 'FSPG2_FilterTypes',
    'bean_name' => 'FSPG2_FilterTypes',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERTYPE',
);

$dictionary['ProductTemplate']['relationships']['fspg2_filtertypes_producttemplates'] = array(
    'lhs_module' => 'FSPG2_FilterTypes',
    'lhs_table' => 'fspg2_filtertypes',
    'lhs_key' => 'id',
    'rhs_module' => 'ProductTemplates',
    'rhs_table' => 'product_templates',
    'rhs_key' => 'fspg2_filtertype_id',
    'relationship_type' => 'one-to-many',
);