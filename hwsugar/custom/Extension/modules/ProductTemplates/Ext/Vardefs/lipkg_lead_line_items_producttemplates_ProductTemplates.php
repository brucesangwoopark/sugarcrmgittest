<?php
// created: 2016-09-23 14:47:30
$dictionary["ProductTemplate"]["fields"]["lipkg_lead_line_items_producttemplates"] = array (
  'name' => 'lipkg_lead_line_items_producttemplates',
  'type' => 'link',
  'relationship' => 'lipkg_lead_line_items_producttemplates',
  'source' => 'non-db',
  'module' => 'lipkg_lead_line_items',
  'bean_name' => false,
  'vname' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_PRODUCTTEMPLATES_TITLE',
  'id_name' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  'link-type' => 'many',
  'side' => 'left',
);
