<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 9:53
 */

$dictionary['FSPG2_FilterTypes']['fields']['fspg2_filterservicepackages'] = array (
    'name' => 'fspg2_filterservicepackages',
    'type' => 'link',
    'relationship' => 'fspg2_filterservicepackages_fspg2_filtertypes',
    'module' => 'FSPG2_FilterServicePackages',
    'bean_name' => 'FSPG2_FilterServicePackages',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPACKAGES',
);