<?php
/**
 * Created by PhpStorm.
 * User: bpark
 * Date: 7/10/2017
 * Time: 3:41 PM
 */
$dictionary['FSPG2_FilterTypes']['fields']['producttemplates'] = array (
    'name' => 'producttemplates',
    'type' => 'link',
    'relationship' => 'fspg2_filtertypes_producttemplates',
    'module' => 'ProductTemplates',
    'bean_name' => 'ProductTemplates',
    'source' => 'non-db',
    'vname' => 'LBL_PRODUCTTEMPLATES',
);