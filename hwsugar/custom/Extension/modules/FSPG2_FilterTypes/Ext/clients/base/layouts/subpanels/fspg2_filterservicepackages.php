<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-17
 * Time: 오전 9:08
 */


$viewdefs['FSPG2_FilterTypes']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_SUBPANEL_FILTERSERVICEPACKAGES',
    'context' => array(
        'link' => 'fspg2_filterservicepackages',
    ),
);