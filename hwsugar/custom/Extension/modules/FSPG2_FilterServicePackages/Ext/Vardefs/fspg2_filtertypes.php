<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 9:15
 */

$dictionary['FSPG2_FilterServicePackages']['fields']['fspg2_filtertypes_name'] = array(
    'required' => true,
    'source' => 'non-db',
    'name' => 'fspg2_filtertypes_name',
    'vname' => 'LBL_FSPG2_FILTERTYPES_NAME',
    'type' => 'relate',
    'rname' => 'name',
    'id_name' => 'fspg2_filtertypes_id',
    'join_name' => 'fspg2_filtertypes',
    'link' => 'fspg2_filtertypes',
    'table' => 'fspg2_filtertypes',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterTypes',
);


$dictionary['FSPG2_FilterServicePackages']['fields']['fspg2_filtertypes_id'] = array(
    'name' => 'fspg2_filtertypes_id',
    'rname' => 'id',
    'vname' => 'LBL_FSPG2_FILTERTYPES_ID',
    'type' => 'id',
    'table' => 'fspg2_filtertypes',
    'isnull' => 'true',
    'module' => 'FSPG2_FilterTypes',
    'dbType' => 'id',
    'reportable' => false,
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
);


$dictionary['FSPG2_FilterServicePackages']['fields']['fspg2_filtertypes'] = array(
    'name' => 'fspg2_filtertypes',
    'type' => 'link',
    'relationship' => 'fspg2_filterservicepackages_fspg2_filtertypes',
    'module' => 'FSPG2_FilterTypes',
    'bean_name' => 'FSPG2_FilterTypes',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERTYPES',
);


$dictionary['FSPG2_FilterServicePackages']['relationships']['fspg2_filterservicepackages_fspg2_filtertypes'] = array(
    'lhs_module' => 'FSPG2_FilterTypes',
    'lhs_table' => 'fspg2_filtertypes',
    'lhs_key' => 'id',
    'rhs_module' => 'FSPG2_FilterServicePackages',
    'rhs_table' => 'fspg2_filterservicepackages',
    'rhs_key' => 'fspg2_filtertypes_id',
    'relationship_type' => 'one-to-many',
);