<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-13
 * Time: 오전 10:10
 */

$dictionary['FSPG2_FilterServicePackages']['fields']['fspg2_filterserviceplans'] = array (
    'name' => 'fspg2_filterserviceplans',
    'type' => 'link',
    'relationship' => 'fspg2_filterserviceplans_fspg2_filterservicepakcages',
    'module' => 'FSPG2_FilterServicePlans',
    'bean_name' => 'FSPG2_FilterServicePlans',
    'source' => 'non-db',
    'vname' => 'LBL_FSPG2_FILTERSERVICEPLANS',
);