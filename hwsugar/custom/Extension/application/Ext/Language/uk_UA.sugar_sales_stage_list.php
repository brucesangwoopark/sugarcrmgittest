<?php
 // created: 2017-03-13 17:19:52

$app_list_strings['sales_stage_list']=array (
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
  'Suspended' => 'Suspended',
  'Cancelled Installation' => 'Cancelled Installation',
  'Transferred' => 'Transferred',
  'Scheduled Installation' => 'Scheduled Installation',
  'CNI' => 'CNI',
  'Scheduled Uninstallation' => 'Scheduled Uninstallation',
);