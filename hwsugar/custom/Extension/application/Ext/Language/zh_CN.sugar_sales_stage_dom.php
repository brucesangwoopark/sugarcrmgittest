<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => '销售前景',
  'Qualification' => '资格',
  'Needs Analysis' => '需要分析',
  'Value Proposition' => '价值陈述',
  'Id. Decision Makers' => '辨识决策人',
  'Perception Analysis' => '看法的分析',
  'Proposal/Price Quote' => '建议 / 出价',
  'Negotiation/Review' => '谈判 / 评估',
  'Closed Won' => '谈成结束',
  'Closed Lost' => '丢单结束',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);