<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Kvalifikatsioon',
  'Needs Analysis' => 'Vajab analüüsi',
  'Value Proposition' => 'Väärtuse kinnitus',
  'Id. Decision Makers' => 'Id. Otsustajad',
  'Perception Analysis' => 'Perception Analysis',
  'Proposal/Price Quote' => 'Pakkumine/Hinnapakkumine',
  'Negotiation/Review' => 'Läbirääkimised/Ülevaade',
  'Closed Won' => 'Closed Won',
  'Closed Lost' => 'Suletud kaotused',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);