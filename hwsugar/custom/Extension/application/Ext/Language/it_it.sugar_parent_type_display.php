<?php
 // created: 2016-07-29 21:27:21

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Tasks' => 'Compito',
  'Opportunities' => 'Opportunità',
  'Products' => 'Prodotto',
  'Quotes' => 'Offerta',
  'Bugs' => 'Bug',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);