<?php
 // created: 2016-07-29 21:27:34

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Compte',
  'Contacts' => 'Contacte',
  'Tasks' => 'Tasca',
  'Opportunities' => 'Oportunitat',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidències',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Project' => 'Projecte',
  'ProjectTask' => 'Tasca de projecte',
  'Prospects' => 'Objectiu',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);