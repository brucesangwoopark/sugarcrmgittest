<?php
 // created: 2016-07-29 21:27:33

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contato',
  'Opportunities' => 'Oportunidade',
  'Tasks' => 'Tarefa',
  'ProductTemplates' => 'Catálogo de Produtos',
  'Quotes' => 'Cotação',
  'Products' => 'Item de Linha de Cotação',
  'Contracts' => 'Contrato',
  'Emails' => 'E-mail',
  'Bugs' => 'Bug',
  'Project' => 'Projeto',
  'ProjectTask' => 'Tarefa de Projeto',
  'Prospects' => 'Alvo',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Meetings' => 'Reunião',
  'Calls' => 'Chamada',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);