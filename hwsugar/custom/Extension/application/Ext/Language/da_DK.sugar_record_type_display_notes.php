<?php
 // created: 2016-07-29 21:27:15

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Salgsmulighed',
  'Tasks' => 'Opgave',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Tilbud',
  'Products' => 'Angiven linjepost',
  'Contracts' => 'Kontrakt',
  'Emails' => 'E-mail',
  'Bugs' => 'Fejl',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Meetings' => 'Møde',
  'Calls' => 'Opkald',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);