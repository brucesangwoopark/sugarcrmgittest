<?php
 // created: 2016-07-29 21:27:34

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Compte',
  'Opportunities' => 'Oportunitat',
  'Cases' => 'Cas',
  'Leads' => 'Client potencial',
  'Contacts' => 'Contactes',
  'Products' => 'Element de línia d\'oferta',
  'Quotes' => 'Pressupost',
  'Bugs' => 'Incidència',
  'Project' => 'Projecte',
  'Prospects' => 'Objectiu',
  'ProjectTask' => 'Tasca de projecte',
  'Tasks' => 'Tasca',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Línia d\'impostos articles',
);