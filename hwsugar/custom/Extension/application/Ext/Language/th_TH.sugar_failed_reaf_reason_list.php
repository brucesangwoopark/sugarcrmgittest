<?php
 // created: 2017-04-11 11:38:12

$app_list_strings['failed_reaf_reason_list']=array (
  'Customer thought we were the utility' => 'Customer thought we were the utility',
  'Did not have spouse approval' => 'Did not have spouse approval',
  'Can not afford' => 'Can not afford',
  'Needs to speak with spouse' => 'Needs to speak with spouse',
  'Thought it was free' => 'Thought it was free',
  'Other' => 'Other',
  '' => '',
);