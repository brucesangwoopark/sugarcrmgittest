<?php
 // created: 2016-07-29 21:27:13

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Организация',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Възможност',
  'Products' => 'Офериран продукт',
  'Quotes' => 'Оферта',
  'Bugs' => 'Проблеми',
  'Cases' => 'Казус',
  'Leads' => 'Потенциален клиент',
  'Project' => 'Проекти',
  'ProjectTask' => 'Задача по проект',
  'Prospects' => 'Целеви клиент',
  'KBContents' => 'База от знания',
  'RevenueLineItems' => 'Приходни позиции',
);