<?php
 // created: 2016-07-29 21:27:33

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Conta',
  'Opportunities' => 'Oportunidade',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Contacts' => 'Contatos',
  'Products' => 'Item de Linha de Cotação',
  'Quotes' => 'Cotação',
  'Bugs' => 'Bug',
  'Project' => 'Projeto',
  'Prospects' => 'Alvo',
  'ProjectTask' => 'Tarefa de Projeto',
  'Tasks' => 'Tarefa',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);