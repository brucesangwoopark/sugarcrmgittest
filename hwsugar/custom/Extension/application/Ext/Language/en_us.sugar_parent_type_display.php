<?php
 // created: 2016-07-29 21:27:11

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Products' => 'Quoted Line Item',
  'Quotes' => 'Quote',
  'Bugs' => 'Bugs',
  'Cases' => 'Case',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Revenue Line Items',
);