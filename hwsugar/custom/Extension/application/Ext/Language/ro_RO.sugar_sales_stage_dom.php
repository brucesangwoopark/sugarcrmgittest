<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospectare',
  'Qualification' => 'Calificare',
  'Needs Analysis' => 'Necesita analiza',
  'Value Proposition' => 'Expunere beneficii',
  'Id. Decision Makers' => 'Identificare decidenti',
  'Perception Analysis' => 'Analiza perceptiei',
  'Proposal/Price Quote' => 'Propunere/Ofertă de preț',
  'Negotiation/Review' => 'Negociere/Revizie',
  'Closed Won' => 'Castigat',
  'Closed Lost' => 'Pierdut',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);