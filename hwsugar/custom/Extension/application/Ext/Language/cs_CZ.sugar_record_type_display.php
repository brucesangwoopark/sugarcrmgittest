<?php
 // created: 2016-07-29 21:27:14

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Účet',
  'Opportunities' => 'Příležitost',
  'Cases' => 'Případ:',
  'Leads' => 'Příležitost',
  'Contacts' => 'Kontakty',
  'Products' => 'Produkt',
  'Quotes' => 'Nabídka',
  'Bugs' => 'Chyba:',
  'Project' => 'Projekty',
  'Prospects' => 'Kontakt',
  'ProjectTask' => 'Projektové úkoly',
  'Tasks' => 'Úkol',
  'KBContents' => 'Znalostní báze',
  'RevenueLineItems' => 'Řádky obchodu',
);