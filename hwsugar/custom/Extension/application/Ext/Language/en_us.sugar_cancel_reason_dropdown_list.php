<?php
 // created: 2017-02-06 17:16:15

$app_list_strings['cancel_reason_dropdown_list']=array (
  '' => '',
  'Agent Presentation' => 'Agent Presentation',
  'Competitor' => 'Competitor',
  'Competitor Fraud' => 'Competitor Fraud',
  'Death' => 'Death',
  'Delinquent Account' => 'Delinquent Account',
  'Elderly' => 'Elderly',
  'Failed Reaffirmation' => 'Failed Reaffirmation',
  'Language Barrier' => 'Language Barrier',
  'Moving' => 'Moving',
  'No Billing Info' => 'No Billing Info',
  'Online Reputation' => 'Online Reputation',
  'Price' => 'Price',
  'Product Confiscation' => 'Product Confiscation',
  'Product Service Issue' => 'Product/Service Issue',
  'Reached end of Term' => 'Reached end of Term',
  'Spouse Disapproval' => 'Spouse Disapproval',
  'Contract is too Long' => 'Contract is too Long',
  'Transfer of Ownership' => 'Transfer of Ownership',
  'Unauthorized Signee' => 'Unauthorized Signee',
);