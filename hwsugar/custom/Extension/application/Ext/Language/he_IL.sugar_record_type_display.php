<?php
 // created: 2016-07-29 21:27:20

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'חשבון',
  'Opportunities' => 'הזדמנות',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Contacts' => 'אנשי קשר',
  'Products' => 'שורת פריט מצוטט',
  'Quotes' => 'הצעת מחיר',
  'Bugs' => 'באג',
  'Project' => 'פרויקט',
  'Prospects' => 'מטרה',
  'ProjectTask' => 'משימת הפרויקט',
  'Tasks' => 'משימה',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);