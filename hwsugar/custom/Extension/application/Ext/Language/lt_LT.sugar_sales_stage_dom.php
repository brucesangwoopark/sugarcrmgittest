<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Identifikavimas',
  'Needs Analysis' => 'Poreikių analizė',
  'Value Proposition' => 'Pasiūlymas',
  'Id. Decision Makers' => 'Sprendimo priėmėjo įvardinimas',
  'Perception Analysis' => 'Detalesnė analizė',
  'Proposal/Price Quote' => 'Pasiūlymas/kainos įvardinimas',
  'Negotiation/Review' => 'Derybos/Peržiūra',
  'Closed Won' => 'Užbaigtas',
  'Closed Lost' => 'Nesėkmingas sandoris',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);