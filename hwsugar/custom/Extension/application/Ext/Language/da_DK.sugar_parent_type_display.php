<?php
 // created: 2016-07-29 21:27:15

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Virksomhed',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Opgave',
  'Opportunities' => 'Salgsmulighed',
  'Products' => 'Angiven linjepost',
  'Quotes' => 'Tilbud',
  'Bugs' => 'Fejl',
  'Cases' => 'Sag',
  'Leads' => 'Kundeemne',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektopgave',
  'Prospects' => 'Mål:',
  'KBContents' => 'Videnbase',
  'RevenueLineItems' => 'Revenue detaljposter',
);