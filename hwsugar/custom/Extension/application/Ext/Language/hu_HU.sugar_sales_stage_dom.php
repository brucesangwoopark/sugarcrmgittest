<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Felmérés',
  'Qualification' => 'Minősítés',
  'Needs Analysis' => 'Igényfelmérés',
  'Value Proposition' => 'Értékajánlat',
  'Id. Decision Makers' => 'Döntéshozók azonosítása',
  'Perception Analysis' => 'Észrevételek elemzése',
  'Proposal/Price Quote' => 'Javaslat / árajánlat',
  'Negotiation/Review' => 'Tárgyalás / felülvizsgálat',
  'Closed Won' => 'Lezárt',
  'Closed Lost' => 'Lezárt, elvesztett',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);