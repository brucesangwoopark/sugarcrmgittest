<?php
 // created: 2016-07-29 21:27:22

$app_list_strings['parent_type_display']=array (
  'Accounts' => '取引先',
  'Contacts' => '取引先担当者',
  'Tasks' => 'タスク',
  'Opportunities' => '商談',
  'Products' => '見積済商品',
  'Quotes' => '見積',
  'Bugs' => 'バグトラッカー',
  'Cases' => 'ケース',
  'Leads' => 'リード',
  'Project' => 'プロジェクト',
  'ProjectTask' => 'プロジェクトタスク',
  'Prospects' => 'ターゲット',
  'KBContents' => 'ナレッジベース',
  'RevenueLineItems' => '商談品目',
);