<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Predviđanje',
  'Qualification' => 'Kvalifikacija',
  'Needs Analysis' => 'Analize potreba',
  'Value Proposition' => 'Preporučena cena',
  'Id. Decision Makers' => 'ID broj donosilaca odluka',
  'Perception Analysis' => 'Analiza sagledavanja',
  'Proposal/Price Quote' => 'Predlog/Cene ponuda',
  'Negotiation/Review' => 'Pregovori/Pregledi',
  'Closed Won' => 'Zatvoreno',
  'Closed Lost' => 'Završeni izgubljeni',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);