<?php
 // created: 2016-07-29 21:27:26

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Account',
  'Opportunities' => 'Opportunity',
  'Cases' => 'Casus',
  'Leads' => 'Lead',
  'Contacts' => 'Contactpersonen',
  'Products' => 'Geoffreerd product',
  'Quotes' => 'Offerte',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'Prospects' => 'Target',
  'ProjectTask' => 'Projecttaak',
  'Tasks' => 'Taak',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Opportunityregels',
);