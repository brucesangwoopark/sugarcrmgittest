<?php
 // created: 2016-10-17 21:07:41

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Наличие на потенциал за сделка',
  'Qualification' => 'Класификация на потенциалната сделка',
  'Needs Analysis' => 'Анализ на нуждите на клиента',
  'Value Proposition' => 'Разработване на решение',
  'Id. Decision Makers' => 'Определяне на ключовите фигури',
  'Perception Analysis' => 'Определяне на стратегия за преговорите',
  'Proposal/Price Quote' => 'Предложение/Ценова оферта',
  'Negotiation/Review' => 'Преговори/Преглед на офертата',
  'Closed Won' => 'Затворени',
  'Closed Lost' => 'Загубени',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);