<?php
 // created: 2017-10-26 13:38:31

$app_list_strings['category_list']=array (
  'collectionsardirect' => 'Collections/AR​ ​–​ ​Direct',
  'collectionsarenbridge' => 'Collections/AR​ ​–​ ​Enbridge',
  'system' => 'System',
  'billingdirect' => 'Billing​ ​–​ ​Direct',
  'billingenbridge' => 'Billing​ ​–​ ​Enbridge',
  'installationservice' => 'Installation​ ​Service',
  'Salescommissions' => 'Sales Commissions',
  'systemissues' => 'System Issues',
  'support' => 'Support',
);