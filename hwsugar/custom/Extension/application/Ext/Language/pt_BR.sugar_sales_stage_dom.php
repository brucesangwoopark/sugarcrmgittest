<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecção',
  'Qualification' => 'Qualificação',
  'Needs Analysis' => 'Análise de necessidades',
  'Value Proposition' => 'Proposta de valor',
  'Id. Decision Makers' => 'Identificar tomadores de decisão',
  'Perception Analysis' => 'Análise de percepção',
  'Proposal/Price Quote' => 'Proposta/cotação de preço',
  'Negotiation/Review' => 'Negociação/análise',
  'Closed Won' => 'Fechadas ganhas',
  'Closed Lost' => 'Fechada perdida',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);