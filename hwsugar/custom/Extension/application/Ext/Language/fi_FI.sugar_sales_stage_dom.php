<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospektointi',
  'Qualification' => 'Valinta',
  'Needs Analysis' => 'Tarveanalyysi',
  'Value Proposition' => 'Arvolupaus',
  'Id. Decision Makers' => 'Tunnista päättäjät',
  'Perception Analysis' => 'Mielikuva-analyysi',
  'Proposal/Price Quote' => 'Tarjous',
  'Negotiation/Review' => 'Neuvottelu/arvio',
  'Closed Won' => 'Suljettu / voitettu',
  'Closed Lost' => 'Suljettu / hävitty',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);