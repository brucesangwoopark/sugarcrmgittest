<?php
 // created: 2016-07-29 21:27:29

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Сделка',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Contacts' => 'Контакты',
  'Products' => 'Продукт коммерческого предложения',
  'Quotes' => 'Коммерческое предложение',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'Prospects' => 'Адресат',
  'ProjectTask' => 'Проектная задача',
  'Tasks' => 'Задача',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);