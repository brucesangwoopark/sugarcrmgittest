<?php
 // created: 2016-07-29 21:27:24

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Uzņēmums',
  'Opportunities' => 'Iespēja',
  'Cases' => 'Pieteikums',
  'Leads' => 'Interesents',
  'Contacts' => 'Kontaktpersonas',
  'Products' => 'Piedāvājuma rinda',
  'Quotes' => 'Piedāvājums',
  'Bugs' => 'Kļūda',
  'Project' => 'Projekts',
  'Prospects' => 'Mērķis',
  'ProjectTask' => 'Projekta uzdevums',
  'Tasks' => 'Uzdevums',
  'KBContents' => 'Zināšanu bāze',
  'RevenueLineItems' => 'Ieņēmumu posteņi',
);