<?php
 // created: 2016-07-29 21:27:40

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Tasks' => 'Задача',
  'Opportunities' => 'Угода',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилки',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Project' => 'Проект',
  'ProjectTask' => 'Задача проекту',
  'Prospects' => 'Цільова аудиторія споживачів',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);