<?php
 //created: 2016-07-29 21:27:33

$app_list_strings['moduleList']['Products']='Elements de línies d\'oferta';
$app_list_strings['moduleList']['TaxRates']='Tipus d\'impostos';
$app_list_strings['moduleList']['ProspectLists']='Llistes d\'objectius';
$app_list_strings['moduleList']['Styleguide']='Guia d\'estil';
$app_list_strings['moduleList']['RevenueLineItems']='Línia d\'impostos articles';