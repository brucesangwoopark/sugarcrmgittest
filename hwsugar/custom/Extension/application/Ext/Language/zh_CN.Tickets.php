<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['cases_tickets_type_dom']['Administration'] = 'Administration';
$app_list_strings['cases_tickets_type_dom']['Product'] = '产品';
$app_list_strings['cases_tickets_type_dom']['User'] = '用户';
$app_list_strings['cases_tickets_status_dom']['New'] = '新建';
$app_list_strings['cases_tickets_status_dom']['Assigned'] = '已分配';
$app_list_strings['cases_tickets_status_dom']['Closed'] = '已关闭';
$app_list_strings['cases_tickets_status_dom']['Pending Input'] = '待输入';
$app_list_strings['cases_tickets_status_dom']['Rejected'] = '已拒绝';
$app_list_strings['cases_tickets_status_dom']['Duplicate'] = '复制';
$app_list_strings['cases_tickets_priority_dom']['P1'] = '高';
$app_list_strings['cases_tickets_priority_dom']['P2'] = '中';
$app_list_strings['cases_tickets_priority_dom']['P3'] = '低';
$app_list_strings['cases_tickets_resolution_dom']['Accepted'] = '已接受';
$app_list_strings['cases_tickets_resolution_dom']['Duplicate'] = '复制';
$app_list_strings['cases_tickets_resolution_dom']['Closed'] = '已关闭';
$app_list_strings['cases_tickets_resolution_dom']['Out of Date'] = '过期';
$app_list_strings['cases_tickets_resolution_dom']['Invalid'] = '无效';
$app_list_strings['cases_tickets_resolution_dom'][''] = '';
$app_list_strings['moduleList']['cases_Tickets'] = 'Tickets';
$app_list_strings['moduleListSingular']['cases_Tickets'] = 'Ticket';
$app_list_strings['tsource_list']['CSR'] = 'CSR';
$app_list_strings['tsource_list']['Arcollections'] = 'AR / Collections';
$app_list_strings['tsource_list']['Sales'] = 'Sales';
$app_list_strings['category_list']['collectionsardirect'] = 'Collections/AR​ ​–​ ​Direct';
$app_list_strings['category_list']['collectionsarenbridge'] = 'Collections/AR​ ​–​ ​Enbridge';
$app_list_strings['category_list']['system'] = 'System';
$app_list_strings['category_list']['billingdirect'] = 'Billing​ ​–​ ​Direct';
$app_list_strings['category_list']['billingenbridge'] = 'Billing​ ​–​ ​Enbridge';
$app_list_strings['category_list']['installationservice'] = 'Installation​ ​Service';
$app_list_strings['category_list']['Salescommissions'] = 'Sales Commissions';
$app_list_strings['category_list']['systemissues'] = 'System Issues';
$app_list_strings['category_list']['support'] = 'Support';
$app_list_strings['department_list']['Marketing'] = 'Marketing and Finance';
$app_list_strings['department_list']['IT'] = 'IT';
$app_list_strings['department_list']['operationscommissions'] = 'Operations – Commissions';
$app_list_strings['department_list']['Operations'] = 'Operations';
$app_list_strings['department_list'][''] = '';
$app_list_strings['subcategory_list']['Credit'] = 'Credit';
$app_list_strings['subcategory_list']['ETC'] = 'ETC';
$app_list_strings['subcategory_list']['Refund'] = 'Refund';
$app_list_strings['subcategory_list']['Refusedpayment'] = 'Refused​ ​Payment';
$app_list_strings['subcategory_list']['Sugar'] = 'Sugar';
$app_list_strings['subcategory_list']['Synchro'] = 'Synchro';
$app_list_strings['subcategory_list']['ChargeOver'] = 'ChargeOver';
$app_list_strings['subcategory_list']['DocuSign'] = 'DocuSign';
$app_list_strings['subcategory_list']['BOX'] = 'BOX';
$app_list_strings['subcategory_list']['transferfee'] = 'Transfer​ ​Fee';
$app_list_strings['subcategory_list']['removalfee'] = 'Removal​ ​Fee';
$app_list_strings['subcategory_list']['billdatechange'] = 'Bill​ ​Date​ ​Change';
$app_list_strings['subcategory_list']['ibcstoprental'] = 'IBC​ ​–​ ​Stop​ ​Rental';
$app_list_strings['subcategory_list']['Job'] = 'Job';
$app_list_strings['subcategory_list']['Schedule'] = 'Schedule';
$app_list_strings['subcategory_list']['rlitojoblink'] = 'RLI​ ​to​ ​Job​ ​Link';
$app_list_strings['subcategory_list']['agentcommission'] = 'Agent Commission';
$app_list_strings['subcategory_list']['overrides'] = 'Over Rides';
$app_list_strings['subcategory_list']['missingdocumentation'] = 'Missing Documentation';
$app_list_strings['subcategory_list']['commissiongridchange'] = 'Commission Grid Change';
$app_list_strings['subcategory_list']['achandarcollections'] = 'ACH and AR Collections';
$app_list_strings['subcategory_list']['other'] = 'Other';
$app_list_strings['subcategory_list']['creditrefund'] = 'Credit/Refund';
$app_list_strings['subcategory_list']['etcapplyreverse'] = 'ETC Apply/Reverse';
$app_list_strings['subcategory_list']['updatemonthlyrental'] = 'Update Monthly Rental';
$app_list_strings['subcategory_list']['dispute'] = 'Dispute';
$app_list_strings['subcategory_list']['general'] = 'General';
$app_list_strings['subcategory_list']['printer'] = 'Printer';
$app_list_strings['subcategory_list']['laptop'] = 'Laptop';
$app_list_strings['subcategory_list']['desktop'] = 'Desktop';
$app_list_strings['subcategory_list']['cabrequired'] = 'Cab Required';
$app_list_strings['subcategory_list']['hrissues'] = 'HR Issues';
