<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Izpēte',
  'Qualification' => 'Kvalifikācija',
  'Needs Analysis' => 'Nepieciešama analīze',
  'Value Proposition' => 'Vērtības piedāvājums',
  'Id. Decision Makers' => 'Lēmumpieņēmēju identificēšana',
  'Perception Analysis' => 'Uztveres analīze',
  'Proposal/Price Quote' => 'Ierosinājums/Cenas piedāvājums',
  'Negotiation/Review' => 'Pārrunas/Caurskate',
  'Closed Won' => 'Slēgts',
  'Closed Lost' => 'Aizvērts bez panākumiem',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);