<?php
 // created: 2016-07-29 21:27:37

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'llogaritë',
  'Opportunities' => 'Mundësi:',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Contacts' => 'Kontaktet',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabim',
  'Project' => 'Projekti',
  'Prospects' => 'Synim',
  'ProjectTask' => 'Detyrat projektuese',
  'Tasks' => 'Detyrë',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);