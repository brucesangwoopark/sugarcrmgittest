<?php
 // created: 2016-07-29 21:27:16

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Firma',
  'Contacts' => 'Kontakt',
  'Opportunities' => 'Verkaufschance',
  'Tasks' => 'Aufgabe',
  'ProductTemplates' => 'Produktkatalog',
  'Quotes' => 'Angebot',
  'Products' => 'Produkt',
  'Contracts' => 'Vertrag',
  'Emails' => 'E-Mail',
  'Bugs' => 'Fehler',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektaufgabe',
  'Prospects' => 'Zielkontakt',
  'Cases' => 'Ticket',
  'Leads' => 'Interessent',
  'Meetings' => 'Meeting',
  'Calls' => 'Anruf',
  'KBContents' => 'Wissensdatenbank',
  'RevenueLineItems' => 'Umsatzposten',
);