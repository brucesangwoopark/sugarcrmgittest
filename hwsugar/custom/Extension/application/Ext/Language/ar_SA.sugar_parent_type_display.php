<?php
 // created: 2016-07-29 21:27:39

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'الحساب',
  'Contacts' => 'جهة الاتصال',
  'Tasks' => 'المهمة',
  'Opportunities' => 'الفرصة',
  'Products' => 'البند المسعر',
  'Quotes' => 'عرض السعر',
  'Bugs' => 'الأخطاء',
  'Cases' => 'الحالة',
  'Leads' => 'العميل المتوقع',
  'Project' => 'المشروع',
  'ProjectTask' => 'مهمة المشروع',
  'Prospects' => 'الهدف',
  'KBContents' => 'قاعدة المعارف',
  'RevenueLineItems' => 'بنود العائدات',
);