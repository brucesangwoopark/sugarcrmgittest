<?php
 // created: 2017-01-30 18:31:10

$app_list_strings['service_type_list']=array (
  'Water Pressure' => 'Water Pressure',
  'Leak' => 'Leak',
  'Prefilter Change' => 'Prefilter Change',
  'Site Inspection' => 'Site Inspection',
  'Filter Relocation' => 'Filter Relocation',
  'Filter Swap' => 'Filter Swap',
  'No Show' => 'No Show',
  'Payment Collection' => 'Payment Collection',
  'Other' => 'Other',
);