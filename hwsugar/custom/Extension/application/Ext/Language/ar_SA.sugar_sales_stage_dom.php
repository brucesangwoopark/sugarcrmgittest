<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'التوقع',
  'Qualification' => 'التأهل',
  'Needs Analysis' => 'تحتاج إلى تحليل',
  'Value Proposition' => 'عرض القيمة',
  'Id. Decision Makers' => 'المعرّف. صُنَّاع القرار',
  'Perception Analysis' => 'تحليل الإدراك',
  'Proposal/Price Quote' => 'عرض السعر/الاقتراح',
  'Negotiation/Review' => 'التفاوض/المراجعة',
  'Closed Won' => 'إغلاق لسبب الفوز',
  'Closed Lost' => 'إغلاق لسبب الخساره',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);