<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => '예측',
  'Qualification' => '선별완료',
  'Needs Analysis' => '수요분석',
  'Value Proposition' => '초기영업',
  'Id. Decision Makers' => '의사결정권자확인',
  'Perception Analysis' => '고객요구사항분석',
  'Proposal/Price Quote' => '제안및견적',
  'Negotiation/Review' => '최종교섭중',
  'Closed Won' => '완료',
  'Closed Lost' => '계약실패',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);