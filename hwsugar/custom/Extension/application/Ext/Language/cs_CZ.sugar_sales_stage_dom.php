<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospekce',
  'Qualification' => 'Kvalifikace',
  'Needs Analysis' => 'Potřebná Analýza',
  'Value Proposition' => 'Analýza hodnoty',
  'Id. Decision Makers' => 'Identifikace rozhodujících činitelů',
  'Perception Analysis' => 'Analýza očekávání',
  'Proposal/Price Quote' => 'Nabídka',
  'Negotiation/Review' => 'Vyjednávání',
  'Closed Won' => 'Uzavřený',
  'Closed Lost' => 'Ukončená zamítnuta',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);