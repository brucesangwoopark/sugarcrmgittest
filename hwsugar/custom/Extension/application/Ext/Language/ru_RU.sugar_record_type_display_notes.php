<?php
 // created: 2016-07-29 21:27:29

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Контрагент',
  'Contacts' => 'Контакт',
  'Opportunities' => 'Сделка',
  'Tasks' => 'Задача',
  'ProductTemplates' => 'Каталог продуктов',
  'Quotes' => 'Коммерческое предложение',
  'Products' => 'Продукт коммерческого предложения',
  'Contracts' => 'Контракт',
  'Emails' => 'E-mail',
  'Bugs' => 'Ошибка',
  'Project' => 'Проект',
  'ProjectTask' => 'Проектная задача',
  'Prospects' => 'Адресат',
  'Cases' => 'Обращение',
  'Leads' => 'Предварительный контакт',
  'Meetings' => 'Встреча',
  'Calls' => 'Звонок',
  'KBContents' => 'База знаний',
  'RevenueLineItems' => 'Доход по продуктам',
);