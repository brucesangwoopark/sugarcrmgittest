<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospezione',
  'Qualification' => 'Qualificazione',
  'Needs Analysis' => 'Analisi Bisogni',
  'Value Proposition' => 'Propos. Valore',
  'Id. Decision Makers' => 'Id. Decisori',
  'Perception Analysis' => 'Analisi Percezione',
  'Proposal/Price Quote' => 'Proposta Economica',
  'Negotiation/Review' => 'Negoziazione/Review',
  'Closed Won' => 'Chiuso',
  'Closed Lost' => 'Chiuso Perso',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);