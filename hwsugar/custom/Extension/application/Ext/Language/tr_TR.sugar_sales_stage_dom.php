<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'İnceleme',
  'Qualification' => 'Kalifikasyon',
  'Needs Analysis' => 'İhtiyaç Analizi',
  'Value Proposition' => 'Değer Önerisi',
  'Id. Decision Makers' => 'Karar Verenlerin Belirlenmesi',
  'Perception Analysis' => 'Persepsiyon Analizi',
  'Proposal/Price Quote' => 'Teklif/Fiyat Verme',
  'Negotiation/Review' => 'Anlaşma/İnceleme',
  'Closed Won' => 'Kapalı',
  'Closed Lost' => 'Başarısızlıkla Kapandı',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);