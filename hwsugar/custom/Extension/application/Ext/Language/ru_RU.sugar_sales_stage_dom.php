<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Сбор информации',
  'Qualification' => 'Оценка',
  'Needs Analysis' => 'Анализ потребностей',
  'Value Proposition' => 'Предложение ценности',
  'Id. Decision Makers' => 'Определение лиц, принимающих решения',
  'Perception Analysis' => 'Анализ реакции',
  'Proposal/Price Quote' => 'Коммерческое предложение',
  'Negotiation/Review' => 'Согласование КП',
  'Closed Won' => 'Закрытая состоявшаяся продажа',
  'Closed Lost' => 'Закрыто без успеха',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);