<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospección',
  'Qualification' => 'Calificación',
  'Needs Analysis' => 'Necesita Análisis',
  'Value Proposition' => 'Propuesta de Valor',
  'Id. Decision Makers' => 'Id. de los responsables de la toma de decisiones',
  'Perception Analysis' => 'Análisis de Percepción',
  'Proposal/Price Quote' => 'Propuesta/Presupuesto',
  'Negotiation/Review' => 'Negociación/Revisión',
  'Closed Won' => 'Cerrado',
  'Closed Lost' => 'Perdido',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);