<?php
 // created: 2016-07-29 21:27:20

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'חשבון',
  'Contacts' => 'איש קשר',
  'Tasks' => 'משימה',
  'Opportunities' => 'הזדמנות',
  'Products' => 'שורת פריט מצוטט',
  'Quotes' => 'הצעת מחיר',
  'Bugs' => 'באגים',
  'Cases' => 'פניית שירות',
  'Leads' => 'ליד',
  'Project' => 'פרויקט',
  'ProjectTask' => 'משימת הפרויקט',
  'Prospects' => 'מטרה',
  'KBContents' => 'מרכז מידע',
  'RevenueLineItems' => 'שורות פרטי הכנסה',
);