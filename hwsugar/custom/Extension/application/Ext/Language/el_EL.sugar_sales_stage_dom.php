<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Προβλέψεις',
  'Qualification' => 'Προσόν',
  'Needs Analysis' => 'Ανάλυση Αναγκών',
  'Value Proposition' => 'Αξία Πρότασης',
  'Id. Decision Makers' => 'Ταυτότητα Υπευθύνου',
  'Perception Analysis' => 'Ανάλυση Αντίληψης',
  'Proposal/Price Quote' => 'Πρόταση/Οικονομική Προσφορά',
  'Negotiation/Review' => 'Σε Διαπραγμάτευση/Αναθεώρηση',
  'Closed Won' => 'Έκλεισε Κερδισμένο',
  'Closed Lost' => 'Έκλεισε Χαμένη',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);