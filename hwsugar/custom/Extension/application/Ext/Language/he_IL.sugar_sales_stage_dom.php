<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'הערכת קשר פוטנציאלי',
  'Qualification' => 'מגבלות',
  'Needs Analysis' => 'מצריך אנליזה',
  'Value Proposition' => 'ערך תחזית',
  'Id. Decision Makers' => 'זהות מקבל ההחלטות',
  'Perception Analysis' => 'אנליזה לתפיסה',
  'Proposal/Price Quote' => 'הצעת מחיר',
  'Negotiation/Review' => 'משא ומתן',
  'Closed Won' => 'נסגר בהצלחה',
  'Closed Lost' => 'נסגר ואבד',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);