<?php
 // created: 2016-07-29 21:27:28

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Cont:',
  'Opportunities' => 'Oportunitate',
  'Cases' => 'Caz:',
  'Leads' => 'Client potențial',
  'Contacts' => 'Contacte',
  'Products' => 'Element din ofertă',
  'Quotes' => 'Ofertă',
  'Bugs' => 'Problema:',
  'Project' => 'Proiect',
  'Prospects' => 'Tinta:',
  'ProjectTask' => 'Sarcina de proiect',
  'Tasks' => 'Sarcina',
  'KBContents' => 'Baza de cunostinte',
  'RevenueLineItems' => 'Linii de venit',
);