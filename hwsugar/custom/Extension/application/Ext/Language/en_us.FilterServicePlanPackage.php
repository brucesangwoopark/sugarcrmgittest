<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['FSPG2_FilterServicePlans'] = 'Filter Service Plans';
$app_list_strings['moduleList']['FSPG2_FilterServiceDetails'] = 'Filter Service Details';
$app_list_strings['moduleList']['FSPG2_FilterServicePackages'] = 'Filter Service Packages';
$app_list_strings['moduleList']['FSPG2_FilterTypes'] = 'Filter Types';
$app_list_strings['moduleListSingular']['FSPG2_FilterServicePlans'] = 'Filter Service Plan';
$app_list_strings['moduleListSingular']['FSPG2_FilterServiceDetails'] = 'Filter Service Detail';
$app_list_strings['moduleListSingular']['FSPG2_FilterServicePackages'] = 'Filter Service Package';
$app_list_strings['moduleListSingular']['FSPG2_FilterTypes'] = 'Filter Type';
$app_list_strings['fspg2_filterservicedetails_units']['Gallon'] = 'Gallon';
$app_list_strings['fspg2_filterservicedetails_units']['M3'] = 'M3';
$app_list_strings['fspg2_filterservicedetails_state'][1] = '1';
$app_list_strings['fspg2_filterservicedetails_state'][2] = '2';
$app_list_strings['fspg2_filterservicedetails_state'][3] = '3';
$app_list_strings['fspg2_filterservicedetails_state'][4] = '4';
$app_list_strings['fspg2_filterservicedetails_state'][5] = '5';
$app_list_strings['fspg2_filterservicedetails_state'][6] = '6';
$app_list_strings['fspg2_filterservicedetails_state'][0] = '';
$app_list_strings['fspg2_filterservicepackages_producttype']['Service'] = 'Service';
