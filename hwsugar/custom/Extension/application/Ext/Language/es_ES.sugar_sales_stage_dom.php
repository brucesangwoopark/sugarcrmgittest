<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospección',
  'Qualification' => 'Calificación',
  'Needs Analysis' => 'Análisis de necesidades',
  'Value Proposition' => 'Propuesta de valor',
  'Id. Decision Makers' => 'Identificación de responsables',
  'Perception Analysis' => 'Análisis de percepción',
  'Proposal/Price Quote' => 'Propuesta/Presupuesto',
  'Negotiation/Review' => 'Negociación/Revisión',
  'Closed Won' => 'Cerrado',
  'Closed Lost' => 'Perdida',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);