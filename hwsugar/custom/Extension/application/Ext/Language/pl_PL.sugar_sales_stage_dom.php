<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Wstępna analiza',
  'Qualification' => 'Kwalifikacje',
  'Needs Analysis' => 'Analiza wymagań',
  'Value Proposition' => 'Propozycja wartości',
  'Id. Decision Makers' => 'Ident. osób decyzyjnych',
  'Perception Analysis' => 'Analiza oferty',
  'Proposal/Price Quote' => 'Propozycja cenowa',
  'Negotiation/Review' => 'Negocjacje/Ocena',
  'Closed Won' => 'Zrealizowano',
  'Closed Lost' => 'Zakończone porażką',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);