<?php
 // created: 2016-07-29 21:27:27

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Kontrahent',
  'Opportunities' => 'Szansa',
  'Cases' => 'Zgłoszenie',
  'Leads' => 'Potencjalny klient',
  'Contacts' => 'Kontakty',
  'Products' => 'Pozycja oferty',
  'Quotes' => 'Oferta',
  'Bugs' => 'Błąd',
  'Project' => 'Projekt',
  'Prospects' => 'Odbiorca',
  'ProjectTask' => 'Zadanie projektowe',
  'Tasks' => 'Zadanie',
  'KBContents' => 'Baza wiedzy',
  'RevenueLineItems' => 'Pozycje szansy',
);