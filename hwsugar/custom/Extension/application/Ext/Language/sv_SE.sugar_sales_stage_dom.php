<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospektering',
  'Qualification' => 'Kvalificering',
  'Needs Analysis' => 'Kräver analys',
  'Value Proposition' => 'Kvalificera budget',
  'Id. Decision Makers' => 'Identifiera beslutsfattare',
  'Perception Analysis' => 'Analyserar uppfattningar',
  'Proposal/Price Quote' => 'Förslag/Offert',
  'Negotiation/Review' => 'Förhandling',
  'Closed Won' => 'Stängd Vunnen',
  'Closed Lost' => 'Stängd förlorad',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);