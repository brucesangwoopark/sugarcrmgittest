<?php
 // created: 2016-07-29 21:27:22

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Azienda',
  'Contacts' => 'Contatto',
  'Opportunities' => 'Opportunità',
  'Tasks' => 'Compito',
  'ProductTemplates' => 'Catalogo Prodotti',
  'Quotes' => 'Offerta',
  'Products' => 'Prodotto',
  'Contracts' => 'Contratto',
  'Emails' => 'Email alternativo',
  'Bugs' => 'Bug',
  'Project' => 'Progetto',
  'ProjectTask' => 'Compiti di Progetto',
  'Prospects' => 'Obiettivo',
  'Cases' => 'Reclamo',
  'Leads' => 'Lead',
  'Meetings' => 'Riunione',
  'Calls' => 'Chiamata',
  'KBContents' => 'Knowledge Base',
  'RevenueLineItems' => 'Elementi dell´Opportunità',
);