<?php
 // created: 2016-07-29 21:27:21

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Opportunities' => 'Lehetőség',
  'Tasks' => 'Feladat',
  'ProductTemplates' => 'Termékkatalógus',
  'Quotes' => 'Árajánlat',
  'Products' => 'Megajánlott Tétel',
  'Contracts' => 'Szerződés',
  'Emails' => 'E-mail:',
  'Bugs' => 'Hiba',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Meetings' => 'Találkozó',
  'Calls' => 'Hívás',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);