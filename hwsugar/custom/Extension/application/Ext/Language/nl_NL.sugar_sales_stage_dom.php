<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospectie',
  'Qualification' => 'Kwalificatie',
  'Needs Analysis' => 'Behoeftenanalyse',
  'Value Proposition' => 'Waardepropositie',
  'Id. Decision Makers' => 'Identificatie van beslissers',
  'Perception Analysis' => 'Perceptie Analyse',
  'Proposal/Price Quote' => 'Voorstel/Offerte',
  'Negotiation/Review' => 'Onderhandeling/Beoordeling',
  'Closed Won' => 'Afgesloten - Gewonnen',
  'Closed Lost' => 'Afgesloten - Verloren',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);