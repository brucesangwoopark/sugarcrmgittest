<?php
 // created: 2016-07-29 21:27:41

$app_list_strings['record_type_display']=array (
  '' => '',
  'Accounts' => 'Контрагент',
  'Opportunities' => 'Угода',
  'Cases' => 'Звернення',
  'Leads' => 'Інтерес',
  'Contacts' => 'Контакти',
  'Products' => 'Продукт комерційної пропозиції',
  'Quotes' => 'Комерційна пропозиція',
  'Bugs' => 'Помилка',
  'Project' => 'Проект',
  'Prospects' => 'Цільова аудиторія споживачів',
  'ProjectTask' => 'Задача проекту',
  'Tasks' => 'Задача',
  'KBContents' => 'База знань',
  'RevenueLineItems' => 'Доходи за продукти',
);