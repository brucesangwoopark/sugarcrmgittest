<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => '引き合い',
  'Qualification' => '見込み',
  'Needs Analysis' => 'ニーズ分析',
  'Value Proposition' => '提案中',
  'Id. Decision Makers' => '意思決定者確認中',
  'Perception Analysis' => '顧客評価中',
  'Proposal/Price Quote' => '最終提案/見積中',
  'Negotiation/Review' => '交渉/見直し',
  'Closed Won' => '完了',
  'Closed Lost' => '失注',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);