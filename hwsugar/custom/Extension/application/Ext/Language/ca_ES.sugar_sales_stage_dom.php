<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecció',
  'Qualification' => 'Qualificació',
  'Needs Analysis' => 'Necessita anàlisi',
  'Value Proposition' => 'Proposta de valor',
  'Id. Decision Makers' => 'ID dels responsables de prendre decisions',
  'Perception Analysis' => 'Anàlisi de percepció',
  'Proposal/Price Quote' => 'Proposta/pressupost',
  'Negotiation/Review' => 'Negociació/revisió',
  'Closed Won' => 'Tancat',
  'Closed Lost' => 'Perdut tancat',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);