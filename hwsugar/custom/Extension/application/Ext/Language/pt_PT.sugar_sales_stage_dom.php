<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospeção',
  'Qualification' => 'Qualificação',
  'Needs Analysis' => 'Análise de Necessidades',
  'Value Proposition' => 'Proposta de Valor',
  'Id. Decision Makers' => 'Identificar decisores',
  'Perception Analysis' => 'Análise de Percepção',
  'Proposal/Price Quote' => 'Proposta/Cotação de Preço',
  'Negotiation/Review' => 'Negociação/Revisão',
  'Closed Won' => 'Ganha',
  'Closed Lost' => 'Perdida',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);