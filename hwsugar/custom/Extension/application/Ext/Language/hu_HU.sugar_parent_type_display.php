<?php
 // created: 2016-07-29 21:27:21

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Kliens',
  'Contacts' => 'Kapcsolat',
  'Tasks' => 'Feladat',
  'Opportunities' => 'Lehetőség',
  'Products' => 'Megajánlott Tétel',
  'Quotes' => 'Árajánlat',
  'Bugs' => 'Hibák',
  'Cases' => 'Eset',
  'Leads' => 'Ajánlás',
  'Project' => 'Projekt',
  'ProjectTask' => 'Projektfeladat',
  'Prospects' => 'Cél',
  'KBContents' => 'Tudásbázis',
  'RevenueLineItems' => 'Bevétel sorok',
);