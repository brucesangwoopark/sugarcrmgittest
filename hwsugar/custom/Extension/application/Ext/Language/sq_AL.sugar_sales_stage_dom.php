<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Kërkime',
  'Qualification' => 'Kualifikimi',
  'Needs Analysis' => 'Nevojitet analizë',
  'Value Proposition' => 'Propozim me vlerë',
  'Id. Decision Makers' => 'Id e vendimmarësve',
  'Perception Analysis' => 'Analiza e perceptimit',
  'Proposal/Price Quote' => 'Kuota Propozim/Çmim',
  'Negotiation/Review' => 'Negocimi/Shqyrtimi',
  'Closed Won' => 'I Mbyllur',
  'Closed Lost' => 'Humbja e mbyllur',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);