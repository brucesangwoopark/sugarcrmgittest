<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Kvalifikácia',
  'Needs Analysis' => 'analýza potieb',
  'Value Proposition' => 'hodnota propozície',
  'Id. Decision Makers' => 'Id. pracovníci, ktorí rozhodujú',
  'Perception Analysis' => 'analýza vnímania',
  'Proposal/Price Quote' => 'Návrh / Cenová ponuka',
  'Negotiation/Review' => 'Vyjednávanie / recenzie',
  'Closed Won' => 'Zatvorený',
  'Closed Lost' => 'Uzatvorená prehra',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);