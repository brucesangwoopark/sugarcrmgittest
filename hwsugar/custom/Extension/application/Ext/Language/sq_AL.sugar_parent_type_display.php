<?php
 // created: 2016-07-29 21:27:37

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'llogaritë',
  'Contacts' => 'Kontakt',
  'Tasks' => 'Detyrë',
  'Opportunities' => 'Mundësi:',
  'Products' => 'Produkti',
  'Quotes' => 'Kuota',
  'Bugs' => 'Gabimet',
  'Cases' => 'Rast',
  'Leads' => 'udhëheqje',
  'Project' => 'Projekti',
  'ProjectTask' => 'Detyrat projektuese',
  'Prospects' => 'Synim',
  'KBContents' => 'baza e njohurisë',
  'RevenueLineItems' => 'Rreshti i llojeve të të ardhurave',
);