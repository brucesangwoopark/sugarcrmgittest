<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => '潛在客戶',
  'Qualification' => '資格',
  'Needs Analysis' => '需求分析',
  'Value Proposition' => '價值主張',
  'Id. Decision Makers' => 'ID 決策者',
  'Perception Analysis' => '感知分析資料',
  'Proposal/Price Quote' => '提議/報價',
  'Negotiation/Review' => '交涉檢閱',
  'Closed Won' => '結束並贏得客戶',
  'Closed Lost' => '結束但客戶流失',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);