<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospektering',
  'Qualification' => 'Kvalifisering',
  'Needs Analysis' => 'Behovsanalyse',
  'Value Proposition' => 'Verdimålsetning',
  'Id. Decision Makers' => 'Id. Beslutningstakere',
  'Perception Analysis' => 'Oppfatnings analyse',
  'Proposal/Price Quote' => 'Forslag/Pris tilbud',
  'Negotiation/Review' => 'Forhandling/Gjennomgang',
  'Closed Won' => 'Lukket Vunnet',
  'Closed Lost' => 'Lukket Mistet',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);