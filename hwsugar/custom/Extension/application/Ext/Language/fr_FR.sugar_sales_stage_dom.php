<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospection',
  'Qualification' => 'Qualification',
  'Needs Analysis' => 'Analyse des besoins',
  'Value Proposition' => 'Proposition de valeur',
  'Id. Decision Makers' => 'Ident. Décideurs',
  'Perception Analysis' => 'Analyse de la perception',
  'Proposal/Price Quote' => 'Devis/Proposition',
  'Negotiation/Review' => 'Négociation/Examen',
  'Closed Won' => 'Fermé',
  'Closed Lost' => 'Perdu',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);