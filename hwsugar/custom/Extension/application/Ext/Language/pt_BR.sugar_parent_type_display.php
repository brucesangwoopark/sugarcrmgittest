<?php
 // created: 2016-07-29 21:27:33

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Conta',
  'Contacts' => 'Contato',
  'Tasks' => 'Tarefa',
  'Opportunities' => 'Oportunidade',
  'Products' => 'Item de Linha de Cotação',
  'Quotes' => 'Cotação',
  'Bugs' => 'Bugs',
  'Cases' => 'Ocorrência',
  'Leads' => 'Potencial',
  'Project' => 'Projeto',
  'ProjectTask' => 'Tarefa de Projeto',
  'Prospects' => 'Alvo',
  'KBContents' => 'Base de Conhecimento',
  'RevenueLineItems' => 'Itens da linha de receita',
);