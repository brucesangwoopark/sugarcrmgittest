<?php
 // created: 2016-07-29 21:27:39

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Jäsen',
  'Contacts' => 'Yhteystiedot',
  'Tasks' => 'Tehtävä',
  'Opportunities' => 'Myyntimahdollisuus',
  'Products' => 'Tarjottu tuoterivi',
  'Quotes' => 'Tarjous',
  'Bugs' => 'Bugit',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Project' => 'Projekti',
  'ProjectTask' => 'Projektitehtävä',
  'Prospects' => 'Tavoite',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);