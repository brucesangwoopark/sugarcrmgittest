<?php
 // created: 2017-01-11 09:55:54

$app_list_strings['cancel_reason_list']=array (
  '' => '',
  'Agent Presentation' => 'Agent Presentation',
  'Buy Out' => 'Buy Out',
  'Can Not Install' => 'Can Not Install',
  'Competitor' => 'Competitor',
  'Death' => 'Death',
  'Delinquent Account' => 'Delinquent Account',
  'Dispute' => 'Dispute',
  'Elderly' => 'Elderly',
  'Failed Reaffirmation' => 'Failed Reaffirmation',
  'Filter Transfer' => 'Filter Transfer',
  'Move Out' => 'Move Out',
  'New Occupant' => 'New Occupant',
  'No Banking Information' => 'No Banking Information',
  'No Reason Given' => 'No Reason Given',
  'Online Reputation' => 'Online Reputation',
  'Part I D Term' => 'Part I D Term',
  'Price' => 'Price',
  'Product Confiscation' => 'Product Confiscation',
  'Product or Service Issue' => 'Product or Service Issue',
  'Renewal' => 'Renewal',
  'Spouse Disapproval' => 'Spouse Disapproval',
  'Unauthorized Signee' => 'Unauthorized Signee',
  'Other' => 'Other',
);