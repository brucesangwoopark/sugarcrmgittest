<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospecting',
  'Qualification' => 'Qualification',
  'Needs Analysis' => 'Needs Analysis',
  'Value Proposition' => 'Value Proposition',
  'Id. Decision Makers' => 'Id. Decision Makers',
  'Perception Analysis' => 'Perception Analysis',
  'Proposal/Price Quote' => 'Proposal/Price Quote',
  'Negotiation/Review' => 'Negotiation/Review',
  'Closed Won' => 'Closed',
  'Closed Lost' => 'Closed Lost',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);