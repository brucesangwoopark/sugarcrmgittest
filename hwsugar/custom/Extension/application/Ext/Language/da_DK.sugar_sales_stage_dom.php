<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Forundersøgelse',
  'Qualification' => 'Kvalifikation',
  'Needs Analysis' => 'Behovsanalyse',
  'Value Proposition' => 'Værdiforslag',
  'Id. Decision Makers' => 'Id. beslutningstagere',
  'Perception Analysis' => 'Perceptionsanalyse',
  'Proposal/Price Quote' => 'Forslag/pristilbud',
  'Negotiation/Review' => 'Forhandling/gennemsyn',
  'Closed Won' => 'Lukket vundet',
  'Closed Lost' => 'Lukket mistet',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);