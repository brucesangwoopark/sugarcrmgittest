<?php
 // created: 2017-04-05 12:49:51

$app_list_strings['customer_status_list']=array (
  'Installed' => 'Installed',
  'Cancelled' => 'Cancelled',
  'IBC' => 'IBC',
  'ATR' => 'ATR',
  'Scheduled' => 'Scheduled',
  'Pending' => 'Pending',
  'Delinquent' => 'Delinquent',
  'CNI' => 'CNI',
  'Transferred' => 'Transferred',
);