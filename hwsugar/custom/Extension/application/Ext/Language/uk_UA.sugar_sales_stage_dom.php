<?php
 // created: 2016-10-17 21:07:43

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Зацікавлення',
  'Qualification' => 'Кваліфікація',
  'Needs Analysis' => 'Потребує аналізу',
  'Value Proposition' => 'Пропозиція вартості',
  'Id. Decision Makers' => 'Особи, що приймають рішення Id.',
  'Perception Analysis' => 'Аналіз реакції',
  'Proposal/Price Quote' => 'Пропозиція/Цінова пропозиція',
  'Negotiation/Review' => 'Обговорення умов/Розгляд',
  'Closed Won' => 'Успішно закритий',
  'Closed Lost' => 'Втрачений',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);