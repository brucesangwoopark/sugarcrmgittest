<?php
 // created: 2016-10-17 21:07:42

$app_list_strings['sales_stage_dom']=array (
  'Prospecting' => 'Prospektion',
  'Qualification' => 'Qualifikation',
  'Needs Analysis' => 'Bedarfserhebung',
  'Value Proposition' => 'Richtangebot',
  'Id. Decision Makers' => 'Entscheider ident.',
  'Perception Analysis' => 'Wahrnehmungsanalyse',
  'Proposal/Price Quote' => 'Preisangebot',
  'Negotiation/Review' => 'Verhandlung/Überarbeitung',
  'Closed Won' => 'Abgeschlossen',
  'Closed Lost' => 'Geschlossen/Verloren',
  'Pre Install' => 'Pre Install',
  'Installed' => 'Installed',
  'Inactive' => 'Inactive',
);