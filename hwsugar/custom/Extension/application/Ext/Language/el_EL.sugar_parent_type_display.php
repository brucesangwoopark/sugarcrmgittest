<?php
 // created: 2016-07-29 21:27:17

$app_list_strings['parent_type_display']=array (
  'Accounts' => 'Λογαριασμός',
  'Contacts' => 'Επαφή',
  'Tasks' => 'Εργασία',
  'Opportunities' => 'Ευκαιρία',
  'Products' => 'Γραμμή Εισηγμένων Ειδών',
  'Quotes' => 'Προσφορά',
  'Bugs' => 'Σφάλματα',
  'Cases' => 'Υπόθεση',
  'Leads' => 'Δυνητικός Πελάτης',
  'Project' => 'Έργο',
  'ProjectTask' => 'Εργασία Έργου',
  'Prospects' => 'Στόχος',
  'KBContents' => 'Βάση Γνώσεων',
  'RevenueLineItems' => 'Γραμμή Εσόδων',
);