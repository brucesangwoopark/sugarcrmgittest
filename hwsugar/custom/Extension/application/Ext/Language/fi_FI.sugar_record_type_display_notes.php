<?php
 // created: 2016-07-29 21:27:39

$app_list_strings['record_type_display_notes']=array (
  'Accounts' => 'Jäsen',
  'Contacts' => 'Yhteystiedot',
  'Opportunities' => 'Myyntimahdollisuus',
  'Tasks' => 'Tehtävä',
  'ProductTemplates' => 'Tuotekatalogi',
  'Quotes' => 'Tarjous',
  'Products' => 'Tarjottu tuoterivi',
  'Contracts' => 'Sopimus',
  'Emails' => 'Mikä tahansa sähköposti',
  'Bugs' => 'Bugi',
  'Project' => 'Projekti',
  'ProjectTask' => 'Projektitehtävä',
  'Prospects' => 'Tavoite',
  'Cases' => 'Palvelupyyntö',
  'Leads' => 'Liidi',
  'Meetings' => 'Kokous',
  'Calls' => 'Puhelu',
  'KBContents' => 'Tietämyskanta',
  'RevenueLineItems' => 'Tuoterivit',
);