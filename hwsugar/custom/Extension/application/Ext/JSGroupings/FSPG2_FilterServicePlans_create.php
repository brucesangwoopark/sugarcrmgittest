<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-15
 * Time: 오전 11:03
 */

foreach ($js_groupings as $key => $groupings) {
    foreach ($groupings as $file => $target) {
        //if the target grouping is found
        if ($target == 'include/javascript/sugar_grp7.min.js') {
            //append the custom JavaScript file
            $js_groupings[$key]['custom/javascript/FSPG2_FilterServicePlans_create.js'] = 'include/javascript/sugar_grp7.min.js';
        }
        break;
    }
}