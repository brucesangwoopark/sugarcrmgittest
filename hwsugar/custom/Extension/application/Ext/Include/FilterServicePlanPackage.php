<?php 
 //WARNING: The contents of this file are auto-generated
$beanList['FSPG2_FilterServiceDetails'] = 'FSPG2_FilterServiceDetails';
$beanFiles['FSPG2_FilterServiceDetails'] = 'modules/FSPG2_FilterServiceDetails/FSPG2_FilterServiceDetails.php';
$modules_exempt_from_availability_check['FSPG2_FilterServiceDetails'] = 'FSPG2_FilterServiceDetails';
$report_include_modules['FSPG2_FilterServiceDetails'] = 'FSPG2_FilterServiceDetails';
$modInvisList[] = 'FSPG2_FilterServiceDetails';
$beanList['FSPG2_FilterServicePackages'] = 'FSPG2_FilterServicePackages';
$beanFiles['FSPG2_FilterServicePackages'] = 'modules/FSPG2_FilterServicePackages/FSPG2_FilterServicePackages.php';
$modules_exempt_from_availability_check['FSPG2_FilterServicePackages'] = 'FSPG2_FilterServicePackages';
$report_include_modules['FSPG2_FilterServicePackages'] = 'FSPG2_FilterServicePackages';
$modInvisList[] = 'FSPG2_FilterServicePackages';
$beanList['FSPG2_FilterServicePlans'] = 'FSPG2_FilterServicePlans';
$beanFiles['FSPG2_FilterServicePlans'] = 'modules/FSPG2_FilterServicePlans/FSPG2_FilterServicePlans.php';
$modules_exempt_from_availability_check['FSPG2_FilterServicePlans'] = 'FSPG2_FilterServicePlans';
$report_include_modules['FSPG2_FilterServicePlans'] = 'FSPG2_FilterServicePlans';
$moduleList[] = 'FSPG2_FilterServicePlans';
$beanList['FSPG2_FilterTypes'] = 'FSPG2_FilterTypes';
$beanFiles['FSPG2_FilterTypes'] = 'modules/FSPG2_FilterTypes/FSPG2_FilterTypes.php';
$modules_exempt_from_availability_check['FSPG2_FilterTypes'] = 'FSPG2_FilterTypes';
$report_include_modules['FSPG2_FilterTypes'] = 'FSPG2_FilterTypes';
$modInvisList[] = 'FSPG2_FilterTypes';

?>