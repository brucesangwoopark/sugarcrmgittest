<?php
// created: 2017-07-31 15:37:11
$viewdefs['abcde_Jobs']['base']['view']['subpanel-for-fspg2_filterservicedetails-abcde_jobs'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'external_key_c',
          'label' => 'LBL_EXTERNAL_KEY',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'job_type_c',
          'label' => 'LBL_JOB_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'status_c',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'cnc_reason_c',
          'label' => 'LBL_CNC_REASON',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'tech',
          'label' => 'LBL_TECH',
          'enabled' => true,
          'id' => 'SUPKG_SYSTEMUSERS_ID_C',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'tech_arrived_c',
          'label' => 'LBL_TECH_ARRIVED',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => true,
        ),
        9 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);