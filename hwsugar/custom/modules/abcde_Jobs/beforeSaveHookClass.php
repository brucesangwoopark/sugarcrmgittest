<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class JobsBeforeSaveHook
{
    function beforeSave($bean, $event, $arguments)
    {
        $id = $bean->id;
        //for logging purposes
        $module = "Jobs";
        $hook = "before_save";
        $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: BEGINNING HOOK, Job id: ' . $id . ', ' . date("Y-m-d H:i:s"));

        // is this a new job?
        if ($this->checkNewJob($id)) {
            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: New job creating, ' . date("Y-m-d H:i:s"));
            // pull RLIs linked to this job
            $bean->load_relationship('abcde_jobs_revenuelineitems_1');
            $rli_beans = $bean->abcde_jobs_revenuelineitems_1->getBeans();
            $num_of_rlis = count($rli_beans);

            // prevent to create installation job for installed rli
            // is this an installation job?
            if (strpos($bean->job_type_c, 'Install') !== false) {
                $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Job Type => ' . $bean->job_type_c . ', ' . date("Y-m-d H:i:s"));
                $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Number of RLIs => ' . $num_of_rlis . ', ' . date("Y-m-d H:i:s"));
                if ($num_of_rlis > 0) {
                    foreach ($rli_beans as $rli_bean) {
                        if ($rli_bean->install_c === '1') {
                            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Installed RLI found!!!! => ' . $rli_bean->id . ', ' . date("Y-m-d H:i:s"));
                            require_once 'custom/api_exceptions/InvalidInstallationJobException.php';
                            throw new InvalidInstallationJobException();
                        }
                    }
                }
            }

            // prevent to create uninstallation job for uninstalled rli
            // is this an uninstallation job?
            if (strpos($bean->job_type_c, 'Uninstall') !== false) {
                $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Job Type => ' . $bean->job_type_c . ', ' . date("Y-m-d H:i:s"));
                $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Number of RLIs => ' . $num_of_rlis . ', ' . date("Y-m-d H:i:s"));
                if ($num_of_rlis > 0) {
                    foreach ($rli_beans as $rli_bean) {
                        if ($rli_bean->uninstall_c === '1') {
                            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Uninstalled RLI found!!!! => ' . $rli_bean->id . ', ' . date("Y-m-d H:i:s"));
                            require_once 'custom/api_exceptions/InvalidUninstallationJobException.php';
                            throw new InvalidUninstallationJobException();
                        }
                    }
                }
            }
        }

        try {

            //check the team from Account
            $team_id = 1; //setting default
            $query = 'SELECT a.team_id as team_id
					FROM accounts a
					JOIN accounts_abcde_jobs_1_c aj ON aj.accounts_abcde_jobs_1accounts_ida = a.id
					WHERE aj.accounts_abcde_jobs_1abcde_jobs_idb = "' . $id . '"';
            $result = $GLOBALS['db']->query($query);
            while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
                $team_id = $row['team_id'];
            }
            $bean->team_id = $team_id;

        }//END OF try
        catch (Exception $e2) {
            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: FAILED, ' . date("Y-m-d H:i:s"));
        }
        $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: ENDING HOOK, ' . date("Y-m-d H:i:s"));
    }

    function checkNewJob($id)
    {
        $query = 'select * from abcde_jobs where id = \'' . $id . '\'';
        $result = $GLOBALS['db']->query($query);
        if ($result->num_rows > 0) {
            return false;
        }

        return true;
    }
}

?>
