<?php
// created: 2017-08-30 15:15:26
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'status_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
);