<?php
// created: 2017-07-31 15:37:10
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'external_key_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_EXTERNAL_KEY',
    'width' => '10%',
    'default' => true,
  ),
  'job_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_JOB_TYPE',
    'width' => '10%',
  ),
  'status_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'cnc_reason_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CNC_REASON',
    'width' => '10%',
  ),
  'tech' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_TECH',
    'id' => 'SUPKG_SYSTEMUSERS_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'supkg_SystemUsers',
    'target_record_key' => 'supkg_systemusers_id_c',
  ),
  'tech_arrived_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_TECH_ARRIVED',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'readonly' => true,
    'vname' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'created_by',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);