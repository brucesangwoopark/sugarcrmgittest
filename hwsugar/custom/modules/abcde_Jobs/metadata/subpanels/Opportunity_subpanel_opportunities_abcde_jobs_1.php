<?php
// created: 2016-11-15 17:18:20
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'external_key_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_EXTERNAL_KEY',
    'width' => '10%',
  ),
  'job_type_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_JOB_TYPE',
    'width' => '10%',
  ),
  'status_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);