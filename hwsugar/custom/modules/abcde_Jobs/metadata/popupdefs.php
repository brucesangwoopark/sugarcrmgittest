<?php
$popupMeta = array (
    'moduleMain' => 'abcde_Jobs',
    'varName' => 'abcde_Jobs',
    'orderBy' => 'abcde_jobs.name',
    'whereClauses' => array (
  'name' => 'abcde_jobs.name',
),
    'searchInputs' => array (
  0 => 'abcde_jobs_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'EXTERNAL_KEY_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_EXTERNAL_KEY',
    'width' => '10%',
  ),
  'JOB_TYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_JOB_TYPE',
    'width' => '10%',
  ),
  'STATUS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
),
);
