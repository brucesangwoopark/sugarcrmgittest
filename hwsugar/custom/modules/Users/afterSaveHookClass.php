<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveUserHook {
	
	static $already_ran = false;//to ensure hook only executes once
	
	function afterSave($bean, $event, $arguments)
	{
		if(self::$already_ran == true) return;
		self::$already_ran = true;
		
		//defining default role
		$default_role = "8e04b2d6-899f-11e6-b6aa-06b20b8677ed";
		
		//for logging purposes
		$module = "Users";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, User id: '.$id.', '.date("Y-m-d H:i:s"));
		
			
		try{
			
			// Only execute on new User record creation
			
			if (empty($bean->fetched_row['id'])) {
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding user to default role: '.$default_role.', '.date("Y-m-d H:i:s"));
				$role = new ACLRole();
				$role->set_relationship(
					'acl_roles_users', 
					array('role_id' => $default_role, 'user_id' => $bean->id), 
					false
					);
			}
		
		}
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED. '.date("Y-m-d H:i:s"));
		}
		
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
	}
}
?>