<?php
// created: 2017-07-27 09:12:54
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'filterchangedate' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_FILTERCHANGEDATE',
    'width' => '10%',
    'default' => true,
  ),
  'trackingnumber' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_TRACKINGNUMBER',
    'width' => '10%',
  ),
  'abcde_jobs_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ABCDE_JOBS_NAME',
    'id' => 'ABCDE_JOBS_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'abcde_Jobs',
    'target_record_key' => 'abcde_jobs_id',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
);