<?php
// created: 2017-07-27 09:12:55
$viewdefs['FSPG2_FilterServiceDetails']['base']['view']['subpanel-for-fspg2_filterserviceplans-fspg2_filterservicedetails'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'filterchangedate',
          'label' => 'LBL_FILTERCHANGEDATE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'trackingnumber',
          'label' => 'LBL_TRACKINGNUMBER',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'abcde_jobs_name',
          'label' => 'LBL_ABCDE_JOBS_NAME',
          'enabled' => true,
          'id' => 'ABCDE_JOBS_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);