<?php
$module_name = 'FSPG2_FilterServiceDetails';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'trackingnumber',
                'label' => 'LBL_TRACKINGNUMBER',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'fspg2_filterserviceplans_name',
                'label' => 'LBL_FSPG2_FILTERSERVICEPLANS_NAME',
                'enabled' => true,
                'id' => 'FSPG2_FILTERSERVICEPLANS_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'abcde_jobs_name',
                'label' => 'LBL_ABCDE_JOBS_NAME',
                'enabled' => true,
                'id' => 'ABCDE_JOBS_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
