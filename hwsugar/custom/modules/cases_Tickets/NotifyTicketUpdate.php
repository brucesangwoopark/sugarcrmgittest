<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-11-10
 * Time: 오전 11:00
 */

class NotifyTicketUpdate
{
    function afterSave($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal('NotifyTicketUpdate');

        require_once 'custom/modules/cases_Tickets/SendMail.php';
        $sendMail = new SendMail();
        $sendMail->send($bean);
    }
}