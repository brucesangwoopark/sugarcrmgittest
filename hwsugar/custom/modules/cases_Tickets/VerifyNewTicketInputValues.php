<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-26
 * Time: 오후 8:48
 */

class VerifyNewTicketInputValues
{
    function beforeSave($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal('VerifyNewTicketInputValues');

        $rli_bean = null;
        $account_bean = null;

        if (gettype($bean->cases_tickets_revenuelineitems) === 'object') {
            $rli_beans = $bean->cases_tickets_revenuelineitems->getBeans();
            if (count($rli_beans) > 0) {
                $rli_bean = $this->getFirstElement($rli_beans);
            }
        } else {
            if ($bean->cases_tickets_revenuelineitems['id'] !== '') {
                $rli_bean = BeanFactory::getBean('RevenueLineItems', $bean->cases_tickets_revenuelineitems['id'], array('disable_row_level_security' => true));
            }
        }

        if (gettype($bean->cases_tickets_accounts) === 'object') {
            $account_beans = $bean->cases_tickets_accounts->getBeans();
            if (count($account_beans) > 0) {
                $account_bean = $this->getFirstElement($account_beans);
            }
        } else {
            if ($bean->cases_tickets_accounts['id'] !== '') {
                $account_bean = BeanFactory::getBean('Accounts', $bean->cases_tickets_accounts['id'], array('disable_row_level_security' => true));
            }
        }

        if ($account_bean === null) {
            require_once 'custom/api_exceptions/AccountRLINotPlaced.php';
            throw new AccountRLINotPlaced();
        }

        if ($rli_bean !== null) {
            if ($rli_bean->account_id !== $account_bean->id) {
                require_once 'custom/api_exceptions/WrongAccountRLICombination.php';
                throw new WrongAccountRLICombination();
            }
        }
    }

    function getFirstElement($ar)
    {
        foreach ($ar as $key => $value) {
            return $value;
        }

        return null;
    }
}