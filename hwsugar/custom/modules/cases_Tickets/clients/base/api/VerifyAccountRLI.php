<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-27
 * Time: 오후 12:07
 */

class VerifyAccountRLI extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'verifyaccountrli' => array(
                'reqType' => 'POST',
                'path' => array('cases_Tickets', 'verifyaccountrli'),
                'method' => 'run',
                'shortHelp' => 'Verify Account and RLI in JSON',
            )
        );
    }

    public function run($api, $args)
    {
        if (isset($args['rli_id']) && !empty($args['rli_id']) && isset($args['account_id']) && !empty($args['account_id'])) {
            $account_id = $args['account_id'];
            $rli_id = $args['rli_id'];

            require_once('include/SugarQuery/SugarQuery.php');
            $sugarQuery = new SugarQuery();
            $sugarQuery->from(BeanFactory::newBean('RevenueLineItems'));
            $sugarQuery->where()->equals('id', $rli_id)->equals('account_id', $account_id);
            $result = $sugarQuery->execute();
            if (count($result) > 0) {
                return array(
                    'result' => 'ok'
                );
            }

            return array(
                'result' => 'no'
            );
        }

        return array(
            'result' => 'error',
            'message' => 'no parameter'
        );
    }
}