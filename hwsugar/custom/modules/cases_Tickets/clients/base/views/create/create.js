({
    extendsFrom: 'CreateView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.on('render', this.pullAccount, this);

        this.model.addValidationTask('check_account_placed', _.bind(this.checkAccountPlaced, this));
        this.model.addValidationTask('check_account_rli', _.bind(this.checkAccountRLICombination, this));
    },

    checkAccountPlaced: function (fields, errors, callback) {
        if (this.model.get(fields.cases_tickets_accounts_name.id_name) === '' || this.model.get(fields.cases_tickets_accounts_name.id_name) === undefined) {
            errors[fields.cases_tickets_accounts_name.name] = errors[fields.cases_tickets_accounts_name.name] || {};
            errors[fields.cases_tickets_accounts_name.name].required = true;
            app.alert.show('cases_tickets_no_account_selected', {
                level: 'error',
                messages: '<b>Account needs to be selected!</b>'
            });
        }

        callback(null, fields, errors);
    },

    checkAccountRLICombination: function (fields, errors, callback) {
        if ((this.model.get(fields.cases_tickets_accounts_name.id_name) !== '' && this.model.get(fields.cases_tickets_accounts_name.id_name) !== undefined) &&
            (this.model.get(fields.cases_tickets_revenuelineitems_name.id_name) !== '' && this.model.get(fields.cases_tickets_revenuelineitems_name.id_name) !== undefined)) {

            var account_id = this.model.get(fields.cases_tickets_accounts_name.id_name);
            var rli_id = this.model.get(fields.cases_tickets_revenuelineitems_name.id_name);

            if (!this.verifyAccountRLI(account_id, rli_id)) {
                errors[fields.cases_tickets_accounts_name.name] = errors[fields.cases_tickets_accounts_name.name] || {};
                errors[fields.cases_tickets_accounts_name.name].check_Revenue_Line_Item_selected = true;

                errors[fields.cases_tickets_revenuelineitems_name.name] = errors[fields.cases_tickets_revenuelineitems_name.name] || {};
                errors[fields.cases_tickets_revenuelineitems_name.name].check_Account_selected = true;

                app.alert.show('cases_tickets_no_account_selected', {
                    level: 'error',
                    messages: '<b>Chosen Account and Revenue Line Item are not related.<br>Please select right Account or Revenue Line Item.</b>'
                });
            }
        }

        callback(null, fields, errors);
    },

    pullAccount: function () {
        var access_token = this.getAccessToken();
        var rli_id = this.model.get('cases_tickets_revenuelineitemsrevenuelineitems_ida');
        if (rli_id === undefined) {
            return;
        }

        var account_id = null;
        var account_name = null;

        $.ajax({
            url: '/hwsugar/rest/v10/RevenueLineItems/account',
            type: 'POST',
            data: {
                rli_id: rli_id
            },
            headers: {
                'OAuth-Token': access_token
            },
            dataType: 'json',
            async: false,
            success: function(result) {
                account_id = result.account_id;
                account_name = result.account_name;
            },
            error: function(result) {
                console.log('pullAccount Error!!');
            }
        });

        this.model.set('cases_tickets_accountsaccounts_ida', account_id);
        this.model.set('cases_tickets_accounts_name', account_name);
    },

    verifyAccountRLI: function (account_id, rli_id) {
        var access_token = this.getAccessToken();
        var ret_val = false;
        $.ajax({
            url: '/hwsugar/rest/v10/cases_Tickets/verifyaccountrli',
            type: 'POST',
            data: {
                account_id: account_id,
                rli_id: rli_id
            },
            headers: {
                'OAuth-Token': access_token
            },
            dataType: 'json',
            async: false,
            success: function(result) {
                if (result.result === 'ok') {
                    ret_val = true;
                }
            },
            error: function(result) {
                console.log('pullAccount Error!!');
            }
        });

        return ret_val;
    },

    getAccessToken: function () {
        var access_token = localStorage.getItem('prod:SugarCRM:AuthAccessToken');
        if (access_token !== null) {
            access_token = access_token.replace("'", "");
            access_token = access_token.replace("'", "");
        }
        return access_token;
    }
})