<?php
$module_name = 'cases_Tickets';
$_module_name = 'cases_tickets';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'cases_tickets_number',
                'label' => 'LBL_NUMBER',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_SUBJECT',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'priority',
                'label' => 'LBL_PRIORITY',
                'default' => true,
                'enabled' => true,
              ),
              4 => 
              array (
                'name' => 'category',
                'label' => 'LBL_CATEGORY',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'assignedto',
                'label' => 'LBL_ASSIGNEDTO',
                'enabled' => true,
                'id' => 'USER_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              7 => 
              array (
                'name' => 'ccwhenresolved',
                'label' => 'LBL_CCWHENRESOLVED',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'closeddate',
                'label' => 'LBL_CLOSEDDATE',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
