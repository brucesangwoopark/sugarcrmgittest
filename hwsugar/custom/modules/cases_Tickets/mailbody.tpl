<table style="border:none;border-collapse:collapse;width: 550px;">
    <tbody>
    <tr style="height:18.75pt">
        <td style="vertical-align:middle;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:18pt;font-family: Roboto Condensed; color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    Ticket Update Notification
                </span>
            </p>
        </td>
        <td style="vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.38;margin-top:0pt; margin-bottom:0pt; text-align:right">
                <span style="font-size:11pt;font-family: Roboto Condensed;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    <img src="https://lh6.googleusercontent.com/eVwRv7pGq3-_-P_YvlqkbFjmOrnIGkschx6ydx7AdpzbChHKdkvOmz6a0-NCBcemmt5GYaaQTPnIQ0YU-ixnYy2ZQI0CMwhDO_NyziSvapj4J6VSERgREu85Lpn_eZoPJ0sXvbvc" width="203" height="31" style="border: none; transform: rotate(0rad);" alt="homewater.png">
                </span>
            </p>
        </td>
    </tr>
    </tbody>
</table>
<table style="border:none;border-collapse:collapse;width: 550px; margin-top: 20px;">
    <tbody>
    <tr style="height:0pt">
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt;">
                <span style="font-size:11pt;font-family: Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Ticket Number
                </span>
            </p>
        </td>
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:12pt;font-family: Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_number}
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:0pt">
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt;">
                <span style="font-size:11pt;font-family: Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Ticket Name
                </span>
            </p>
        </td>
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:12pt;font-family: Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_name}
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:0pt">
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:11pt;font-family:Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Status
                </span>
            </p>
        </td>
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:12pt;font-family:Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_status}
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:0pt">
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:11pt;font-family:Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Updated
                </span>
            </p>
        </td>
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:12pt;font-family:Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_date_updated} by {$ticket_updater_full_name}
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:21pt">
        <td colspan="2"
            style="border-left:0.5pt solid rgb(204,204,204); border-right:0.5pt solid rgb(204,204,204); border-top:0.5pt solid rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:11pt;font-family:Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Message
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:148.5pt">
        <td colspan="2"
            style="border-left:0.5pt solid rgb(204,204,204);border-right:0.5pt solid rgb(204,204,204);border-bottom:0.5pt solid rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2; margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:14pt;font-family:Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_body}
                </span>
            </p>
        </td>
    </tr>
    <tr style="height:0pt">
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:11pt;font-family:Calibri;color:rgb(153,153,153);background-color:transparent;vertical-align:baseline;">
                    Opened by
                </span>
            </p>
        </td>
        <td style="border-width:0.5pt;border-style:solid;border-color:rgb(204,204,204);vertical-align:top;padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt">
                <span style="font-size:12pt;font-family:Calibri;color:rgb(0,0,0);background-color:transparent;vertical-align:baseline;">
                    {$ticket_opener_full_name} ({$ticket_opener_id})
                </span>
            </p>
        </td>
    </tr>
    </tbody>
</table>
<table style="border:none; border-collapse: collapse; width: 550px; margin-top: 20px;">
    <tbody>
    <tr style="height:33pt">
        <td style="vertical-align:middle;padding:5pt; width: 170px;"><br></td>
        <td style="vertical-align:middle;background-color:rgb(0,175,224);padding:5pt">
            <p dir="ltr" style="line-height:1.2;margin-top:0pt;margin-bottom:0pt;text-align:center">
                <a href="{$ticket_link}" style="text-decoration-line:none">
                    <span style="font-size:14pt;font-family:Calibri;background-color:transparent;font-weight:700;text-decoration-line:underline;vertical-align:baseline;">
                        GET DETAILS
                    </span>
                </a>
            </p>
        </td>
        <td style="vertical-align:middle; padding:5pt; width: 170px;"><br></td>
    </tr>
    </tbody>
</table>