<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-26
 * Time: 오후 8:44
 */

class CompleteSearchableField
{
    function afterSave($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal('CompleteSearchableField');

        $account_name = '';

        if (gettype($bean->cases_tickets_accounts) === 'object') {
            $account_beans = $bean->cases_tickets_accounts->getBeans();
            if (count($account_beans) > 0) {
                $account_bean = $this->getFirstElement($account_beans);
                $account_name = str_replace(' - Account', '', $account_bean->name);
            }
        } else {
            $account_name = str_replace(' - Account', '', $bean->cases_tickets_accounts['name']);
        }

        $created_by_name = $this->getUserFullName($bean->created_by);
        $modified_by_name = $this->getUserFullName($bean->created_by);
        $assignedto_name = '';
        if ($bean->status !== 'New') {
            $user_bean = BeanFactory::getBean('Users', $bean->user_id_c, array('disable_row_level_security' => true));
            $assignedto_name = "{$user_bean->first_name} {$user_bean->last_name}";
        }

        $closeddate = '';
        if ($bean->status === 'Closed') {
            if ($bean->closeddate === '') {
                $closeddate = $bean->date_modified;
            } else {
                $closeddate = $bean->closeddate;
            }
        }

        $query = "
            update cases_tickets
            set 
                ccreatedby = '{$created_by_name}',
                custassignedto = '{$assignedto_name}',
                modifiedbyc = '{$modified_by_name}',
                caccountname = '{$account_name}',
                closeddate = '{$closeddate}'
            where id = '{$bean->id}';
        ";
        $GLOBALS['db']->query($query);
    }

    function getFirstElement($ar)
    {
        foreach ($ar as $key => $value) {
            return $value;
        }

        return null;
    }

    function getUserFullName($user_id)
    {
        return BeanFactory::getBean('Users', $user_id)->full_name;
    }
}