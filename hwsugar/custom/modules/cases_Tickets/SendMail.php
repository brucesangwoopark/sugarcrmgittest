<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-11-10
 * Time: 오후 4:46
 */

class SendMail
{
    function send($ticket_bean, $replaced_mailbody = null)
    {
        $sugar_smarty = new Sugar_Smarty();

        $sugar_smarty->assign('ticket_name', $ticket_bean->name);
        $ticket_number = $this->getTicketNumber($ticket_bean);
        $sugar_smarty->assign('ticket_number', $ticket_number);
        $sugar_smarty->assign('ticket_status', $ticket_bean->status);

        $user_bean = BeanFactory::getBean('Users', $ticket_bean->modified_user_id);
        $sugar_smarty->assign('ticket_date_updated', date('Y-m-d H:i:s'));
        $sugar_smarty->assign('ticket_updater_full_name', $user_bean->full_name);

        if ($replaced_mailbody === null) {
            $description = $ticket_bean->description;
        } else {
            $description = $replaced_mailbody;
        }

        $description = str_replace('<', "&lt;", $description);
        $description = str_replace('>', "&gt;", $description);
        $description = str_replace('"', "&quot;", $description);
        $description = str_replace(' ', "&nbsp;", $description);
        $description = str_replace("\n", "<br>", $description);
        $sugar_smarty->assign('ticket_body', $description);

        $user_bean = BeanFactory::getBean('Users', $ticket_bean->created_by);
        $sugar_smarty->assign('ticket_opener_full_name', $user_bean->full_name);
        $sugar_smarty->assign('ticket_opener_id', $user_bean->user_name);

        $link = $GLOBALS['sugar_config']['site_url'] . '/#' . $ticket_bean->module_name . '/' . $ticket_bean->id;
        $sugar_smarty->assign('ticket_link', $link);

        $mail_body = $sugar_smarty->fetch('custom/modules/cases_Tickets/mailbody.tpl');

        $mailer = MailerFactory::getSystemDefaultMailer();
        $mailer->setSubject("[Ticket #{$ticket_number}] {$ticket_bean->name}");
        $mailer->setHtmlBody($mail_body);
        $mailer->addRecipientsTo(new EmailIdentity($user_bean->email1, $user_bean->full_name));
        //$mailer->addRecipientsCc(new EmailIdentity('sugar.tickets@homewater.com', 'SugarTicketUpdate'));
        $mailer->addRecipientsCc(new EmailIdentity('ekic1001@gmail.com', 'Bruce Park'));
        if ($mailer->send()) {
            $GLOBALS['log']->fatal('Notification Email sent');
        } else {
            $GLOBALS['log']->fatal('ERROR: Mail sending failed!');
        }
    }

    function getTicketNumber($ticket_bean)
    {
        $query = "
            select *
            from cases_tickets
            where id = '{$ticket_bean->id}';";
        $result = $GLOBALS['db']->query($query);
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $ticket_number = $row ['cases_tickets_number'];
        }

        return $ticket_number;
    }
}