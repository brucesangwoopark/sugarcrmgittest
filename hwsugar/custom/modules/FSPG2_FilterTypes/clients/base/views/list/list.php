<?php
$module_name = 'FSPG2_FilterTypes';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'length',
                'label' => 'LBL_LENGTH',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'width',
                'label' => 'LBL_WIDTH',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'height',
                'label' => 'LBL_HEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'weight',
                'label' => 'LBL_WEIGHT',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'price',
                'label' => 'LBL_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'weekly_mail_out',
                'label' => 'LBL_WEEKLY_MAIL_OUT',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'shipping_price',
                'label' => 'LBL_SHIPPING_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
