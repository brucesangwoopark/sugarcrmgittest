<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class BeforeSaveLeadsHook {
	
	static $already_ran = false;//uncomment if you wish the hook to ONLY execute once
	
	function beforeSave($bean, $event, $arguments)
	{
		if(self::$already_ran == true) return;//uncomment if you wish the hook to ONLY execute once
		self::$already_ran = true;//uncomment if you wish the hook to ONLY execute once
		
		$date_entered = $bean->date_entered;
		$date_modified = $bean->date_modified;
    
		$module = "Leads";
		$hook = "before_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Lead id: '.$id.', '.date("Y-m-d H:i:s"));
		
		if ($date_entered === $date_modified){ // only new Leads, not updates
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Generating new RA rumber, '.date("Y-m-d H:i:s"));
			/*
			$query =  "SELECT greatest(MAX(oc.ra_num_c),MAX(lc.ra_num_c)) as max_ra_num
						FROM opportunities_cstm oc, leads_cstm lc";
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
			  $max_ra_num = $row['max_ra_num'];
			}
			if ($max_ra_num == null) { //in case of first entry
			  $max_ra_num = 300000;
			}
			$ra_num = $max_ra_num + 1;
			$bean->ra_num_c = $ra_num;
			*/
			
			//insert record into ra_num table to generate new ra_num (it is auto-incrementing)
			$query = 'INSERT INTO ra_num SET id = "'.$id.'"';
			$GLOBALS['db']->query($query);
			//select from ra_num to retrieve ra number
			$query = 'SELECT ra FROM ra_num WHERE id = "'.$id.'"';
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
			  $ra_num = $row['ra'];
			}
			$bean->ra_num_c = $ra_num;
			$bean->converted = 0; //ensuring Lead in unconverted in case of Lead copy
			
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: New RA rumber: '.$ra_num.', '.date("Y-m-d H:i:s"));
			
			//auto-populate the CSR field with the current user
			global $current_user;
			$csr = $current_user->id;
			$bean->user_id_c = $csr;
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Auto-populating CSR field with: '.$csr.', '.date("Y-m-d H:i:s"));
			
		 }
		 
		 //update account and opportunity names
		 $bean->account_name = $bean->first_name.' '.$bean->last_name.' - Account';
		 $bean->opportunity_name = $bean->first_name.' '.$bean->last_name.' - Opportunity';
		 $GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
		 
		 
		 //find the correct regional team based on the office of the IC
		$team_id = 1; //setting default to Global Team
		$query = 'SELECT t.id as team_id
				FROM teams t
				JOIN supkg_systemusers_cstm suc ON suc.office_c = t.name
				WHERE suc.id_c = "'.$bean->supkg_systemusers_id_c.'"';
		//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Query: '.$query.', '.date("Y-m-d H:i:s"));
		$result = $GLOBALS['db']->query($query);
		while($row = $GLOBALS['db']->fetchByAssoc($result))
		{
			$team_id = $row['team_id'];
		}
		$bean->team_id = $team_id;
   }
}
?>
