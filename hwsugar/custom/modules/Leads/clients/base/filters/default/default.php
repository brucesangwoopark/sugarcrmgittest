<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['Leads']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'first_name' => array(),
        'last_name' => array(),
        'primary_address_street' => array(),
        'primary_address_city' => array(),
        'primary_address_state' => array(),
        'primary_address_postalcode' => array(),
        'primary_address_country' => array(),
        'ra_num_c' => array(),
    ),
    
    'quicksearch_field' => 
    array (
    0 => 'first_name',
    1 => 'last_name',
    2 => 'primary_address_street',
    3 => 'primary_address_city',
    4 => 'primary_address_state',
    5 => 'primary_address_postalcode',
    6 => 'primary_address_country',
    7 => 'ra_num_c',
    ), 
    'quicksearch_priority' => 7,
);
?>