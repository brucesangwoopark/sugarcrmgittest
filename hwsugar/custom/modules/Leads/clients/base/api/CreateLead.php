<?php

/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-17
 * Time: 오전 10:37
 */
class CreateLead extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'createLeadVeriscan' => array(
                'reqType' => 'POST',
                'path' => array('createleadveriscan'),
                'pathVars' => array('', ''),
                'method' => 'createLeadVeriscan',
                'shortHelp' => 'Creating a new lead through Veriscan',
                'longHelp' => '',
            )
        );
    }

    function createLeadVeriscan($api, $args)
    {
        $GLOBALS['log']->fatal('[VeriscanAPI] createLeadVeriscan invoked ' . date("Y-m-d H:i:s"));
        $GLOBALS['log']->fatal('[VeriscanAPI] $args => ' . print_r($args, true) . ' ' . date("Y-m-d H:i:s"));

        $email = null;
        $address_street = null;
        $address_unit = null;
        $address_city = null;
        $address_state = null;
        $address_country = null;
        $address_postalcode = null;
        $home_phone_number = null;

        $data = null;
        if (empty($args['Data'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] Data index is not defined ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'Data index is not defined'
            );
        }

        $data = $args['Data'];
        if (empty($data['FirstName'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] FirstName index is not defined ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'FirstName index is not defined'
            );
        }

        if (empty($data['LastName'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] LastName index is not defined ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'LastName index is not defined'
            );
        }

        // DeviceLogin value is agent's badge number
        if (empty($data['DeviceLogin'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] DeviceLogin is not defined ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'DeviceLogin is not defined'
            );
        }

        if (empty($data['BirthDate'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] BirthDate is not defined ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'BirthDate is not defined'
            );
        }

        $birthday = explode('T', $data['BirthDate']); // expecting YYYY-MM-DDThh:mm:ss
        if (count($birthday) == 2) { // $birthdate[0] = date, $birthdate[1] = time
            $birthday = $birthday[0];
        } else {
            return array(
                'result' => 'error',
                'message' => 'Invalid BirthDate format'
            );
        }

        $home_phone_number = $data['Phone'];

        $agent_badge = $data['DeviceLogin'];
        $first_name = $data['FirstName'];
        $last_name = $data['LastName'];

        if (empty($data['Address'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] no address error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'no address'
            );
        }

        if (empty($data['City'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] no City error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'no City'
            );
        }

        if (empty($data['State'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] no State error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'no State'
            );
        }

        if (empty($data['Country'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] no Country error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'no Country'
            );
        }

        if (empty($data['PostalCode'])) {
            $GLOBALS['log']->fatal('[VeriscanAPI] no PostalCode error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'no PostalCode'
            );
        }

        $address_street = $data['Address'];
        $address_city = $data['City'];
        $address_state = $data['State'];
        $address_country = null;
        if (in_array(strtolower($data['CountryCode']), ['usa', 'us', 'united states', 'unitedstates'])) {
            $address_country = 'United States';
        }
        if (in_array(strtolower($data['CountryCode']), ['ca', 'can', 'canada'])) {
            $address_country = 'Canada';
        }
        if ($address_country === null) {
            return array(
                'result' => 'error',
                'message' => 'country code is not valid. Received country code => ' . $data['CountryCode']
            );
        }

        $address_postalcode = $data['PostalCode'];

        $email = $data['Email'];

        // verify badge number
        $agent_id = '';
        $query = 'select id_c, id_num_c from supkg_systemusers_cstm where id_num_c = \'' . $agent_badge . '\'';
        $rs_sel = $GLOBALS['db']->query($query);
        if ($rs_sel->num_rows <= 0) {
            $GLOBALS['log']->fatal('[VeriscanAPI] wrong badge number error ' . date("Y-m-d H:i:s"));
            return array(
                'result' => 'error',
                'message' => 'invalid badge number'
            );
        } else {
            while ($row = $GLOBALS['db']->fetchByAssoc($rs_sel)) {
                $agent_id = $row['id_c'];
            }
        }

        $GLOBALS['log']->fatal('[VeriscanAPI] ready to create a new Lead ' . date("Y-m-d H:i:s"));

        $lead_bean = BeanFactory::newBean('Leads');

        $lead_bean->first_name = ucfirst(strtolower($first_name));
        $lead_bean->last_name = ucfirst(strtolower($last_name));
        $lead_bean->supkg_systemusers_id_c = $agent_id;
        $lead_bean->phone_work = $home_phone_number;
        $lead_bean->birthdate = $birthday;
        $lead_bean->primary_address_street = $address_street;
        $lead_bean->compartment_number_c = $address_unit;
        $lead_bean->primary_address_city = $address_city;
        $lead_bean->primary_address_state = $address_state;
        $lead_bean->primary_address_country = $address_country;
        $lead_bean->primary_address_postalcode = $address_postalcode;

        $ret_save = $lead_bean->save();
        if ($ret_save !== null) {
            $GLOBALS['log']->fatal("[VeriscanAPI] new Lead successfully created, Lead ID: {$ret_save} " . date("Y-m-d H:i:s"));

            $sugaremail = new SugarEmailAddress();
            $sugaremail->addAddress($email, true);
            $sugaremail->save($ret_save, 'Leads');

            return array(
                'result' => 'ok',
                'lead_id' => $ret_save
            );
        }

        $GLOBALS['log']->fatal('[VeriscanAPI] failed to create a new Lead ' . date("Y-m-d H:i:s"));
        return array(
            'result' => 'error',
            'message' => 'creating new lead'
        );
    }
}