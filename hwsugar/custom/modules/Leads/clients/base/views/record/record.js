({
    extendsFrom: 'RecordView',

    initialize: function(options) {
        this._super('initialize', [options]);
        app.error.errorName2Keys['invalid_address'] = 'ERR_INVAL_ADDRESS';
        this.model.addValidationTask('validate_sin', _.bind(this._validateSin, this));
        this.model.addValidationTask('validate_address', _.bind(this._validateAddress, this));
        this.context.on('button:credit_check:click', this._checkCredit, this);
        window.lol = this;
        window.setTimeout(this._storeSmartyStreet, 8000);
    },

    _validateSin: function (fields, errors, callback) {
        var sin = this.model.get('sin_c') || '';

        $('#validatesinmsg').remove();

        var insertMessage = function (message) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            var target = document.getElementById('content').firstChild.firstChild.childNodes[1].firstChild.firstChild.childNodes[1].childNodes[3];
            var node = document.createElement('div');

            node.innerHTML = '<br/>' + message;
            node.id = 'validatesinmsg';
            node.style.fontSize = '15px';
            node.style.fontWeight = 'bold';
            node.style.color = '#f44242';
            node.className = 'addValCustABCDErrMsg'; // stupid name to ensure I am the only one using this class
            target.insertBefore(node, target.firstChild);
        };

        if (sin !== '') {
            var reg_veri_sin = /^[0-9]{9}$/;
            if (reg_veri_sin.exec(sin) === null) {
                errors['sin_c'] = errors['sin_c'] || {};
                errors['sin_c'].invalid_sin = true;
                insertMessage('This SIN/SSN is not valid. (only numbers are allowed)<br/>');
            }
        }

        callback(null, fields, errors);
    },

    _storeSmartyStreet: function () {
        try{
            if(lol.model.get('primary_address_country') !== 'United States')
                return;

            var street = lol.model.get('primary_address_street') || '';
            var city = lol.model.get('primary_address_city') || '';
            var state = lol.model.get('primary_address_state') || '';
            var postal = (lol.model.get('primary_address_postalcode') || '').toUpperCase();

            var request = new XMLHttpRequest();
            var urlSplit = document.URL.split('/');
            request.open('GET', urlSplit[0] + '/smartystreet.php?id=' + urlSplit[urlSplit.length - 1] + '&'
                + 'street=' + encodeURIComponent(street || 'Placeholder') +  '&'
                + 'city=' + encodeURIComponent(city) + '&'
                + 'state=' + encodeURIComponent(state) + '&'
                + 'postalcode=' + encodeURIComponent(postal)
            );
            request.send();
        }catch(e){};
    }, 

    _validateAddress: function (fields, errors, callback) {
        //skip validation if box is checked
        var skip = document.getElementById('addValCustlalalabypass') || {checked : false};
        if (skip.checked){
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            callback(null, fields, errors);
            return;
        }

        var street = this.model.get('primary_address_street') || '';
        var city = this.model.get('primary_address_city') || '';
        var state = this.model.get('primary_address_state') || '';
        var postal = (this.model.get('primary_address_postalcode') || '').toUpperCase();

        var insertMessage = function (message) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            if(message === '')
                return;
            var target = document.getElementById('content').firstChild.firstChild.childNodes[1].firstChild.firstChild.childNodes[1].childNodes[3];
            var node = document.createElement('div');

            var skip = document.createElement('span');
            skip.innerHTML = '<br/><input type="checkbox" id="addValCustlalalabypass"/>  Ignore address errors, I know what I\'m doing';
            skip.style.fontSize = '12px';
            skip.style.color = '#717171';

            node.innerHTML = '<br/>' + message;
            node.style.fontSize = '15px';
			node.style.color = '#f44242';
			node.style.fontWeight = 'bold';
            node.className = 'addValCustABCDErrMsg'; // stupid name to ensure I am the only one using this class
            node.appendChild(skip);
            target.insertBefore(node, target.firstChild);
        };

        if(this.model.get('primary_address_country') === 'United States') {

            var reg_postal = /^\d{5}(?:[-\s]\d{4})?$/;
            if (reg_postal.exec(postal) === null) {
                console.log('us zipcode not in right format');
                errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
                errors['primary_address_postalcode'].invalid_zipcode = true;
                insertMessage('This zipcode is not recognized as a valid zipcode. (ex. 12345 or 12345-1234)<br/>');
                callback(null, fields, errors);
                return;
            }

            var requestUrl = 'https://us-street.api.smartystreets.com/street-address?auth-id=23379202066372234&'
                    + 'street=' + encodeURIComponent(street || 'Placeholder') +  '&'// I don't want CSRs to see 400 error from smartystreet when they hope to find an address without providing street...
                    + 'city=' + encodeURIComponent(city) + '&'
                    + 'state=' + encodeURIComponent(state) + '&'
                    + 'postalcode=' + encodeURIComponent(postal)
            ;

            var resolver = function (resolve, reject) {
                var request = new XMLHttpRequest();
                request.open('GET', requestUrl);
                request.setRequestHeader('Accept', 'application/json');
                request.onreadystatechange = function () {
                    if(this.readyState == 4){
                        if(this.status == 200)
                            resolve(request.responseText);
                        else
                            reject(this.status);
                    }
                }
                request.send();
            };

            var that = this; //too lazy to figure out what this is

            new Promise(resolver)
                .then(
                function (responseTxt) {
                    var json = JSON.parse(responseTxt);

                    //completely wrong address
                    if(json.length === 0){
                        var fields = ['primary_address_street', 'primary_address_city', 'primary_address_state', 'primary_address_postalcode'];
                        for(var iter = 0; iter < fields.length; iter++){
                            errors[fields[iter]] = errors[fields[iter]] || {};
                            errors[fields[iter]].invalid_address = true;
                        }
                        insertMessage('This address is not recognized as a valid address.<br/>');
                    }
                    else {
                        json = json[0];
                        var updateMessage = function(message, field, original, update){
                            return message + '<strong>Field: </strong>' + original + '<br/>Updated to: ' + update + '<br/>';
                        };
                        var message = '';
                        if(street !== json.delivery_line_1){
                            that.model.set('primary_address_street', json.delivery_line_1);
                            message = updateMessage(message, 'Address Street', street, json.delivery_line_1);
                            errors['primary_address_street'] = errors['primary_address_street'] || {};
                            errors['primary_address_street'].invalid_address = true;
                        }

                        if(city !== json.components.city_name){
                            that.model.set('primary_address_city', json.components.city_name);
                            message = updateMessage(message, 'Address City', city, json.components.city_name);
                            errors['primary_address_city'] = errors['primary_address_city'] || {};
                            errors['primary_address_city'].invalid_address = true;
                        }

                        if(state !== json.components.state_abbreviation){
                            that.model.set('primary_address_state', json.components.state_abbreviation);
                            message = updateMessage(message, 'Address State', state, json.components.state_abbreviation);
                            errors['primary_address_state'] = errors['primary_address_state'] || {};
                            errors['primary_address_state'].invalid_address = true;
                        }

                        if(postal !== json.components.zipcode){
                            that.model.set('primary_address_postalcode', json.components.zipcode);
                            message = updateMessage(message, 'Address Postal&sol;Zip Code', postal, json.components.zipcode);
                            errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
                            errors['primary_address_postalcode'].invalid_address = true;
                        }

                        insertMessage(message === '' ? '' : message + 'Please review changes, save again to confirm<br/>');
                    }
                    callback(null, fields, errors);
                })
                .catch(
                    function(code){
                        alert('Location verification return with error ' + code + '. Please contact technical support.');
                        callback(null, fields, errors);
                });
        }
        else if(this.model.get('primary_address_country') === 'Canada') {

            var reg_postal = /^[A-Z]\d[A-Z] \d[A-Z]\d$/;
            if (reg_postal.exec(postal) === null) {
                console.log('canada postal code not in right format');
                errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
                errors['primary_address_postalcode'].invalid_postal_code = true;
                insertMessage('This postal code is not recognized as a valid postal code. (ex. M5V 1Z4)<br/>');
                callback(null, fields, errors);
                return;
            }

            var requestUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address='
                    + encodeURIComponent(street + ' ' + city + ' ' + state + ' ' +  this.model.get('primary_address_country') + ' ' + postal);
            var resolver = function (resolve, reject) {
                var request = new XMLHttpRequest();
                request.open('GET', requestUrl);
                request.setRequestHeader('Accept', 'application/json');
                request.onreadystatechange = function () {
                    if(this.readyState == 4){
                        if(this.status == 200)
                            resolve(request.responseText);
                        else
                            reject(this.status);
                    }
                }
                request.send();
            }

            var that = this;

            new Promise(resolver)
                .then(
                    function (responseText) {
                        var json = JSON.parse(responseText);

                        if (json.status === "ZERO_RESULTS" || json.results[0].address_components.length < 8) {
                            var fields = ['primary_address_street', 'primary_address_city', 'primary_address_state', 'primary_address_postalcode'];
                            for(var iter = 0; iter < fields.length; iter++){
                                errors[fields[iter]] = errors[fields[iter]] || {};
                                errors[fields[iter]].invalid_address = true;
                                insertMessage('This address is not recognized as a valid address.<br/>');
                            }
							callback(null, fields, errors);
                        }
                        else if (json.status === "OK") {
                            var resStreet = ((json.results[0].address_components.filter(field => field.types.indexOf('street_number') !== -1)[0] || {long_name: ''}).long_name + ' '
                                + (json.results[0].address_components.filter(field => field.types.indexOf('route') !== -1)[0] || {short_name: ''}).short_name).trim();
                            var resCity = ((json.results[0].address_components.filter(field => field.types.indexOf('sublocality') !== -1)[0])
                                || (json.results[0].address_components.filter(field => field.types.indexOf('locality') !== -1)[0])
                                || (json.results[0].address_components.filter(field => field.types.indexOf('administrative_area_level_2') !== -1)[0])
                                || {long_name: ''}).long_name;
                            var resState = (json.results[0].address_components.filter(field => field.types.indexOf('administrative_area_level_1') !== -1)[0]
                                || {short_name: ''}).short_name;
                            var resPostal = (json.results[0].address_components.filter(field => field.types.indexOf('postal_code') !== -1)[0]
                                || {long_name: ''}).long_name;

                            //if postal code does not match, throw error regardless
                            if(resPostal === '' || ((resPostal !== postal) && (resPostal !== postal.substr(0, 3) + ' ' + postal.substr(3)))){
                                errors['primary_address_postalcode'] = errors['primary_address_postalcode'] || {};
                                errors['primary_address_postalcode'].invalid_address = true;
                                insertMessage('Do you mean: ' + json.results[0].formatted_address + '?');
                                callback(null, fields, errors);
                            } else {
                                that.model.set('primary_address_postalcode', postal);
                                // street city state
                                var message = '';
                                var updateMessage = function(message, field, original, update){
                                    return message + '<strong>Field: </strong>' + original + '<br/>Updated to: ' + update + '<br/>';
                                };
                                if(!resStreet || (street !== resStreet)){
                                    if(resStreet) {
                                        that.model.set('primary_address_street', resStreet);
                                        message = updateMessage(message, 'Address Street', street, resStreet);
                                    }
                                    errors['primary_address_street'] = errors['primary_address_street'] || {};
                                    errors['primary_address_street'].invalid_address = true;
                                }

                                if(!resCity || (city !== resCity)){
                                    if(resCity) {
                                        that.model.set('primary_address_city', resCity);
                                        message = updateMessage(message, 'Address City', city, resCity);
                                    }
                                    errors['primary_address_city'] = errors['primary_address_city'] || {};
                                    errors['primary_address_city'].invalid_address = true;
                                }

                                if(!resState || (state !== resState)){
                                    if(resState) {
                                        that.model.set('primary_address_state', resState);
                                        message = updateMessage(message, 'Address State', state, resState);
                                    }
                                    errors['primary_address_state'] = errors['primary_address_state'] || {};
                                    errors['primary_address_state'].invalid_address = true;
                                }
                                insertMessage(message);
                            }
                            callback(null, fields, errors);
                        }
                        //server/request error
                        else {
                            throw(json.status + ' (' + json.error_message + ')');
                        }
                    }
                )
                .catch (
                    function(code){
                        alert('Location verification return with error ' + code + '. Please contact technical support.');
                        callback(null, fields, errors);
                    }
                );
        }
        else {
            errors['primary_address_country'] = errors['primary_address_country'] || {};
            errors['primary_address_country'].invalid_address = true;
            callback(null, fields, errors);
        }
    },

    _checkCredit: function () {
        var insertMessage = function (message) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            var target = document.getElementById('content').firstChild.firstChild.childNodes[1].firstChild.firstChild.childNodes[1].childNodes[3];
            var node = document.createElement('div');

            node.innerHTML = '<br/>' + message;
            node.style.fontSize = '15px';
            node.style.color = '#f44242';
            node.style.fontWeight = 'bold';
            node.className = 'addValCustABCDErrMsg'; // stupid name to ensure I am the only one using this class
            target.insertBefore(node, target.firstChild);
        };


        var request = new XMLHttpRequest();
        var urlSplit = document.URL.split('/');
        request.open('GET', urlSplit[0] + '/creditcheck.php?id=' + urlSplit[urlSplit.length - 1]);
        request.setRequestHeader('Accept', 'application/json');
        request.onreadystatechange = function () {
            if(this.readyState == 4){
                if(this.status == 200)
                    insertMessage('Credit check: ' + request.responseText);
                else
                    alert('Credit Validation return with error ' + this.status + '. Please contact technical support.');
            }
        }
        request.send();

    }
})
