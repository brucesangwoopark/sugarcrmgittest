<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveLeadsHook {
	
	static $already_ran = false;//uncomment if you wish the hook to ONLY execute once
	
	function afterSave($bean, $event, $arguments)
	{
		if(self::$already_ran == true) return;//uncomment if you wish the hook to ONLY execute once
		self::$already_ran = true;//uncomment if you wish the hook to ONLY execute once
		
		$module = "Leads";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Lead id: '.$id.', '.date("Y-m-d H:i:s"));
		
		//find the correct regional team based on the office of the IC
		/*
		$team_id = 1; //setting default to Global Team
		$query = 'SELECT t.id as team_id
				FROM teams t
				JOIN supkg_systemusers_cstm suc ON suc.office_c = t.name
				WHERE suc.id_c = "'.$bean->supkg_systemusers_id_c.'"';
		//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Query: '.$query.', '.date("Y-m-d H:i:s"));
		$result = $GLOBALS['db']->query($query);
		while($row = $GLOBALS['db']->fetchByAssoc($result))
		{
			$team_id = $row['team_id'];
		}
		*/
		//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: team_id: '.$team_id.', '.date("Y-m-d H:i:s"));
		//add relationship to team
		//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding team: '.$bean->team_id.', '.date("Y-m-d H:i:s"));
		$bean->load_relationship('teams');
		$bean->teams->replace(array($bean->team_id));
		/*
		$query = 'UPDATE leads
				SET team_id = "'.$team_id.'", team_set_id = "'.$team_id.'"
				WHERE id = "'.$id.'"';
		$GLOBALS['db']->query($query);
		*/
		
		//check if Lead has associated Account and Opportunity
		if(!empty($bean->account_id)){
			$query = 'UPDATE accounts_cstm
					SET supkg_systemusers_id_c = "'.$bean->supkg_systemusers_id_c.'",
						supkg_systemusers_id1_c = "'.$bean->supkg_systemusers_id1_c.'"
					WHERE id_c = "'.$bean->account_id.'"';
			$GLOBALS['db']->query($query);
		}
		if(!empty($bean->opportunity_id)){
			$query = 'UPDATE opportunities_cstm
					SET supkg_systemusers_id_c = "'.$bean->supkg_systemusers_id_c.'",
						supkg_systemusers_id1_c = "'.$bean->supkg_systemusers_id1_c.'"
					WHERE id_c = "'.$bean->opportunity_id.'"';
			$GLOBALS['db']->query($query);
		}
		if($bean->converted == "1"){
			//find which team the records should be assigned to
			$team_id = 0;
			$query = 'SELECT t.id as team_id
					FROM teams t
					JOIN supkg_systemusers_cstm suc ON suc.office_c = t.name
					WHERE suc.id_c = "'.$bean->supkg_systemusers_id_c.'"';
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result))
			{
				$team_id = $row['team_id'];
			}
			//only execute below if a team was found
			if($team_id != 0){
				//update associated records to assign team
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating associated records to Team: '.$team_id.', '.date("Y-m-d H:i:s"));
				$query = 'UPDATE leads l
						JOIN accounts a ON a.id = l.account_id
						JOIN opportunities o ON o.id = l.opportunity_id
						JOIN accounts_abcde_jobs_1_c aj ON aj.accounts_abcde_jobs_1accounts_ida = a.id
						JOIN abcde_jobs j ON j.id = aj.accounts_abcde_jobs_1abcde_jobs_idb
						JOIN revenue_line_items rli ON rli.opportunity_id = o.id
						JOIN notes n ON n.parent_id = a.id
						SET l.team_id = "'.$team_id.'",
							l.team_set_id = "'.$team_id.'",
							a.team_id = "'.$team_id.'",
							a.team_set_id = "'.$team_id.'",
							o.team_id = "'.$team_id.'",
							o.team_set_id = "'.$team_id.'",
							j.team_id = "'.$team_id.'",
							j.team_set_id = "'.$team_id.'",
							rli.team_id = "'.$team_id.'",
							rli.team_set_id = "'.$team_id.'",
							n.team_id = "'.$team_id.'",
							n.team_set_id = "'.$team_id.'"
						WHERE l.id = "'.$id.'"';
				$GLOBALS['db']->query($query);
			}
		}

        if (isset($bean->onconverting) && $bean->onconverting === true) {
            // a174743a-52cd-11e7-a348-0e75c7a95c08
            if ($bean->created_by === 'a174743a-52cd-11e7-a348-0e75c7a95c08') { // veriscan ID
            	$query = "update leads_cstm set user_id_c = '{$GLOBALS['current_user']->id}' where id_c = '{$id}';";
            	$GLOBALS['db']->query($query);
            }
        }

		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
   }
}
?>
