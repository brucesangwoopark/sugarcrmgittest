<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['delinquentonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'delinquentonly',
            'name'              => 'LBL_DELINQUENT_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'Delinquent',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>