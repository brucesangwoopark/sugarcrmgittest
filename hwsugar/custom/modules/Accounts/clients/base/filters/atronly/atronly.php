<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['atronly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'atronly',
            'name'              => 'LBL_ATR_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'ATR',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>