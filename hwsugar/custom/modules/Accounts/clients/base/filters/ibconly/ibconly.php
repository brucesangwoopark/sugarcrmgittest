<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['ibconly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'ibconly',
            'name'              => 'LBL_IBC_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'IBC',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>