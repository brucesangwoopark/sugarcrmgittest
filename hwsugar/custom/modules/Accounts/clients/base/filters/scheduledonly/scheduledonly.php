<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['scheduledonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'scheduledonly',
            'name'              => 'LBL_SCHEDULED_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'Scheduled',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>