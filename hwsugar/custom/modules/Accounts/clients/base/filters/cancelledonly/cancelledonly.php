<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['cancelledonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'cancelledonly',
            'name'              => 'LBL_CANCELLED_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'Cancelled',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>