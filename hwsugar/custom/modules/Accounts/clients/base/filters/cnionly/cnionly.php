<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['cnionly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'cnionly',
            'name'              => 'LBL_CNI_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'CNI',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>