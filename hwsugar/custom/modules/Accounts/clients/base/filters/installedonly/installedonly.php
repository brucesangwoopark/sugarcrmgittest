<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['installedonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'installedonly',
            'name'              => 'LBL_INSTALLED_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'Installed',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>