<?php
$module_name = "Accounts";
$viewdefs[$module_name]['base']['filter']['pendingonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'pendingonly',
            'name'              => 'LBL_PENDING_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'customer_status_c' => 'Pending',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>