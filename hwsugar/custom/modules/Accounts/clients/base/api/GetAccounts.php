<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-11
 * Time: 오후 5:02
 */

class GetAccounts extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getAccounts' => array(
                'reqType' => 'POST',
                'path' => array('Accounts', 'list'),
                'method' => 'getAccountList',
                'shortHelp' => 'Returning Accounts in JSON',
            )
        );
    }

    function getAccountList($api, $args)
    {
        if (!isset($args['p']) || empty($args['p'])) {
            return array(
                'result' => 'error',
                'message' => 'no parameter'
            );
        }

        $p = $args['p'];

        require_once('include/SugarQuery/SugarQuery.php');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean('Accounts'));
        $sugarQuery->where()->queryOr()->contains('name', $p)->contains('unique_id_c', $p);
        $results = $sugarQuery->execute();

        return array(
            'result' => 'ok',
            'data' => $results
        );
    }
}