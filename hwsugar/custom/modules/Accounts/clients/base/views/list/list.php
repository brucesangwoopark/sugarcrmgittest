<?php
$viewdefs['Accounts'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'link' => true,
                'label' => 'LBL_LIST_ACCOUNT_NAME',
                'enabled' => true,
                'default' => true,
                'width' => 'xlarge',
              ),
              1 => 
              array (
                'name' => 'unique_id_c',
                'label' => 'LBL_UNIQUE_ID',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'customer_status_c',
                'label' => 'LBL_CUSTOMER_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'agent_c',
                'label' => 'LBL_AGENT',
                'enabled' => true,
                'id' => 'SUPKG_SYSTEMUSERS_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'billing_address_street',
                'label' => 'LBL_BILLING_ADDRESS_STREET',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'compartment_number_c',
                'label' => 'LBL_COMPARTMENT_NUMBER',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'billing_address_postalcode',
                'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'billing_address_city',
                'label' => 'LBL_LIST_CITY',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'billing_address_state',
                'label' => 'LBL_BILLING_ADDRESS_STATE',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'billing_address_country',
                'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'external_key_c',
                'label' => 'LBL_EXTERNAL_KEY',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'synchroteam_external_key_c',
                'label' => 'LBL_SYNCHROTEAM_EXTERNAL_KEY',
                'enabled' => true,
                'default' => true,
              ),
              12 => 
              array (
                'name' => 'date_entered',
                'type' => 'datetime',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              13 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
