<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveAccountHook {
	
	static $already_ran = false;//ensures hook only executes once
	
	function afterSave($bean, $event, $arguments)
	{
		if(self::$already_ran == true) return;//ensures hook only executes once
		self::$already_ran = true;//ensures hook only executes once
		
		$id = $bean->id;
		//for logging purposes
		$module = "Accounts";
		$hook = "after_save";
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Account id: '.$id.', '.date("Y-m-d H:i:s"));

		if (isset($bean->onconverting) && $bean->onconverting === true) {
		    if ($bean->user_id_c === 'a174743a-52cd-11e7-a348-0e75c7a95c08') { // veriscan ID
                $query = "update accounts_cstm set user_id_c = '{$GLOBALS['current_user']->id}' where id_c = '{$id}';";
                $GLOBALS['db']->query($query);
            }
        }

		try{
			//check if unique_id_c field has a value
			
			//update the IC name and number
			$query = 'SELECT su.first_name as first_name, su.last_name as last_name, suc.id_num_c as id_num
						FROM supkg_systemusers su
						JOIN supkg_systemusers_cstm suc ON suc.id_c = su.id
						WHERE su.id = "'.$bean->supkg_systemusers_id_c.'"';
			$result = $GLOBALS['db']->query($query);
			$first_name = ""; //setting default
			$last_name = ""; //setting default
			$agent_id = ""; //setting default
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
			  $first_name = $row['first_name'];
			  $last_name = $row['last_name'];
			  $agent_id = $row['id_num'];
			}
			$agent_name = $first_name." ".$last_name;
			$query = 'UPDATE accounts_cstm
						SET agent_name_c = "'.$agent_name.'",
							agent_id_c = "'.$agent_id.'"
						WHERE id_c = "'.$id.'"';
			$GLOBALS['db']->query($query);
			
			if(empty($bean->unique_id_c)){
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Generating new unique Account Number, '.date("Y-m-d H:i:s"));
				/*
				$query =  "SELECT MAX(unique_id_c) AS max_unique_id FROM accounts_cstm";
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
				  $max_unique_id = $row['max_unique_id'];
				}
				if ($max_unique_id == null) { //in case of first entry
				  $max_unique_id = 2000000;
				}
				$unique_id = $max_unique_id + 1;
				
				$query = 'UPDATE accounts_cstm SET unique_id_c = "'.$unique_id.'" WHERE id_c = "'.$id.'"';
				$GLOBALS['db']->query($query);
				*/
				//insert record into ra_num table to generate new ra_num (it is auto-incrementing)
				$unique_id = 0; //setting default
				$query = 'INSERT INTO account_unique_id SET id = "'.$id.'"';
				$GLOBALS['db']->query($query);
				//select from ra_num to retrieve ra number
				$query = 'SELECT unique_id FROM account_unique_id WHERE id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
				  $unique_id = $row['unique_id'];
				}
				//update unique_id_c field
				$query = 'UPDATE accounts_cstm
						SET unique_id_c = "'.$unique_id.'"
						WHERE id_c = "'.$id.'"';
				$GLOBALS['db']->query($query);
			}
			else{
				$unique_id = $bean->unique_id_c;
			}
			
			//check if chargeover_instance_c field has a value
			if(empty($bean->chargeover_instance_c)){
				if($bean->billing_address_country == 'United States'){
					$query = 'UPDATE accounts_cstm SET chargeover_instance_c = "1" WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					$chargeover_instance = 1;
				}
				if($bean->billing_address_country == 'Canada'){
					$query = 'UPDATE accounts_cstm SET chargeover_instance_c = "2" WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					$chargeover_instance = 2;
				}
			}
			else{
				$chargeover_instance = $bean->chargeover_instance_c;
			}
			
			//check if status of customer has been set to "Cancelled"
			if(($bean->customer_status_c == 'Cancelled') && empty($bean->cancel_date_c)){
				$query = 'UPDATE accounts_cstm SET cancel_date_c = "'.date("Y-m-d H:i:s").'" WHERE id_c = "'.$id.'"';
				$bean->db->query($query, true);
			}
			
			if(($bean->customer_status_c != 'Cancelled') && !empty($bean->cancel_date_c)){
				$query = 'UPDATE accounts_cstm SET cancel_date_c = "" WHERE id_c = "'.$id.'"';
				$bean->db->query($query, true);
			}
			
			if($bean->cancel_all_products_c == "1"){
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Cancelling all associated products, '.date("Y-m-d H:i:s"));      
				//get all associated RLIs
				$rli_id = array();
				$query = 'SELECT id FROM revenue_line_items WHERE account_id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
				  $rli_id[] = $row['id'];
				}
				//set "uninstall_c" = "1" for all assocaited RLIs, i.e. "cancel product"
				for ($i = 0; $i < sizeof($rli_id); $i++){
					$rli = BeanFactory::getBean('RevenueLineItems', $rli_id[$i]);
					$rli->uninstall_c = "1";
					$rli->save();
				}
				
			}//END OF if($bean->cancel_all_products_c = "1")
			
			
			if($bean->failed_reaf_c != "1")
			{
				
				//
				//
				//
				//PART 1 - CHARGEOVER CUSTOMER
				//
				//
				//
				//credentials
				/*
				$query = 'SELECT username, password
						FROM crede_credentials
						WHERE name = "ChargeOver"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure below only executes once
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$username = $row['username'];
					$password = $row['password'];
					$counter++;
				}
				$encoding = base64_encode($username.':'.$password);
				*/
				
				//US CUSTOMER
				//$url_base = (($_SERVER['HTTPS'] == "on") ? 'https' : 'http' ) . '://' . $_SERVER['SERVER_NAME'];
				if($chargeover_instance == 1){
					//ChargeOver Nevada
					//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=COCRED&bid=2';
					$url = 'http://www.thefilterboss.com/init.php?pid=COCRED&bid=2';
				}
				//CANADIAN CUSTOMER
				if($chargeover_instance == 2){
					//Ontario ChargeOver
					//$url = 'http://www.thefilterboss.com/init.php?pid=COCRED'; //STAGING
					$url = 'http://www.thefilterboss.com/init.php?pid=COCRED&bid=1';
				}
				$curl = curl_init($url);
				$headr = array();
				$headr[] = 'Accept: application/json';
				$headr[] = 'Content-type: application/json';
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
				curl_setopt($curl, CURLOPT_HTTPGET, true);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//make REST call
				$response = curl_exec($curl);
				curl_close($curl);
				$jsonResponse = json_decode($response, true);
				$encoding_CO = base64_encode($response);
								
				
				
				//check if ChargeOver customer already exists
				if(!empty($bean->external_key_c)){
					$chargeover_customer_exists = true;
					$chargeover_customer_id = $bean->external_key_c;
				}
				else{
					$chargeover_customer_exists = false;
				}
				
				//getting all entries in email array
				$emailArray = $bean->email;
				//all emails are 1st entry in nested array
				$matches = array();
				foreach ($emailArray as $i){
					foreach ($i as $j){
						$matches[] = $j;
						break;
					}
					break;
				}
				//append all emails address to email string, seperated by ;
				$email = '';
				$length = sizeof($matches);
				for ($i = 0; $i < $length; $i++){
					$email.=$matches[$i];
					if ($i != ($length-1) ) $email.= ';';
				}
				//create placeholders in case that phone number or email is empty 			
				if(empty($bean->phone_office)) $superuser_phone = 'placeHolderPhone'; else $superuser_phone = $bean->phone_office;
				if(empty($bean->email)) $email = 'systemdefault@filtergroup.com';
				
				
				//POST new customer
				if (!$chargeover_customer_exists){
					
					//check if Customer is in Canada or US to set currency_id
					//if ($bean->billing_address_country == "Canada") $currency_id = 2; else $currency_id = 1;
					if($chargeover_instance == 1) $currency_id = 1; else $currency_id = 2;
					if($chargeover_instance == 1) $brand_id = 1; else $brand_id = 1;
								
					//create JSON String with Sugar attributes to pass to ChargeOver API
					$name = $bean->name;
                    $name = substr($name, 0, strlen($name) - 10);
					$json = '{"external_key": "'.$unique_id.'",'
							//"token": ,
							.'"company": "'.$name.'",'
							.'"bill_addr1": "'.$bean->billing_address_street.'",'
							//'"bill_addr2": ,
							//"bill_addr3": ,
							.'"bill_city": "'.$bean->billing_address_city.'",'
							.'"bill_state": "'.$bean->billing_address_state.'",'
							.'"bill_postcode": "'.$bean->billing_address_postalcode.'",'
							.'"bill_country": "'.$bean->billing_address_country.'",'
							//"bill_notes": ,
							.'"ship_addr1": "'.$bean->shipping_address_street.'",'
							//"ship_addr2": ,
							//"ship_addr3": ,
							.'"ship_city": "'.$bean->shipping_address_city.'",'
							.'"ship_state": "'.$bean->shipping_address_state.'",'
							.'"ship_postcode": "'.$bean->shipping_address_postalcode.'",'
							.'"ship_country": "'.$bean->shipping_address_country.'",'
							//"ship_notes": ,
							.'"terms_id": 2,'
							//"class_id": ,
							//.'"custom_1": "Sugar",'
							//.'"custom_2": ,'
							//.'"custom_3": ,'
							//"custom_4": ,
							//.'"custom_5": "Sugar",'
							//"custom_6": ,
							//"admin_id": ,
							//"campaign_id": ,
							//"custtype_id": ,
							.'"currency_id": '.$currency_id.','
							.'"language_id": 1,'
							.'"brand_id": '.$brand_id.','
							//"no_taxes": ,
							//"no_dunning": ,
							//"write_datetime": ,
							//"write_ipaddr": ,
							//"mod_datetime": ,
							//"mod_ipaddr": ,
							//"terms_name": ,
							//"terms_days": ,
							//"paid": ,
							//"total": ,
							//"balance": ,
							//"url_paymethodlink": ,
							//"admin_name": ,
							//"admin_email": ,
							//"currency_symbol": ,
							//"currency_iso4217": ,
							//"display_as": ,
							//"ship_block": ,
							//"bill_block": ,
							.'"superuser_name": "'.$name.'",'
							//"superuser_first_name": ,
							//"superuser_last_name": ,
							.'"superuser_phone": "'.$superuser_phone.'",'
							.'"superuser_email": "'.$email.'"'
							//"superuser_token": ,
							//"customer_id": ,
							//.'"invoice_delivery": ,
							//"dunning_delivery": ,
							//"customer_status_id": ,
							//"customer_status_name": ,
							//"customer_status_str": ,
							//"customer_status_state": ,
							//"superuser_username": ';
							.'}';	
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver JSON query: '.$json.' '.date("Y-m-d H:i:s"));
					
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating new ChargeOver customer... '.date("Y-m-d H:i:s"));
					
					if($chargeover_instance == 1){
						//Nevada ChargeOver
						$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/customer';
					}
					if($chargeover_instance == 2){
						//Ontario ChargeOver
						//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/customer'; //STAGING
						//$url = 'https://greenlifewater-new.chargeover.com/api/v3/customer';
						$url = 'https://billingcdn.filtergroup.com/api/v3/customer';
					}
					
					//open curl for POST
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: ' . strlen($json);
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding_CO;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					//get the HTTP status code from ChargeOver
					$code = $jsonResponse['code'];
					$message = $jsonResponse['message'];
					$message = substr($message, 0, 30).'...';//truncating string in case it is a long message
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: "'.$message.'", '.date("Y-m-d H:i:s"));
					
					//update custom response code and message fields
					/*
					$query =  'UPDATE accounts_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					$query =  'UPDATE accounts_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					*/
					
					//201 Created - Object Successfully created
					if ($code == 201){
						//get the new ChargeOver customer id
						$coId = $jsonResponse['response']['id'];
						//enter new (CO) customer id as external key
						$query = 'UPDATE accounts_cstm SET external_key_c = "'.$coId.'"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						if($chargeover_instance == 1){
							//Nevada ChargeOver
							//update the ChargeOver customer link field
							$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://nationalhomewaterservices.chargeover.com/admin/r/customer/view/'.$coId.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
						}
						if($chargeover_instance == 2){
							//Ontario ChargeOver
							//update the ChargeOver customer link field
							//$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://greenlifewater-staging.chargeover.com/admin/r/customer/view/'.$coId.'" WHERE id_c = "'.$id.'"'; //STAGING
							//$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://greenlifewater-new.chargeover.com/admin/r/customer/view/'.$coId.'" WHERE id_c = "'.$id.'"';
							$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://billingcdn.filtergroup.com/admin/r/customer/view/'.$coId.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
						}
						
						//get the payment_link
						$payment_link = 0; //setting default
						if($chargeover_instance == 1){
							//Nevada ChargeOver
							$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/customer/'.$coId;
						}
						if($chargeover_instance == 2){
							//Ontario ChargeOver
							//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/customer/'.$coId; //STAGING
							//$url = 'https://greenlifewater-new.chargeover.com/api/v3/customer/'.$coId;
							$url = 'https://billingcdn.filtergroup.com/api/v3/customer/'.$coId;
						}
						//open curl for GET
						$curl = curl_init($url);
						$headr = array();
						$headr[] = 'Accept: application/json';
						$headr[] = 'Content-type: application/json';
						$headr[] = 'Authorization: Basic '.$encoding_CO;
						curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
						curl_setopt($curl, CURLOPT_HTTPGET, true);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						//make REST call
						$response = curl_exec($curl);
						curl_close($curl);
						$jsonResponse = json_decode($response, true);
						$payment_link = $jsonResponse['response']['url_paymethodlink'];
						
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating ChargeOver payment link, '.date("Y-m-d H:i:s"));
						$query = 'UPDATE accounts_cstm SET chargeover_payment_link_c = "'.$payment_link.'" WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						
						
						//make note that the conversion for Account was successful, in both custom field and by creating a Note in Sugar
						$query = 'UPDATE accounts_cstm SET converted_c = "(S)Account to (CO)Customer successful" WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						$note = new Note();
						$note->name = 'ChargeOver Customer: '.$coId.' created';
						$note->parent_type = 'Accounts';
						$note->parent_id = $id;
						//$note->team_id = $bean->team_id;
						//$note->team_set_id = $bean->team_id;
						$note->save();
						//add team relationship
						/*
						$note->load_relationship('teams');
						$note->teams->replace(array($bean->team_id));
						*/
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Customer created, external id: '.$coId.', '.date("Y-m-d H:i:s"));
						
						//AF Invoice - only create for US customer, if there is no upfront fee
						if($chargeover_instance == 1 && $bean->needs_activation_fee_c === '1'){
							//Check what AF should be applied
							$af_item_id = ""; //setting default for CO Activiation Fee Item ID
							
							$query = 'SELECT ptc.chargeover_id_c as af_item_id
											FROM product_templates pt
											JOIN product_templates_cstm ptc ON ptc.id_c = pt.id
											WHERE pt.name = "AF"
											AND ptc.country_c = "'; if($chargeover_instance == 1) $query .= 'United States"'; else $query .= 'Canada"';
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
								$af_item_id = $row['af_item_id'];
							}
							//POST AF invoice
							if(empty($bean->activation_fee_c)){ $activation_fee = 19.95;} else {$activation_fee = $bean->activation_fee_c;}
							if(empty($af_item_id)) $af_item_id = 8;
							
							$json = '{
										"customer_id": '.$coId.','
										.'"bill_city": "'.$bean->billing_address_city.'",'
										.'"bill_state": "'.$bean->billing_address_state.'",'
										.'"bill_postcode": "'.$bean->billing_address_postalcode.'",'
										.'"bill_country": "'.$bean->billing_address_country.'",'
										.'"line_items": [
											{
												"item_id": '.$af_item_id.',
												"descrip": "Activation Fee",
												"line_rate": '.$activation_fee.',
												"line_quantity": 1
											}
										]
									}';
									
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Activation Fee Invoice JSON query: '.$json.' '.date("Y-m-d H:i:s"));
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating new ChargeOver Activation Fee Invoice '.date("Y-m-d H:i:s"));
							
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/invoice';
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/invoice'; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/invoice';
								$url = 'https://billingcdn.filtergroup.com/api/v3/invoice';
							}
							//open curl for POST
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-Length: ' . strlen($json);
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding_CO;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
							curl_setopt($curl, CURLOPT_POST, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							$invoice_id = $jsonResponse['response']['id'];
							
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: New invoice: '.$invoice_id.' created for Customer: '.$coId.', '.date("Y-m-d H:i:s"));
							
							//get the invoice link through the token
							$invoice_token = 0; //setting default
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/invoice/'.$invoice_id;
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/invoice/'.$invoice_id; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/invoice/'.$invoice_id;
								$url = 'https://billingcdn.filtergroup.com/api/v3/invoice/'.$invoice_id;
							}
							//open curl for GET
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding_CO;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_HTTPGET, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							$invoice_token= $jsonResponse['response']['token'];
							
							$invoice_link = 0; //setting default
							if($chargeover_instance == 1){
								$invoice_link = 'https://nationalhomewaterservices.chargeover.com/r/trans/pay/'.$invoice_token;
							}
							if($chargeover_instance == 2){
								//$invoice_link = 'https://greenlifewater-staging.chargeover.com/r/trans/pay/'.$invoice_token; //STAGING
								//$invoice_link = 'https://greenlifewater-new.chargeover.com/r/trans/pay/'.$invoice_token;
								$invoice_link = 'https://billingcdn.filtergroup.com/r/trans/pay/'.$invoice_token;
							}
							
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating ChargeOver invoice link: '.$invoice_link.', '.date("Y-m-d H:i:s"));
							$query = 'UPDATE accounts_cstm SET chargeover_invoice_link_c = "'.$invoice_link.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							//END OF AF invoice
						} else {
                            $GLOBALS['log']->fatal('['.$module.']['.$hook.']: No Activation Fee, '.date("Y-m-d H:i:s"));
                        }
					}
					else{//$code != 201
						//make note that the conversion for Account was NOT successful
						$query = 'UPDATE accounts_cstm SET converted_c = "fail" WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
					}			
				}//END OF if ($chargeover_customer_exists)
				
				//PUT/update existing customer
				else{//i.e. if($chargeover_customer_exists)
					if($chargeover_instance == 1) $brand_id = 1; else $brand_id = 1;
					$name = $bean->name;
                    $name = substr($name, 0, strlen($name) - 10);
					$json = '{"external_key": "'.$unique_id.'",'
							//"token": ,
							.'"company": "'.$name.'",'
							.'"bill_addr1": "'.$bean->billing_address_street.'",'
							//'"bill_addr2": ,
							//"bill_addr3": ,
							.'"bill_city": "'.$bean->billing_address_city.'",'
							.'"bill_state": "'.$bean->billing_address_state.'",'
							.'"bill_postcode": "'.$bean->billing_address_postalcode.'",'
							.'"bill_country": "'.$bean->billing_address_country.'",'
							//"bill_notes": ,
							.'"ship_addr1": "'.$bean->shipping_address_street.'",'
							//"ship_addr2": ,
							//"ship_addr3": ,
							.'"ship_city": "'.$bean->shipping_address_city.'",'
							.'"ship_state": "'.$bean->shipping_address_state.'",'
							.'"ship_postcode": "'.$bean->shipping_address_postalcode.'",'
							.'"ship_country": "'.$bean->shipping_address_country.'",'
							//"ship_notes": ,
							.'"terms_id": 2,'
							//"class_id": ,
							//.'"custom_1": "Sugar",'
							//.'"custom_2": ,'
							//.'"custom_3": ,'
							//"custom_4": ,
							//.'"custom_5": "Sugar",'
							//"custom_6": ,
							//"admin_id": ,
							//"campaign_id": ,
							//"custtype_id": ,
							//"currency_id": ,
							.'"language_id": 1,'
							.'"brand_id": '.$brand_id.','
							//"no_taxes": ,
							//"no_dunning": ,
							//"write_datetime": ,
							//"write_ipaddr": ,
							//"mod_datetime": ,
							//"mod_ipaddr": ,
							//"terms_name": ,
							//"terms_days": ,
							//"paid": ,
							//"total": ,
							//"balance": ,
							//"url_paymethodlink": ,
							//"admin_name": ,
							//"admin_email": ,
							//"currency_symbol": ,
							//"currency_iso4217": ,
							//"display_as": ,
							//"ship_block": ,
							//"bill_block": ,
							.'"superuser_name": "'.$name.'",'
							//"superuser_first_name": ,
							//"superuser_last_name": ,
							.'"superuser_phone": "'.$superuser_phone.'",'
							.'"superuser_email": "'.$email.'"'
							//"superuser_token": ,
							//"customer_id": ,
							//.'"invoice_delivery": ,
							//"dunning_delivery": ,
							//"customer_status_id": ,
							//"customer_status_name": ,
							//"customer_status_str": ,
							//"customer_status_state": ,
							//"superuser_username": ';
							.'}';	
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver JSON query: '.$json.' '.date("Y-m-d H:i:s"));
					
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating ChargeOver Customer: '.$chargeover_customer_id.', '.date("Y-m-d H:i:s"));
					//open curl for PUT
					if($chargeover_instance == 1){
						//Nevada ChargeOver
						$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/customer/'.$chargeover_customer_id;
					}
					if($chargeover_instance == 2){
						//Ontario ChargeOver
						//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/customer/'.$chargeover_customer_id; //STAGING
						//$url = 'https://greenlifewater-new.chargeover.com/api/v3/customer/'.$chargeover_customer_id;
						$url = 'https://billingcdn.filtergroup.com/api/v3/customer/'.$chargeover_customer_id;
					}
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: ' . strlen($json);
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding_CO;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					//get HTTP status code from ChargeOver
					$code = $jsonResponse['code'];
					$message = $jsonResponse['message'];
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: "'.$message.'", '.date("Y-m-d H:i:s"));
									
					//update custom response code and message fields
					/*
					$query =  'UPDATE accounts_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					$query =  'UPDATE accounts_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
					$bean->db->query($query, true);
					*/
					
					//202 Accepted - successfully updated object
					if ($code == 202){
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Customer updated, external id: '.$chargeover_customer_id.', '.date("Y-m-d H:i:s"));
						
						$query = 'UPDATE accounts_cstm SET updated_c = "(S)Account to (CO)Customer update successful, '.date("Y-m-d H:i:s").'"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						
						if($chargeover_instance == 1){
							//Nevada ChargeOver
							//update the ChargeOver customer link field, in case that Customer created outside of Sugar
							$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://nationalhomewaterservices.chargeover.com/admin/r/customer/view/'.$chargeover_customer_id.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
						}
						if($chargeover_instance == 2){
							//Ontario ChargeOver
							//update the ChargeOver customer link field, in case that Customer created outside of Sugar
							//$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://greenlifewater-staging.chargeover.com/admin/r/customer/view/'.$chargeover_customer_id.'" WHERE id_c = "'.$id.'"'; //STAGING
							//$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://greenlifewater-new.chargeover.com/admin/r/customer/view/'.$chargeover_customer_id.'" WHERE id_c = "'.$id.'"';
							$query = 'UPDATE accounts_cstm SET chargeover_customer_link_c = "https://billingcdn.filtergroup.com/admin/r/customer/view/'.$chargeover_customer_id.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
						}
						
						//get the payment_link, in the case that customer was created before the payment link was implemented
						if(empty($bean->chargeover_payment_link_c)){
							$payment_link = 0; //setting default
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = 'https://nationalhomewaterservices.chargeover.com/api/v3/customer/'.$chargeover_customer_id;
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/customer/'.$chargeover_customer_id; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/customer/'.$chargeover_customer_id;
								$url = 'https://billingcdn.filtergroup.com/api/v3/customer/'.$chargeover_customer_id;
							}
							//open curl for GET
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding_CO;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_HTTPGET, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							$payment_link = $jsonResponse['response']['url_paymethodlink'];
							
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating ChargeOver payment link, '.date("Y-m-d H:i:s"));
							$query = 'UPDATE accounts_cstm SET chargeover_payment_link_c = "'.$payment_link.'" WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							
						}
					}
					else{//$code != 202
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Customer NOT updated, external id: '.$chargeover_customer_id.', '.date("Y-m-d H:i:s"));
						
						$query = 'UPDATE accounts_cstm SET updated_c = "(S)Account to (CO)Customer update FAILED, '.date("Y-m-d H:i:s").'"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						
					}
				}
				//
				//
				//
				//END OF PART 1 - CHARGEOVER CUSTOMER
				//
				//
				//
				
				//
				//
				//
				//PART 2 - SYNCHROTEAM CUSTOMER
				//
				//
				//
				
				//credentials
				/*
				$query = 'SELECT username, password
						FROM crede_credentials
						WHERE name = "Synchroteam"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure below only executes once
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$username = $row['username'];
					$password = $row['password'];
					$counter++;
				}
				$encoding_ST = base64_encode($username.':'.$password);
				*/
				//Synchroteam credentials
				//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=STCRED';
				$url = 'http://www.thefilterboss.com/init.php?pid=STCRED';
				$curl = curl_init($url);
				$headr = array();
				$headr[] = 'Accept: application/json';
				$headr[] = 'Content-type: application/json';
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
				curl_setopt($curl, CURLOPT_HTTPGET, true);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//make REST call
				$response = curl_exec($curl);
				curl_close($curl);
				$jsonResponse = json_decode($response, true);
				$encoding_ST = base64_encode($response);
				
				
				//check if Synchroteam Customer exists
				if(!empty($bean->synchroteam_external_key_c)){
					//get Synchroteam customer_id
					$synchroteam_customer_id = $bean->synchroteam_external_key_c;
					$synchroteam_customer_exists = true;
				}
				else{
					$synchroteam_customer_exists = false;
				}
				
				if(!$synchroteam_customer_exists){ //i.e. Synchroteam Customer does NOT exist
					//DO NOT POST NEW CUSTOMER - THIS IS DONE FROM JOBS MODULE
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Synchroteam Customer NOT created yet, '.date("Y-m-d H:i:s"));
				}
				//PUT/UPDATE CUSTOMER
				else{
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating Synchroteam Customer: '.$synchroteam_customer_id.', '.date("Y-m-d H:i:s"));
					
					//get all relevant information
					//construct address string	
					$address = $bean->billing_address_street.','.$bean->billing_address_postalcode.' '.$bean->billing_address_city.', '.$bean->billing_address_state.', '.$bean->billing_address_country;
					//get email
					$email = ""; //set default in case email has not been set
					$query = 'SELECT e.email_address as email
							FROM email_addresses e
							JOIN email_addr_bean_rel er ON er.email_address_id = e.id
							JOIN accounts a ON er.bean_id = a.id
							WHERE er.deleted <> "1"
							AND a.id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below only executes once - note: this will get the first email entry
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$email = $row['email'];					
						$counter++;
					}
					
					//construct JSON string
					$name = $bean->name;
                    $name = substr($name, 0, strlen($name) - 10);
					$json = '{
									"id": '.$synchroteam_customer_id.',
									"Name":"'.$name.'",
									"Address":"'.$address.'",
									"ContactEmail":"'.$email.'",
									"ContactPhone":"'.$bean->phone_office.'",
									"CustomFieldValues": [
										{
										  "Name": "external_key",
										  "Value": "'.$id.'"
										},
										{
										  "Name": "Chargeover",
										  "Value": "'.$bean->chargeover_payment_link_c.'"
										}
									 ]
								}';
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Synchroteam JSON query: '.$json.' '.date("Y-m-d H:i:s"));				
					
					//open curl for PUT
					$url = 'https://apis.synchroteam.com/Api/v1/Customer/Update';
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: ' . strlen($json);
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding_ST;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					$code = $jsonResponse['code'];
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Synchroteam response code: '.$code.', '.date("Y-m-d H:i:s"));
					
					if ($code == $synchroteam_customer_id && !empty($synchroteam_customer_id)){//Customer is updated
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Synchroteam Customer updated, external id: '.$synchroteam_customer_id.', '.date("Y-m-d H:i:s"));
						$query = 'UPDATE accounts_cstm
								SET synchroteam_updated_c = "(S)Account to (Synchroteam)Customer update successful, '.date("Y-m-d H:i:s").'"
								WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);		
					}
					else{//Customer is NOT updated
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Synchroteam Customer NOT updated, external id: '.$synchroteam_customer_id.', '.date("Y-m-d H:i:s"));
						$query = 'UPDATE accounts_cstm
								SET synchroteam_updated_c = "(S)Account to (Synchroteam)Customer update FAILED"
								WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);		
					}
					
				}//END OF Synchroteam Customer update
				
				//
				//
				//
				//END OF PART 2 - SYNCHROTEAM CUSTOMER
				//
				//
				//
			}//END OF if($bean->failed_reaf_c != "1")
			else{//REAFF FAILED
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Account is flagged as a failed re-affirmation, '.date("Y-m-d H:i:s"));
				
				//attach note to Account stating the failed reaf
				if($bean->failed_reaf_note_added_c != "1"){
					$note = new Note();
					$note->name = 'The re-affirmation was failed, date: '.date("Y-m-d H:i:s");
					$note->parent_type = 'Accounts';
					$note->parent_id = $id;
					//$note->team_id = $bean->team_id;
					//$note->team_set_id = $bean->team_id;
					$note->save();
					//add team relationship
					/*
					$note->load_relationship('teams');
					$note->teams->replace(array($bean->team_id));
					*/
					
					$query='UPDATE accounts_cstm
							SET failed_reaf_note_added_c = "1"
							WHERE id_c = "'.$id.'"';
					$GLOBALS['db']->query($query);
				}
				//find all associated RLIs
				$rli_id = array();
				$query = 'SELECT id
						FROM revenue_line_items
						WHERE account_id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$rli_id[] = $row['id'];
				}
				
				//update status of all associated RLI to "Cancelled Installation"
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Setting status of all associated RLI to "Cancelled Installation", '.date("Y-m-d H:i:s"));
				for ($i = 0; $i < sizeof($rli_id); $i++){
					$query = 'UPDATE revenue_line_items
							SET sales_stage = "Cancelled Installation"
							WHERE id = "'.$rli_id[$i].'"';
					$GLOBALS['db']->query($query);
					$query = 'UPDATE revenue_line_items_cstm
							SET install_c = "", install_scheduled_c = "", uninstall_c = "1", cancel_date_c = "'.date("Y-m-d H:i:s").'"
							WHERE id_c = "'.$rli_id[$i].'"';
					$GLOBALS['db']->query($query);
				}
				
				//find all associated jobs				
				$job_id = array();
				$query = 'SELECT jc.external_key_c as job_id
						FROM abcde_jobs_cstm jc
						JOIN abcde_jobs j ON j.id = jc.id_c
						JOIN accounts_abcde_jobs_1_c aj ON aj.accounts_abcde_jobs_1abcde_jobs_idb = jc.id_c
						WHERE aj.accounts_abcde_jobs_1accounts_ida = "'.$id.'"
						AND j.deleted <> "1"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$job_id[] = $row['job_id'];
				}
				
				//Synchroteam credentials
				//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=STCRED';
				$url = 'http://www.thefilterboss.com/init.php?pid=STCRED';
				$curl = curl_init($url);
				$headr = array();
				$headr[] = 'Accept: application/json';
				$headr[] = 'Content-type: application/json';
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
				curl_setopt($curl, CURLOPT_HTTPGET, true);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//make REST call
				$response = curl_exec($curl);
				curl_close($curl);
				$jsonResponse = json_decode($response, true);
				$encoding_ST = base64_encode($response);
				
				//delete associated jobs
				for ($i = 0; $i < sizeof($job_id); $i++){
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Deleting Synchroteam Job: '.$job_id[$i].', '.date("Y-m-d H:i:s"));
					$url = 'https://apis.synchroteam.com/Api/v1/Jobs/Delete/'.$job_id[$i];
					//open curl for DELETE
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding_ST;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					$code = $jsonResponse['code'];
					
					if($code == 1){
						//attach Note to Account
						$note = new Note();
						$note->name = 'Installation Job #'.$job_id[$i].' has been deleted, '.date("Y-m-d H:i:s");
						$note->parent_type = 'Accounts';
						$note->parent_id = $id;
						//$note->team_id = $bean->team_id;
						//$note->team_set_id = $bean->team_id;
						$note->save();
						//add team relationship
						/*
						$note->load_relationship('teams');
						$note->teams->replace(array($bean->team_id));
						*/
						//change status of Job in Sugar to "cancelled"
						//update Job status in Sugar
						$query = 'UPDATE abcde_jobs_cstm SET status_c = "cancelled" WHERE id_c = "'.$job_id[$i].'"';
						$GLOBALS['db']->query($query);
					}
				}//END OF for ($i = 0; $i < sizeof($job_id); $i++)
				
				//set Account status to "Cancelled"
				$query = 'UPDATE accounts_cstm SET customer_status_c = "Cancelled",
							cancel_reason_dropdown_c = "Failed Reaffirmation",
							cancel_date_c = "'.date("Y-m-d H:i:s").'"
						WHERE id_c = "'.$id.'"';
				$GLOBALS['db']->query($query);
			}//END OF REAF FAILED
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));//uncomment if you wish the hook to ONLY execute once
    }
}
?>