<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-17
 * Time: 오전 9:23
 */

class AfterSaveFilterServicePlansHook
{
    //var $country_code = '';

    function afterSave($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: BEGINNING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));

        $country_code = $this->getCountry($bean->id);

        if ($arguments['isUpdate']) {
            $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: Update record " . date('Y-m-d H:i:s'));

            if ($bean->updated['isactive']) {
                if ($this->isVIP($bean->fspg2_filterservicepackages_id)) {
                    $co_api = $this->createCoApi($country_code);
                    if ($bean->isactive) {
                        if ($co_api !== null) {
                            $resp = $co_api->action(ChargeOverAPI_Object::TYPE_PACKAGE, $bean->co_sub_id, 'unsuspend');
                            $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: CO response => " . print_r($resp, true) . ' ' . date('Y-m-d H:i:s'));
                            if (200 <= $resp->code && $resp->code < 300) {
                                $this->createDetail("[Auto] Chargeover Subscription {$bean->co_sub_id} activated", null, $bean->id);
                                $nextservicedate_from_currentdate = $this->calcNextServiceDate(date('Y-m-d'), $bean->fspg2_filterservicepackages_id);
                                $nextservicedate_from_lastservicedate = $this->calcNextServiceDate($bean->lastservicedate, $bean->fspg2_filterservicepackages_id);
                                $nextservicedate = $nextservicedate_from_currentdate;
                                if ($nextservicedate_from_currentdate < $nextservicedate_from_lastservicedate) {
                                    $nextservicedate = $nextservicedate_from_lastservicedate;
                                }
                                $this->updateNextservicedate($nextservicedate, $bean->id);
                                $this->createDetail("[Auto] Next Service Date updated: {$nextservicedate}", null, $bean->id);
                            }
                        }
                    } else {
                        if ($co_api !== null) {
                            $resp = $co_api->action(ChargeOverAPI_Object::TYPE_PACKAGE, $bean->co_sub_id, 'suspend');
                            $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: CO response => " . print_r($resp, true) . ' ' . date('Y-m-d H:i:s'));
                            if (200 <= $resp->code && $resp->code < 300) {
                                $this->createDetail("[Auto] Chargeover Subscription {$bean->co_sub_id} suspended", null, $bean->id);
                                $this->updateNextservicedate(null, $bean->id);
                                $this->createDetail("[Auto] Next Service Date removed", null, $bean->id);
                            }
                        }
                    }
                } else {

                }
            }

            if ($bean->updated['lastservicedate']) {
                $nextservicedate = $this->calcNextServiceDate($bean->lastservicedate, $bean->fspg2_filterservicepackages_id);
                $this->updateNextservicedate($nextservicedate, $bean->id);
                $this->createDetail("[Auto] Next Service Date updated: {$nextservicedate}", null, $bean->id);
            }

            if ($bean->updated['nextservicedate']) {
                $this->createDetail("[Auto] Next Service Date updated: {$bean->nextservicedate}", null, $bean->id);
            }
        } else {
            $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: Create record " . date('Y-m-d H:i:s'));
        }

        $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: ENDDING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));
    }

    function createCoApi($country_code)
    {
        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$country_code]['username'],
            $sugar_config['chargeover'][$country_code]['password']
        );

        return $api;
    }

    function getCountry($fspl_id)
    {
        require_once('include/SugarQuery/SugarQuery.php');
        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('RevenueLineItems'));
        $query->where()->equals('fspg2_filterserviceplans_id', $fspl_id);
        $result = $query->execute();
        if (count($result) > 0) {
            $account_id = $result[0]['account_id'];

            $account_bean = BeanFactory::getBean('Accounts', $account_id);

            if ($account_bean->billing_address_country === 'Canada') {
                return 'canada';
            }

            if ($account_bean->billing_address_country === 'United States') {
                return 'us';
            }
        }

        return null;
    }

    function calcNextServiceDate($lastservicedate, $fsp_id)
    {
        $fsp_bean = BeanFactory::getBean('FSPG2_FilterServicePackages', $fsp_id);
        $freq = $fsp_bean->frequency;
        if ($lastservicedate === '' || $lastservicedate === null) {
            $lastservicedate = date('Y-m-d');
        }
        $lastservicedate = strtotime("{$lastservicedate} +{$freq} month");
        $nextservicedate = date('Y-m-d', $lastservicedate);
        return $nextservicedate;
    }

    function isVIP($fspg_id)
    {
        $fspg_bean = BeanFactory::getBean('FSPG2_FilterServicePackages', $fspg_id);
        return $fspg_bean->isvip;
    }

    function updateNextservicedate($date, $id)
    {
        if ($date === null) {
            $query = "update fspg2_filterserviceplans set nextservicedate = null where id = '{$id}';";
        } else {
            $query = "update fspg2_filterserviceplans set nextservicedate = '{$date}' where id = '{$id}';";
        }

        $GLOBALS['db']->query($query);
    }

    function updateLastservicedate($date, $id)
    {
        if ($date === null) {
            $query = "update fspg2_filterserviceplans set lastservicedate = null where id = '{$id}';";
        } else {
            $query = "update fspg2_filterserviceplans set lastservicedate = '{$date}' where id = '{$id}';";
        }

        $GLOBALS['db']->query($query);
    }

    function createDetail($name, $description, $id)
    {
        $fsd = BeanFactory::newBean('FSPG2_FilterServiceDetails');
        $fsd->name = $name;
        $fsd->description = $description;
        $fsd->fspg2_filterserviceplans_id = $id;
        $fsd->filterchangedate = null;
        $fsd->save();
    }
}