<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-16
 * Time: 오후 4:08
 */

class BeforeDeleteFilterServicePlansHook
{
    var $country_code = '';

    function beforeDelete($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: BEGINNING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));

        $this->country_code = $this->getCountry($bean->id);

        if ($bean->co_sub_id !== null || $bean->co_sub_id !== '') {
            $co_api = $this->createCoApi();
            if ($co_api !== null) {
                $resp = $co_api->action(ChargeOverAPI_Object::TYPE_PACKAGE, $bean->co_sub_id, 'cancel');
                $GLOBALS['log']->fatal("[{$bean->module_name}][afterSave]: CO response => " . print_r($resp, true) . ' ' . date('Y-m-d H:i:s'));
            }
        }

        $this->updateRLIs($bean->id);

        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: ENDDING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));
    }

    function createCoApi()
    {
        if ($this->country_code === null) {
            return null;
        }

        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$this->country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$this->country_code]['username'],
            $sugar_config['chargeover'][$this->country_code]['password']
        );

        return $api;
    }

    function updateRLIs($fspl_id)
    {
        $query = "update revenue_line_items set fspg2_filterserviceplans_id = null where fspg2_filterserviceplans_id = '{$fspl_id}';";
        $GLOBALS['db']->query($query);
    }

    function getCountry($fspl_id)
    {
        require_once('include/SugarQuery/SugarQuery.php');
        $query = new SugarQuery();
        $query->from(BeanFactory::newBean('RevenueLineItems'));
        $query->where()->equals('fspg2_filterserviceplans_id', $fspl_id);
        $result = $query->execute();
        if (count($result) > 0) {
            $account_id = $result[0]['account_id'];

            $account_bean = BeanFactory::getBean('Accounts', $account_id);

            if ($account_bean->billing_address_country === 'Canada') {
                return 'canada';
            }

            if ($account_bean->billing_address_country === 'United States') {
                return 'us';
            }
        }

        return null;
    }
}