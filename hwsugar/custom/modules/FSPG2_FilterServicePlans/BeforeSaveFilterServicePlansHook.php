<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-29
 * Time: 오후 3:56
 */

class BeforeSaveFilterServicePlansHook
{
    function beforeSave($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeSave]: BEGINNING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));

        if ($arguments['isUpdate']) {
            $GLOBALS['log']->fatal("[{$bean->module_name}][beforeSave]: Update record " . date('Y-m-d H:i:s'));

            $this->createFieldsUpdatedInfo($bean);
        }

        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeSave]: ENDDING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));
    }

    function createFieldsUpdatedInfo($org_bean)
    {
        require_once 'include/SugarQuery/SugarQuery.php';
        // get Filter Service Plans
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean($org_bean->module_name));
        $sugarQuery->where()
            ->queryAnd()
                ->equals('id', $org_bean->id);
        $fspl_row = $sugarQuery->execute();

        $org_bean->updated = array();
        foreach ($org_bean as $key => $value) {
            if (array_key_exists($key, $fspl_row[0])) {
                $org_bean->updated[$key] = false;
                $comparable_var = null;
                switch (gettype($value)) {
                    case 'boolean':
                        $comparable_var = $fspl_row[0][$key] === '1' ? true : false;
                        break;
                    case 'integer':
                        $comparable_var = intval($fspl_row[0][$key]);
                        break;
                    default:
                        $comparable_var = $fspl_row[0][$key];
                        if (is_null($comparable_var)) {
                            $comparable_var = '';
                        }
                }

                if ($comparable_var !== $value) {
                    $GLOBALS['log']->fatal("[{$org_bean->module_name}][beforeSave]: '{$key}' value is updated!!! From DB: {$comparable_var}, From UI: {$value} " . date('Y-m-d H:i:s'));
                    $org_bean->updated[$key] = true;
                }
            }
        }
    }
}