<?php
// created: 2017-07-27 09:03:37
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'fspg2_filterservicepackages_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_FSPG2_FILTERSERVICEPACKAGES_NAME',
    'id' => 'FSPG2_FILTERSERVICEPACKAGES_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'FSPG2_FilterServicePackages',
    'target_record_key' => 'fspg2_filterservicepackages_id',
  ),
  'isactive' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_ISACTIVE',
    'width' => '10%',
  ),
  'lastservicedate' => 
  array (
    'type' => 'date',
    'readonly' => true,
    'vname' => 'LBL_LASTSERVICEDATE',
    'width' => '10%',
    'default' => true,
  ),
  'nextservicedate' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_NEXTSERVICEDATE',
    'width' => '10%',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);