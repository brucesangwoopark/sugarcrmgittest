<?php
$module_name = 'FSPG2_FilterServicePlans';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'isactive',
                'label' => 'LBL_ISACTIVE',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'fspg2_filterservicepackages_name',
                'label' => 'LBL_FSPG2_FILTERSERVICEPACKAGES_NAME',
                'enabled' => true,
                'id' => 'FSPG2_FILTERSERVICEPACKAGES_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'lastservicedate',
                'label' => 'LBL_LASTSERVICEDATE',
                'enabled' => true,
                'readonly' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'nextservicedate',
                'label' => 'LBL_NEXTSERVICEDATE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
