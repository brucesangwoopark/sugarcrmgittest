<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-28
 * Time: 오전 10:34
 */

class GetMailoutList extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getMailoutList' => array(
                'reqType' => 'POST',
                'path' => array('FSPG2_FilterServicePlans', 'getmailoutlist'),
                'method' => 'run',
                'shortHelp' => 'Get Mailout List in JSON',
            )
        );
    }

    function run($api, $args)
    {
        if (!isset($args['nextservicedate'])) {
            return array(
                'result' => 'error',
                'message' => 'nextservicedate is not set'
            );
        }
        $nextservicedate = $args['nextservicedate'];

        $query = $this->getquery($nextservicedate);
        $result = $GLOBALS['db']->query($query);
        $result_set = array();
        while($row = $GLOBALS['db']->fetchByAssoc($result)){
            $result_set[] = $row;
        }

        return array(
            'result' => 'ok',
            'numrow' => $result->num_rows,
            'data' => $result_set
        );
    }

    function getquery($date)
    {
        $query = "
            select 
                fspl.id as fspl_id, 
                fspl.name as fspl_name, 
                fspl.date_entered as fspl_date_entered, 
                fspl.date_modified as fspl_date_modified, 
                fspl.modified_user_id as fspl_modified_user_id, 
                fspl.created_by as fspl_created_by, 
                fspl.description as fspl_description, 
                fspl.deleted as fspl_deleted, 
                fspl.isactive as fspl_isactive, 
                fspl.isenrolled as fspl_isenrolled, 
                fspl.commissionpaid as fspl_commissionpaid, 
                fspl.lastservicedate as fspl_lastservicedate, 
                fspl.nextservicedate as fspl_nextservicedate, 
                fspl.team_id as fspl_team_id, 
                fspl.team_set_id as fspl_team_set_id, 
                fspl.acl_team_set_id as fspl_acl_team_set_id, 
                fspl.assigned_user_id as fspl_assigned_user_id, 
                fspl.fspg2_filterservicepackages_id as fspl_fspg2_filterservicepackages_id, 
                fspl.co_sub_id as fspl_co_sub_id, 
                fspg.id as fspg_id, 
                fspg.name as fspg_name, 
                fspg.date_entered as fspg_date_entered, 
                fspg.date_modified as fspg_date_modified, 
                fspg.modified_user_id as fspg_modified_user_id, 
                fspg.created_by as fspg_created_by, 
                fspg.description as fspg_description, 
                fspg.deleted as fspg_deleted, 
                fspg.producttype as fspg_producttype, 
                fspg.productcount as fspg_productcount, 
                fspg.billtypecode as fspg_billtypecode, 
                fspg.price as fspg_price, 
                fspg.currency_id as fspg_currency_id, 
                fspg.base_rate as fspg_base_rate, 
                fspg.standingrequest as fspg_standingrequest, 
                fspg.secondprice as fspg_secondprice, 
                fspg.installation as fspg_installation, 
                fspg.filtermaintenance as fspg_filtermaintenance, 
                fspg.frequency as fspg_frequency, 
                fspg.isvip as fspg_isvip, 
                fspg.effectivefrom as fspg_effectivefrom, 
                fspg.effectiveto as fspg_effectiveto, 
                fspg.isactive as fspg_isactive, 
                fspg.team_id as fspg_team_id, 
                fspg.team_set_id as fspg_team_set_id, 
                fspg.acl_team_set_id as fspg_acl_team_set_id, 
                fspg.assigned_user_id as fspg_assigned_user_id, 
                fspg.fspg2_filtertypes_id as fspg_fspg2_filtertypes_id, 
                ft.id as ft_id, 
                ft.name as ft_name, 
                ft.date_entered as ft_date_entered, 
                ft.date_modified as ft_date_modified, 
                ft.modified_user_id as ft_modified_user_id, 
                ft.created_by as ft_created_by, 
                ft.description as ft_description, 
                ft.deleted as ft_deleted, 
                ft.length as ft_length, 
                ft.width as ft_width, 
                ft.height as ft_height, 
                ft.weight as ft_weight, 
                ft.team_id as ft_team_id, 
                ft.team_set_id as ft_team_set_id, 
                ft.acl_team_set_id as ft_acl_team_set_id, 
                ft.assigned_user_id as ft_assigned_user_id, 
                ft.price as ft_price, 
                ft.currency_id as ft_currency_id, 
                ft.base_rate as ft_base_rate, 
                ft.weekly_mail_out as ft_weekly_mail_out, 
                ft.shipping_price as ft_shipping_price, 
                rli.id as rli_id, 
                rli.name as rli_name, 
                rli.date_entered as rli_date_entered, 
                rli.date_modified as rli_date_modified, 
                rli.modified_user_id as rli_modified_user_id, 
                rli.created_by as rli_created_by, 
                rli.description as rli_description, 
                rli.deleted as rli_deleted, 
                rli.product_template_id as rli_product_template_id, 
                rli.account_id as rli_account_id, 
                rli.total_amount as rli_total_amount, 
                rli.type_id as rli_type_id, 
                rli.quote_id as rli_quote_id, 
                rli.manufacturer_id as rli_manufacturer_id, 
                rli.category_id as rli_category_id, 
                rli.mft_part_num as rli_mft_part_num, 
                rli.vendor_part_num as rli_vendor_part_num, 
                rli.date_purchased as rli_date_purchased, 
                rli.cost_price as rli_cost_price, 
                rli.discount_price as rli_discount_price, 
                rli.discount_amount as rli_discount_amount, 
                rli.discount_rate_percent as rli_discount_rate_percent, 
                rli.discount_amount_usdollar as rli_discount_amount_usdollar, 
                rli.discount_select as rli_discount_select, 
                rli.deal_calc as rli_deal_calc, 
                rli.deal_calc_usdollar as rli_deal_calc_usdollar, 
                rli.list_price as rli_list_price, 
                rli.cost_usdollar as rli_cost_usdollar, 
                rli.discount_usdollar as rli_discount_usdollar, 
                rli.list_usdollar as rli_list_usdollar, 
                rli.status as rli_status, 
                rli.tax_class as rli_tax_class, 
                rli.website as rli_website, 
                rli.weight as rli_weight, 
                rli.quantity as rli_quantity, 
                rli.support_name as rli_support_name, 
                rli.support_description as rli_support_description, 
                rli.support_contact as rli_support_contact, 
                rli.support_term as rli_support_term, 
                rli.date_support_expires as rli_date_support_expires, 
                rli.date_support_starts as rli_date_support_starts, 
                rli.pricing_formula as rli_pricing_formula, 
                rli.pricing_factor as rli_pricing_factor, 
                rli.serial_number as rli_serial_number, 
                rli.asset_number as rli_asset_number, 
                rli.book_value as rli_book_value, 
                rli.book_value_usdollar as rli_book_value_usdollar, 
                rli.book_value_date as rli_book_value_date, 
                rli.best_case as rli_best_case, 
                rli.likely_case as rli_likely_case, 
                rli.worst_case as rli_worst_case, 
                rli.date_closed as rli_date_closed, 
                rli.date_closed_timestamp as rli_date_closed_timestamp, 
                rli.next_step as rli_next_step, 
                rli.commit_stage as rli_commit_stage, 
                rli.sales_stage as rli_sales_stage, 
                rli.probability as rli_probability, 
                rli.lead_source as rli_lead_source, 
                rli.campaign_id as rli_campaign_id, 
                rli.opportunity_id as rli_opportunity_id, 
                rli.product_type as rli_product_type, 
                rli.assigned_user_id as rli_assigned_user_id, 
                rli.team_id as rli_team_id, 
                rli.team_set_id as rli_team_set_id, 
                rli.currency_id as rli_currency_id, 
                rli.base_rate as rli_base_rate, 
                rli.acl_team_set_id as rli_acl_team_set_id, 
                rli.fspg2_filterserviceplans_id as rli_fspg2_filterserviceplans_id, 
                rlic.id_c as rlic_id_c, 
                rlic.inactive_c as rlic_inactive_c, 
                rlic.from_conversion_c as rlic_from_conversion_c, 
                rlic.uninstall_c as rlic_uninstall_c, 
                rlic.associated_account_c as rlic_associated_account_c, 
                rlic.uninstall_scheduled_c as rlic_uninstall_scheduled_c, 
                rlic.install_scheduled_c as rlic_install_scheduled_c, 
                rlic.cancel_reason_c as rlic_cancel_reason_c, 
                rlic.cancel_date_c as rlic_cancel_date_c, 
                rlic.install_date_c as rlic_install_date_c, 
                rlic.uninstall_date_c as rlic_uninstall_date_c, 
                rlic.install_c as rlic_install_c, 
                rlic.cc_c as rlic_cc_c, 
                rlic.removal_fee_c as rlic_removal_fee_c, 
                rlic.etc_fee_c as rlic_etc_fee_c, 
                rlic.filter_size_c as rlic_filter_size_c, 
                rlic.paperwork_num_c as rlic_paperwork_num_c, 
                rlic.sales_stage_info_c as rlic_sales_stage_info_c, 
                rlic.who_cancelled_c as rlic_who_cancelled_c, 
                rlic.when_cancelled_c as rlic_when_cancelled_c, 
                acc.id as acc_id, 
                acc.name as acc_name, 
                acc.date_entered as acc_date_entered, 
                acc.date_modified as acc_date_modified, 
                acc.modified_user_id as acc_modified_user_id, 
                acc.created_by as acc_created_by, 
                acc.description as acc_description, 
                acc.deleted as acc_deleted, 
                acc.facebook as acc_facebook, 
                acc.twitter as acc_twitter, 
                acc.googleplus as acc_googleplus, 
                acc.account_type as acc_account_type, 
                acc.industry as acc_industry, 
                acc.annual_revenue as acc_annual_revenue, 
                acc.phone_fax as acc_phone_fax, 
                acc.billing_address_street as acc_billing_address_street, 
                acc.billing_address_city as acc_billing_address_city, 
                acc.billing_address_state as acc_billing_address_state, 
                acc.billing_address_postalcode as acc_billing_address_postalcode, 
                acc.billing_address_country as acc_billing_address_country, 
                acc.rating as acc_rating, 
                acc.phone_office as acc_phone_office, 
                acc.phone_alternate as acc_phone_alternate, 
                acc.website as acc_website, 
                acc.ownership as acc_ownership, 
                acc.employees as acc_employees, 
                acc.ticker_symbol as acc_ticker_symbol, 
                acc.shipping_address_street as acc_shipping_address_street, 
                acc.shipping_address_city as acc_shipping_address_city, 
                acc.shipping_address_state as acc_shipping_address_state, 
                acc.shipping_address_postalcode as acc_shipping_address_postalcode, 
                acc.shipping_address_country as acc_shipping_address_country, 
                acc.parent_id as acc_parent_id, 
                acc.sic_code as acc_sic_code, 
                acc.duns_num as acc_duns_num, 
                acc.campaign_id as acc_campaign_id, 
                acc.assigned_user_id as acc_assigned_user_id, 
                acc.team_id as acc_team_id, 
                acc.team_set_id as acc_team_set_id, 
                acc.acl_team_set_id as acc_acl_team_set_id, 
                accc.id_c as accc_id_c, 
                accc.external_key_c as accc_external_key_c, 
                accc.response_code_c as accc_response_code_c, 
                accc.response_message_c as accc_response_message_c, 
                accc.converted_c as accc_converted_c, 
                accc.complete_conversion_c as accc_complete_conversion_c, 
                accc.updated_c as accc_updated_c, 
                accc.customer_status_c as accc_customer_status_c, 
                accc.supkg_systemusers_id_c as accc_supkg_systemusers_id_c, 
                accc.reaf_completed_c as accc_reaf_completed_c, 
                accc.tech_dispatched_c as accc_tech_dispatched_c, 
                accc.notes_c as accc_notes_c, 
                accc.installer_notes_c as accc_installer_notes_c, 
                accc.install_complete_c as accc_install_complete_c, 
                accc.cancel_reason_c as accc_cancel_reason_c, 
                accc.credit_result_c as accc_credit_result_c, 
                accc.synchroteam_external_key_c as accc_synchroteam_external_key_c, 
                accc.synchroteam_updated_c as accc_synchroteam_updated_c, 
                accc.synchroteam_customer_link_c as accc_synchroteam_customer_link_c, 
                accc.failed_reaf_c as accc_failed_reaf_c, 
                accc.user_id_c as accc_user_id_c, 
                accc.supkg_systemusers_id1_c as accc_supkg_systemusers_id1_c, 
                accc.chargeover_customer_link_c as accc_chargeover_customer_link_c, 
                accc.house_type_c as accc_house_type_c, 
                accc.cell_phone_c as accc_cell_phone_c, 
                accc.install_date_c as accc_install_date_c, 
                accc.cancel_date_c as accc_cancel_date_c, 
                accc.cancel_reason_dropdown_c as accc_cancel_reason_dropdown_c, 
                accc.chargeover_payment_link_c as accc_chargeover_payment_link_c, 
                accc.chargeover_instance_c as accc_chargeover_instance_c, 
                accc.unique_id_c as accc_unique_id_c, 
                accc.cancel_all_products_c as accc_cancel_all_products_c, 
                accc.is_renter_owner_c as accc_is_renter_owner_c, 
                accc.failed_reaf_note_added_c as accc_failed_reaf_note_added_c, 
                accc.chargeover_invoice_link_c as accc_chargeover_invoice_link_c, 
                accc.activation_fee_c as accc_activation_fee_c, 
                accc.agent_name_c as accc_agent_name_c, 
                accc.agent_id_c as accc_agent_id_c, 
                accc.chargeover_status_c as accc_chargeover_status_c, 
                accc.compartment_number_c as accc_compartment_number_c
            from fspg2_filterserviceplans fspl
            left join fspg2_filterservicepackages fspg on fspl.fspg2_filterservicepackages_id = fspg.id
            left join fspg2_filtertypes ft on fspg.fspg2_filtertypes_id = ft.id
            left join revenue_line_items rli on rli.fspg2_filterserviceplans_id = fspl.id
            left join revenue_line_items_cstm rlic on rli.id = rlic.id_c
            left join accounts acc on rli.account_id = acc.id
            left join accounts_cstm accc on acc.id = accc.id_c
            where 
                fspl.deleted = '0' and
                fspl.nextservicedate = '{$date}' and
                fspl.isactive = '1' and
                fspg.isvip = '0';
        ";

        return $query;
    }
}