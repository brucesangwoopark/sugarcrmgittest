<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-28
 * Time: 오전 10:40
 */

class UpdateMailoutedFilterServicePlan extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'updateMailoutedFilterServicePlan' => array(
                'reqType' => 'POST',
                'path' => array('FSPG2_FilterServicePlans', 'updatemailoutedfspl'),
                'method' => 'run',
                'shortHelp' => 'Updating Mailouted Filter Service Plan in JSON',
            )
        );
    }

    function run($api, $args)
    {
        if (!isset($args['fspl_id'])) {
            return array(
                'result' => 'error',
                'message' => 'no filter service plan ID'
            );
        }
        $fspl_id = $args['fspl_id'];

        if (!isset($args['trackingnumber'])) {
            return array(
                'result' => 'error',
                'message' => 'no tracking number'
            );
        }
        $trackingnumber = $args['trackingnumber'];

        if (!isset($args['current_date'])) {
            return array(
                'result' => 'error',
                'message' => 'no current date'
            );
        }
        $current_date = $args['current_date'];
        if (!$this->validateDateFormat($current_date)) {
            return array(
                'result' => 'error',
                'message' => 'date format YYYY-MM-DD'
            );
        }

        $detail_title = 'Mailout accomplished';
        if (isset($args['detail_title'])) {
            $detail_title = $args['detail_title'];
        }

        $detail_description = '';
        if (isset($args['detail_description'])) {
            $detail_description = $args['detail_description'];
        }

        $fspl_bean = BeanFactory::getBean('FSPG2_FilterServicePlans', $fspl_id);
        if ($fspl_bean->id === null) {
            return array(
                'result' => 'error',
                'message' => "{$fspl_id} doesn't exist."
            );
        }
        $fspg_id = $fspl_bean->fspg2_filterservicepackages_id;

        $new_nextservicedate = $this->calcNextServiceDate($current_date, $fspg_id);
        $this->updateLastAndNextServiceDate($current_date, $new_nextservicedate, $fspl_id);

        $detail_id = $this->createDetail($detail_title, $detail_description, $trackingnumber, $fspl_id);

        return array(
            'result' => 'ok',
            'fspl_id' => $fspl_id,
            'nxtservicedate' => $new_nextservicedate,
            'detail_id' => $detail_id
        );
    }

    function calcNextServiceDate($current_date, $fsp_id)
    {
        $fsp_bean = BeanFactory::getBean('FSPG2_FilterServicePackages', $fsp_id);
        $freq = $fsp_bean->frequency;
        $nextservicedate = date('Y-m-d', "{$current_date} +{$freq} month");
        return $nextservicedate;
    }

    function updateLastAndNextServiceDate($current_date, $nextservicedate, $id)
    {
        $query = "
            update fspg2_filterserviceplans
            set 
                lastservicedate = '{$current_date}',
                nextservicedate = '{$nextservicedate}'
            where id = '{$id}';
        ";
        $result = $GLOBALS['db']->query($query);
    }

    function createDetail($name, $description, $trackingnumber, $id)
    {
        $fsd = BeanFactory::newBean('FSPG2_FilterServiceDetails');
        $fsd->name = $name;
        $fsd->description = $description;
        $fsd->trackingnumber = $trackingnumber;
        $fsd->fspg2_filterserviceplans_id = $id;
        $detail_id = $fsd->save();

        return $detail_id;
    }

    function validateDateFormat($date)
    {
        $date = DateTime::createFromFormat('Y-m-d', $date);
        if ($date) {
            return true;
        }

        return false;
    }
}