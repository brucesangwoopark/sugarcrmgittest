<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-14
 * Time: 오후 2:56
 */

class CreateFilterServicePlans extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'createFilterServicePlan' => array(
                'reqType' => 'POST',
                'path' => array('FSPG2_FilterServicePlans', 'create'),
                'method' => 'createFilterServicePlan',
                'shortHelp' => 'Creating Filter Service Plans in JSON',
            )
        );
    }

    function createFilterServicePlan($api, $args)
    {
        $account_id = $args['account_id'];
        $opportunity_id = $args['opportunity_id'];
        $fsp_id = $args['fsp_id'];
        $rli_ids = $args['rli_ids'];

        $GLOBALS['log']->fatal("Beginning creating Filter Service Plan " . date('Y-m-d H:i:s'));

        $co_customer_id = $this->getCoCustomerID($account_id);
        $country_code = $this->getCountry($account_id);

        $co_sub_info = $this->getCoSubInfo($opportunity_id, $country_code); // return holduntil_datetime, paymethod, creditcard_id, ach_id
        if ($co_sub_info === null) {
            $GLOBALS['log']->fatal("Aborting!!! Filter Service Plan, No chargeover subscription " . date('Y-m-d H:i:s'));
            return array(
                'result' => 'error',
                'message' => 'The selected opportunity doesn\'t have a Chargeover subscription.',
            );
        }

        $co_custom_vals = $this->generateCoCustVals($opportunity_id, $country_code); // return custom_2, custom_3
        $ra_num = $this->getRANumber($opportunity_id);

        $fsp_info = $this->getFilterServicePackage($fsp_id);
        $fsp_isvip = $fsp_info->isvip;

        // initialize variable for Chargeover Sub ID
        $co_sub_id = null;
        if ($fsp_isvip === '1') {
            $GLOBALS['log']->fatal("VIP Package selected, Package ID => {$fsp_id} " . date('Y-m-d H:i:s'));

            $resp = $this->createCoPackage(
                $country_code,
                array(
                    'paymethod' => $co_sub_info['paymethod'],
                    'ach_id' => $co_sub_info['ach_id'],
                    'creditcard_id' => $co_sub_info['creditcard_id'],
                    'holduntil_datetime' => $co_sub_info['holduntil_datetime'],
                    'custom_2' => $co_custom_vals['custom_2'],
                    'custom_3' => $co_custom_vals['custom_3'],
                    'customer_id' => $co_customer_id,
                ),
                array(
                    'id' => $fsp_id,
                    'name' => $fsp_info->name,
                    'price' => $fsp_info->price,
                ),
                $ra_num);
            if (200 <= $resp->code && $resp->code < 300) {
                $GLOBALS['log']->fatal('Create Filter Service Plan CO Response => ' . print_r($resp, true) . ' ' . date("Y-m-d H:i:s"));
                $co_sub_id = $resp->response->id;
            } else {
                return array(
                    'result' => 'error',
                    'message' => 'Chargeover error',
                );
            }
        }

        $fspl_bean = BeanFactory::newBean('FSPG2_FilterServicePlans');
        $fspl_name = str_replace(' - Opportunity', '', $this->getOpportunityName($opportunity_id));
        $fspl_bean->name = "{$fspl_name} - FSP";
        $fspl_bean->isactive = true;
        if ($co_sub_id !== null) {
            $fspl_bean->co_sub_id = $co_sub_id;
        }
        $fspl_bean->nextservicedate = $this->calcNextServiceDate($fsp_id);
        $fspl_bean->fspg2_filterservicepackages_id = $fsp_id;
        $ret_save = $fspl_bean->save();
        if ($ret_save !== false) {
            $fspl_id = $ret_save;
            $this->updateRLIs($rli_ids, $fspl_bean->id);
            if ($co_sub_id === null) { // Chargeover not applicable
                $GLOBALS['log']->fatal("Ending creating Filter Service Plan, Non VIP " . date('Y-m-d H:i:s'));
                return array(
                    'result' => 'ok',
                    'type' => 'non-co',
                );
            } else {
                $resp = $this->updateExKeyCoPackage($co_sub_id, $fspl_id, $country_code);
                if (200 <= $resp->code && $resp->code < 300) {
                    $GLOBALS['log']->fatal("Ending creating Filter Service Plan, VIP " . date('Y-m-d H:i:s'));
                    return array(
                        'result' => 'ok',
                        'type' => 'co',
                        'co_customer_id' => $co_customer_id,
                        'co_package_id' => $co_sub_id,
                        'fsp_id' => $fspl_id,
                    );
                }
            }
        }

        $GLOBALS['log']->fatal("Aborting!! Filter Service Plan, save() => {$ret_save} " . date('Y-m-d H:i:s'));
        return array(
            'result' => 'error',
            'message' => 'module save error',
        );
    }

    function updateRLIs($rli_ids, $fspl_id)
    {
        foreach ($rli_ids as $rli_id) {
            $query = "update revenue_line_items set fspg2_filterserviceplans_id = '{$fspl_id}' where id = '{$rli_id}';";
            $GLOBALS['db']->query($query);
        }
    }

    function calcNextServiceDate($fsp_id)
    {
        $fsp_bean = BeanFactory::getBean('FSPG2_FilterServicePackages', $fsp_id);
        $freq = $fsp_bean->frequency;
        $nextservicedate = date('Y-m-d', strtotime(date('Y-m-d') . " +{$freq} month"));
        return $nextservicedate;
    }

    function getOpportunityName($opportunity_id)
    {
        $opportunity_bean = BeanFactory::getBean('Opportunities', $opportunity_id);
        return $opportunity_bean->name;
    }

    function getFilterServicePackage($fsp_id)
    {
        $fsp_bean = BeanFactory::getBean('FSPG2_FilterServicePackages', $fsp_id);
        return $fsp_bean;
    }

    function getCoSubscriptionID($opportunity_id)
    {
        $opportunity_bean = BeanFactory::getBean('Opportunities', $opportunity_id);
        return $opportunity_bean->external_key_c;
    }

    function getCoCustomerID($account_id)
    {
        $account_bean = BeanFactory::getBean('Accounts', $account_id);
        return intval($account_bean->external_key_c);
    }

    function getAccountNumber($account_id)
    {
        $account_bean = BeanFactory::getBean('Accounts', $account_id);
        return $account_bean->unique_id_c;
    }

    function getCountry($account_id)
    {
        $account_bean = BeanFactory::getBean('Accounts', $account_id);

        if ($account_bean->billing_address_country === 'Canada') {
            return 'canada';
        }

        if ($account_bean->billing_address_country === 'United States') {
            return 'us';
        }

        return null;
    }

    function getRANumber($opportunity_id)
    {
        $opportunity_bean = BeanFactory::getBean('Opportunities', $opportunity_id);
        return $opportunity_bean->ra_num_c;
    }

    function getCoSubInfo($opportunity_id, $country_code)
    {
        $subscription_id = $this->getCoSubscriptionID($opportunity_id);
        $api = $this->createCoApi($country_code);
        $response = $api->findById(ChargeOverAPI_Object::TYPE_PACKAGE, $subscription_id);
        $GLOBALS['log']->fatal('CO response => ' . print_r($response, true) . ' ' . date('Y-m-d H:i:s'));

        $holduntil_datetime = substr($response->response->holduntil_datetime, 0, 10);
        if (isset($response->response->next_invoice_datetime)) {
            $next_invoice_datetime = substr($response->response->next_invoice_datetime, 0, 10);
            if ($holduntil_datetime < $next_invoice_datetime) {
                $holduntil_datetime = $next_invoice_datetime;
            }
        }

        if (200 <= $response->code && $response->code < 300) {
            return array(
                'holduntil_datetime' => $holduntil_datetime,
                'paymethod' => $response->response->paymethod,
                'creditcard_id' => $response->response->creditcard_id,
                'ach_id' => $response->response->ach_id,
            );
        }

        return null;
    }

    function generateCoCustVals($opportunity_id, $country_code)
    {
        if ($country_code === 'us') {
            return array(
                'custom_2' => '',
                'custom_3' => ''
            );
        }

        if ($country_code === 'canada') {
            $opportunity_bean = BeanFactory::getBean('Opportunities', $opportunity_id);
            if ($opportunity_bean->enbridge_customer_c === '1') {
                return array(
                    'custom_2' => 'enbridge',
                    'custom_3' => $opportunity_bean->enbridge_number_c
                );
            } else {
                return array(
                    'custom_2' => 'direct',
                    'custom_3' => ''
                );
            }
        }

        return null;
    }

    function createCoApi($country_code)
    {
        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$country_code]['username'],
            $sugar_config['chargeover'][$country_code]['password']
        );

        return $api;
    }

    function updateExKeyCoPackage($package_id, $external_key, $country_code)
    {
        $co_api = $this->createCoApi($country_code);

        require_once 'chargeover/ChargeOverAPI.php';

        $co_package = new ChargeOverAPI_Object_Package(array(
            'external_key' => $external_key,
        ));

        $resp = $co_api->modify($package_id, $co_package);

        return $resp;
    }

    function createCoPackage($country_code, $co_info, $fsp_info, $ra_num)
    {
        $co_api = $this->createCoApi($country_code);

        require_once 'chargeover/ChargeOverAPI.php';

        // create package
        $co_package = new ChargeOverAPI_Object_Package();
        $co_package->setTermsId(3);
        if ($country_code === 'canada') {
            $co_package->setCurrencyId(2);
        } else {
            $co_package->setCurrencyId(1);
        }
        $co_package->setNickname('Filter Service Plan');
        $co_package->setPaymethod($co_info['paymethod']);
        if ($co_info['paymethod'] === 'cre') {
            $co_package->setCreditcardId($co_info['creditcard_id']);
        }
        if ($co_info['paymethod'] === 'ach') {
            $co_package->setAchId($co_info['ach_id']);
        }
        $co_package->setCustom_1($ra_num);
        $co_package->setCustom_2($co_info['custom_2']);
        $co_package->setCustom_3($co_info['custom_3']);
        $co_package->setCustom_5('Sugar');
        $co_package->setHolduntilDatetime($co_info['holduntil_datetime']);
        $co_package->setCustomerId($co_info['customer_id']);

        // create item
        $lineitem = new ChargeOverAPI_Object_LineItem();
        if ($country_code === 'canada') {
            $lineitem->setItemId(172);
        } else { // country_code is 'us'
            $lineitem->setItemId(12);
        }
        $lineitem->setDescrip($fsp_info['name']);
        $lineitem->setCustom_1($fsp_info['id']);

        $tierset = new ChargeOverAPI_Object_Tierset();
        $tierset->setBase($fsp_info['price']);
        $tierset->setPricemodel('fla');

        $lineitem->setTierset($tierset);

        $co_package->addLineItems($lineitem);

        $resp = $co_api->create($co_package);

        return $resp;
    }
}