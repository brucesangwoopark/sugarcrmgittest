<?php
$popupMeta = array (
    'moduleMain' => 'ProductTemplates',
    'varName' => 'ProductTemplate',
    'orderBy' => 'producttemplates.name',
    'whereClauses' => array (
  'name' => 'producttemplates.name',
  'category_name' => 'producttemplates.category_name',
),
    'searchInputs' => array (
  0 => 'name',
  1 => 'category_name',
),
    'searchdefs' => array (
  0 => 'name',
  1 => 'category_name',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'TYPE_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_TYPE',
    'sortable' => true,
    'default' => true,
    'name' => 'type_name',
  ),
  'COST_PRICE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_COST_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'cost_price',
  ),
  'UPFRONT_FEE_C' => 
  array (
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_UPFRONT_FEE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'CATEGORY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_CATEGORY',
    'sortable' => true,
    'default' => true,
    'name' => 'category_name',
  ),
  'COMBO_C' => 
  array (
    'type' => 'bool',
    'default' => true,
    'label' => 'LBL_COMBO',
    'width' => '10%',
    'name' => 'combo_c',
  ),
  'COUNTRY_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_COUNTRY',
    'width' => '10%',
    'name' => 'country_c',
  ),
),
);
