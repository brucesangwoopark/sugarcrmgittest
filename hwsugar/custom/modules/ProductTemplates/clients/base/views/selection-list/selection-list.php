<?php
$viewdefs['ProductTemplates'] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'favorites' => false,
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'link' => true,
                'enabled' => true,
                'default' => true,
              ),
              1 => 
              array (
                'name' => 'type_name',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'cost_price',
                'type' => 'currency',
                'related_fields' => 
                array (
                  0 => 'cost_usdollar',
                  1 => 'currency_id',
                  2 => 'base_rate',
                ),
                'currency_field' => 'currency_id',
                'base_rate_field' => 'base_rate',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'upfront_fee_c',
                'label' => 'LBL_UPFRONT_FEE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'category_name',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'combo_c',
                'label' => 'LBL_COMBO',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'country_c',
                'label' => 'LBL_COUNTRY',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'qty_in_stock',
                'enabled' => true,
                'default' => false,
              ),
              8 => 
              array (
                'name' => 'list_price',
                'type' => 'currency',
                'related_fields' => 
                array (
                  0 => 'list_usdollar',
                  1 => 'currency_id',
                  2 => 'base_rate',
                ),
                'currency_field' => 'currency_id',
                'base_rate_field' => 'base_rate',
                'enabled' => true,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'attributes_c',
                'label' => 'LBL_ATTRIBUTES',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'discount_price',
                'type' => 'currency',
                'related_fields' => 
                array (
                  0 => 'discount_usdollar',
                  1 => 'currency_id',
                  2 => 'base_rate',
                ),
                'currency_field' => 'currency_id',
                'base_rate_field' => 'base_rate',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
