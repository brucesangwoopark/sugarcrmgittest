<?php
// created: 2017-11-06 17:26:12
$viewdefs['ProductTemplates']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'category_name' => 
    array (
    ),
    'attributes_c' => 
    array (
    ),
    'country_c' => 
    array (
    ),
    'active_c' => 
    array (
    ),
  ),
  'quicksearch_field' => 
  array (
    0 => 'name',
    1 => 'country_c',
  ),
  'quicksearch_priority' => 10,
);