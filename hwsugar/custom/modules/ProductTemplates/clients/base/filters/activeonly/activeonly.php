<?php
$module_name = "ProductTemplates";
$viewdefs[$module_name]['base']['filter']['activeonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'activeonly',
            'name'              => 'LBL_ACTIVE_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'active_c' => '1',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>