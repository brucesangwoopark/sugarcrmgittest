<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class LeadLineItemsHook {
	
	//$count tracks the number of hook executions
	static $count = 0;
	
  function afterSave($bean, $event, $arguments)
  {
		self::$count++;
		//for logging purposes
		$module = "LeadLineItems";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK #'.self::$count.', LLI: '.$id.', '.date("Y-m-d H:i:s"));
					
		try{		
		}
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK #'.self::$count.', '.date("Y-m-d H:i:s"));
	}
}
?>