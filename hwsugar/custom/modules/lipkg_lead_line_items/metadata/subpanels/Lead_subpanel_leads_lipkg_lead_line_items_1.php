<?php
// created: 2016-11-07 21:05:57
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'lipkg_lead_line_items_producttemplates_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_PRODUCTTEMPLATES_TITLE',
    'id' => 'LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATESPRODUCTTEMPLATES_IDA',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ProductTemplates',
    'target_record_key' => 'lipkg_lead_line_items_producttemplatesproducttemplates_ida',
  ),
  'sales_stage_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_SALES_STAGE',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);