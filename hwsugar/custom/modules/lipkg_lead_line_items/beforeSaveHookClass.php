<?php

require_once 'include/utils.php';

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class BeforeSaveLeadLineItemsHook {
    static $already_ran = false;
    
    function beforeSave($bean, $event, $arguments)
    {	
        if(self::$already_ran == true) return;//uncomment if you wish the hook to ONLY execute once
		self::$already_ran = true;//uncomment if you wish the hook to ONLY execute once
		
		$date_entered = $bean->date_entered;
		$date_modified = $bean->date_modified;
    
		$module = "LeadLineItems";
		$hook = "before_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, LLI: '.$id.', '.date("Y-m-d H:i:s"));
        
        try {
            // retrieve lead id
            $query = "select *
                    from leads_lipkg_lead_line_items_1_c
                    where leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = '" . $id . "'";
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$lead_id = $row['leads_lipkg_lead_line_items_1leads_ida'];
                $GLOBALS['log']->fatal('retrieved lead id from lead id => ' . $lead_id);
			}
            
            // retrieve lead's coutry
            $query = "select *
                    from leads
                    where id = '" . $lead_id . "'";
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$country_lead = $row['primary_address_country'];
                $GLOBALS['log']->fatal('retrieved country code from lead id => ' . $country_lead);
			}
            
            $query = "select country_c
                    from 
                        product_templates pt,
                        product_templates_cstm ptc,
                        lipkg_lead_line_items_producttemplates_c lllip
                    where
                        lllip.lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb = '" . $id . "' and
                        pt.id = ptc.id_c and
                        lllip.lipkg_lead_line_items_producttemplatesproducttemplates_ida = pt.id;";
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$country_product = $row['country_c'];
                $GLOBALS['log']->fatal('retrieved country from product => ' . $country_product);
			}
			
			//if ($country_product === 'United States')
			//	$country_product = 'US';
			
//			if ($country_product !== $country_lead) {
//				die('the countries are not matched!!!');
//			}
            
        } catch (Exception $e1) {
            $GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
        }
        
        $GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
    }
}
