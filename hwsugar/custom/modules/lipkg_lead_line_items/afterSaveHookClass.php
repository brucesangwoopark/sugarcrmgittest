<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveLeadLineItemsHook {
	
	//to ensure hook only executes once
	static $already_ran = false;
	
  function afterSave($bean, $event, $arguments)
  {
		if(self::$already_ran == true) return;//to ensure hook only executes once
		self::$already_ran = true;//to ensure hook only executes once
		
		//for logging purposes
		$module = "LeadLineItems";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, LLI: '.$id.', '.date("Y-m-d H:i:s"));
					
		try{
			//get associated Lead id and ra_num
			// Only execute on new LLI record creation
			
			if (empty($bean->fetched_row['id'])) {
				$query = 'SELECT l.id as lead_id, lc.ra_num_c as ra_num
						FROM leads_cstm lc
						JOIN leads l ON l.id = lc.id_c
						JOIN leads_lipkg_lead_line_items_1_c lb ON lb.leads_lipkg_lead_line_items_1leads_ida = l.id
						JOIN lipkg_lead_line_items li ON li.id = lb.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb 
						WHERE li.id = "'.$id.'"';
				$counter = 0; //to ensure below loop only executes once
				$result = $GLOBALS['db']->query($query);
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$lead_id = $row['lead_id'];
					$ra_num = $row['ra_num'];
					$counter++;
				}
				//get the item#, i.e. how many LLI there are for that Lead
				$query = 'SELECT count(*) as item_num
						FROM leads_lipkg_lead_line_items_1_c
						WHERE leads_lipkg_lead_line_items_1leads_ida = "'.$lead_id.'"';		
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$item_num = $row['item_num'];
				}
				$name = $ra_num.' - '.$item_num;
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Filling in default name of: '.$name.', '.date("Y-m-d H:i:s"));
				$query = 'UPDATE lipkg_lead_line_items
						SET name = "'.$name.'"
						WHERE id = "'.$id.'"';
				$GLOBALS['db']->query($query);
				//$bean->name = $name;
				//$bean->save();
			}//END OF if (empty($bean->fetched_row['id']))
			//update activation_fee_c field
			//check if Lead is in Canada or US
			$query = 'SELECT primary_address_country as country
					FROM leads l
					JOIN leads_lipkg_lead_line_items_1_c llli ON llli.leads_lipkg_lead_line_items_1leads_ida = l.id
					WHERE llli.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = "'.$id.'"';
			$country = 0; //setting default
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$country = $row['country'];
			}
			if ($country == "United States"){
				//get current value of activation_fee_c on Leads
				$query = 'SELECT lc.activation_fee_c as activation_fee
						FROM leads_cstm lc
						JOIN leads_lipkg_lead_line_items_1_c llli ON llli.leads_lipkg_lead_line_items_1leads_ida = lc.id_c
						WHERE llli.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = "'.$id.'"';
				$lead_activation_fee = 0; //setting default
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$lead_activation_fee = $row['activation_fee'];
				}
				
				//get the product price of the current LLI
				$query = 'SELECT pt.cost_price as product_price
						FROM product_templates pt
						JOIN lipkg_lead_line_items_producttemplates_c llipt ON llipt.lipkg_lead_line_items_producttemplatesproducttemplates_ida = pt.id
						JOIN lipkg_lead_line_items lli ON lli.id = llipt.lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb
						WHERE lli.id = "'.$id.'"';
				$product_price = ""; //setting default
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$product_price = $row['product_price'];
				}
				
				//update product_price on LLI
				$query = 'UPDATE lipkg_lead_line_items
						SET product_price = "'.$product_price.'"
						WHERE id = "'.$id.'"';
				$GLOBALS['db']->query($query);
				
				//update activation_fee_c field on Lead, if needed
				if($product_price > $lead_activation_fee){
					$query = 'UPDATE leads_cstm lc
							JOIN leads_lipkg_lead_line_items_1_c llli ON llli.leads_lipkg_lead_line_items_1leads_ida = lc.id_c
							SET lc.activation_fee_c = "'.$product_price.'"
							WHERE llli.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = "'.$id.'"';
					$GLOBALS['db']->query($query);
				}
			}//END OF if ($country == "United States")
		}
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
	}
}
?>
