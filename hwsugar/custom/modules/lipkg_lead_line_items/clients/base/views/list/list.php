<?php
$module_name = 'lipkg_lead_line_items';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'lipkg_lead_line_items_producttemplates_name',
                'label' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_PRODUCTTEMPLATES_TITLE',
                'enabled' => true,
                'id' => 'LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATESPRODUCTTEMPLATES_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'leads_lipkg_lead_line_items_1_name',
                'label' => 'LBL_LEADS_LIPKG_LEAD_LINE_ITEMS_1_FROM_LEADS_TITLE',
                'enabled' => true,
                'id' => 'LEADS_LIPKG_LEAD_LINE_ITEMS_1LEADS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'product_price',
                'label' => 'LBL_PRODUCT_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
