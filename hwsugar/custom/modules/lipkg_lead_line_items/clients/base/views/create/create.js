({
    extendsFrom: 'CreateView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.addValidationTask('validate_selected_product', _.bind(this._validateSelectedProduct, this));
    },

    _validateSelectedProduct: function (fields, errors, callback) {
        var insertMessage = function (message) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            var target = document.getElementById('drawers').firstChild.firstChild.childNodes[1].firstChild.firstChild.childNodes[1].childNodes[4];
            var node = document.createElement('div');

            node.innerHTML = message;
            node.style.fontSize = '15px';
            node.style.fontWeight = 'bold';
            node.style.color = '#f44242';
            node.style.height = '50px';
            node.className = 'addValCustABCDErrMsg'; // stupid name to ensure I am the only one using this class
            target.insertBefore(node, target.firstChild);
        };

        var pt_bean = App.data.createBean('ProductTemplates', {
            id: this.model.attributes.lipkg_lead_line_items_producttemplatesproducttemplates_ida
        });
        var lead_country = this.model.link.bean.attributes.primary_address_country;
        pt_bean.fetch({
            success: function (data) {
                console.log(data);
                var pt_country = data.get('country_c');
                if (lead_country !== pt_country) {
                    insertMessage('Check whether or not the selected product\'s country and Lead customer\'s country are the same.');

                    errors['lipkg_lead_line_items_producttemplates_name'] = errors['lipkg_lead_line_items_producttemplates_name'] || {};
                    errors['lipkg_lead_line_items_producttemplates_name'].wrong_country = true;
                }

                callback(null, fields, errors);
            },
            error: function () {
                /* do stuff */
            }
        });
    }
})