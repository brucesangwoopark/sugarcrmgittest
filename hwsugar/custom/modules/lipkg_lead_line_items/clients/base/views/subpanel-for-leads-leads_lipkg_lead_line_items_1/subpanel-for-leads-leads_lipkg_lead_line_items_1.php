<?php
// created: 2016-11-07 21:06:02
$viewdefs['lipkg_lead_line_items']['base']['view']['subpanel-for-leads-leads_lipkg_lead_line_items_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'lipkg_lead_line_items_producttemplates_name',
          'label' => 'LBL_LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATES_FROM_PRODUCTTEMPLATES_TITLE',
          'enabled' => true,
          'id' => 'LIPKG_LEAD_LINE_ITEMS_PRODUCTTEMPLATESPRODUCTTEMPLATES_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'sales_stage_c',
          'label' => 'LBL_SALES_STAGE',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_entered',
          'label' => 'LBL_DATE_ENTERED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        4 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);