({
    extendsFrom: 'RelateField',

    initialize: function (options) {
        this._super('initialize', [options]);
    },

    getFilterOptions: function () {
        var lead_country = this.model.link.bean.get('primary_address_country');
        var custom_FilterOptions = new app.utils.FilterOptions().config({
            'initial_filter': 'pt_location_filter_template',
            'initial_filter_label': 'Active/Country',
            'filter_populate': {
                'country_c': [lead_country],
                'active_c': '1',
            }
        }).format();

        return custom_FilterOptions;
    },

    openSelectDrawer: function () {
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                filterOptions: this.getFilterOptions(),
                parent: this.context
            }
        }, _.bind(this.setValue, this));
    }
})