<?php
// created: 2017-08-01 11:40:20
$viewdefs['FSPG2_FilterServicePackages']['base']['view']['subpanel-for-fspg2_filtertypes-fspg2_filterservicepackages'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'isactive',
          'label' => 'LBL_ISACTIVE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'isvip',
          'label' => 'LBL_ISVIP',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'frequency',
          'label' => 'LBL_FREQUENCY',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'effectivefrom',
          'label' => 'LBL_EFFECTIVEFROM',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'effectiveto',
          'label' => 'LBL_EFFECTIVETO',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);