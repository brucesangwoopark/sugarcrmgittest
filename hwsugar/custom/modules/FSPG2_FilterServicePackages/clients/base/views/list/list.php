<?php
$module_name = 'FSPG2_FilterServicePackages';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'isactive',
                'label' => 'LBL_ISACTIVE',
                'enabled' => true,
                'default' => true,
                'width' => 'xxsmall',
              ),
              2 => 
              array (
                'name' => 'isvip',
                'label' => 'LBL_ISVIP',
                'enabled' => true,
                'default' => true,
                'width' => 'xxsmall',
              ),
              3 => 
              array (
                'name' => 'price',
                'label' => 'LBL_PRICE',
                'enabled' => true,
                'related_fields' => 
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'frequency',
                'label' => 'LBL_FREQUENCY',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'effectivefrom',
                'label' => 'LBL_EFFECTIVEFROM',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'effectiveto',
                'label' => 'LBL_EFFECTIVETO',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
