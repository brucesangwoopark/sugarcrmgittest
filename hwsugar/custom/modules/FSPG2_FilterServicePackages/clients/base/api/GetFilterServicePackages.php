<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-11
 * Time: 오후 4:20
 */

class GetFilterServicePackages extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getFilterServicePackages' => array(
                'reqType' => 'POST',
                'path' => array('FSPG2_FilterServicePackages', 'list'),
                'method' => 'getFilterServicePackageList',
                'shortHelp' => 'Returning all Filter Service Packages in JSON',
            )
        );
    }

    function getFilterServicePackageList($api, $args)
    {
        $current_date = date('Y-m-d');

        require_once('include/SugarQuery/SugarQuery.php');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::newBean('FSPG2_FilterServicePackages'));
        $sugarQuery->where()->equals('isactive', '1');
        $results = $sugarQuery->execute();
        $result_rows = array();
        foreach ($results as $row) {
            if ($row['effectiveto'] === null) {
                if ($row['effectivefrom'] <= $current_date) {
                    $result_rows[] = $row;
                }
            } else {
                if ($row['effectivefrom'] <= $current_date && $current_date <= $row['effectiveto']) {
                    $result_rows[] = $row;
                }
            }
        }

        return array(
            'result' => 'ok',
            'data' => $result_rows
        );
    }
}
