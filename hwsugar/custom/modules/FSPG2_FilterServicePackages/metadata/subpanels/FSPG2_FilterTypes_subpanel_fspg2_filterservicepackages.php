<?php
// created: 2017-08-01 11:40:19
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'isactive' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_ISACTIVE',
    'width' => '10%',
  ),
  'isvip' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_ISVIP',
    'width' => '10%',
  ),
  'frequency' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_FREQUENCY',
    'width' => '10%',
  ),
  'effectivefrom' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EFFECTIVEFROM',
    'width' => '10%',
    'default' => true,
  ),
  'effectiveto' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EFFECTIVETO',
    'width' => '10%',
    'default' => true,
  ),
);