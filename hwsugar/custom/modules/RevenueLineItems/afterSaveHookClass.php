<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveRevenueLineItemHook {
	
	//static $already_ran = false;
	
  function afterSave($bean, $event, $arguments)
  {
		
		//if(self::$already_ran == true) return;
		//self::$already_ran = true;
		
		//for logging purposes
		$module = "RevenueLineItems";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, RevenueLineItem id: '.$id.', '.date("Y-m-d H:i:s"));

		try{
			//check which user last modified the record
			$user_name = "";
			$query = 'SELECT CONCAT(first_name, " ", last_name) as user_name
									FROM users
									WHERE id = "'.$bean->modified_user_id.'"';
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$user_name = $row['user_name'];
			}
			
			//change default name in case of new RLI created OUTSIDE of Lead conversion
			$date_entered = $bean->date_entered;
			$date_modified = $bean->date_modified;
    	//if (($date_entered === $date_modified) && ($bean->from_conversion_c != "1")){ // only new RLI, not updates, NOT from conversion
			$query = 'SELECT from_conversion_c FROM revenue_line_items_cstm WHERE id_c = "'.$id.'"';
			$result = $GLOBALS['db']->query($query);
			$counter = 0; //to ensure below only executes once
			$from_conversion = 0; //setting default
			while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
				$from_conversion = $row ['from_conversion_c'];
				$counter++;
			}
				
			if(($from_conversion != "1") && ($date_entered === $date_modified)){
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: RLI created outside of Lead conversion, '.date("Y-m-d H:i:s"));
				
				$query = 'SELECT oc.id_c as opportunity_id, oc.ra_num_c as ra_num
						FROM opportunities_cstm oc
						JOIN revenue_line_items rli ON rli.opportunity_id = oc.id_c
						WHERE rli.id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure below only executes once
				$opportunity_id = 0;
				$ra_num = 0; //setting default
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$opportunity_id = $row ['opportunity_id'];
					$ra_num = $row['ra_num'];
					$counter++;
				}
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Opportunity: '.$opportunity_id.', with ra_num: '.$ra_num.' '.date("Y-m-d H:i:s"));
				
				$account_id = $bean->associated_account_c;
				
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Account: '.$account_id.', '.date("Y-m-d H:i:s"));
				
				
				$schedule_job = false;
				
				if(!empty($account_id)){
					$schedule_job = true;
				}
				
				//check if installation job has already been scheduled
				$install_scheduled = 0; //setting default
				$query = 'SELECT install_scheduled_c FROM revenue_line_items_cstm WHERE id_c = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure below only executes once
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$install_scheduled = $row['install_scheduled_c'];
					$counter++;
				}
				if($install_scheduled){
					$schedule_job = false;
				}
				
				if($schedule_job){
					
					$num_items = 0;//setting default
					$query = 'SELECT COUNT(*) as num_items
							FROM revenue_line_items
							WHERE opportunity_id = "'.$opportunity_id.'"
							AND deleted <> "1"';
					$result = $GLOBALS['db']->query($query);
					while($row = $GLOBALS['db']->fetchByAssoc($result)){
						$num_items = $row['num_items'];
					}
					$name = $ra_num.' - '.$num_items;
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Saving default name: '.$name.', '.date("Y-m-d H:i:s"));
					$query = 'UPDATE revenue_line_items SET name = "'.$name.'" WHERE id = "'.$id.'"';
					$GLOBALS['db']->query($query);
					//$bean->name = $name;
					//$bean->save();
					
					//
					//
					//
					//NEW JOB CREATION
					//
					//
					//
					$job = BeanFactory::newBean('abcde_Jobs');
					
					//get all needed job attributes
					$query = 'SELECT rli.opportunity_id as opportunity_id, oc.ra_num_c as ra_num, ao.account_id as account_id, rli.product_template_id as product
							FROM revenue_line_items rli
							JOIN opportunities_cstm oc ON oc.id_c = rli.opportunity_id
							JOIN accounts_opportunities ao ON ao.opportunity_id = oc.id_c
							WHERE rli.id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure the below only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$opportunity_id = $row['opportunity_id'];
						$ra_num = $row['ra_num'];
						$account_id = $row['account_id'];
						$product = $row['product'];
						$counter++;
					}
					
					
					
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Account: '.$account_id.', '.date("Y-m-d H:i:s"));
					
					$num_jobs = 0;
					$query = 'SELECT COUNT(*) as num_jobs
							FROM accounts_abcde_jobs_1_c aj
							WHERE aj.accounts_abcde_jobs_1accounts_ida = "'.$account_id.'"';
					$result = $GLOBALS['db']->query($query);
					while($row = $GLOBALS['db']->fetchByAssoc($result)){
						$num_jobs = $row['num_jobs'];
					}
														
					//set job attributes
					$job->opportunity_id_c = $opportunity_id;
					$job->associated_account_c = $account_id;
					$job->product_c = $product;
					//
					//
					//FIND JOB TYPE BASED ON PRODUCT CATEGORY
					//
					//
					$query = 'SELECT pc.name as product_category
							FROM product_categories pc
							JOIN revenue_line_items rli ON rli.category_id = pc.id
							WHERE rli.id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below loop only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
					{
						$product_category = $row['product_category'];
						$counter++;
					}
					if($product_category == "Whole Home" || $product_category == "D-Scaler"){
							$job->job_type_c = 'Install WH or DS';
					}
					if($product_category == "Under Counter"){
						$job->job_type_c = 'Install UC';
					}
					//
					//
					//END OF JOB TYPE MAPPING
					//
					//
					
					
					//$job->job_type_c = 'Install UC';
					$name = $ra_num.' - Job #'.($num_jobs+1).' - Installation';
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new installation job: '.$name.', '.date("Y-m-d H:i:s"));
					$job->name = $name;
					
					//set from_conversion flag to "true" - it will act as though it was created from conversion originally
					$job->from_conversion_c = 'true';
					$job->num_items_c = 1;
					$job->team_id = $bean->team_id;
					$job->team_set_id = $bean->team_id;
					$job->save(); //note: this will trigger the hook in the Jobs module, with above attributes set
					
					//add relationship between Job and RLI
					$job->load_relationship('abcde_jobs_revenuelineitems_1');
					$job->abcde_jobs_revenuelineitems_1->add($id);
					//add relationship between job and team based on RLI primary team
					/*
					$job->load_relationship('teams');
					$job->teams->replace(array($bean->team_id));
					*/
					$job->save();
					
					//set install_scheduled flag
					$query = 'UPDATE revenue_line_items_cstm SET install_scheduled_c = "1" WHERE id_c = "'.$id.'"';
					$GLOBALS['db']->query($query);
					//
					//
					//
					//END OF JOB CREATION
					//
					//
					//
				}//END OF if($schedule_job)
				else{// i.e. if(!$schedule_job)
					//trigger the Opportunity hook again
					//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Triggering Opportunity hook again, '.date("Y-m-d H:i:s"));
					//$opp = BeanFactory::getBean('Opportunities', $opportunity_id);
					//$opp->save();
				}
			}//END OF if (($date_entered === $date_modified) && ($bean->from_conversion_c != "1")), i.e. NEW RLI
			
			if(($bean->sales_stage == "Pre Install" || $bean->sales_stage == "Scheduled Installation") && $bean->uninstall_c == "1"){ //PFC - Pre Flow Cancel
				//create Note stating that the "cancel product" checkbox was checked
				static $cancel_note_added = false;
				if(!$cancel_note_added){
					$note = BeanFactory::newBean('Notes');
					$note->name = '"Cancel Product" checked by User: '.$user_name;
					$note->description = '"Cancel Product" checked by User: '.$user_name.' for item: '.$bean->name.'.  Item was in "'.$bean->sales_stage.'" status before cancellation, i.e. a pre-flow cancel. Date: '.date("Y-m-d H:i:s");
					$note->parent_type = 'Accounts';
					$note->parent_id = $bean->account_id;
					$note->team_id = $bean->team_id;
					$note->team_set_id = $bean->team_id;
					$note->save();
					//add team relationship
					/*
					$note->load_relationship('teams');
					$note->teams->replace(array($bean->team_id));
					*/
					$cancel_note_added = true;
				}
				//find most recent associated "Installation" job
				$query = 'SELECT jc.external_key_c as job_id, jc.id_c as sugar_job_id
								FROM abcde_jobs_cstm jc
								JOIN abcde_jobs j ON j.id = jc.id_c
								JOIN abcde_jobs_revenuelineitems_1_c jrli ON jrli.abcde_jobs_revenuelineitems_1abcde_jobs_ida = jc.id_c
								WHERE jrli.abcde_jobs_revenuelineitems_1revenuelineitems_idb = "'.$id.'"
								AND j.deleted <> "1"
								AND ((jc.status_c <> "deleted" AND jc.status_c <> "cancelled") OR jc.status_c IS NULL)';
				$result = $GLOBALS['db']->query($query);
				$job_id = 0; //setting default
				$sugar_job_id = 0; //setting default
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$job_id = $row['job_id'];
					$sugar_job_id = $row['sugar_job_id'];
				}
				if($job_id != 0){
					//Synchroteam credentials
					//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=STCRED';
					$url = 'http://www.thefilterboss.com/init.php?pid=STCRED';
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-type: application/json';
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_HTTPGET, true);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					$st_encoding = base64_encode($response);
					
					//cancel the job
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Cancelling Synchroteam Job: '.$job_id.', '.date("Y-m-d H:i:s"));
					$url = 'https://apis.synchroteam.com/Api/v2/Jobs/Cancel?num='.$job_id;
					//open curl for PUT
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$st_encoding;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, "");
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					$code = $jsonResponse['code'];
																			
					if($code == 1){ //i.e.if job cancelled
						//update Job status in Sugar
						$query = 'UPDATE abcde_jobs_cstm SET status_c = "cancelled" WHERE id_c = "'.$sugar_job_id.'"';
						$GLOBALS['db']->query($query);
						//attach a note stating that job was deleted on associated Account
						static $cancel_job_note_added = false; //ensures note only added once
						if(!$cancel_job_note_added){
							$note = new Note();
							$note->name = 'Installation Job #'.$job_id.' has been cancelled by User: '.$user_name.', '.date("Y-m-d H:i:s");
							$note->description = '"Cancel Product" checked by User: '.$user_name.' for item: '.$bean->name.'.  Item was in "'.$bean->sales_stage.'" status before cancellation and associated installation job is automatically cancelled. Date: '.date("Y-m-d H:i:s");
							$note->parent_type = 'Accounts';
							$note->parent_id = $bean->account_id;
							$note->team_id = $bean->team_id;
							$note->team_set_id = $bean->team_id;
							$note->save();
							$cancel_job_note_added = true;
							//add team relationship
							/*
							$note->load_relationship('teams');
							$note->teams->replace(array($bean->team_id));
							*/
						}
					}//END OF if($code == 1), i.e. if job deleted
					
					
					
					$query = 'UPDATE revenue_line_items rli
										JOIN revenue_line_items_cstm rlic ON rlic.id_c = rli.id
										SET rli.sales_stage = "Cancelled Installation",
												rlic.install_c = "0",
												rlic.install_scheduled_c = "0",
												rlic.uninstall_c = "1",
												rlic.uninstall_scheduled_c = "0"';
					if(empty($bean->cancel_date_c)) $query .= ',rlic.cancel_date_c = "'.date("Y-m-d H:i:s").'"';
					$query .=	' WHERE rli.id = "'.$id.'"';
					$GLOBALS['db']->query($query);
					
					//SETTING bean variables in case where RLI is triggered again
					
					if(empty($bean->cancel_date_c)) $bean->cancel_date_c = date("Y-m-d H:i:s");
					$bean->sales_stage = "Cancelled Installation";
					$bean->install_c = "0"; //unchecks "install" on item
					$bean->install_scheduled_c = "0"; //removes flag from "install_scheduled_c". i.e. there is no longer an installation job scheduled for item
					$bean->uninstall_c = "1";
					$bean->uninstall_scheduled_c = "0";
					
					//check if there are any "Installed" or "Pre Install" RLI associated with the Account
					$num_active_items = 0; //setting default
					$query = 'SELECT COUNT(*) as num_active_items
							FROM revenue_line_items rli
							WHERE (rli.sales_stage = "Installed" OR rli.sales_stage = "Pre Install")
							AND rli.account_id = "'.$bean->account_id.'"';
					$result = $GLOBALS['db']->query($query);
					while($row = $GLOBALS['db']->fetchByAssoc($result))
					{
						$num_active_items = $row['num_active_items'];
					}
					if($num_active_items == 0){
						//update "customer status" for the Account to "Installed"
						$query = 'UPDATE accounts_cstm SET customer_status_c = "Cancelled" WHERE id_c = "'.$bean->account_id.'"';
						$GLOBALS['db']->query($query);
						//update cancel_date_c field IF it is empty
						$query = 'SELECT cancel_date_c FROM accounts_cstm WHERE id_c = "'.$bean->account_id.'"';
						$result = $GLOBALS['db']->query($query);
						$cancel_date = 0; //setting default
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
							$cancel_date = $row['cancel_date_c'];
						}
						if($cancel_date == 0){
							$query = 'UPDATE accounts_cstm SET cancel_date_c = "'.date("Y-m-d H:i:s").'" WHERE id_c = "'.$bean->account_id.'"';
							$GLOBALS['db']->query($query);
						}
					}


                    // mail notification for cancel job
                    $account_bean = BeanFactory::getBean('Accounts', $bean->account_id);
                    $state = $account_bean->billing_address_state;
                    $country = $account_bean->billing_address_country;

                    if ($state === 'CA' && $country === 'United States') {
                        $customer_name = str_replace(' - Account', '', $account_bean->name);
                        $customer_office_phone = $account_bean->phone_office;
                        $customer_mobile_phone = $account_bean->cell_phone_c;

                        $customer_address = $account_bean->billing_address_street . ', ' . $account_bean->billing_address_city . ', ' . $account_bean->billing_address_state . ', ' . $account_bean->billing_address_country . ' ' . $account_bean->billing_address_postalcode;
                        if ($account_bean->compartment_number_c !== null) {
                        	$customer_address = '#' . $account_bean->compartment_number_c . ', ' . $customer_address;
						}

                        $agent_name = '';
                        $ra_num = '';
                        $job_bean = BeanFactory::getBean('abcde_Jobs', $job_id);
                        $opp_beans = $job_bean->opportunities_abcde_jobs_1->getBeans();
                        foreach ($opp_beans as $opp_bean) {
                            $agent_name = $opp_bean->agent_c;
                            $ra_num = $opp_bean->ra_num_c;
                        }

                        $mail_subject = "Job Cancellation #{$job_id} - {$customer_name}";
                        $mail_body = "
									Job ID: <b>{$job_id}</b><br>
									Name: <b>{$customer_name}</b><br>
									Address: <b>{$customer_address}</b><br>
									Office Phone: <b>{$customer_office_phone}</b><br>
									Mobile Phone: <b>{$customer_mobile_phone}</b><br>
									Agent: <b>{$agent_name}</b><br>
									RA #: <b>{$ra_num}</b><br>
								";

                        $mailer = MailerFactory::getSystemDefaultMailer();
                        $mailer->setSubject($mail_subject);
                        $mailer->setHtmlBody($mail_body);
                        $mailer->addRecipientsTo(new EmailIdentity('chaznedaroglu@homewater.com', 'Can Haznedaroglu'));
                        //$mailer->addRecipientsTo(new EmailIdentity('tburgess@homewater.com', 'Todd Burgess'));
                        $mailer->addRecipientsTo(new EmailIdentity('install@homewateramerica.com', 'Install Homewater'));
                        if ($mailer->send()) {
                            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: Notification Email sent ' . date("Y-m-d H:i:s"));
                        } else {
                            $GLOBALS['log']->fatal('[' . $module . '][' . $hook . ']: ERROR: Mail sending failed!');
                        }
                    }
				}//END OF if($job_id != 0)
			}//END OF PFC - Pre Flow Cancel
			
			if($bean->uninstall_c == "1" && empty($bean->uninstall_scheduled_c) && $bean->sales_stage == "Installed"){ //Cancellation of Installed item
				//create Note stating that the "cancel product" checkbox was checked
				static $cancel_note_added = false;
				if(!$cancel_note_added){
					$note = BeanFactory::newBean('Notes');
					$note->name = '"Cancel Product" checked by User: '.$user_name;
					$note->description = '"Cancel Product" checked by User: '.$user_name.' for item: '.$bean->name.'.  Item was in "Installed" status before cancellation. Date: '.date("Y-m-d H:i:s");
					$note->parent_type = 'Accounts';
					$note->parent_id = $bean->account_id;
					$note->team_id = $bean->team_id;
					$note->team_set_id = $bean->team_id;
					$note->save();
					//add team relationship
					/*
					$note->load_relationship('teams');
					$note->teams->replace(array($bean->team_id));
					*/
					$cancel_note_added = true;
				}
				//get all needed job attributes
				$opportunity_id = "";
				$ra_num = "";
				$product = "";
				$query = 'SELECT rli.opportunity_id as opportunity_id, oc.ra_num_c as ra_num, rli.product_template_id as product
						FROM revenue_line_items rli
						JOIN opportunities_cstm oc ON oc.id_c = rli.opportunity_id
						JOIN accounts_opportunities ao ON ao.opportunity_id = oc.id_c
						WHERE rli.id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$opportunity_id = $row['opportunity_id'];
					$ra_num = $row['ra_num'];
					$product = $row['product'];
				}
				//check how many job are assocaited to Account
				$num_jobs = 0;
				$query = 'SELECT COUNT(*) as num_jobs
						FROM accounts_abcde_jobs_1_c aj
						WHERE aj.accounts_abcde_jobs_1accounts_ida = "'.$bean->account_id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$num_jobs = $row['num_jobs'];
				}
				//BEGINNING OF UNINSTALLAION JOB CREATION
				//create new uninstall job
				$name = $ra_num.' - Job #'.($num_jobs+1).' - Uninstallation';
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new uninstallation job: '.$name.', '.date("Y-m-d H:i:s"));
				$job = BeanFactory::newBean('abcde_Jobs');
				$job->name = $name;
				//find product type, i.e. which type of uninstallation job to create
				$query = 'SELECT pc.name as product_category
						FROM product_categories pc
						JOIN revenue_line_items rli ON rli.category_id = pc.id
						WHERE rli.id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result))
				{
					$product_category = $row['product_category'];
				}
				if($product_category == "Whole Home" || $product_category == "D-Scaler"){
					$job->job_type_c = 'Uninstall WH or DS';
				}
				if($product_category == "Under Counter"){
					$job->job_type_c = 'Uninstall UC';
				}
				$job->opportunity_id_c = $opportunity_id;
				$job->from_conversion_c = 'true'; //set from_conversion flag to "true" - it will act as though it was created from conversion originally
				$job->num_items_c = 1;
				$job->team_id = $bean->team_id;
				$job->team_set_id = $bean->team_id;
				$job->save(); //note: this will trigger the hook in the Jobs module, with above attributes set
				//add relationship between Job and RLI
				$job->load_relationship('abcde_jobs_revenuelineitems_1');
				$job->abcde_jobs_revenuelineitems_1->add($id);
				//add relationship between job and team based on RLI primary team
				/*
				$job->load_relationship('teams');
				$job->teams->replace(array($bean->team_id));
				*/
				$job->save();
				//END OF UNINSTALLATION JOB CREATION
				
				//set appropriate install/uninstall flags on item
				$query = 'UPDATE revenue_line_items rli
									JOIN revenue_line_items_cstm rlic ON rlic.id_c = rli.id
									SET rlic.install_c = "0",
											rlic.install_scheduled_c = "0",
											rlic.uninstall_c = "1",
											rlic.uninstall_scheduled_c = "1"';
				if(empty($bean->cancel_date_c)) $query .= ',rlic.cancel_date_c = "'.date("Y-m-d H:i:s").'"';
				$query .=	' WHERE rli.id = "'.$id.'"';
				$GLOBALS['db']->query($query);

                $GLOBALS['log']->fatal('['.$module.']['.$hook.']: Saving who cancelled: '. $bean->modified_user_id .', '.date("Y-m-d H:i:s"));
                $this->updateWhoCancelled($bean->id, $GLOBALS['current_user']->id, date("Y-m-d H:i:s"));
				
				//SETTING bean variables in case where RLI is triggered again
				if(empty($bean->cancel_date_c)) $bean->cancel_date_c = date("Y-m-d H:i:s");
				$bean->install_c = "0";
				$bean->install_scheduled_c = "0";
				$bean->uninstall_c = "1";
				$bean->uninstall_scheduled_c = "1"; //set uninstall scheduled flag - this prevents more than one uninstallation job from being scheduled
				
			}//END OF Cancellation of Installed item
			
			if($bean->install_c == "1" && empty($bean->install_scheduled_c) && $bean->sales_stage != "Installed"){ //Reschedule installation job
				//create Note stating that the "install product" checkbox was checked
				static $install_note_added = false;
				if(!$install_note_added){
					$note = BeanFactory::newBean('Notes');
					$note->name = '"Install" Product checked by User: '.$user_name.', '.date("Y-m-d H:i:s");
					$note->description = '';
					$note->description = '"Install" Product checked by User: '.$user_name.' for item: '.$bean->name.'.  Item is currently in "'.$bean->sales_stage.'" status. Date: '.date("Y-m-d H:i:s");
					$note->parent_type = 'Accounts';
					$note->parent_id = $bean->account_id;
					$note->team_id = $bean->team_id;
					$note->team_set_id = $bean->team_id;
					$note->save();
					//add team relationship
					/*
					$note->load_relationship('teams');
					$note->teams->replace(array($bean->team_id));
					*/
					$install_note_added = true;
				}
				//get all needed job attributes
				$opportunity_id = "";
				$ra_num = "";
				$product = "";
				$query = 'SELECT rli.opportunity_id as opportunity_id, oc.ra_num_c as ra_num, rli.product_template_id as product
						FROM revenue_line_items rli
						JOIN opportunities_cstm oc ON oc.id_c = rli.opportunity_id
						JOIN accounts_opportunities ao ON ao.opportunity_id = oc.id_c
						WHERE rli.id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure the below only executes once
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$opportunity_id = $row['opportunity_id'];
					$ra_num = $row['ra_num'];
					$product = $row['product'];
				}
				//check how many job are assocaited to Account
				$num_jobs = 0;
				$query = 'SELECT COUNT(*) as num_jobs
						FROM accounts_abcde_jobs_1_c aj
						WHERE aj.accounts_abcde_jobs_1accounts_ida = "'.$bean->account_id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$num_jobs = $row['num_jobs'];
				}
				//BEGINNING OF UNINSTALLAION JOB CREATION			
				//create new install job
				$name = $ra_num.' - Job #'.($num_jobs+1).' - Installation';
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new Installation job: '.$name.', '.date("Y-m-d H:i:s"));
				$job = BeanFactory::newBean('abcde_Jobs');						
				$job->name = $name;
				$query = 'SELECT pc.name as product_category
						FROM product_categories pc
						JOIN revenue_line_items rli ON rli.category_id = pc.id
						WHERE rli.id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result))
				{
					$product_category = $row['product_category'];
				}
				if($product_category == "Whole Home" || $product_category == "D-Scaler"){
					$job->job_type_c = 'Install WH or DS';
				}
				if($product_category == "Under Counter"){
					$job->job_type_c = 'Install UC';
				}
				$job->opportunity_id_c = $opportunity_id;
				$job->from_conversion_c = 'true'; //set from_conversion flag to "true" - it will act as though it was created from conversion originally
				$job->num_items_c = 1;
				$job->team_id = $bean->team_id;
				$job->team_set_id = $bean->team_id;
				$job->save(); //note: this will trigger the hook in the Jobs module, with above attributes set
				//add relationship between Job and RLI
				$job->load_relationship('abcde_jobs_revenuelineitems_1');
				$job->abcde_jobs_revenuelineitems_1->add($id);
				//add relationship between job and team based on RLI primary team
				/*
				$job->load_relationship('teams');
				$job->teams->replace(array($bean->team_id));
				*/
				$job->save();
				//END OF INSTALLAION JOB CREATION			
				//set appropriate install/uninstall flags on item
				//SETTING bean variables in case where RLI is triggered again
				$bean->install_c = "1";
				$bean->install_scheduled_c = "1"; //set install scheduled flag - this prevents more than one installation job from being scheduled
				$bean->uninstall_c = "0"; //unchecks "cancel product" on item
				$bean->uninstall_scheduled_c = "0";
				
				$query = 'UPDATE revenue_line_items rli
									JOIN revenue_line_items_cstm rlic ON rlic.id_c = rli.id
									SET rlic.install_c = "1",
											rlic.install_scheduled_c = "1",
											rlic.uninstall_c = "0",
											rlic.uninstall_scheduled_c = "0"
									WHERE rli.id = "'.$id.'"';
				$GLOBALS['db']->query($query);	

			}//END OF Reschedule installation job
			
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK '.date("Y-m-d H:i:s"));
	}

	function updateWhoCancelled($rli_id, $user_id, $cancelled_date)
	{
        $query = "UPDATE revenue_line_items rli
				  JOIN revenue_line_items_cstm rlic ON rlic.id_c = rli.id
				  SET 
				  	rlic.who_cancelled_c = '{$user_id}',
				  	rlic.when_cancelled_c = '{$cancelled_date}'
				  WHERE rli.id = '{$rli_id}'";

        $GLOBALS['db']->query($query);
	}
}
?>
