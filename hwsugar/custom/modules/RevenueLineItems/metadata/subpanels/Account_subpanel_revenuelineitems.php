<?php
// created: 2017-03-02 13:33:45
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'product_template_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT',
    'id' => 'PRODUCT_TEMPLATE_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ProductTemplates',
    'target_record_key' => 'product_template_id',
  ),
  'category_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_CATEGORY_NAME',
    'id' => 'CATEGORY_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ProductCategories',
    'target_record_key' => 'category_id',
  ),
  'cost_price' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_COST_PRICE',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => '10%',
    'default' => true,
  ),
  'serial_number' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SERIAL_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'install_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_INSTALL',
    'width' => '10%',
  ),
  'uninstall_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_UNINSTALL',
    'width' => '10%',
  ),
  'install_date_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_INSTALL_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'cancel_date_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CANCEL_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'uninstall_date_c' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_UNINSTALL_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'discount_usdollar' => 
  array (
    'usage' => 'query_only',
    'sortable' => false,
  ),
  'currency_id' => 
  array (
    'name' => 'currency_id',
    'usage' => 'query_only',
  ),
);