<?php
// created: 2016-11-25 17:49:03
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'product_template_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_PRODUCT',
    'id' => 'PRODUCT_TEMPLATE_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ProductTemplates',
    'target_record_key' => 'product_template_id',
  ),
  'category_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'editview' => false,
      'detailview' => false,
      'quickcreate' => false,
    ),
    'vname' => 'LBL_CATEGORY_NAME',
    'id' => 'CATEGORY_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'ProductCategories',
    'target_record_key' => 'category_id',
  ),
  'serial_number' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_SERIAL_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'sales_stage' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_SALES_STAGE',
    'width' => '10%',
    'default' => true,
  ),
  'uninstall_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_UNINSTALL',
    'width' => '10%',
  ),
  'discount_usdollar' => 
  array (
    'usage' => 'query_only',
    'sortable' => false,
  ),
  'currency_id' => 
  array (
    'name' => 'currency_id',
    'usage' => 'query_only',
  ),
);