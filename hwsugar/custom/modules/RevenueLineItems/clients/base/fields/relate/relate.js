({
    extendsFrom: 'RelateField',

    initialize: function (options) {
        this._super('initialize', [options]);
    },

    getFilterOptions: function (force) {
        if (this._filterOptions && !force) {
            return this._filterOptions;
        }

        var access_token = this.getAccessToken();
        var account_id = this.model.get('account_id');
        if (account_id === undefined) {
            if (this.model.link === undefined) {
                account_id = null;
            } else {
                account_id = this.model.link.bean.get('account_id');
                if (account_id === undefined) {
                    account_id = null;
                }
            }
        }

        var filter_body = {
            'initial_filter': 'pt_location_filter_template',
            'initial_filter_label': 'Active/Country',
            'filter_populate': {
                //'country_c': [country],
                'active_c': '1',
            }
        };

        if (account_id !== null) {
            $.ajax({
                url: '/hwsugar/rest/v10/RevenueLineItems/account',
                type: 'POST',
                data: {
                    account_id: account_id
                },
                headers: {
                    'OAuth-Token': access_token
                },
                dataType: 'json',
                async: false,
                success: function (result) {
                    filter_body.filter_populate.country_c = [result.account_country];
                },
                error: function (result) {
                    console.log('pullAccount Error!!');
                }
            });
        }

        this._filterOptions = new app.utils.FilterOptions().config(filter_body).format();

        return this._filterOptions;
    },

    openSelectDrawer: function () {
        app.drawer.open({
            layout: 'selection-list',
            context: {
                module: this.getSearchModule(),
                filterOptions: this.getFilterOptions(),
                parent: this.context
            }
        }, _.bind(this.setValue, this));
    },

    getAccessToken: function () {
        var access_token = localStorage.getItem('prod:SugarCRM:AuthAccessToken');
        if (access_token !== null) {
            access_token = access_token.replace("'", "");
            access_token = access_token.replace("'", "");
        }
        return access_token;
    }
})