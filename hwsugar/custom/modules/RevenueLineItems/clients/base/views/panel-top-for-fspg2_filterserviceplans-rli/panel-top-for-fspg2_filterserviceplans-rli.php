<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-23
 * Time: 오후 11:58
 */

$viewdefs['RevenueLineItems']['base']['view']['panel-top-for-fspg2_filterserviceplans-rli'] = array(
    'type' => 'panel-top',
    'buttons' => array(
        array(
            'type' => 'actiondropdown',
            'name' => 'panel_dropdown',
            'css_class' => 'pull-right',
            'buttons' => array(
                array(
                    'type' => 'sticky-rowaction',
                    'icon' => 'fa-plus',
                    'name' => 'create_button',
                    'dismiss_label' => true,
                    'label' => ' ',
                    'acl_action' => 'create',
                    'tooltip' => 'LBL_CREATE_BUTTON_LABEL',
                    'css_class' => 'disabled',
                ),
                array(
                    'type' => 'link-action',
                    'name' => 'select_button',
                    'label' => 'LBL_ASSOC_RELATED_RECORD',
                    'css_class' => 'disabled',
                ),
            ),
        ),
    ),
);