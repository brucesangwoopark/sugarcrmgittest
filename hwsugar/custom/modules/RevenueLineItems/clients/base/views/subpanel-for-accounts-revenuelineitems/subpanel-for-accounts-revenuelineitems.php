<?php
// created: 2017-03-02 13:33:45
$viewdefs['RevenueLineItems']['base']['view']['subpanel-for-accounts-revenuelineitems'] = array (
  'type' => 'subpanel-list',
  'favorite' => true,
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'product_template_name',
          'sortable' => false,
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category_name',
          'sortable' => false,
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'cost_price',
          'label' => 'LBL_COST_PRICE',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'serial_number',
          'label' => 'LBL_SERIAL_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'install_c',
          'label' => 'LBL_INSTALL',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'uninstall_c',
          'label' => 'LBL_UNINSTALL',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'install_date_c',
          'label' => 'LBL_INSTALL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'cancel_date_c',
          'label' => 'LBL_CANCEL_DATE',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'uninstall_date_c',
          'label' => 'LBL_UNINSTALL_DATE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'selection' => 
  array (
  ),
  'rowactions' => 
  array (
    'css_class' => 'pull-right',
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
      ),
    ),
  ),
);