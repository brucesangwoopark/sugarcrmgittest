<?php
// created: 2016-12-08 01:30:25
$viewdefs['RevenueLineItems']['base']['view']['subpanel-for-opportunities-revenuelineitems'] = array (
  'type' => 'subpanel-list',
  'favorite' => true,
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'product_template_name',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category_name',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'serial_number',
          'label' => 'LBL_SERIAL_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_closed',
          'label' => 'LBL_DATE_CLOSED',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'date_closed_timestamp',
          ),
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'probability',
          'label' => 'LBL_PROBABILITY',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'commit_stage',
          'label' => 'LBL_COMMIT_STAGE_FORECAST',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'probability',
          ),
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'quantity',
          'label' => 'LBL_QUANTITY',
          'enabled' => true,
          'default' => true,
        ),
        8 => 'date_closed',
        9 => 'sales_stage',
        10 => 'probability',
        11 => 'commit_stage',
        12 => 'quantity',
      ),
    ),
  ),
  'selection' => 
  array (
    'type' => 'multi',
    'actions' => 
    array (
      0 => 
      array (
        'name' => 'quote_button',
        'type' => 'button',
        'label' => 'LBL_GENERATE_QUOTE',
        'primary' => true,
        'events' => 
        array (
          'click' => 'list:massquote:fire',
        ),
        'acl_action' => 'massquote',
      ),
      1 => 
      array (
        'name' => 'delete_button',
        'type' => 'button',
        'label' => 'LBL_DELETE',
        'acl_action' => 'delete',
        'primary' => true,
        'events' => 
        array (
          'click' => 'list:massdelete:fire',
        ),
        'related_fields' => 
        array (
          0 => 'sales_stage',
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'css_class' => 'pull-right',
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-eye',
        'acl_action' => 'view',
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
      ),
    ),
  ),
);