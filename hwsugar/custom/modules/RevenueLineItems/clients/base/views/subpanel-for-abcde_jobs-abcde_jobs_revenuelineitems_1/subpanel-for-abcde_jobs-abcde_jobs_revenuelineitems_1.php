<?php
// created: 2016-11-25 17:49:06
$viewdefs['RevenueLineItems']['base']['view']['subpanel-for-abcde_jobs-abcde_jobs_revenuelineitems_1'] = array (
  'favorite' => true,
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'product_template_name',
          'sortable' => false,
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'category_name',
          'sortable' => false,
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'serial_number',
          'label' => 'LBL_SERIAL_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'sales_stage',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'uninstall_c',
          'label' => 'LBL_UNINSTALL',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);