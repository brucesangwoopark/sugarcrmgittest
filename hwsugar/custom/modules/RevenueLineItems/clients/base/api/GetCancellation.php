<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-20
 * Time: 오전 8:31
 */

class GetCancellation extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getReport' => array(
                'reqType' => 'POST',
                'path' => array('RevenueLineItems', 'cancellationreport'),
                'method' => 'getReport',
                'shortHelp' => 'Returning Cancelled RevenueLineItems in JSON',
                //'noLoginRequired' => true,
            )
        );
    }

    public function getReport($api, $args)
    {
        $from = $args['from'];
        $to = $args['to'];

        $query = "
            select 
                rli.id as 'rli_id',
                rli.name as 'rli_name',
                rli.date_entered as 'rli_date_entered',
                rli.date_modified as 'rli_date_modified',
                rli.modified_user_id as 'rli_modified_user_id',
                rli.created_by as 'rli_created_by',
                rli.description as 'rli_description',
                rli.deleted as 'rli_deleted',
                rli.product_template_id as 'rli_product_template_id',
                rli.account_id as 'rli_account_id',
                rli.total_amount as 'rli_total_amount',
                rli.type_id as 'rli_type_id',
                rli.quote_id as 'rli_quote_id',
                rli.manufacturer_id as 'rli_manufacturer_id',
                rli.category_id as 'rli_category_id',
                rli.mft_part_num as 'rli_mft_part_num',
                rli.vendor_part_num as 'rli_vendor_part_num',
                rli.date_purchased as 'rli_date_purchased',
                rli.cost_price as 'rli_cost_price',
                rli.discount_price as 'rli_discount_price',
                rli.discount_amount as 'rli_discount_amount',
                rli.discount_rate_percent as 'rli_discount_rate_percent',
                rli.discount_amount_usdollar as 'rli_discount_amount_usdollar',
                rli.discount_select as 'rli_discount_select',
                rli.deal_calc as 'rli_deal_calc',
                rli.deal_calc_usdollar as 'rli_deal_calc_usdollar',
                rli.list_price as 'rli_list_price',
                rli.cost_usdollar as 'rli_cost_usdollar',
                rli.discount_usdollar as 'rli_discount_usdollar',
                rli.list_usdollar as 'rli_list_usdollar',
                rli.status as 'rli_status',
                rli.tax_class as 'rli_tax_class',
                rli.website as 'rli_website',
                rli.weight as 'rli_weight',
                rli.quantity as 'rli_quantity',
                rli.support_name as 'rli_support_name',
                rli.support_description as 'rli_support_description',
                rli.support_contact as 'rli_support_contact',
                rli.support_term as 'rli_support_term',
                rli.date_support_expires as 'rli_date_support_expires',
                rli.date_support_starts as 'rli_date_support_starts',
                rli.pricing_formula as 'rli_pricing_formula',
                rli.pricing_factor as 'rli_pricing_factor',
                rli.serial_number as 'rli_serial_number',
                rli.asset_number as 'rli_asset_number',
                rli.book_value as 'rli_book_value',
                rli.book_value_usdollar as 'rli_book_value_usdollar',
                rli.book_value_date as 'rli_book_value_date',
                rli.best_case as 'rli_best_case',
                rli.likely_case as 'rli_likely_case',
                rli.worst_case as 'rli_worst_case',
                rli.date_closed as 'rli_date_closed',
                rli.date_closed_timestamp as 'rli_date_closed_timestamp',
                rli.next_step as 'rli_next_step',
                rli.commit_stage as 'rli_commit_stage',
                rli.sales_stage as 'rli_sales_stage',
                rli.probability as 'rli_probability',
                rli.lead_source as 'rli_lead_source',
                rli.campaign_id as 'rli_campaign_id',
                rli.opportunity_id as 'rli_opportunity_id',
                rli.product_type as 'rli_product_type',
                rli.assigned_user_id as 'rli_assigned_user_id',
                rli.team_id as 'rli_team_id',
                rli.team_set_id as 'rli_team_set_id',
                rli.currency_id as 'rli_currency_id',
                rli.base_rate as 'rli_base_rate',
                rli.acl_team_set_id as 'rli_acl_team_set_id',
                rli.fspg2_filterserviceplans_id as 'rli_fspg2_filterserviceplans_id',
                rlic.id_c as 'rlic_id_c',
                rlic.inactive_c as 'rlic_inactive_c',
                rlic.from_conversion_c as 'rlic_from_conversion_c',
                rlic.uninstall_c as 'rlic_uninstall_c',
                rlic.associated_account_c as 'rlic_associated_account_c',
                rlic.uninstall_scheduled_c as 'rlic_uninstall_scheduled_c',
                rlic.install_scheduled_c as 'rlic_install_scheduled_c',
                rlic.cancel_reason_c as 'rlic_cancel_reason_c',
                rlic.cancel_date_c as 'rlic_cancel_date_c',
                rlic.install_date_c as 'rlic_install_date_c',
                rlic.uninstall_date_c as 'rlic_uninstall_date_c',
                rlic.install_c as 'rlic_install_c',
                rlic.cc_c as 'rlic_cc_c',
                rlic.removal_fee_c as 'rlic_removal_fee_c',
                rlic.etc_fee_c as 'rlic_etc_fee_c',
                rlic.filter_size_c as 'rlic_filter_size_c',
                rlic.who_cancelled_c as 'rlic_who_cancelled_c',
                rlic.when_cancelled_c as 'rlic_when_cancelled_c',
                u.id as 'u_id',
                u.user_name as 'u_user_name',
                u.user_hash as 'u_user_hash',
                u.system_generated_password as 'u_system_generated_password',
                u.pwd_last_changed as 'u_pwd_last_changed',
                u.authenticate_id as 'u_authenticate_id',
                u.sugar_login as 'u_sugar_login',
                u.picture as 'u_picture',
                u.first_name as 'u_first_name',
                u.last_name as 'u_last_name',
                u.is_admin as 'u_is_admin',
                u.external_auth_only as 'u_external_auth_only',
                u.receive_notifications as 'u_receive_notifications',
                u.description as 'u_description',
                u.date_entered as 'u_date_entered',
                u.date_modified as 'u_date_modified',
                u.last_login as 'u_last_login',
                u.modified_user_id as 'u_modified_user_id',
                u.created_by as 'u_created_by',
                u.title as 'u_title',
                u.department as 'u_department',
                u.phone_home as 'u_phone_home',
                u.phone_mobile as 'u_phone_mobile',
                u.phone_work as 'u_phone_work',
                u.phone_other as 'u_phone_other',
                u.phone_fax as 'u_phone_fax',
                u.status as 'u_status',
                u.address_street as 'u_address_street',
                u.address_city as 'u_address_city',
                u.address_state as 'u_address_state',
                u.address_country as 'u_address_country',
                u.address_postalcode as 'u_address_postalcode',
                u.default_team as 'u_default_team',
                u.team_set_id as 'u_team_set_id',
                u.deleted as 'u_deleted',
                u.portal_only as 'u_portal_only',
                u.show_on_employees as 'u_show_on_employees',
                u.employee_status as 'u_employee_status',
                u.messenger_id as 'u_messenger_id',
                u.messenger_type as 'u_messenger_type',
                u.reports_to_id as 'u_reports_to_id',
                u.is_group as 'u_is_group',
                u.preferred_language as 'u_preferred_language',
                u.acl_role_set_id as 'u_acl_role_set_id',
                u.acl_team_set_id as 'u_acl_team_set_id',
                acc.id as 'acc_id',
                acc.name as 'acc_name',
                acc.date_entered as 'acc_date_entered',
                acc.date_modified as 'acc_date_modified',
                acc.modified_user_id as 'acc_modified_user_id',
                acc.created_by as 'acc_created_by',
                acc.description as 'acc_description',
                acc.deleted as 'acc_deleted',
                acc.facebook as 'acc_facebook',
                acc.twitter as 'acc_twitter',
                acc.googleplus as 'acc_googleplus',
                acc.account_type as 'acc_account_type',
                acc.industry as 'acc_industry',
                acc.annual_revenue as 'acc_annual_revenue',
                acc.phone_fax as 'acc_phone_fax',
                acc.billing_address_street as 'acc_billing_address_street',
                acc.billing_address_city as 'acc_billing_address_city',
                acc.billing_address_state as 'acc_billing_address_state',
                acc.billing_address_postalcode as 'acc_billing_address_postalcode',
                acc.billing_address_country as 'acc_billing_address_country',
                acc.rating as 'acc_rating',
                acc.phone_office as 'acc_phone_office',
                acc.phone_alternate as 'acc_phone_alternate',
                acc.website as 'acc_website',
                acc.ownership as 'acc_ownership',
                acc.employees as 'acc_employees',
                acc.ticker_symbol as 'acc_ticker_symbol',
                acc.shipping_address_street as 'acc_shipping_address_street',
                acc.shipping_address_city as 'acc_shipping_address_city',
                acc.shipping_address_state as 'acc_shipping_address_state',
                acc.shipping_address_postalcode as 'acc_shipping_address_postalcode',
                acc.shipping_address_country as 'acc_shipping_address_country',
                acc.parent_id as 'acc_parent_id',
                acc.sic_code as 'acc_sic_code',
                acc.duns_num as 'acc_duns_num',
                acc.campaign_id as 'acc_campaign_id',
                acc.assigned_user_id as 'acc_assigned_user_id',
                acc.team_id as 'acc_team_id',
                acc.team_set_id as 'acc_team_set_id',
                acc.acl_team_set_id as 'acc_acl_team_set_id',
                accc.id_c as 'accc_id_c',
                accc.external_key_c as 'accc_external_key_c',
                accc.response_code_c as 'accc_response_code_c',
                accc.response_message_c as 'accc_response_message_c',
                accc.converted_c as 'accc_converted_c',
                accc.complete_conversion_c as 'accc_complete_conversion_c',
                accc.updated_c as 'accc_updated_c',
                accc.customer_status_c as 'accc_customer_status_c',
                accc.supkg_systemusers_id_c as 'accc_supkg_systemusers_id_c',
                accc.reaf_completed_c as 'accc_reaf_completed_c',
                accc.tech_dispatched_c as 'accc_tech_dispatched_c',
                accc.notes_c as 'accc_notes_c',
                accc.installer_notes_c as 'accc_installer_notes_c',
                accc.install_complete_c as 'accc_install_complete_c',
                accc.cancel_reason_c as 'accc_cancel_reason_c',
                accc.credit_result_c as 'accc_credit_result_c',
                accc.synchroteam_external_key_c as 'accc_synchroteam_external_key_c',
                accc.synchroteam_updated_c as 'accc_synchroteam_updated_c',
                accc.synchroteam_customer_link_c as 'accc_synchroteam_customer_link_c',
                accc.failed_reaf_c as 'accc_failed_reaf_c',
                accc.user_id_c as 'accc_user_id_c',
                accc.supkg_systemusers_id1_c as 'accc_supkg_systemusers_id1_c',
                accc.chargeover_customer_link_c as 'accc_chargeover_customer_link_c',
                accc.house_type_c as 'accc_house_type_c',
                accc.cell_phone_c as 'accc_cell_phone_c',
                accc.install_date_c as 'accc_install_date_c',
                accc.cancel_date_c as 'accc_cancel_date_c',
                accc.cancel_reason_dropdown_c as 'accc_cancel_reason_dropdown_c',
                accc.chargeover_payment_link_c as 'accc_chargeover_payment_link_c',
                accc.chargeover_instance_c as 'accc_chargeover_instance_c',
                accc.unique_id_c as 'accc_unique_id_c',
                accc.cancel_all_products_c as 'accc_cancel_all_products_c',
                accc.is_renter_owner_c as 'accc_is_renter_owner_c',
                accc.failed_reaf_note_added_c as 'accc_failed_reaf_note_added_c',
                accc.chargeover_invoice_link_c as 'accc_chargeover_invoice_link_c',
                accc.activation_fee_c as 'accc_activation_fee_c',
                accc.agent_name_c as 'accc_agent_name_c',
                accc.agent_id_c as 'accc_agent_id_c',
                accc.chargeover_status_c as 'accc_chargeover_status_c',
                accc.compartment_number_c as 'accc_compartment_number_c',
                opp.id as 'opp_id',
                opp.name as 'opp_name',
                opp.date_entered as 'opp_date_entered',
                opp.date_modified as 'opp_date_modified',
                opp.modified_user_id as 'opp_modified_user_id',
                opp.created_by as 'opp_created_by',
                opp.description as 'opp_description',
                opp.deleted as 'opp_deleted',
                opp.opportunity_type as 'opp_opportunity_type',
                opp.campaign_id as 'opp_campaign_id',
                opp.lead_source as 'opp_lead_source',
                opp.amount as 'opp_amount',
                opp.amount_usdollar as 'opp_amount_usdollar',
                opp.date_closed as 'opp_date_closed',
                opp.date_closed_timestamp as 'opp_date_closed_timestamp',
                opp.next_step as 'opp_next_step',
                opp.sales_stage as 'opp_sales_stage',
                opp.sales_status as 'opp_sales_status',
                opp.probability as 'opp_probability',
                opp.best_case as 'opp_best_case',
                opp.worst_case as 'opp_worst_case',
                opp.commit_stage as 'opp_commit_stage',
                opp.total_revenue_line_items as 'opp_total_revenue_line_items',
                opp.closed_revenue_line_items as 'opp_closed_revenue_line_items',
                opp.included_revenue_line_items as 'opp_included_revenue_line_items',
                opp.mkto_sync as 'opp_mkto_sync',
                opp.mkto_id as 'opp_mkto_id',
                opp.assigned_user_id as 'opp_assigned_user_id',
                opp.team_id as 'opp_team_id',
                opp.team_set_id as 'opp_team_set_id',
                opp.currency_id as 'opp_currency_id',
                opp.base_rate as 'opp_base_rate',
                opp.acl_team_set_id as 'opp_acl_team_set_id',
                concat(u.first_name, ' ', u.last_name) as user_real_name,
                TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) as 'period_of_use',
                CASE
                    WHEN acc.billing_address_country = 'Canada' AND TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) <= 10 THEN 'IBC'
                    WHEN acc.billing_address_country = 'United States' AND TO_DAYS(rlic.when_cancelled_c) - TO_DAYS(rlic.install_date_c) <= 3 THEN 'IBC'
                    ELSE ''
                END as 'ex_cancel_reason'
            from revenue_line_items rli
            join revenue_line_items_cstm rlic on rli.id = rlic.id_c
            join users u on rlic.who_cancelled_c = u.id
            left join accounts acc on acc.id = rli.account_id
            left join accounts_cstm accc on acc.id = accc.id_c
            left join opportunities opp on rli.opportunity_id = opp.id
            where 
                rli.deleted = 0 and 
                left(rlic.when_cancelled_c, 10) >= '{$from}' and 
                left(rlic.when_cancelled_c, 10) <= '{$to}' and
                rlic.uninstall_c = '1';
";

        $result = $GLOBALS['db']->query($query);
        $result_set = array();
        while($row = $GLOBALS['db']->fetchByAssoc($result)){
            $result_set[] = $row;
        }

        return array(
            'result' => 'ok',
            'numrow' => $result->num_rows,
            'data' => $result_set,
        );
    }
}