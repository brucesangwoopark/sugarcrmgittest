<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-12
 * Time: 오전 2:17
 */

class GetRevenueLineItems extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getRevenueLineItems' => array(
                'reqType' => 'POST',
                'path' => array('RevenueLineItems', 'list'),
                'method' => 'getRevenueLineItemList',
                'shortHelp' => 'Returning RevenueLineItems in JSON',
            )
        );
    }

    function getRevenueLineItemList($api, $args)
    {
        if (!isset($args['opportunity_id']) || empty($args['opportunity_id'])) {
            return array(
                'result' => 'error',
                'message' => 'no parameter'
            );
        }

        $opportunity_id = $args['opportunity_id'];

        require_once('include/SugarQuery/SugarQuery.php');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::getBean('RevenueLineItems'));
        //$sugarQuery->where()->equals('opportunity_id', $opportunity_id)->equals('uninstall_c', '0')->equals('install_c', '1');
        $sugarQuery->where()->equals('opportunity_id', $opportunity_id);
        $results = $sugarQuery->execute();

        return array(
            'result' => 'ok',
            'data' => $results
        );
    }
}