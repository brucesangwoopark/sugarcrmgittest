<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-27
 * Time: 오후 12:07
 */

class GetAccount extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getRLIID' => array(
                'reqType' => 'POST',
                'path' => array('RevenueLineItems', 'account'),
                'method' => 'run',
                'shortHelp' => 'Returning Associated Account ID in JSON',
            )
        );
    }

    public function run($api, $args)
    {
        if (isset($args['rli_id']) && !empty($args['rli_id'])) {
            $rli_id = $args['rli_id'];
            $rli_bean = BeanFactory::getBean('RevenueLineItems', $rli_id, array('disable_row_level_security' => true));
            $account_bean = BeanFactory::getBean('Accounts', $rli_bean->account_id, array('disable_row_level_security' => true));

            return array(
                'account_id' => $rli_bean->account_id,
                'account_name' => $account_bean->name,
            );
        }

        if (isset($args['account_id']) && !empty($args['account_id'])) {
            $account_id = $args['account_id'];
            $account_bean = BeanFactory::getBean('Accounts', $account_id, array('disable_row_level_security' => true));

            return array(
                'account_id' => $account_id,
                'account_name' => $account_bean->name,
                'account_country' => $account_bean->billing_address_country,
            );
        }

        return array(
            'result' => 'error',
            'message' => 'no parameter'
        );
    }
}