<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-31
 * Time: 오후 9:15
 */

class BeforeDeleteRevenueLineItemsHook
{
    function beforeDelete($bean, $event, $arguments)
    {
        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: BEGINNING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));

        if ($bean->fspg2_filterserviceplans_id !== null) {
            $fspl_id = $bean->fspg2_filterserviceplans_id;
            $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: Filter Service Plans linked ID: {$fspl_id} " . date('Y-m-d H:i:s'));
            $fspl_bean = BeanFactory::retrieveBean('FSPG2_FilterServicePlans', $fspl_id);
            $fspl_bean->mark_deleted($fspl_id);
            $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: Filter Service Plan removed ID: {$fspl_id} " . date('Y-m-d H:i:s'));
        }

        $GLOBALS['log']->fatal("[{$bean->module_name}][beforeDelete]: ENDING HOOK, Plan id: {$bean->id} " . date('Y-m-d H:i:s'));
    }
}