<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-12
 * Time: 오전 1:22
 */

class GetOpportunities extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'getOpportunities' => array(
                'reqType' => 'POST',
                'path' => array('Opportunities', 'list'),
                'method' => 'getOpportunityList',
                'shortHelp' => 'Returning Opportunities in JSON',
            )
        );
    }

    function getOpportunityList($api, $args)
    {
        if (!isset($args['account_id']) || empty($args['account_id'])) {
            return array(
                'result' => 'error',
                'message' => 'no parameter'
            );
        }

        $account_id = $args['account_id'];

        require_once('include/SugarQuery/SugarQuery.php');
        $sugarQuery = new SugarQuery();
        $sugarQuery->from(BeanFactory::getBean('Opportunities'));
        $sugarQuery->joinTable('accounts_opportunities', array('alias' => 'ao'))->on()->equalsField('opportunities.id', 'ao.opportunity_id');
        $sugarQuery->where()->equals('ao.account_id', $account_id);
        $results = $sugarQuery->execute();

        return array(
            'result' => 'ok',
            'data' => $results
        );
    }
}