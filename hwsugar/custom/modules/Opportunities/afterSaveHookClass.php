<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveOpportunitiesHook {
	
	//$count tracks the number of hook executions
	static $count = 0;
	
	function afterSave($bean, $event, $arguments)
	{
		self::$count++;
		$id = $bean->id;
		//for logging purposes
		$module = "Opportunities";
		$hook = "after_save";
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK #'.self::$count.', Opportunity id: '.$id.', '.date("Y-m-d H:i:s"));

		$chargeover_url_us = 'https://nationalhomewaterservices.chargeover.com';
		// $chargeover_url_us = 'https://greenlifewater-staging.chargeover.com';
		$chargeover_url_ca = 'https://billingcdn.filtergroup.com';
		// $chargeover_url_ca = 'https://greenlifewater-staging.chargeover.com';
		// $custom_chargeover_auth = '1GecBEqXlS67I49Rr0zQDPhyHZdOCnvF:jUbDKG0kFinyp1mN2Zg7JtVwuhaIQL4z';

        $suspend = false; //flag to see if package should be suspended if update/upgrade fails
        $account_id = '';

		try{
			//check if unique_id_c field has a value
			if(empty($bean->unique_id_c)){
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Generating new unique Opportunity Number, '.date("Y-m-d H:i:s"));
				//insert record into ra_num table to generate new ra_num (it is auto-incrementing)
				$unique_id = 0; //setting default
				$query = 'INSERT INTO opportunity_unique_id SET id = "'.$id.'"';
				$GLOBALS['db']->query($query);
				//select from ra_num to retrieve ra number
				$query = 'SELECT unique_id FROM opportunity_unique_id WHERE id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
				  $unique_id = $row['unique_id'];
				}
				//update unique_id_c field
				$query = 'UPDATE opportunities_cstm
						SET unique_id_c = "'.$unique_id.'"
						WHERE id_c = "'.$id.'"';
				$GLOBALS['db']->query($query);
			}
			else{
				$unique_id = $bean->unique_id_c;
			}
			if($bean->failed_reaf_c != "1"){
				//find chargeover_instance_c field from associated Account
				$chargeover_instance = 0; //setting default
				$query = 'SELECT ac.chargeover_instance_c
						FROM accounts_cstm ac
						JOIN accounts_opportunities ao ON ao.account_id = ac.id_c
						WHERE ao.opportunity_id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				$counter = 0; //to ensure below only executes once
				while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
					$chargeover_instance = $row['chargeover_instance_c'];
					$counter++;
				}
				
				if($chargeover_instance == 0){//in case where Account does NOT have chargeover_instance_c field written and you try to save an Opportunity
					$query = 'SELECT l.primary_address_country as primary_address_country
							FROM leads l
							WHERE l.opportunity_id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below only executes once
                    $primary_address_country = '';
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$primary_address_country = $row['primary_address_country'];
						$counter++;
					}
					if($primary_address_country == 'United States'){
						$chargeover_instance = 1;
					}
					if($primary_address_country == 'Canada'){
						$chargeover_instance = 2;
					}
				}
				
				//attach Note to Lead to make note of Lead conversion date
				//NOTE: this should ONLY be done ONCE
				$lead_found = false;
				$query = 'SELECT id FROM leads WHERE opportunity_id = "'.$id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$lead_found = true;
					$lead_id = $row['id'];
				}
				
				if($lead_found){
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Lead: '.$lead_id.', '.date("Y-m-d H:i:s"));
					//check if lead_conversion_note checkbox is checked
					$lead_conversion_note = 0;
					$query = 'SELECT lead_conversion_note_added_c FROM opportunities_cstm WHERE id_c = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$lead_conversion_note = $row['lead_conversion_note_added_c'];
						$counter++;
					}
					
					if($lead_conversion_note == 0){
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding Lead conversion date Note to Lead: '.$lead_id.', '.date("Y-m-d H:i:s"));
						
						$note = new Note();
						$note->name = 'Lead was converted: '.date("Y-m-d H:i:s");
						$note->parent_type = 'Leads';
						$note->parent_id = $lead_id;
						$note->team_id = $bean->team_id;
						//$note->team_set_id = $bean->team_id;
						$note->save();
						//add team relationship
						/*
						$note->load_relationship('teams');
						$note->teams->replace(array($team_id));
						*/
						//update lead_conversion_note_added flag
						$query = 'UPDATE opportunities_cstm
								SET lead_conversion_note_added_c = "1"
								WHERE id_c = "'.$id.'"';
						$GLOBALS['db']->query($query);
					}
				}
				//END OF Lead conversion note creation
				
				//ChargeOver credentials
                $url = '';
				if($chargeover_instance == 1){
					//Nevada ChargeOver
					//$url = 'http://ec2-34-197-54-229.compute-1.amazonaws.com/init.php?pid=COCRED&bid=2';
					$url = 'http://www.thefilterboss.com/init.php?pid=COCRED&bid=2';
				}
				if($chargeover_instance == 2){
					//Ontario ChargeOver
					//$url = 'http://www.thefilterboss.com/init.php?pid=COCRED'; //STAGING
					$url = 'http://www.thefilterboss.com/init.php?pid=COCRED&bid=1';
				}
				$curl = curl_init($url);
				$headr = array();
				$headr[] = 'Accept: application/json';
				$headr[] = 'Content-type: application/json';
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
				curl_setopt($curl, CURLOPT_HTTPGET, true);
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				//make REST call
				$response = curl_exec($curl);
				curl_close($curl);
				$jsonResponse = json_decode($response, true);
				$encoding = base64_encode($response);
				if (isset($custom_chargeover_auth)) {
					$encoding = base64_encode($custom_chargeover_auth);
				}
				
				//FIRST, CHECK IF PACKAGE ALREADY EXISTS
				if(!empty($bean->external_key_c)){
					$exists = true;
					$package_id = $bean->external_key_c;
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)Package Exists w/ package_id: '.$package_id.', '.date("Y-m-d H:i:s"));
				}
				else{
					$exists = false;
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)Package DOES NOT Exist, '.date("Y-m-d H:i:s"));
				}

				//CASE 1: ChargeOver package does NOT exist
				//POST new package
				if (!$exists){
					//check if Opportunity has associated RLI, i.e. check if ANY RLI has opportunity_id
					//NO RLI --> $conversion = true, RLI --> $conversion = false
					$query = 'SELECT id FROM revenue_line_items WHERE opportunity_id = "'.$id.'" AND deleted <> "1"';
					$result = $GLOBALS['db']->query($query);
					$conversion = true; //set as default
					while($row = $GLOBALS['db']->fetchByAssoc($result)){
						$conversion = false;//there is a RLI associated with the opportunity
					}
					
					$query = 'SELECT from_conversion_c FROM opportunities_cstm WHERE id_c = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$from_conversion = $row ['from_conversion_c'];
						$counter++;
					}
					
					if($from_conversion == "1"){
						$conversion = false;//to trick it into triggering the ChargeOver push
					}
					
					$query = 'SELECT created_outside_conversion_c FROM opportunities_cstm WHERE id_c = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$created_outside_conversion_c = $row ['created_outside_conversion_c'];
						$counter++;
					}
					
					if($created_outside_conversion_c == "1"){
						$conversion = false;
					}
					
					if ($conversion == true){
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Opportunity '.$id.', created from Lead Conversion, '.date("Y-m-d H:i:s"));

						//check if Account was successfully converted
						$query = 'SELECT ac.converted_c as converted
									FROM accounts_cstm ac
									JOIN accounts_opportunities ao ON ao.account_id = ac.id_c
									JOIN opportunities o ON o.id = ao.opportunity_id
									WHERE o.id = "'.$id.'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below only executes once
                        $account_conversion = '';
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
							$account_conversion = $row ['converted'];
							$counter++;
						}
						
						//check value of Account converted flag
						if($account_conversion != "fail"){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Account was successfully converted, '.date("Y-m-d H:i:s"));
							//for ChargeOver
							$item_id = array(); //for external key, i.e. (CO) item_id
							
							//for RLI
							$product_template_id = array();
							$product_name = array();
							$sales_stage = array();
							$combo = array();
							
							$RLI_id = array();
							
							$num_items = 0;
							//get the product information from LeadLineItems
							$query = 'select pt.id as product_template_id, ptc.chargeover_id_c as item_id, ptc.combo_c as combo, li.name as name, l.id as lead_id, lic.sales_stage_c as sales_stage
									from leads l
									join leads_lipkg_lead_line_items_1_c lb on lb.leads_lipkg_lead_line_items_1leads_ida = l.id
									join lipkg_lead_line_items li on li.id = lb.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb
									join lipkg_lead_line_items_cstm lic on li.id = lic.id_c
									join lipkg_lead_line_items_producttemplates_c lip on lip.lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb = li.id
									join product_templates pt on pt.id = lip.lipkg_lead_line_items_producttemplatesproducttemplates_ida
									join product_templates_cstm ptc on ptc.id_c = pt.id
									where l.opportunity_id = "'.$id.'"
									and li.deleted <> "1"
									and lip.deleted <> "1"';
							$result = $GLOBALS['db']->query($query);
							//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: SQL query: '.$query.', '.date("Y-m-d H:i:s"));
							while($row = $GLOBALS['db']->fetchByAssoc($result))
							{
								$product_template_id[$num_items] = $row['product_template_id'];
								$product_name[$num_items] = $row['name'];
								$item_id[$num_items] = $row['item_id'];
								$sales_stage[$num_items] = $row['sales_stage'];
								$combo[$num_items] = $row['combo'];
								
								$lead_id = $row['lead_id'];
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item#'.$num_items.', (S)product_template_id: '.$product_template_id[$num_items].', (S)name: '.$product_name[$num_items].', '.date("Y-m-d H:i:s"));
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item#'.$num_items.', (CO)item_id: '.$item_id[$num_items].', '.date("Y-m-d H:i:s"));
								
								//create new RLI in Sugar
								$RLI = new RevenueLineItem();
								$RLI->opportunity_id = $id;
								$RLI->product_template_id = $product_template_id[$num_items];
								$RLI->name = $product_name[$num_items];
								$RLI->sales_stage = $sales_stage[$num_items];
								$RLI->from_conversion_c = "1"; // flag to track that RLI was created from conversion
								$RLI->install_scheduled_c = "1";
								$RLI->install_c = "1";
								//set primary team
								$RLI->team_id = $bean->team_id;
								//$RLI->team_set_id = $bean->team_id;
								$RLI->save();
								//add relationship to team
								/*
								$RLI->load_relationship('teams');
								$RLI->teams->replace(array($team_id));
								*/
								
								//get new RLI id
								$RLI_id[] = $RLI->id;
								
								$num_items++;
							}
							
							//ONLY CREATE PACKAGE IF ABLE TO RETRIEVE ITEM INFORMATION, i.e. $num_items != 0
							if ($num_items != 0){
								//set deleted flag of all associated LLIs to "1"
								$query = 'UPDATE lipkg_lead_line_items lli
										JOIN leads_lipkg_lead_line_items_1_c llli ON llli.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = lli.id
										SET lli.deleted = "1"
										WHERE llli.leads_lipkg_lead_line_items_1leads_ida = "'.$lead_id.'"';
								$GLOBALS['db']->query($query);
								
								//
								//
								//
								//NEW SYNCHROTEAM JOB CREATION
								//
								//
								//
															
								//get ra_num from the Associated Lead
								$query = 'SELECT lc.ra_num_c as ra_num
										FROM leads l
										JOIN leads_cstm lc ON lc.id_c = l.id
										JOIN opportunities o ON l.opportunity_id = o.id
										WHERE o.id = "'.$id.'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
									$ra_num = $row['ra_num'];
									$counter++;
								}
								
								//check if products are part of a combo
								$job = array();
								$is_combo = false; //to track if there is a combo
								$num_jobs = 0; //to count the number of jobs
								$combo_items = 0; //to count how many items are in the combo
								
								for($i = 0; $i < $num_items; $i++){
									if($combo[$i] == "1"){
										$is_combo = true;
										$combo_items++;
									}
									else{
										//create new job
										$job[$num_jobs] = BeanFactory::newBean('abcde_Jobs');
										
										//set job attributes
										$job[$num_jobs]->opportunity_id_c = $id;
										$name = $ra_num.' - Job #'.($num_jobs+1);
										$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new job: '.$name.', '.date("Y-m-d H:i:s"));
										$job[$num_jobs]->name = $name;
										//set from_conversion flag to "true"
										//the Job hook will check this to see if Job was created from the Lead conversion or not
										$job[$num_jobs]->from_conversion_c = 'true';
										$job[$num_jobs]->num_items_c = 1;
										
										//
										//
										//FIND JOB TYPE BASED ON PRODUCT CATEGORY
										//
										//
										$query = 'SELECT pc.name as product_category
												FROM product_categories pc
												JOIN revenue_line_items rli ON rli.category_id = pc.id
												WHERE rli.id = "'.$RLI_id[$i].'"';
										$result = $GLOBALS['db']->query($query);
										$counter = 0; //to ensure below loop only executes once
										while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
										{
											$product_category = $row['product_category'];
											$counter++;
										}
										if($product_category == "Whole Home" || $product_category == "D-Scaler"){
												$job[$num_jobs]->job_type_c = 'Install WH or DS';
										}
										if($product_category == "Under Counter"){
											$job[$num_jobs]->job_type_c = 'Install UC';
										}
										//
										//
										//END OF JOB TYPE MAPPING
										//
										//
										
										$job[$num_jobs]->save(); //note: this will trigger the hook in the Jobs module, with above attributes set
										
										$job[$num_jobs]->team_id = $bean->team_id;
										//$job[$num_jobs]->team_set_id = $bean->team_id;
										//add relationship between Job and RLI
										$job[$num_jobs]->load_relationship('abcde_jobs_revenuelineitems_1');
										$job[$num_jobs]->abcde_jobs_revenuelineitems_1->add($RLI_id[$i]);
										//add relationship to team
										/*
										$job[$num_jobs]->load_relationship('teams');
										$job[$num_jobs]->teams->replace(array($team_id));
										*/
										$job[$num_jobs]->save();//trigger the after_save hook in Jobs again, now that booking information is added
										
										$num_jobs++;
									}
								}
								
								//only execute below if there is a combo
								if ($is_combo){
									//create new combo job
									$job_combo = BeanFactory::newBean('abcde_Jobs');
									//set all attributes except the items/products
									$job_combo->opportunity_id_c = $id;
									$name = $ra_num.' - Job #'.($num_jobs+1).' - Combo';
									$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new job: '.$name.', '.date("Y-m-d H:i:s"));
									$job_combo->name = $name;
																		
									//set from_conversion flag to "true"
									//the Job hook will check this to see if Job was created from the Lead conversion or not
									$job_combo->from_conversion_c = 'true';
									$job_combo->num_items_c = $combo_items;
									$job_combo->job_type_c = 'Install COMBO';
									$job_combo->save();
									
									//Add relationship between Job and RLIs
									for($i = 0; $i < $num_items; $i++){
										if($combo[$i] == "1"){
											$job_combo->load_relationship('abcde_jobs_revenuelineitems_1');
											$job_combo->abcde_jobs_revenuelineitems_1->add($RLI_id[$i]);
										}
									}
									//add team relationship
									/*
									$job_combo->load_relationship('teams');
									$job_combo->teams->replace(array($team_id));
									*/
									$job_combo->team_id = $bean->team_id;
									//$job_combo->team_set_id = $bean->team_id;
									$job_combo->save();//trigger the after_save hook in Jobs again, now that booking information is added
									
									$num_jobs++;
								}//end of if($is_combo)
								
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Finished creating jobs. '.$num_jobs.' created, '.date("Y-m-d H:i:s"));
								//
								//
								//
								//END OF JOB CREATION
								//
								//
								//
								
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Successfully able to retrieve associated products. Continue with conversion, '.date("Y-m-d H:i:s"));
								
								//update from_conversion flag
								$query = 'UPDATE opportunities_cstm SET from_conversion_c = "1" WHERE id_c = "'.$id.'"';
								$GLOBALS['db']->query($query);
								
								//find ChargeOver customer_id
								$query = 'SELECT account_id FROM accounts_opportunities WHERE opportunity_id = "'.$id.'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
								{
									$account_id = $row['account_id'];
									$counter++;
								}
								$query = 'SELECT external_key_c FROM accounts_cstm WHERE id_c = "'.$account_id.'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
								{
									$customer_id = $row['external_key_c'];
									$counter++;
								}
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)customer_id: '.$customer_id.', '.date("Y-m-d H:i:s"));
								
								//check which currency, CAD or USD, should be pushed to ChargeOver, based on Account's country
								$query = 'SELECT billing_address_country FROM accounts WHERE id = "'.$account_id.'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
								{
									$country = $row['billing_address_country'];
									$counter++;
								}
								/*
								$currency_id = 1;//setting default to USD
								if($country == "Canada"){
									$currency_id = 2;
								}
								*/
								if($chargeover_instance == 1){
									$currency_id = 1;
								}
								if($chargeover_instance == 2){
									$currency_id = 2;
								}
								//CREATE NEW MAPPING TO OPPORTUNITIES
								
								if(empty($bean->active_date_c)){
									$active_date = date("Y-m-d");
								}
								else{
									$active_date = $bean->active_date_c;
								}
								// the effective date is +30 days updated 2017-06-23 
								$effectiveDate = date('Y-m-d', strtotime("+30 days", strtotime($active_date)));
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Effective Date : +30 days '.date("Y-m-d H:i:s"));
								
								//Enbridge
								if($chargeover_instance == 1){
									$custom_2 = '';
									$custom_3 = '';
								}
								if($chargeover_instance == 2){
									if($bean->enbridge_customer_c == "1"){
										$custom_2 = 'enbridge';
										$custom_3 = $bean->enbridge_number_c;
									}
									else{
										$custom_2 = 'direct';
										$custom_3 = '';
									}
								}
								//
								//CHECKING PAYMENT INFO
								//
								//check if customer has ACH or CC
								$paymethod = 'inv'; //default paymethod
																
								//check if customer has ACH 1st
								if($chargeover_instance == 1){
									//Nevada ChargeOver
									$url = $chargeover_url_us . '/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;									
								}
								if($chargeover_instance == 2){
									//Ontario ChargeOver
									//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/ach?where=customer_id:EQUALS:'.$customer_id; //STAGING
									//$url = 'https://greenlifewater-new.chargeover.com/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;
									$url = $chargeover_url_ca . '/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;
								}
								$curl = curl_init($url);
								$headr = array();
								$headr[] = 'Accept: application/json';
								$headr[] = 'Content-type: application/json';
								$headr[] = 'Authorization: Basic '.$encoding;
								curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
								curl_setopt($curl, CURLOPT_HTTPGET, true);
								curl_setopt($curl, CURLOPT_HEADER, false);
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
								//make REST call
								$response = curl_exec($curl);
								curl_close($curl);
								$jsonResponse = json_decode($response, true);
								
								$response = $jsonResponse['response'];

								if (empty($response)) $has_ach = false; else $has_ach = true;
								
								if($has_ach){
									$ach_id = $response[0]['ach_id'];
								}
								
								//check for CC
								if($chargeover_instance == 1){
									//Nevada ChargeOver
									$url = $chargeover_url_us . '/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;									
								}
								if($chargeover_instance == 2){
									//Ontario ChargeOver
									//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id; //STAGING
									//$url = 'https://greenlifewater-new.chargeover.com/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;
									$url = $chargeover_url_ca . '/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;
								}
																
								$curl = curl_init($url);
								$headr = array();
								$headr[] = 'Accept: application/json';
								$headr[] = 'Content-type: application/json';
								$headr[] = 'Authorization: Basic '.$encoding;
								curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
								curl_setopt($curl, CURLOPT_HTTPGET, true);
								curl_setopt($curl, CURLOPT_HEADER, false);
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
								//make REST call
								$response = curl_exec($curl);
								curl_close($curl);
								$jsonResponse = json_decode($response, true);
								
								$response = $jsonResponse['response'];
								
								if (empty($response)) $has_cc = false; else $has_cc = true;
							
								if($has_cc){
									$creditcard_id = $response[0]['creditcard_id'];
								}
								
								//check whether customer has ACH, CC or both (default to CC if both)
								if($has_cc){
									$paymethod = 'cre';
								}
								elseif($has_ach){
									$paymethod = 'ach';
								}
								//
								//END OF - CHECKING PAYMENT INFO
								//
								
								$json = '{'
										.'"terms_id": 3,'
										//"class_id": null,
										//"admin_id": null,
										.'"currency_id": '.$currency_id.','
										//.'"external_key": "'.$unique_id.'",'
										//"token": "3kxl72zog137",
										.'"nickname": "'.$bean->name.'",'
										.'"paymethod": "'.$paymethod.'",';
										//"paycycle": "mon",
										//"bill_addr1": null,
										//"bill_addr2": null,
										//"bill_addr3": null,
										//"bill_city": null,
										//"bill_state": null,
										//"bill_postcode": null,
										//"bill_country": null,
										//"bill_notes": null,
										//"ship_addr1": null,
										//"ship_addr2": null,
										//"ship_addr3": null,
										//"ship_city": null,
										//"ship_state": null,
										//"ship_postcode": null,
										//"ship_country": null,
										//"ship_notes": null,
										if($paymethod == "cre"){
											$json .= '"creditcard_id": '.$creditcard_id.',';
										}
										if($paymethod == "ach"){
											$json .= '"ach_id": '.$ach_id.',';
										}
										//"tokenized_id": null,
										//"cache_next_invoice": "2016-10-19",
										$json .=
										'"custom_1": "'.$bean->ra_num_c.'",'
										.'"custom_2": "'.$custom_2.'",'
										.'"custom_3": "'.$custom_3.'",'
										//"custom_4": null,
										.'"custom_5": "Sugar",'
										//"write_datetime": "2016-06-30 10:31:46",
										//"mod_datetime": "2016-09-19 15:33:04",
										//"start_datetime": "2016-07-29 00:00:00",
										//"suspendfrom_datetime": null,
										//"suspendto_datetime": null,
										//"cancel_datetime": null,
										.'"holduntil_datetime": "'.$effectiveDate.'",'
										//"terms_name": "Net 30",
										//"terms_days": 30,
										//"currency_symbol": "CAD$",
										//"currency_iso4217": "CAD",
										//"amount_collected": 0,
										//"amount_invoiced": 74.44,
										//"amount_due": 74.44,
										//"is_overdue": true,
										//"days_overdue": 25,
										//"next_invoice_datetime": "2016-10-19 00:00:00",
										//"package_id": 37804,
										.'"customer_id": '.$customer_id.','
										//"package_status_id": 3,
										//"package_status_name": "Over Due",
										//"package_status_str": "active-overdue",
										//"package_status_state": "a",
										.'"line_items": [';
										$active_items = false; //to track if there are any items that installed
										for($i = 0; $i < $num_items; $i++){
											if($sales_stage[$i] == "Installed" || $sales_stage[$i] == "Scheduled Uninstallation" || $sales_stage[$i] == "Transferred"){
												$active_items = true;
												$json.='{'
													.'"item_id": '.$item_id[$i].','
													.'"descrip": "'.$product_name[$i].'",'
													//.'"external_key": "'.$RLI_id[$i].'",'
													.'"custom_1": "'.$sales_stage[$i].'"'
												.'}';
												if(!empty($sales_stage[$i+1])) if($sales_stage[$i+1] == "Installed" || $sales_stage[$i+1] == "Scheduled Uninstallation" || $sales_stage[$i+1] == "Transferred") $json.=','; //only add comma if adding another "Installed" item
											}
										}
										$json.=']'
										.'}';
															
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: JSON query: '.$json.', '.date("Y-m-d H:i:s"));
								
								//only create package if there are any active items
								if($active_items){
									if($chargeover_instance == 1){
										//Nevada ChargeOver
										$url = $chargeover_url_us . '/api/v3/package';
									}
									if($chargeover_instance == 2){
										//Ontario ChargeOver
										//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package'; //STAGING
										//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package';
										$url = $chargeover_url_ca . '/api/v3/package';
									}
									//open curl for POST
									$curl = curl_init($url);
									$headr = array();
									$headr[] = 'Accept: application/json';
									$headr[] = 'Content-Length: ' . strlen($json);
									$headr[] = 'Content-type: application/json';
									$headr[] = 'Authorization: Basic '.$encoding;
									curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
									curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
									curl_setopt($curl, CURLOPT_POST, true);
									curl_setopt($curl, CURLOPT_HEADER, false);
									curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
									//make REST call
									$response = curl_exec($curl);
									curl_close($curl);
									$jsonResponse = json_decode($response, true);
									//get the HTTP status code from ChargeOver
									$code = $jsonResponse['code'];
									$message = $jsonResponse['message'];
									$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: "'.$message.'", '.date("Y-m-d H:i:s"));
									
									//update custom response code and message fields
									/*
									$query = 'UPDATE opportunities_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
									$bean->db->query($query, true);
									$query = 'UPDATE opportunities_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
									$bean->db->query($query, true);
									*/
									//check if Account complete_conversion_c flag is "pass" or "fail"
									$query = 'SELECT *
												FROM accounts_cstm ac
												JOIN accounts_opportunities ao ON ac.id_c = ao.account_id
												WHERE ao.opportunity_id = "'.$id.'"';
									$result = $GLOBALS['db']->query($query);
									$counter = 0; //to ensure below loop only executes once
									$complete_conversion = "";
									$account_id = "";
									while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
									{
										$complete_conversion = $row['complete_conversion_c'];
										$account_id = $row['id_c'];
										$counter++;
									}
																
									//201 Created - Object Successfully created
									if ($code == 201){
										//set from_conversion_c flag to "1"
										$query = 'UPDATE opportunities_cstm
												SET from_conversion_c = "1"
												WHERE id_c = "'.$id.'"';
										$GLOBALS['db']->query($query);
										
										//get the new ChargeOver package id
										$coId = $jsonResponse['response']['id'];
										
										$query =  'UPDATE opportunities_cstm SET external_key_c = "'.$coId.'"  WHERE id_c = "'.$id.'"';
										$bean->db->query($query, true);
										$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity to (CO)Package through Lead conversion successful"  WHERE id_c = "'.$id.'"';
										$bean->db->query($query, true);
										
										//only write over Account complete_conversion_c if is NOT "fail"
										if($complete_conversion != "fail"){
											$query = 'UPDATE accounts_cstm SET complete_conversion_c = "pass" WHERE id_c = "'.$account_id.'"';
											$GLOBALS['db']->query($query);
										}
							
										$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Package created, external id: '.$coId.', '.date("Y-m-d H:i:s"));
										//Create new Note for Opportunity
										$note = new Note();
										$note->name = 'ChargeOver Package: '.$coId.' created. Billing date is: '.$effectiveDate;
										$note->parent_type = 'Opportunities';
										$note->parent_id = $id;
										$note->team_id = $bean->team_id;
										//$note->team_set_id = $bean->team_id;
										$note->save();
										//add team relationship
										/*
										$note->load_relationship('teams');
										$note->teams->replace(array($team_id));
										*/
									}
									else{//$code != 201
										$query = 'UPDATE opportunities_cstm SET converted_c = "fail"  WHERE id_c = "'.$id.'"';
										$bean->db->query($query, true);
										$query = 'UPDATE accounts_cstm SET complete_conversion_c = "fail" WHERE id_c = "'.$account_id.'"';
										$GLOBALS['db']->query($query);
									}
								}// END OF if($active_items)
								else{//i.e. there are NO active items
									$GLOBALS['log']->fatal('['.$module.']['.$hook.']: There are no installed items, ChargeOver subscription not created. '.date("Y-m-d H:i:s"));
								}
							}//END OF if($num_items != 0)
							else{// $num_items == 0
								$query = 'UPDATE opportunities_cstm SET converted_c = "fail"  WHERE id_c = "'.$id.'"';
								$bean->db->query($query, true);
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Unable to retrieve associated products. Exiting hook, '.date("Y-m-d H:i:s"));
							}
						}//END OF if($account_conversion != "fail")
						else{//$account_conversion == "fail"
							$query = 'UPDATE opportunities_cstm SET converted_c = "fail"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Account was NOT successfully converted, '.date("Y-m-d H:i:s"));
						}
					}//END OF LEAD CONVERSION
					else{
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Opportunity '.$id.', created OUTSIDE of Lead Conversion, '.date("Y-m-d H:i:s"));
						//update "created_outside_conversion_c" flag
						$query = 'UPDATE opportunities_cstm SET	created_outside_conversion_c = "1" WHERE id_c = "'.$id.'"';
						$GLOBALS['db']->query($query);
						
						//check if ra_num_c is blank
						/*
						$ra = 0;
						$query = 'SELECT ra_num_c FROM opportunities_cstm WHERE id_c = "'.$id.'"';
						$result = $GLOBALS['db']->query($query);
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
						  $ra = $row['ra_num_c'];
						}
						*/
						//if($ra == 0){
						if(empty($bean->ra_num_c)){
							//generate new ra num for newly created Opportunity
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Generating new RA rumber, '.date("Y-m-d H:i:s"));
							/*
							$query =  "SELECT greatest(MAX(oc.ra_num_c),MAX(lc.ra_num_c)) as max_ra_num
										FROM opportunities_cstm oc, leads_cstm lc";
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
							  $max_ra_num = $row['max_ra_num'];
							}
							if ($max_ra_num == null) { //in case of first entry
							  $max_ra_num = 300000;
							}
							$ra_num = $max_ra_num + 1;
							*/
							
							//insert record into ra_num table to generate new ra_num (it is auto-incrementing)
							$query = 'INSERT INTO ra_num SET id = "'.$id.'"';
							$GLOBALS['db']->query($query);
							//select from ra_num to retrieve ra number
							$query = 'SELECT ra FROM ra_num WHERE id = "'.$id.'"';
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
							  $ra_num = $row['ra'];
							}
			
							$bean->ra_num_c = $ra_num;
							//$bean->save();
							$query = 'UPDATE opportunities_cstm SET ra_num_c = "'.$ra_num.'" WHERE id_c = "'.$id.'"';
							$GLOBALS['db']->query($query);
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: New RA rumber: '.$ra_num.', '.date("Y-m-d H:i:s"));
						}
						
						//find associated Account
						$query = 'SELECT account_id FROM accounts_opportunities WHERE opportunity_id = "'.$id.'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below loop only executes once
						$account_id = 0;
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
						{
							$account_id = $row['account_id'];
							$counter++;
						}
						
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Associated Account id: '.$account_id.', '.date("Y-m-d H:i:s"));
						
						
						
						$query = 'SELECT external_key_c FROM accounts_cstm WHERE id_c = "'.$account_id.'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below loop only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
						{
							$customer_id = $row['external_key_c'];
							$counter++;
						}
						
						//check if the customer_id was successfully retrieved
						if(isset($customer_id)) $customer_found = true; else $customer_found = false;
						
						//only execute the below if customer_id was found
						if ($customer_found){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)customer_id: '.$customer_id.', '.date("Y-m-d H:i:s"));
							//get all RLI associated with opportunity
							$item_id = array();
							$product_name = array();
							$sales_stage = array();
							$product_template_id = array();
							$RLI_id = array();
							$query = 'SELECT rli.id as id, rli.product_template_id as product_template_id, rli.name as name, rli.sales_stage as sales_stage
									FROM revenue_line_items rli
									WHERE rli.opportunity_id = "'.$id.'" AND rli.deleted <> "1"';
							$result = $GLOBALS['db']->query($query);
							$num_items = 0;
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
								$RLI_id[] = $row['id'];
								$product_template_id[] = $row['product_template_id'];
								$product_name[] = $row['name'];
								$sales_stage[] = $row['sales_stage'];
								$num_items++;
							}
							
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: List of all associated RLI\'s, '.date("Y-m-d H:i:s"));
							
							//Find ChargeOver item_id from Sugar product_template_id (external_key)
							for ($i = 0; $i < $num_items; $i++){
								$query = 'SELECT ptc.chargeover_id_c as item_id
									FROM product_templates_cstm ptc
									WHERE ptc.id_c = "'.$product_template_id[$i].'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
									$item_id[] = $row['item_id'];
									$counter++;
								}
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item #'.$i.', (S)RLI_id: '.$RLI_id[$i].', (S)product: '.$product_template_id[$i].', (CO)item_id: '.$item_id[$i].', '.date("Y-m-d H:i:s"));
								//trigger all associated RLI hooks
								//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Triggering the RLI hook..., '.date("Y-m-d H:i:s"));
								$rli = BeanFactory::getBean('RevenueLineItems', ($RLI_id[$i]));
								$rli->associated_account_c = $account_id;
								$query = 'UPDATE revenue_line_items_cstm
										SET associated_account_c = "'.$account_id.'"
										WHERE id_c = "'.$RLI_id[$i].'"';
								$GLOBALS['db']->query($query);
								//$rli->save();
							}
							
							//check which currency, CAD or USD, should be pushed to ChargeOver, based on Account's country
							$query = 'SELECT billing_address_country FROM accounts WHERE id = "'.$account_id.'"';
							$result = $GLOBALS['db']->query($query);
							$counter = 0; //to ensure below loop only executes once
							while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
							{
								$country = $row['billing_address_country'];
								$counter++;
							}
							/*
							$currency_id = 1;//setting default to USD
							if($country == "Canada"){
								$currency_id = 2;
							}
							*/
							if($chargeover_instance == 1){
								$currency_id = 1;
							}
							if($chargeover_instance == 2){
								$currency_id = 2;
							}
							//CREATE NEW MAPPING TO OPPORTUNITIES
							
							if(empty($bean->active_date_c)){
								$active_date = date("Y-m-d");
							}
							else{
								$active_date = $bean->active_date_c;
							}
							// the effective date is +30 days updated 2017-06-23
							$effectiveDate = date('Y-m-d', strtotime("+30 days", strtotime($active_date)));
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Effective Date : +30 days '.date("Y-m-d H:i:s"));
							
							//Enbridge
							if($chargeover_instance == 1){
								$custom_2 = '';
								$custom_3 = '';
							}
							if($chargeover_instance == 2){
								if($bean->enbridge_customer_c == "1"){
									$custom_2 = 'enbridge';
									$custom_3 = $bean->enbridge_number_c;
								}
								else{
									$custom_2 = 'direct';
									$custom_3 = '';
								}
							}
							
							//
							//CHECKING PAYMENT INFO
							//
							//check if customer has ACH or CC
							$paymethod = 'inv'; //default paymethod
														
							//check if customer has ACH 1st
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = $chargeover_url_us . '/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;									
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/ach?where=customer_id:EQUALS:'.$customer_id; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;
								$url = $chargeover_url_ca . '/api/v3/ach?where=customer_id:EQUALS:'.$customer_id;
							}
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_HTTPGET, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							
							$response = $jsonResponse['response'];

							if (empty($response)) $has_ach = false; else $has_ach = true;
							
							if($has_ach){
								$ach_id = $response[0]['ach_id'];
							}
							
							//check for CC
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = $chargeover_url_us . '/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;									
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;
								$url = $chargeover_url_ca . '/api/v3/creditcard?where=customer_id:EQUALS:'.$customer_id;
							}
															
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_HTTPGET, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							
							$response = $jsonResponse['response'];
							
							if (empty($response)) $has_cc = false; else $has_cc = true;
						
							if($has_cc){
								$creditcard_id = $response[0]['creditcard_id'];
							}
							
							//check whether customer has ACH, CC or both (default to CC if both)
							if($has_cc){
								$paymethod = 'cre';
							}
							elseif($has_ach){
								$paymethod = 'ach';
							}
							
							//
							//END OF - CHECKING PAYMENT INFO
							//
							
							$json = '{'
									.'"terms_id": 3,'
									//"class_id": null,
									//"admin_id": null,
									.'"currency_id": '.$currency_id.','
									//.'"external_key": "'.$unique_id.'",'
									//"token": "3kxl72zog137",
									.'"nickname": "'.$bean->name.'",'
									.'"paymethod": "'.$paymethod.'",';
									//"paycycle": "mon",
									//"bill_addr1": null,
									//"bill_addr2": null,
									//"bill_addr3": null,
									//"bill_city": null,
									//"bill_state": null,
									//"bill_postcode": null,
									//"bill_country": null,
									//"bill_notes": null,
									//"ship_addr1": null,
									//"ship_addr2": null,
									//"ship_addr3": null,
									//"ship_city": null,
									//"ship_state": null,
									//"ship_postcode": null,
									//"ship_country": null,
									//"ship_notes": null,
									if($paymethod == "cre"){
										$json .= '"creditcard_id": '.$creditcard_id.',';
									}
									if($paymethod == "ach"){
										$json .= '"ach_id": '.$ach_id.',';
									}
									//"tokenized_id": null,
									//"cache_next_invoice": "2016-10-19",
									$json .=
									'"custom_1": "'.$bean->ra_num_c.'",'
									.'"custom_2": "'.$custom_2.'",'
									.'"custom_3": "'.$custom_3.'",'
									//"custom_4": null,
									.'"custom_5": "Sugar",'
									//"write_datetime": "2016-06-30 10:31:46",
									//"mod_datetime": "2016-09-19 15:33:04",
									//"start_datetime": "2016-07-29 00:00:00",
									//"suspendfrom_datetime": null,
									//"suspendto_datetime": null,
									//"cancel_datetime": null,
									.'"holduntil_datetime": "'.$effectiveDate.'",'
									//"terms_name": "Net 30",
									//"terms_days": 30,
									//"currency_symbol": "CAD$",
									//"currency_iso4217": "CAD",
									//"amount_collected": 0,
									//"amount_invoiced": 74.44,
									//"amount_due": 74.44,
									//"is_overdue": true,
									//"days_overdue": 25,
									//"next_invoice_datetime": "2016-10-19 00:00:00",
									//"package_id": 37804,
									.'"customer_id": '.$customer_id.','
									//"package_status_id": 3,
									//"package_status_name": "Over Due",
									//"package_status_str": "active-overdue",
									//"package_status_state": "a",
									.'"line_items": [';
									$active_items = false; //to track if there are any items that installed 
									for($i = 0; $i < $num_items; $i++){
										if($sales_stage[$i] == "Installed" || $sales_stage[$i] == "Scheduled Uninstallation" || $sales_stage[$i] == "Transferred"){
											$active_items = true;
											$json.='{'
												.'"item_id": '.$item_id[$i].','
												.'"descrip": "'.$product_name[$i].'",'
												//.'"external_key": "'.$RLI_id[$i].'",'
												.'"custom_1": "'.$sales_stage[$i].'"'
											.'}';
											if(!empty($sales_stage[$i+1])) if($sales_stage[$i+1] == "Installed" || $sales_stage[$i+1] == "Scheduled Uninstallation" || $sales_stage[$i+1] == "Transferred") $json.=','; //only add comma if adding another "Installed" item
										}
									}
									$json.=']'
									.'}';
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: JSON query: '.$json.', '.date("Y-m-d H:i:s"));
							
							//only create package if there are any active items
							if($active_items){
								if($chargeover_instance == 1){
									//Nevada ChargeOver
									$url = $chargeover_url_us . '/api/v3/package';
								}
								if($chargeover_instance == 2){
									//Ontario ChargeOver
									//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package'; //STAGING
									//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package';
									$url = $chargeover_url_ca . '/api/v3/package';
								}
								//open curl for POST
								$curl = curl_init($url);
								$headr = array();
								$headr[] = 'Accept: application/json';
								$headr[] = 'Content-Length: ' . strlen($json);
								$headr[] = 'Content-type: application/json';
								$headr[] = 'Authorization: Basic '.$encoding;
								curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
								curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
								curl_setopt($curl, CURLOPT_POST, true);
								curl_setopt($curl, CURLOPT_HEADER, false);
								curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
								//make REST call
								$response = curl_exec($curl);
								curl_close($curl);
								$jsonResponse = json_decode($response, true);
								//get the HTTP status code and message from ChargeOver
								$code = $jsonResponse['code'];
								$message = $jsonResponse['message'];
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: "'.$message.'", '.date("Y-m-d H:i:s"));
								
								//update custom response code and message fields
								
								//$query = 'UPDATE opportunities_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
								//$GLOBALS['db']->query($query);
								//$query = 'UPDATE opportunities_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
								//$GLOBALS['db']->query($query);
								$bean->response_code_c = $code;
								$bean->response_message_c = $message;
								//$bean->save();
								
								//201 Created - Object Successfully created
								if ($code == 201){
									//get the new ChargeOver Package id
									$coId = $jsonResponse['response']['id'];
									//update custom fields
									$query = 'UPDATE opportunities_cstm SET external_key_c = "'.$coId.'"  WHERE id_c = "'.$id.'"';
									$GLOBALS['db']->query($query);
									//$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity created OUTSIDE of Lead conversion. Sync to (CO)Package successful"  WHERE id_c = "'.$id.'"';
									//$GLOBALS['db']->query($query);
									$bean->external_key_c = $coId;
									$bean->converted_c = '(S)Opportunity created OUTSIDE of Lead conversion. Sync to (CO)Package successful';
									//$bean->save();
	
									$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Package created, external id: '.$coId.', '.date("Y-m-d H:i:s"));
									//Create new Note for Opportunity
									$note = new Note();
									$note->name = 'ChargeOver Package: '.$coId.' created. Billing date is: '.$effectiveDate;
									$note->parent_type = 'Opportunities';
									$note->parent_id = $id;
									$note->team_id = $bean->team_id;
									//$note->team_set_id = $bean->team_id;
									$note->save();
									//add team relationship
									/*
									$note->load_relationship('teams');
									$note->teams->replace(array($team_id));
									*/
								}
								else{//$code != 201
									$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (S)Opportunity to (CO)Package NOT successful, '.date("Y-m-d H:i:s"));
									//update custom fields
									/*
									$query =  'UPDATE opportunities_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
									$bean->db->query($query, true);
									$query =  'UPDATE opportunities_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
									$bean->db->query($query, true);
									*/
									$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity created OUTSIDE of Lead Conversion. Sync to (CO)Package NOT successful"  WHERE id_c = "'.$id.'"';
									$bean->db->query($query, true);
									
									//update associated Account "complete conversion" flag to indicate that an associated Opportunity not synched properly to (CO)
									$query = 'UPDATE accounts_cstm SET complete_conversion_c = "fail" WHERE id_c = "'.$account_id.'"';
									$GLOBALS['db']->query($query);
								}
							}// END OF if($active_items)
							else{//i.e. there are NO active items
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: There are no installed items, ChargeOver subscription not created. '.date("Y-m-d H:i:s"));
							}
						}// END OF if ($customer_found){
						else{//customer_id NOT found
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)customer_id NOT successfully retrieved. Exiting hook, '.date("Y-m-d H:i:s"));
							//update custom fields
							/*
							$query =  'UPDATE opportunities_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							$query =  'UPDATE opportunities_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							*/
							$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity created OUTSIDE of Lead Conversion. Sync to (CO)Package NOT successful"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);

							//update associated Account "complete conversion" flag to indicate that an associated Opportunity not synched properly to (CO)
							$query = 'UPDATE accounts_cstm SET complete_conversion_c = "fail" WHERE id_c = "'.$account_id.'"';
							$GLOBALS['db']->query($query);
						}
						//$bean->save();
					}//End of Opportunity Creation OUTSIDE of Lead Conversion
				}
				//CASE 2: ChargeOver package exists
				//PUT/update existing package
				else{
					//Find associated Account (needed to set conversion flag if unsuccessful)
					$query = 'SELECT account_id FROM accounts_opportunities WHERE opportunity_id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below loop only executes once
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
						$account_id = $row['account_id'];
						$counter++;
					}
					
					//Enbridge
					if($chargeover_instance == 1){
						$custom_2 = '';
						$custom_3 = '';
					}
					if($chargeover_instance == 2){
						if($bean->enbridge_customer_c == "1"){
							$custom_2 = 'enbridge';
							$custom_3 = $bean->enbridge_number_c;
						}
						else{
							$custom_2 = 'direct';
							$custom_3 = '';
						}
					}
					
					$json = '{'
							//"class_id": "",
							//.'"external_key": "'.$unique_id.'",'
							.'"nickname": "'.$bean->name.'",'
							//"admin_id": "",
							//"bill_addr1": "",
							//"bill_addr2": "",
							//"bill_addr3": "",
							//"bill_city": "",
							//"bill_state": "",
							//"bill_postcode": "",
							//"bill_country": "",
							//"bill_notes": "",
							//"ship_addr1": "",
							//"ship_addr2": "",
							//"ship_addr3": "",
							//"ship_city": "",
							//"ship_state": "",
							//"ship_postcode": "",
							//"ship_country": "",
							//"ship_notes": "",
							//"terms_id": "",
							.'"custom_1": "'.$bean->ra_num_c.'",'
							.'"custom_2": "'.$custom_2.'",'
							.'"custom_3": "'.$custom_3.'",'
							//"custom_4": "",
							.'"custom_5": "Sugar"'
							.'}';
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: JSON query: '.$json.', '.date("Y-m-d H:i:s"));
					if($chargeover_instance == 1){
						//Nevada ChargeOver
						$url = $chargeover_url_us . '/api/v3/package/'.$package_id;
					}
					if($chargeover_instance == 2){
						//Ontario ChargeOver
						//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package/'.$package_id; //STAGING
						//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package/'.$package_id;
						$url = $chargeover_url_ca . '/api/v3/package/'.$package_id;
					}
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: ' . strlen($json);
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					$jsonResponse = json_decode($response, true);
					//get HTTP status code from ChargeOver
					$code = $jsonResponse['code'];
					$message = $jsonResponse['message'];
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message:"'.$message.'", '.date("Y-m-d H:i:s"));
					//202 Accepted - successfully updated object
					if ($code == 202){
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Package updated, external id: '.$package_id.', '.date("Y-m-d H:i:s"));
						$query = 'UPDATE opportunities_cstm SET updated_c = "(S)Opportunity to (CO)Package update successful"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
					}
					else{//$code != 202
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Package NOT updated, external id: '.$package_id.', '.date("Y-m-d H:i:s"));
						$query = 'UPDATE opportunities_cstm SET updated_c = "(S)Opportunity to (CO)Package update FAILED, Package suspended"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						//SUSPEND AND SKIP UPGRADE
						$suspend = true;
					}
				
					if (!$suspend){//i.e. the update was successful
						//UPDATE/UPGRADE ALL ASSOCIATED RLIs 
						
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: (CO)Associated Package id: '.$package_id.', '.date("Y-m-d H:i:s"));
						
						//get list of all associated line_items
						if($chargeover_instance == 1){
							//Nevada ChargeOver
							$url = $chargeover_url_us . '/api/v3/package/'.$package_id;
						}
						if($chargeover_instance == 2){
							//Ontario ChargeOver
							//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package/'.$package_id; //STAGING
							//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package/'.$package_id;
							$url = $chargeover_url_ca . '/api/v3/package/'.$package_id;
						}
						//open curl for GET
						$curl = curl_init($url);
						$headr = array();
						$headr[] = 'Accept: application/json';
						$headr[] = 'Content-type: application/json';
						$headr[] = 'Authorization: Basic '.$encoding;
						curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
						curl_setopt($curl, CURLOPT_HTTPGET, true);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						//make REST call
						$response = curl_exec($curl);
						curl_close($curl);
						$jsonResponse = json_decode($response, true);
						//get all line_item external_ids
						$line_items = $jsonResponse['response']['line_items'];
						$num_CO_items = sizeof($line_items);
						$item_id_CO = array();
						$line_item_id = array();
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: List of all associated items from (CO) Package: '.$package_id.', '.date("Y-m-d H:i:s"));
						for ($i = 0; $i < $num_CO_items; $i++){
							$line_item_id[] = $line_items[$i]['line_item_id'];
							$item_id_CO[] = $line_items[$i]['external_key'];
						}
						for ($i = 0; $i < $num_CO_items; $i++){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item #'.$i.', line_item_id: '.$line_item_id[$i].', external_key: '.$item_id_CO[$i].', '.date("Y-m-d H:i:s"));
						}
												
						//get list of all products associated with Sugar Opportunity
						$item_id_S = array();
						$product_template_id = array();
						$sales_stage = array();
						
						$query =  'SELECT id, product_template_id, sales_stage FROM revenue_line_items WHERE opportunity_id="'.$id.'" AND deleted <> "1"';
						$result = $GLOBALS['db']->query($query);
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: List of all associated items from (S) Opportunity: '.$id.', '.date("Y-m-d H:i:s"));
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
							$item_id_S[] = $row['id'];
							$product_template_id[] = $row['product_template_id'];
							$sales_stage[] = $row['sales_stage'];
						}
						
						$num_S_items = sizeof($item_id_S);
						for ($i = 0; $i < sizeof($item_id_S); $i++){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item #'.$i.', RLI id: '.$item_id_S[$i].', Sales Stage: '.$sales_stage[$i].', '.date("Y-m-d H:i:s"));
						}
						
						//keep array of items to cancel, insert and update
						//cancel - only need to track line_item_id
						$cancel = array();
						//insert - below are list of attributes to push to ChargeOver
						$insert = array();
						$insert_external_key = array();
						$insert_name = array();
						$insert_sales_stage = array();
						//update - below are a list of attributes to push to ChargeOver
						$update = array();
						$update_item_id = array(); //to change product type
						$update_name = array();
						$update_sales_stage = array();
						
						//PART 1
						//check if CO items exist in S
						for ($i = 0; $i < $num_CO_items; $i++){
							$match = false;
							for ($j = 0; $j < $num_S_items; $j++){
								//MATCH
								if (($item_id_CO[$i] == $item_id_S[$j]) &&($sales_stage[$j] == "Installed" || $sales_stage[$j] == "Scheduled Uninstallation" || $sales_stage[$j] == "Transferred")){
									$match = true;
									//UPDATE --> specify "line_item_id": "whatever"
									$update[] = $line_item_id[$i];
									
									//check if product changed
									$query = 'SELECT rli.product_template_id as product_template_id, rli.name as name, rli.sales_stage as sales_stage, ptc.chargeover_id_c as item_id
											FROM revenue_line_items rli
											JOIN product_templates_cstm ptc ON ptc.id_c = rli.product_template_id
											WHERE id = "'.$item_id_S[$j].'"';
									$result = $GLOBALS['db']->query($query);
									$product = '';
									$counter = 0; //to ensure below loop only executes once
									while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
										$product = $row['product_template_id'];
										$update_name[] = $row['name'];
										$update_sales_stage[] = $row['sales_stage'];
										$update_item_id[] = $row['item_id'];
										$counter++;
									}
								}
							}
							//NO MATCH
							if (!$match){
								//CANCEL --> specify "line_item_id":"whatever" and "cancel":"true"
								$cancel[] = $line_item_id[$i];
							}
						}
						
						//PART 2
						//check if S items exist in CO
						for ($i = 0; $i < $num_S_items; $i++){
							$match = false;
							if($sales_stage[$i] != "Installed" && $sales_stage[$i] != "Scheduled Uninstallation" || $sales_stage[$j] == "Transferred") $match = true; // to simulate a match so no insertion into CO happens
							for ($j = 0; $j < $num_CO_items; $j++){
								//MATCH, OR Sugar item is not installed
								if ($item_id_S[$i] == $item_id_CO[$j]){
									$match = true;
									//NOTHING
								}
							}
							//NO MATCH
							if (!$match){
								//INSERT INTO PACKAGE --> do NOT specify "line_item_id"
								
								//find associated (CO)item_id from (S)product
								$query = 'SELECT chargeover_id_c as item_id
										FROM product_templates_cstm
										WHERE id_c = "'.$product_template_id[$i].'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
									$insert[] = $row['item_id'];
									$counter++;
								}
								
								$insert_external_key[] = $item_id_S[$i];
								
								//get list of attributes from Sugar RLI
								$query = 'SELECT * FROM revenue_line_items WHERE id = "'.$item_id_S[$i].'"';
								$result = $GLOBALS['db']->query($query);
								$counter = 0; //to ensure below loop only executes once
								while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
									$insert_name[] = $row['name'];
									$insert_sales_stage[] = $row['sales_stage'];
									$counter++;
								}
							}
						}
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: #items, cancel: '.sizeof($cancel).', update: '.sizeof($update).', insert: '.sizeof($insert).', '.date("Y-m-d H:i:s"));
						
						$update_items = true;
						if(sizeof($cancel) == 0 && sizeof($update) == 0 && sizeof($insert) == 0){
							$update_items = false;
						}
						
						if($update_items){
							//Create 1 JSON String to update, insert and cancel items
							$json = '{'
									.'"line_items": [';
									//first the items to cancel
									if(sizeof($cancel) != 0){
										for ($i = 0; $i < sizeof($cancel); $i++){
											//no more fields needed, only require "line_item_id" and "cancel"
											$json .= '{'
														.'"line_item_id": "'.$cancel[$i].'",'
														.'"cancel": true'
														.'}';
											if ($i != (sizeof($cancel) - 1)) $json .= ',';
										}
									}
									//second the items to update
									if(sizeof($update) != 0){
										if (sizeof($cancel) != 0) $json .= ',';
										for ($i = 0; $i < sizeof($update); $i++){
											$json .= '{'
														.'"line_item_id": "'.$update[$i].'",'
														.'"item_id": "'.$update_item_id[$i].'",'
														.'"descrip": "'.$update_name[$i].'",'
														.'"custom_1": "'.$update_sales_stage[$i].'"'
														.'}';
											if ($i != (sizeof($update) - 1)) $json .= ',';
										}
									}
									//lastly the items to insert
									if(sizeof($insert) != 0){
										if(sizeof($cancel) != 0 || sizeof($update) != 0) $json .= ',';
										for ($i = 0; $i < sizeof($insert); $i++){
											$json .= '{'
														.'"item_id": "'.$insert[$i].'",'
														//.'"external_key": "'.$insert_external_key[$i].'",'
														.'"descrip": "'.$insert_name[$i].'",'
														.'"custom_1": "'.$insert_sales_stage[$i].'"'
														.'}';
											if ($i != (sizeof($insert) - 1)) $json .= ',';
										}
									}
							$json .= ']'
									.'}';
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: JSON query: '.$json.' '.date("Y-m-d H:i:s"));
							if($chargeover_instance == 1){
								//Nevada ChargeOver
								$url = $chargeover_url_us . '/api/v3/package/'.$package_id.'?action=upgrade';
							}
							if($chargeover_instance == 2){
								//Ontario ChargeOver
								//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package/'.$package_id.'?action=upgrade'; //STAGING
								//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package/'.$package_id.'?action=upgrade';
								$url = $chargeover_url_ca . '/api/v3/package/'.$package_id.'?action=upgrade';
							}
							//open curl for POST
							$curl = curl_init($url);
							$headr = array();
							$headr[] = 'Accept: application/json';
							$headr[] = 'Content-Length: ' . strlen($json);
							$headr[] = 'Content-type: application/json';
							$headr[] = 'Authorization: Basic '.$encoding;
							curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
							curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
							curl_setopt($curl, CURLOPT_POST, true);
							curl_setopt($curl, CURLOPT_HEADER, false);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
							//make REST call
							$response = curl_exec($curl);
							curl_close($curl);
							$jsonResponse = json_decode($response, true);
							$code = $jsonResponse['code'];
							$message = $jsonResponse['message'];
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: "'.$message.'", '.date("Y-m-d H:i:s"));
							
							//update custom response code and message fields
							/*
							$query =  'UPDATE opportunities_cstm SET response_code_c = "'.$code.'"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							$query =  'UPDATE opportunities_cstm SET response_message_c = "'.$message.'"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							*/
						}//end of if($update_items)
					}//END OF RLI UPGRADE/UPDATE
					//check if RLI update successful
					if($update_items){//items were updated
						if ($code == 200 && $message == ""){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: All RLIs updated successfully, '.date("Y-m-d H:i:s"));
							$query = 'UPDATE opportunities_cstm SET rli_updated_c = "All RLIs updated successfully"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							//update converted flag - in case where the initial conversion was NOT successful
							$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity to (CO)Package successful"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
						}
						else{//RLIs NOT updated successfully
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: RLI update FAILED, '.date("Y-m-d H:i:s"));
							$query = 'UPDATE opportunities_cstm SET rli_updated_c = "RLIs update FAILED, (CO) Package suspended"  WHERE id_c = "'.$id.'"';
							$bean->db->query($query, true);
							
							//SUSPEND SUSCRIPTION
							$suspend = true;
						}
					}
					else{//no items to update
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: There are no RLIs to update, '.date("Y-m-d H:i:s"));
						$query = 'UPDATE opportunities_cstm SET rli_updated_c = "There are no RLIs to update"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
						//update converted flag - in case where the initial conversion was NOT successful
						$query = 'UPDATE opportunities_cstm SET converted_c = "(S)Opportunity to (CO)Package successful"  WHERE id_c = "'.$id.'"';
						$bean->db->query($query, true);
					}
					
					//UNSUSPEND PACKAGE, after update and upgrade --> Will suspend regardless if there is issue with either
					//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Unsuspending (CO) Package id: '.$package_id.', '.date("Y-m-d H:i:s"));
					if($chargeover_instance == 1){
						//Nevada ChargeOver
						$url = $chargeover_url_us . '/api/v3/package/'.$package_id.'?action=unsuspend';
					}
					if($chargeover_instance == 2){
						//Ontario ChargeOver
						//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package/'.$package_id.'?action=unsuspend'; //STAGING
						//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package/'.$package_id.'?action=unsuspend';
						$url = $chargeover_url_ca . '/api/v3/package/'.$package_id.'?action=unsuspend';
					}
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: 0';
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					
					$jsonResponse = json_decode($response, true);
					//get HTTP status code and message
					$code = $jsonResponse['code'];
					$message = $jsonResponse['message'];
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: '.$message.', '.date("Y-m-d H:i:s"));
					
				}//END OF PACKAGE UPDATE
				if ($suspend){
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Suspending (CO) Package id: '.$package_id.', '.date("Y-m-d H:i:s"));
					if($chargeover_instance == 1){
						//Nevada ChargeOver
						$url = $chargeover_url_us . '/api/v3/package/'.$package_id.'?action=suspend';
					}
					if($chargeover_instance == 2){
						//Ontario ChargeOver
						//$url = 'https://greenlifewater-staging.chargeover.com/api/v3/package/'.$package_id.'?action=suspend'; //STAGING
						//$url = 'https://greenlifewater-new.chargeover.com/api/v3/package/'.$package_id.'?action=suspend';
						$url = $chargeover_url_ca . '/api/v3/package/'.$package_id.'?action=suspend';
					}
					$curl = curl_init($url);
					$headr = array();
					$headr[] = 'Accept: application/json';
					$headr[] = 'Content-Length: 0';
					$headr[] = 'Content-type: application/json';
					$headr[] = 'Authorization: Basic '.$encoding;
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headr);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_HEADER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					//make REST call
					$response = curl_exec($curl);
					curl_close($curl);
					
					$jsonResponse = json_decode($response, true);
					//get HTTP status code and message
					$code = $jsonResponse['code'];
					$message = $jsonResponse['message'];
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ChargeOver Status:'.$code.', message: '.$message.', '.date("Y-m-d H:i:s"));
					
					//update associated Account "complete conversion" flag to indicate that an associated Opportunity not synched properly to (CO)
					$query = 'UPDATE accounts_cstm SET complete_conversion_c = "fail" WHERE id_c = "'.$account_id.'"';
					$GLOBALS['db']->query($query);
				}
				else{//!suspend, i.e. successful
					//check if Account complete_conversion_c flag is "pass" or "fail"
					$query = 'SELECT *
								FROM accounts_cstm ac
								JOIN accounts_opportunities ao ON ac.id_c = ao.account_id
								WHERE ao.opportunity_id = "'.$id.'"';
					$result = $GLOBALS['db']->query($query);
					$counter = 0; //to ensure below loop only executes once
					$complete_conversion = "";
					while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
					{
						$complete_conversion = $row['complete_conversion_c'];
						$counter++;
					}
					$query = 'UPDATE accounts_cstm SET complete_conversion_c = "pass" WHERE id_c = "'.$account_id.'"';
					$GLOBALS['db']->query($query);
				}
			}//END OF if($bean->failed_reaf_c != "1")
			else{//REAFF FAILED
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Opportunity is flagged as a failed re-affirmation, '.date("Y-m-d H:i:s"));
				
				//create note and convert LLI to RLI ONLY if it is a NEW Opportunity (i.e. will execute only first time)
				if ($bean->date_entered === $bean->date_modified){
					$note = new Note();
					$note->name = 'The re-affirmation was failed';
					$note->parent_type = 'Opportunities';
					$note->parent_id = $id;
					$note->team_id = $bean->team_id;
					//$note->team_set_id = $bean->team_id;
					$note->save();
					//add team relationship
					/*
					$note->load_relationship('teams');
					$note->teams->replace(array($team_id));
					*/
				}
				//
				//
				//
				//BEGINNING OF LLI TO RLI CONVERSION
				//
				//
				//
				
				//NOTE, this will only execute once, since after the LLI to RLI conversion, the LLI will be flagged as deleted
				
				$item_id = array(); //for external key, i.e. (CO) item_id
				$product_template_id = array();
				$product_name = array();
				$sales_stage = array();
				$combo = array();
				$num_items = 0;
				//get the product information from LeadLineItems
				$query = 'select pt.id as product_template_id, ptc.chargeover_id_c as item_id, ptc.combo_c as combo, li.name as name, l.id as lead_id, lic.sales_stage_c as sales_stage
						from leads l
						join leads_lipkg_lead_line_items_1_c lb on lb.leads_lipkg_lead_line_items_1leads_ida = l.id
						join lipkg_lead_line_items li on li.id = lb.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb
						join lipkg_lead_line_items_cstm lic on li.id = lic.id_c
						join lipkg_lead_line_items_producttemplates_c lip on lip.lipkg_lead_line_items_producttemplateslipkg_lead_line_items_idb = li.id
						join product_templates pt on pt.id = lip.lipkg_lead_line_items_producttemplatesproducttemplates_ida
						join product_templates_cstm ptc on ptc.id_c = pt.id
						where l.opportunity_id = "'.$id.'"
						and li.deleted <> "1"
						and lip.deleted <> "1"';
				$result = $GLOBALS['db']->query($query);
				//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: SQL query: '.$query.', '.date("Y-m-d H:i:s"));
				while($row = $GLOBALS['db']->fetchByAssoc($result))
				{
					$product_template_id[$num_items] = $row['product_template_id'];
					$product_name[$num_items] = $row['name'];
					$item_id[$num_items] = $row['item_id'];
					$sales_stage[$num_items] = $row['sales_stage'];
					$combo[$num_items] = $row['combo'];
					
					$lead_id = $row['lead_id'];
					//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item#'.$num_items.', (S)product_template_id: '.$product_template_id[$num_items].', (S)name: '.$product_name[$num_items].', '.date("Y-m-d H:i:s"));
					//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Item#'.$num_items.', (CO)item_id: '.$item_id[$num_items].', '.date("Y-m-d H:i:s"));
					
					//create new RLI in Sugar
					$RLI = new RevenueLineItem();
					$RLI->opportunity_id = $id;
					$RLI->product_template_id = $product_template_id[$num_items];
					$RLI->name = $product_name[$num_items];
					$RLI->sales_stage = $sales_stage[$num_items];
					$RLI->from_conversion_c = "1"; // flag to track that RLI was created from conversion
					$RLI->team_id = $bean->team_id;
					//$RLI->team_set_id = $bean->team_id;
					$RLI->save();
					//add team relationship
					/*
					$RLI->load_relationship('teams');
					$RLI->teams->replace(array($team_id));
					*/
					$num_items++;
				}
				
				//ONLY CREATE PACKAGE IF ABLE TO RETRIEVE ITEM INFORMATION, i.e. $num_items != 0
				if ($num_items != 0){
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Converting associated LLI to RLI..., '.date("Y-m-d H:i:s"));
					//set deleted flag of all associated LLIs to "1"
					$query = 'UPDATE lipkg_lead_line_items lli
							JOIN leads_lipkg_lead_line_items_1_c llli ON llli.leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb = lli.id
							SET lli.deleted = "1"
							WHERE llli.leads_lipkg_lead_line_items_1leads_ida = "'.$lead_id.'"';
					$GLOBALS['db']->query($query);
				}
				//
				//
				//
				//END OF LLI TO RLI CONVERSION
				//
				//
				//
				
			}//END OF REAF FAILED
			
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED, '.date("Y-m-d H:i:s"));
		}
		
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK #'.self::$count.', '.date("Y-m-d H:i:s"));
    }
}
?>
