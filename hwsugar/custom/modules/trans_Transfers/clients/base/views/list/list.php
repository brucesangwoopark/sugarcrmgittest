<?php
$module_name = 'trans_Transfers';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'billing_address_street',
                'label' => 'LBL_BILLING_ADDRESS_STREET',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'billing_address_city',
                'label' => 'LBL_BILLING_ADDRESS_CITY',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'billing_address_state',
                'label' => 'LBL_BILLING_ADDRESS_STATE',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'billing_address_postalcode',
                'label' => 'LBL_BILLING_ADDRESS_POSTALCODE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'billing_address_country',
                'label' => 'LBL_BILLING_ADDRESS_COUNTRY',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'transfer_date',
                'label' => 'LBL_TRANSFER_DATE',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'old_account_id_c',
                'label' => 'LBL_OLD_ACCOUNT_ID',
                'enabled' => true,
                'id' => 'ACCOUNT_ID_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'new_account_id_c',
                'label' => 'LBL_NEW_ACCOUNT_ID',
                'enabled' => true,
                'id' => 'ACCOUNT_ID1_C',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              11 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
