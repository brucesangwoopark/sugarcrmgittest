({
    extendsFrom: 'CreateView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.model.addValidationTask('validate_address', _.bind(this._validateAddress, this));
    },

    _validateAddress: function (fields, errors, callback) {
        //skip validation if box is checked
        var skip = document.getElementById('addValCustlalalabypass') || {checked : false};
        if (skip.checked) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while (oldMsg.length > 0) {
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            callback(null, fields, errors);
            return;
        }

        var insertMessage = function (message) {
            var oldMsg = document.getElementsByClassName('addValCustABCDErrMsg');
            while(oldMsg.length > 0){
                oldMsg[0].parentNode.removeChild(oldMsg[0]);
            }
            if(message === '')
                return;
            //var target = document.getElementById('content').firstChild.firstChild.childNodes[1].firstChild.firstChild.childNodes[1].childNodes[3];
            var node = document.createElement('div');

            var skip = document.createElement('span');
            skip.innerHTML = '<br/><input type="checkbox" id="addValCustlalalabypass"/>  Ignore address errors, I know what I\'m doing';
            skip.style.fontSize = '12px';
            skip.style.color = '#717171';

            node.id = 'ignore_address_error_msgbox';
            node.innerHTML = '<br/>' + message;
            node.style.fontSize = '15px';
            node.style.color = '#f44242';
            node.style.fontWeight = 'bold';
            node.className = 'addValCustABCDErrMsg'; // stupid name to ensure I am the only one using this class
            node.appendChild(skip);

            if ($('#drawers').html() === '') {
                $('#content').find('.record.tab-layout').prepend(node);
            } else {
                $('#drawers').find('.record.tab-layout').prepend(node);
            }
        };

        var is_ownership_transferring = this.model.get('transfer_ownership_c');
        if (is_ownership_transferring) {
            /* do stuff */
            callback(null, fields, errors);
        } else {
            var selected_country = this.model.get('new_address_country_c');
            if (selected_country === null || selected_country === '') {
                insertMessage('Select country');

                errors['new_address_country_c'] = errors['new_address_country_c'] || {};
                errors['new_address_country_c'].invalid_address = true;

                callback(null, fields, errors);
            }

            var street = this.model.get('new_address_street_c');
            var city = this.model.get('new_address_city_c');
            var state = this.model.get('new_address_state_c');
            var postal = (this.model.get('new_address_postalcode_c')).toUpperCase();

            var that = this;

            if (selected_country === 'United States') {
                var requestUrl = 'https://us-street.api.smartystreets.com/street-address?auth-id=23379202066372234&'
                //var requestUrl = 'https://us-street.api.smartystreets.com/street-address?auth-id=4d398a33-4aee-05e8-4f81-2df7bbb07acb&auth-token=hllxGJqS7c6k3iTVOAVR&'
                    + 'street=' + encodeURIComponent(street || 'Placeholder') +  '&'// I don't want CSRs to see 400 error from smartystreet when they hope to find an address without providing street...
                    + 'city=' + encodeURIComponent(city) + '&'
                    + 'state=' + encodeURIComponent(state) + '&'
                    + 'postalcode=' + encodeURIComponent(postal);

                $.ajax({
                    url: requestUrl,
                    headers: {
                        'Accept': 'application/json'
                    }
                }).done(function (data) {
                    var json = data;

                    if(json.length === 0){
                        var fields = ['new_address_street_c', 'new_address_city_c', 'new_address_state_c', 'new_address_postalcode_c'];
                        for(var iter = 0; iter < fields.length; iter++){
                            errors[fields[iter]] = errors[fields[iter]] || {};
                            errors[fields[iter]].invalid_address = true;
                        }
                        insertMessage('This address is not recognized as a valid address.');
                    }
                    else {
                        json = json[0];
                        var updateMessage = function(message, field, original, update){
                            return message + '<strong>Field: </strong>' + original + '<br/>Updated to: ' + update + '<br/>';
                        };
                        var message = '';
                        if(street !== json.delivery_line_1){
                            that.model.set('new_address_street_c', json.delivery_line_1);
                            message = updateMessage(message, 'Address Street', street, json.delivery_line_1);
                            errors['new_address_street_c'] = errors['new_address_street_c'] || {};
                            errors['new_address_street_c'].invalid_address = true;
                        }

                        if(city !== json.components.city_name){
                            that.model.set('new_address_city_c', json.components.city_name);
                            message = updateMessage(message, 'Address City', city, json.components.city_name);
                            errors['new_address_city_c'] = errors['new_address_city_c'] || {};
                            errors['new_address_city_c'].invalid_address = true;
                        }

                        if(state !== json.components.state_abbreviation){
                            that.model.set('new_address_state_c', json.components.state_abbreviation);
                            message = updateMessage(message, 'Address State', state, json.components.state_abbreviation);
                            errors['new_address_state_c'] = errors['new_address_state_c'] || {};
                            errors['new_address_state_c'].invalid_address = true;
                        }

                        if(postal !== json.components.zipcode){
                            that.model.set('new_address_postalcode_c', json.components.zipcode);
                            message = updateMessage(message, 'Address Postal&sol;Zip Code', postal, json.components.zipcode);
                            errors['new_address_postalcode_c'] = errors['new_address_postalcode_c'] || {};
                            errors['new_address_postalcode_c'].invalid_address = true;
                        }

                        if (message !== '') {
                            insertMessage(message === '' ? '' : message + 'Please review changes, save again to confirm');
                        }
                    }
                    callback(null, fields, errors);
                });
            } else if (selected_country === 'Canada') {
                var requestUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address='
                    + encodeURIComponent(street + ' ' + city + ' ' + state + ' ' + selected_country + ' ' + postal);

                $.ajax({
                    url: requestUrl,
                    headers: {
                        'Accept': 'application/json'
                    }
                }).done(function (data) {
                    console.log(data);
                    if (data.status === 'ZERO_RESULTS') {
                        var fields = ['new_address_street_c', 'new_address_city_c', 'new_address_state_c', 'new_address_postalcode_c'];
                        for(var iter = 0; iter < fields.length; iter++){
                            errors[fields[iter]] = errors[fields[iter]] || {};
                            errors[fields[iter]].invalid_address = true;
                            insertMessage('This address is not recognized as a valid address.<br/>');
                        }
                        callback(null, fields, errors);
                    }
                    else if (data.status === 'OK') {
                        var resStreet = ((data.results[0].address_components.filter(field => field.types.indexOf('street_number') !== -1)[0] || {long_name: ''}).long_name + ' '
                            + (data.results[0].address_components.filter(field => field.types.indexOf('route') !== -1)[0] || {short_name: ''}).short_name).trim();
                        var resCity = ((data.results[0].address_components.filter(field => field.types.indexOf('sublocality') !== -1)[0])
                            || (data.results[0].address_components.filter(field => field.types.indexOf('locality') !== -1)[0])
                    || (data.results[0].address_components.filter(field => field.types.indexOf('administrative_area_level_2') !== -1)[0])
                    || {long_name: ''}).long_name;
                        var resState = (data.results[0].address_components.filter(field => field.types.indexOf('administrative_area_level_1') !== -1)[0]
                            || {short_name: ''}).short_name;
                        var resPostal = (data.results[0].address_components.filter(field => field.types.indexOf('postal_code') !== -1)[0]
                            || {long_name: ''}).long_name;

                        //if postal code does not match, throw error regardless
                        if(resPostal === '' || ((resPostal !== postal) && (resPostal !== postal.substr(0, 3) + ' ' + postal.substr(3)))){
                            errors['new_address_postalcode_c'] = errors['new_address_postalcode_c'] || {};
                            errors['new_address_postalcode_c'].invalid_address = true;
                            insertMessage(data.results[0].formatted_address + '<br/>Please review changes, save again to confirm.<br/>');
                        } else {
                            that.model.set('new_address_postalcode_c', postal);
                            // street city state
                            var message = '';
                            var updateMessage = function(message, field, original, update){
                                return message + '<strong>Field: </strong>' + original + '<br/>Updated to: ' + update + '<br/>';
                            };
                            if(street !== resStreet){
                                if(resStreet) {
                                    that.model.set('new_address_street_c', resStreet);
                                    message = updateMessage(message, 'Address Street', street, resStreet);
                                }
                                errors['new_address_street_c'] = errors['new_address_street_c'] || {};
                                errors['new_address_street_c'].invalid_address = true;
                            }

                            if(!resCity || (city !== resCity)){
                                if(resCity) {
                                    that.model.set('new_address_city_c', resCity);
                                    message = updateMessage(message, 'Address City', city, resCity);
                                }
                                errors['new_address_city_c'] = errors['new_address_city_c'] || {};
                                errors['new_address_city_c'].invalid_address = true;
                            }

                            if(!resState || (state !== resState)){
                                if(resState) {
                                    that.model.set('new_address_state_c', resState);
                                    message = updateMessage(message, 'Address State', state, resState);
                                }
                                errors['new_address_state_c'] = errors['new_address_state_c'] || {};
                                errors['new_address_state_c'].invalid_address = true;
                            }

                            if (message !== '') {
                                insertMessage(message);
                            }
                        }
                        callback(null, fields, errors);
                    }
                    //server/request error
                    else {
                        throw(json.status + ' (' + json.error_message + ')');
                    }
                });
            }
        }
    }
})