<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['trans_Transfers']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'transfer_date' => array(),
        'billing_address_street' => array(),
        'billing_address_city' => array(),
        'billing_address_postalcode' => array(),
        'billing_address_state' => array(),
        'billing_address_country' => array(),
    ),
    
    'quicksearch_field' => 
    array (
    0 => 'name',
    1 => 'transfer_date',
    2 => 'billing_address_street',
    3 => 'billing_address_city',
    4 => 'billing_address_postalcode',
    5 => 'billing_address_state',
    6 => 'billing_address_country',
    ), 
    'quicksearch_priority' => 1,
);
?>