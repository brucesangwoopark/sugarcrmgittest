<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveTransfersHook {
	
	//to ensure hook only executes once
	//static $already_ran = false;
	
  function afterSave($bean, $event, $arguments)
  {
		//if(self::$already_ran == true) return;//to ensure hook only executes once
		//self::$already_ran = true;//to ensure hook only executes once
		
		//for logging purposes
		$module = "Transfers";
		$hook = "after_save";
		$id = $bean->id;
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Transfer: '.$id.', '.date("Y-m-d H:i:s"));
					
		try{
			if($bean->transfer_complete_c != "1"){
				if($bean->transfer_ownership_c != "1"){
					//Get customer's Account id
					$old_account = ""; //setting default
					$old_account = $bean->account_id1_c;
					
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Getting customer\'s new address from Account: '.$old_account.', '.date("Y-m-d H:i:s"));				
									
					//Get customer's info through Account bean
					$old_account_bean = BeanFactory::getBean('Accounts', $old_account);
					
					//name transfer
					$name = $old_account_bean->name.' - Transfer of Address';
					$query = 'UPDATE trans_transfers SET name = "'.$name.'" WHERE id = "'.$id.'"';
					$GLOBALS['db']->query($query);
					
									
					//Get customer's new address
					$new_address_street = $bean->new_address_street_c;
					$new_address_city = $bean->new_address_city_c;
					$new_address_state = $bean->new_address_state_c;
					$new_address_postalcode = $bean->new_address_postalcode_c;
					$new_address_country = $bean->new_address_country_c;
					$new_phone = $bean->new_phone_c;
					$new_address_compartment_number = $bean->compartment_number_c;
					
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating a new Contact with Customer\'s new address, '.date("Y-m-d H:i:s"));				
					//Create new Contact with new Customer's old address information
					$new_contact = BeanFactory::newBean('Contacts');
					$new_contact->first_name = $name;
					$new_contact->primary_address_street = $new_address_street;
					$new_contact->primary_address_city = $new_address_city;
					$new_contact->primary_address_state = $new_address_state;
					$new_contact->primary_address_postalcode = $new_address_postalcode;
					$new_contact->primary_address_country = $new_address_country;
					$new_contact->phone_work = $new_phone;
					//setting "most_recent_address_c" checkbox/flag in Contact
					$new_contact->most_recent_address_c = '1';
					$new_contact->compartment_number_c = $new_address_compartment_number;
					//save contact
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Saving new Contact, '.date("Y-m-d H:i:s"));												
					$new_contact->save();
					
					//Add relationship between new Contact and Customer Account, using account_contacts bridge table				
					$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding relationship between Account: '.$old_account.' and Contact: '.$new_contact->id.', '.date("Y-m-d H:i:s"));												
					$new_contact->load_relationship('accounts');
					$new_contact->accounts->add($old_account);
					
					//
					//
					//UNINSTALLATION AND INSTALLATION JOB CREATION
					//
					//
					
					//STEP 1
					//find all associated RLI
					$RLI_id = array();
					$query = 'SELECT rli.id as rli_id
										FROM revenue_line_items rli
										WHERE rli.account_id = "'.$old_account.'"';
					$result = $GLOBALS['db']->query($query);
					while($row = $GLOBALS['db']->fetchByAssoc($result))
					{
						$RLI_id[] = $row['rli_id'];
					}
					
					//STEP 2
					//create uninstallation Job(s) for each RLI associated w/ Account w/ OLD address
					//AND
					//create installation Job(s) for each RLI associated w/ Account w/ NEW address
					for ($i = 0; $i < sizeof($RLI_id); $i++){
						//
						//create uninstallation job
						//
						
						//find product type
						$query = 'SELECT pc.name as product_category
										FROM product_categories pc
										JOIN revenue_line_items rli ON rli.category_id = pc.id
										WHERE rli.id = "'.$RLI_id[$i].'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below loop only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
						{
							$product_category = $row['product_category'];
							$counter++;
						}
						
						//find associated Opportunity
						$opportunity_id = ""; //setting default
						$query = 'SELECT opportunity_id
											FROM revenue_line_items
											WHERE id = "'.$RLI_id[$i].'"';	
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below loop only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0)
						{
							$opportunity_id = $row['opportunity_id'];
							$counter++;
						}
						
						
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating uninstall job for rli: '.$RLI_id[$i].', '.date("Y-m-d H:i:s"));												
						
						$uninstall_job = BeanFactory::newBean('abcde_Jobs');
						//Set job attributes
						$uninstall_job->name = substr($old_account_bean->name, 0, strlen($old_account_bean->name) - 10).' - Transfer Uninstallation Job #'.($i+1);
						if($product_category == "Whole Home" || $product_category == "D-Scaler"){
								$uninstall_job->job_type_c = 'Uninstall WH or DS';
						}
						if($product_category == "Under Counter"){
							$uninstall_job->job_type_c = 'Uninstall UC';
						}
						$uninstall_job->num_items_c = 1;
						$uninstall_job->transfer_address_c = '1'; //setting flag indicating the uninstall job is assoicated to a transfer address change
						$uninstall_job->opportunity_id_c = $opportunity_id;
						$uninstall_job->from_conversion_c = 'true'; //to simulate the job being created from Lead conversion (i.e. created automatically)
						
						$uninstall_job->istransferred_c = '1'; // to check unit transfer
						
						//NOTE: the location of the job will be default be set to the address of the associated Account
						
						$uninstall_job->save();
						
						//add relationship between Job and RLI
						$uninstall_job->load_relationship('abcde_jobs_revenuelineitems_1');
						$uninstall_job->abcde_jobs_revenuelineitems_1->add($RLI_id[$i]);
						
						//add relationship between Job and Account
						$uninstall_job->load_relationship('accounts_abcde_jobs_1');
						$uninstall_job->accounts_abcde_jobs_1->add($old_account);
						
						$uninstall_job->save();
						
						//
						//end of uninstallation job creation
						//
						
						//
						//create installation job
						//
						
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Creating install job for rli: '.$RLI_id[$i].', '.date("Y-m-d H:i:s"));												
						
						$install_job = BeanFactory::newBean('abcde_Jobs');
						//Set job attributes
						$install_job->name = substr($old_account_bean->name, 0, strlen($old_account_bean->name) - 10).' - Transfer Installation Job #'.($i+1);
						if($product_category == "Whole Home" || $product_category == "D-Scaler"){
								$install_job->job_type_c = 'Install WH or DS';
						}
						if($product_category == "Under Counter"){
							$install_job->job_type_c = 'Install UC';
						}
						$install_job->num_items_c = 1;
						$install_job->transfer_address_c = '1'; //setting flag indicating the install job is assoicated to a transfer address change
						$install_job->opportunity_id_c = $opportunity_id;
						$install_job->from_conversion_c = 'true'; //to simulate the job being created from Lead conversion (i.e. created automatically
						
						//set customer's NEW address for installation job
                        $temp_address = $new_address_street.','.$new_address_postalcode.' '.$new_address_city.', '.$new_address_state.', '.$new_address_country;
						if ($new_address_compartment_number != null)
                            $temp_address = '#' . $new_address_compartment_number . ',' . $temp_address;
                        $install_job->location = $temp_address;
						
						$install_job->istransferred_c = '1'; // to check unit transfer
						
						$install_job->save();
						
						//add relationship between Job and RLI
						$install_job->load_relationship('abcde_jobs_revenuelineitems_1');
						$install_job->abcde_jobs_revenuelineitems_1->add($RLI_id[$i]);
						
						//add relationship between Job and Account
						$install_job->load_relationship('accounts_abcde_jobs_1');
						$install_job->accounts_abcde_jobs_1->add($old_account);
						
						$install_job->save();
						
						//
						//end of installation job creation
						//

						// save new enbridge number into the buffer table (trans_transfers_transfer_enbridge)
						if ($bean->new_enbridge_number_c != null) {
                            $query = 'insert into trans_transfers_transfer_enbridge(opportunity_id, job_id, enbridge_number) VALUES(\'' . $opportunity_id . '\', \'' . $install_job->id . '\', \'' . $bean->new_enbridge_number_c . '\')';
                            $GLOBALS['db']->query($query);
						}
					}// END OF for ($i = 0; $i < sizeof($RLI_id); $i++)
					
					//update "transfer_complete_c" flag
					$query = 'UPDATE trans_transfers_cstm SET transfer_complete_c = "1" WHERE id_c = "'.$id.'"';
					$GLOBALS['db']->query($query);
					//
					//
					//END OF UNINSTALLATION AND INSTALLATION JOB CREATION
					//
					//
				
				
					//create Note of Transfer on Account
					$note = BeanFactory::newBean('Notes');
					$note->name = 'Customer Address Change Pending, '.date("Y-m-d H:i:s");
					$note->description = 'The customer is moving and their address change is pending.  Their new address is currently being stored as an associated Contact.
																Once the uninstallation of the item(s) from their old address has been completed, the address on the Account level will be updated and the
																Contact which is currently (at the time of the note creation) holding the customer\'s new address will hold their old address.';
					$note->parent_type = 'Accounts';
					$note->parent_id = $old_account;
					$note->save();
				
					
				}// END OF if($bean->transfer_ownership_c != "1")
				
				else{//i.e. transfer of ownership
					
					if (empty($bean->account_id1_c) && empty($bean->account_id_c)){
						$name = 'NAME PENDING - TRANSFER TO - NAME PENDING';
						$query = 'UPDATE trans_transfers SET name = "'.$name.'" WHERE id = "'.$id.'"';
						$GLOBALS['db']->query($query);
					}
					
					//case where old_account is added but missing the address
//					if(!empty($bean->account_id1_c) && ($bean->billing_address_street == "" || $bean->billing_address_city == ""
//														|| $bean->billing_address_state == "" || $bean->billing_address_postalcode == "" || $bean->billing_address_country == "" )){
//						$old_account_id = $bean->account_id1_c;
//						//get old account name
//						$old_account_name = ""; //setting default
//						$query = 'SELECT name FROM accounts WHERE id = "'.$old_account_id.'"';
//						$result = $GLOBALS['db']->query($query);
//						$counter = 0; //to ensure below only executes once
//						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
//							$old_account_name = $row['name'];
//							$counter++;
//						}
//						$old_account_name = substr($old_account_name, 0, strlen($old_account_name) - 10);
//						//rename Transfer
//						$name = $old_account_name.' - TRANSFER TO - NAME PENDING';
//						$query = 'UPDATE trans_transfers SET name = "'.$name.'" WHERE id = "'.$id.'"';
//						$GLOBALS['db']->query($query);
//
//						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Updating address of Transfer: '.$id.', from Account: '.$old_account_id.' '.date("Y-m-d H:i:s"));
//						//get address of old_account
//						$query = 'SELECT billing_address_street street, billing_address_city city, billing_address_state state,
//									billing_address_postalcode postalcode, billing_address_country country
//								FROM accounts
//								WHERE id = "'.$old_account_id.'"';
//						$result = $GLOBALS['db']->query($query);
//						$counter = 0; //to ensure below only executes once - note: this will get the first email entry
//						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
//							$street = $row['street'];
//							$city = $row['city'];
//							$state = $row['state'];
//							$postalcode = $row['postalcode'];
//							$country = $row['country'];
//							$counter++;
//						}
//						//update transfer address
//						$query = 'UPDATE trans_transfers
//								SET billing_address_street = "'.$street.'",
//									billing_address_city = "'.$city.'",
//									billing_address_state = "'.$state.'",
//									billing_address_postalcode = "'.$postalcode.'",
//									billing_address_country = "'.$country.'"
//								WHERE id = "'.$id.'"';
//						$GLOBALS['db']->query($query);
//					}// END OF case where old_account is added but missing the address
					
					//case where the old_account AND new_account is added to the transfer
					if(!empty($bean->account_id1_c) && !empty($bean->account_id_c)){
						
						//trigger transfer process below
						$old_account_id = $bean->account_id1_c;
						$new_account_id = $bean->account_id_c;
											
						$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Beginning transfer process. Old Account: '.$old_account_id.', New Account: '.$new_account_id.', '.date("Y-m-d H:i:s"));
						
						//get old account name
						$old_account_name = ""; //setting default
						$query = 'SELECT name FROM accounts WHERE id = "'.$old_account_id.'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
							$old_account_name = $row['name'];
							$counter++;
						}				
						$old_account_name = substr($old_account_name, 0, strlen($old_account_name) - 10);
						
						//get new account name
						$new_account_name = ""; //setting default
						$query = 'SELECT name FROM accounts WHERE id = "'.$new_account_id.'"';
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
							$new_account_name = $row['name'];					
							$counter++;
						}
						$new_account_name = substr($new_account_name, 0, strlen($new_account_name) - 10);
						
						//rename Transfer
						$name = $old_account_name.' - TRANSFER TO - '.$new_account_name;
						$query = 'UPDATE trans_transfers SET name = "'.$name.'" WHERE id = "'.$id.'"';
						$GLOBALS['db']->query($query);
					
						//STEP 1)delete new account Opportunity that was created from conversion process
						$new_opportunity_id = ""; //setting default
						$query = 'SELECT opportunity_id
								FROM accounts_opportunities
								WHERE account_id = "'.$new_account_id.'"';	
						$result = $GLOBALS['db']->query($query);
						$counter = 0; //to ensure below only executes once
						while(($row = $GLOBALS['db']->fetchByAssoc($result)) && $counter == 0){
							$new_opportunity_id = $row['opportunity_id'];					
							$counter++;
						}
						/*
						if($new_opportunity_id != ""){
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Deleting Opportunity: '.$new_opportunity_id.', '.date("Y-m-d H:i:s"));
							$query = 'UPDATE opportunities SET deleted = "1" WHERE id = "'.$new_opportunity_id.'"';
							$GLOBALS['db']->query($query);
						}
						*/
						
						//STEP 2)find all Opportunities associated with old account
						$old_opportunity_id = array();
						$query = 'SELECT opportunity_id FROM accounts_opportunities WHERE account_id = "'.$old_account_id.'" AND deleted <> "1"';
						$result = $GLOBALS['db']->query($query);
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
							$old_opportunity_id[] = $row['opportunity_id'];					
						}
						
						
						//copy all OLD customer's RLI to NEW customer's Opportunity
						
						//find the NEW Opportunity created for the new customer
						$query = 'SELECT opportunity_id FROM accounts_opportunities WHERE account_id = "'.$new_account_id.'"';
						$result = $GLOBALS['db']->query($query);
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
							$new_opportunity_id = $row['opportunity_id'];					
						}
						
						$new_opp = BeanFactory::getBean('Opportunities', $new_opportunity_id);
						
						$rli_count = 0; //to count how many RLIs are transferred
						//For each old opportunity
						for ($i = 0; $i < sizeof($old_opportunity_id); $i++){
							//$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Copying Opportunity: '.$old_opportunity_id[$i].' and creating a new Opportunity, '.date("Y-m-d H:i:s"));
							$old_opp = BeanFactory::getBean('Opportunities', $old_opportunity_id[$i]);
							
							/*
							//STEP 3)create a new copied Opportunity
							$new_opp = BeanFactory::newBean('Opportunities');
							//create relationship to new Account (should update accounts_opportunities bridge table)
												
							//set new Opportunity attributes
							$new_opp->name = $new_account_name.' - Opportunity'; if($i > 0) $new_opp->name = $new_opp->name.' #'.($i+1);
							//new_ra number should be generated
							$query =  "SELECT greatest(MAX(oc.ra_num_c),MAX(lc.ra_num_c)) as max_ra_num
										FROM opportunities_cstm oc, leads_cstm lc";
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
								$max_ra_num = $row['max_ra_num'];
							}
							if ($max_ra_num == null) { //in case of first entry
								$max_ra_num = 300000;
							}
							$ra_num = $max_ra_num + 1;
							$new_opp->ra_num_c = $ra_num;
							*/
							$new_opp->transferred_opportunity_id_c .= $old_opportunity_id[$i];
							$new_opp->save();
							
							//$ra_num = $new_opp->ra_num_c;
				
										
							//STEP 4)find all RLIs associated to old opportunity
							$old_rli_id = array();
							$query = 'SELECT id
									FROM revenue_line_items
									WHERE opportunity_id = "'.$old_opportunity_id[$i].'"
									AND deleted <> "1"';	
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
								$old_rli_id[] = $row['id'];					
							}
							
							$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Copying all RLI from Opportunity: '.$old_opportunity_id[$i].' and creating a new Opportunity, '.date("Y-m-d H:i:s"));
							//For each old RLI
							for($j = 0; $j < sizeof($old_rli_id); $j++){
								$rli_count++;
								$old_rli = BeanFactory::getBean('RevenueLineItems', $old_rli_id[$j]);
								$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Changing the status of rli: '.$old_rli_id[$j].' to "Transferred", '.date("Y-m-d H:i:s"));
								//$query = 'UPDATE revenue_line_items SET sales_stage = "Transferred" WHERE id = "'.$old_rli_id[$j].'"';
								//$GLOBALS['db']->query($query);
								$old_rli->sales_stage = 'Transferred';
								$old_rli->save();
								//4a)Create a new RLI and copy information with NEW account_id and opportunity_id, with status of "Installed"
								$new_rli = BeanFactory::newBean('RevenueLineItems');
								//$new_rli->name = $old_rli->name.' - Transferred';
								$new_rli->name = ($new_opp->ra_num_c).' - '.$rli_count;
								$new_rli->product_template_id = $old_rli->product_template_id;
								$new_rli->account_id = $new_account_id;
								$new_rli->category_id = $old_rli->category_id;
								$new_rli->mft_part_num = $old_rli->mft_part_num;
								$new_rli->serial_number = $old_rli->serial_number;
								$new_rli->sales_stage = 'Installed';
								$new_rli->opportunity_id = $new_opp->id;
								$new_rli->from_conversion_c = '1';
								$new_rli->associated_account_c = $new_account_id;
								$new_rli->save();
								
								/*
								$query = 'UPDATE revenue_line_items SET sales_stage = "Installed" WHERE id = "'.$new_rli->id.'"';
								$GLOBALS['db']->query($query);
								
								//4b)change status of OLD RLI to "transferred"
								$query = 'UPDATE revenue_line_items SET sales_stage = "Transferred" WHERE id = "'.$old_rli_id[$j].'"';
								$GLOBALS['db']->query($query);
								*/
							
							}//END OF For each RLI
							
							//STEP 5)retrieve the bean of OLD and NEW opportunity, and save --> will trigger Opportunities after_save hook
							$old_opp->save();
							$new_opp->load_relationship('accounts');
							$new_opp->accounts->add($new_account_id);
							
							//update relationship opportunity and lead
							$new_opp->load_relationship('leads');
							$query = 'SELECT id FROM leads WHERE account_id = "'.$new_account_id.'"';
							$result = $GLOBALS['db']->query($query);
							while($row = $GLOBALS['db']->fetchByAssoc($result)){
								$lead_id = $row['id'];					
							}
							$new_opp->leads->add($lead_id);
							$new_opp->save();
						}	
						//END OF For each opportunity
						
						//STEP 6)update status of old Account to "Cancelled" and cancel_reason_dropdown_c to "Moving"
						$query = 'UPDATE accounts_cstm SET customer_status_c = "Cancelled", cancel_reason_dropdown_c = "Moving", cancel_date_c = "'.date("Y-m-d H:i:s").'" WHERE id_c = "'.$old_account_id.'"';
						$GLOBALS['db']->query($query);
						
						//STEP 7)update status of new Account to "Installed"
						$query = 'UPDATE accounts_cstm SET customer_status_c = "Installed", install_date_c = "'.date("Y-m-d H:i:s").'" WHERE id_c = "'.$new_account_id.'"';
						$GLOBALS['db']->query($query);
						
						//update transfer_date field in transfer
						$query = 'UPDATE trans_transfers SET transfer_date = "'.date("Y-m-d H:i:s").'" WHERE id = "'.$id.'"';
						$GLOBALS['db']->query($query);
						
						//update relationship between Transfer and old and new Accounts
						$bean->load_relationship('trans_transfers_accounts');
						$bean->trans_transfers_accounts->add($old_account_id);
						$bean->trans_transfers_accounts->add($new_account_id);
						
						//update "transfer_complete_c" flag
						$query = 'UPDATE trans_transfers_cstm SET transfer_complete_c = "1" WHERE id_c = "'.$id.'"';
						$GLOBALS['db']->query($query);
						
						
					}// END OF if(!empty($bean->old_account_id) && !empty($bean->new_account_id)){
				
						
				}//END OF transfer of ownership
				
			}//END OF $bean->transfer_complete_c != "1"
			else{//i.e. transfer already complete
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Transfer process has already been completed, '.date("Y-m-d H:i:s"));		
			}
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
	}
}
?>
