<?php
$popupMeta = array (
    'moduleMain' => 'supkg_SystemUsers',
    'varName' => 'supkg_SystemUsers',
    'orderBy' => 'supkg_systemusers.first_name, supkg_systemusers.last_name',
    'whereClauses' => array (
  'first_name' => 'supkg_systemusers.first_name',
  'last_name' => 'supkg_systemusers.last_name',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_NAME',
    'link' => true,
    'orderBy' => 'last_name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
    'name' => 'name',
  ),
  'USERTYPE_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'label' => 'LBL_USERTYPE',
    'width' => '10%',
  ),
  'ID_NUM_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_ID_NUM',
    'width' => '10%',
  ),
),
);
