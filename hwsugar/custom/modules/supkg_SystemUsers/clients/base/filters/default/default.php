<?php
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$viewdefs['supkg_SystemUsers']['base']['filter']['default'] = array(
 
    'default_filter' => 'agentonly',
    'fields' => array(
        'last_name' => array(),
        'first_name' => array(),
        'id_num_c' => array(),
        'usertype_c' => array(),
    ),
    
    'quicksearch_field' => 
    array (
    0 => 'last_name',
    1 => 'first_name',
    2 => 'id_num_c',
    3 => 'usertype_c',
    ), 
    'quicksearch_priority' => 2,
);
?>