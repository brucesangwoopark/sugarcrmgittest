<?php
$module_name = "supkg_SystemUsers";
$viewdefs[$module_name]['base']['filter']['agentonly'] = array(
    'create' => false,
    'filters' => array(
        array(
            'id' => 'agentonly',
            'name' => 'LBL_AGENT_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'usertype_c' => 'Agent',
                    'is_active_c' => 'Y',
                ),
            ),
            'editable' => false,
        ),
    ),
);
?>
