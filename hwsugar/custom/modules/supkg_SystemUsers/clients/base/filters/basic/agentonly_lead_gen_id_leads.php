<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-09-01
 * Time: 오후 4:08
 */

$viewdefs['supkg_SystemUsers']['base']['filter']['basic']['filters'][] = array(
    'id' => 'agentonly_lead_gen_id_leads',
    'name' => 'LBL_AGENT_ONLY_FILTER',
    'filter_definition' => array(
        array(
            'usertype_c' => 'Agent',
            'is_active_c' => 'Y',
        ),
    ),
    'editable' => false,
    'is_template' => true,
);