<?php
$module_name = "supkg_SystemUsers";
$viewdefs[$module_name]['base']['filter']['techonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'techonly',
            'name'              => 'LBL_TECH_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    'usertype_c' => 'Tech',
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>