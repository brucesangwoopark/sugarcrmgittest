<?php
$module_name = 'supkg_SystemUsers';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'supkg_SystemUsers',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
              10 => 
              array (
                'type' => 'vcard',
                'name' => 'vcard_button',
                'label' => 'LBL_VCARD_DOWNLOAD',
                'acl_action' => 'edit',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
              ),
              1 => 
              array (
                'name' => 'full_name',
                'label' => 'LBL_NAME',
                'dismiss_label' => true,
                'type' => 'fullname',
                'fields' => 
                array (
                  0 => 'salutation',
                  1 => 'first_name',
                  2 => 'last_name',
                ),
              ),
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'usertype_c',
                'label' => 'LBL_USERTYPE',
              ),
              1 => 
              array (
                'name' => 'id_num_c',
                'label' => 'LBL_ID_NUM',
              ),
              2 => 
              array (
                'name' => 'agent_category_c',
                'label' => 'LBL_AGENT_CATEGORY',
                'span' => 12,
              ),
              3 => 
              array (
                'name' => 'tech_category_c',
                'label' => 'LBL_TECH_CATEGORY',
              ),
              4 => 
              array (
              ),
              5 => 
              array (
                'name' => 'office_c',
                'label' => 'LBL_OFFICE',
              ),
              6 => 
              array (
                'name' => 'external_key_c',
                'label' => 'LBL_EXTERNAL_KEY',
              ),
            ),
          ),
          2 => 
          array (
            'columns' => 2,
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'phone_home',
              ),
              1 => 
              array (
                'name' => 'phone_mobile',
                'comment' => 'Mobile phone number of the contact',
                'label' => 'LBL_MOBILE_PHONE',
              ),
              2 => 'phone_work',
              3 => 'phone_other',
              4 => 'phone_fax',
              5 => 'email',
              6 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              7 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              8 => 
              array (
                'name' => 'do_not_call',
                'comment' => 'An indicator of whether contact can be called',
                'label' => 'LBL_DO_NOT_CALL',
              ),
              9 => 
              array (
              ),
            ),
          ),
          3 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL1',
            'label' => 'LBL_RECORDVIEW_PANEL1',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'primary_address_street',
                'comment' => 'The street address used for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_STREET',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'primary_address_postalcode',
                'comment' => 'Postal code for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
              ),
              2 => 
              array (
                'name' => 'primary_address_city',
                'comment' => 'City for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_CITY',
              ),
              3 => 
              array (
                'name' => 'primary_address_state',
                'comment' => 'State for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_STATE',
              ),
              4 => 
              array (
                'name' => 'primary_address_country',
                'comment' => 'Country for primary address',
                'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
              ),
            ),
          ),
          4 => 
          array (
            'newTab' => false,
            'panelDefault' => 'expanded',
            'name' => 'LBL_RECORDVIEW_PANEL2',
            'label' => 'LBL_RECORDVIEW_PANEL2',
            'columns' => 2,
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'alt_address_street',
                'comment' => 'Street address for alternate address',
                'label' => 'LBL_ALT_ADDRESS_STREET',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'alt_address_postalcode',
                'comment' => 'Postal code for alternate address',
                'label' => 'LBL_ALT_ADDRESS_POSTALCODE',
              ),
              2 => 
              array (
                'name' => 'alt_address_city',
                'comment' => 'City for alternate address',
                'label' => 'LBL_ALT_ADDRESS_CITY',
              ),
              3 => 
              array (
                'name' => 'alt_address_state',
                'comment' => 'State for alternate address',
                'label' => 'LBL_ALT_ADDRESS_STATE',
              ),
              4 => 
              array (
                'name' => 'alt_address_country',
                'comment' => 'Country for alternate address',
                'label' => 'LBL_ALT_ADDRESS_COUNTRY',
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
