<?php
// created: 2016-09-22 21:38:14
$viewdefs['Products']['base']['view']['subpanel-for-leads-products_leads_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => 'true',
        ),
        1 => 
        array (
          'label' => 'LBL_LIST_STATUS',
          'enabled' => true,
          'default' => true,
          'name' => 'status',
        ),
        2 => 
        array (
          'target_record_key' => 'account_id',
          'target_module' => 'Accounts',
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'account_name',
        ),
        3 => 
        array (
          'target_record_key' => 'contact_id',
          'target_module' => 'Contacts',
          'label' => 'LBL_LIST_CONTACT_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'contact_name',
        ),
        4 => 
        array (
          'label' => 'LBL_LIST_DATE_PURCHASED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_purchased',
        ),
        5 => 
        array (
          'label' => 'LBL_LIST_DISCOUNT_PRICE',
          'enabled' => true,
          'default' => true,
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'name' => 'discount_price',
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
        ),
        6 => 
        array (
          'label' => 'LBL_LIST_SUPPORT_EXPIRES',
          'enabled' => true,
          'default' => true,
          'name' => 'date_support_expires',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);