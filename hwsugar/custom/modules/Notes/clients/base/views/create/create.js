({
    extendsFrom: 'CreateView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.on('render', this.pullAccount, this);
    },

    pullAccount: function () {
        try {
            var account = this.model.link.bean.get('cases_tickets_accounts');
            if (account === undefined) {
                console.log('this is not note creation from Ticket module.');
                return;
            }

            this.model.set('parent_type', 'Accounts');
            this.model.set('parent_id', account.id);
            this.model.set('parent_name', account.name);
        } catch(err) {
            console.log('this is not note creation from Ticket module.');
        }
    }
})