<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveNotesHook {
	
	static $already_ran = false;//uncomment if you wish the hook to ONLY execute once
	
	function afterSave($bean, $event, $arguments)
	{
		$id = $bean->id;
		//for logging purposes
		$module = "Notes";
		$hook = "after_save";
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Job id: '.$id.', '.date("Y-m-d H:i:s"));
		
		try{
			/*
			$team_id = 1; //setting default
			$query = 'SELECT a.team_id as team_id
					FROM accounts a
					WHERE a.id = "'.$bean->parent_id.'"';
			$result = $GLOBALS['db']->query($query);
			while($row = $GLOBALS['db']->fetchByAssoc($result)){
				$team_id = $row['team_id'];
			}
			
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding team: '.$team_id.', '.date("Y-m-d H:i:s"));
			$query = 'UPDATE notes
				SET team_id = "'.$team_id.'", team_set_id = "'.$team_id.'"
				WHERE id = "'.$id.'"';
			$GLOBALS['db']->query($query);
			*/
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Adding team: '.$bean->team_id.', '.date("Y-m-d H:i:s"));
			$bean->load_relationship('teams');
			$bean->teams->replace(array($bean->team_id));

			if (isset($bean->cases_tickets_notescases_tickets_ida)) {
                require_once 'custom/modules/cases_Tickets/SendMail.php';
                $sendMail = new SendMail();
                $ticket_bean = BeanFactory::getBean('cases_Tickets', $bean->cases_tickets_notescases_tickets_ida);
                $sendMail->send($ticket_bean, $bean->description);
			}
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED, '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
   }
}
?>
