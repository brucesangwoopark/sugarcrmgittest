<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class BeforeSaveNotesHook {
	
	function beforeSave($bean, $event, $arguments)
	{
		$id = $bean->id;
		//for logging purposes
		$module = "Notes";
		$hook = "before_save";
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Job id: '.$id.', '.date("Y-m-d H:i:s"));
		
		try{
			
			//check the team
			if ($bean->team_id == "1"){
				$team_id = 1; //setting default
				$query = 'SELECT a.team_id as team_id
						FROM accounts a
						WHERE a.id = "'.$bean->parent_id.'"';
				$result = $GLOBALS['db']->query($query);
				while($row = $GLOBALS['db']->fetchByAssoc($result)){
					$team_id = $row['team_id'];
				}
				$bean->team_id = $team_id;
			}
			
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED, '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));
    }
}
?>
