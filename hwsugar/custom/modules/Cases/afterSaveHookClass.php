<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
 
class AfterSaveCasesHook {
		
	function afterSave($bean, $event, $arguments)
	{
		$id = $bean->id;
		//for logging purposes
		$module = "Cases";
		$hook = "after_save";
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: BEGINNING HOOK, Case id: '.$id.', '.date("Y-m-d H:i:s"));
				
		try{
			//if the type is "Call-Back", then set the default date to tomorrow
			if($bean->type = "Call Back" && empty($bean->call_back_date_c)){
				//set call back day to whatever day is 20 hours ahead (server is already 4 hours ahead)
				$call_back_date = date("Y-m-d", strtotime('+20 hour'));
				$GLOBALS['log']->fatal('['.$module.']['.$hook.']: Setting call-back date to: "'.$call_back_date.'", ,'.date("Y-m-d H:i:s"));
				$query = 'UPDATE cases_cstm
						SET call_back_date_c = "'.$call_back_date.'"
						WHERE id_c = "'.$id.'"';
				$GLOBALS['db']->query($query);
			}
			
		}//END OF try
		catch(Exception $e2){
			$GLOBALS['log']->fatal('['.$module.']['.$hook.']: FAILED '.date("Y-m-d H:i:s"));
		}
		$GLOBALS['log']->fatal('['.$module.']['.$hook.']: ENDING HOOK, '.date("Y-m-d H:i:s"));//uncomment if you wish the hook to ONLY execute once
    }
}
?>