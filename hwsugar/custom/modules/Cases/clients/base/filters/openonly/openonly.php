<?php
$module_name = "Cases";
$viewdefs[$module_name]['base']['filter']['openonly'] = array(
    'create'               => false,
    'filters'              => array(
        array(
            'id'                => 'openonly',
            'name'              => 'LBL_OPEN_ONLY_FILTER',
            'filter_definition' => array(
                array(
                    '$or' => array(
                      array('status' => 'New'),
                      array('status' => 'Assigned'),
                      array('status' => 'Pending Input'),
                      array('status' => 'Rejected'),
                      array('status' => 'Duplicate'),  
                    ),
                ),
            ),
            'editable'          => false,
        ),
    ),
);
?>