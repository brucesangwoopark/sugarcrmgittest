<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$relationships = array (
  'opportunities_abcde_jobs_1' => 
  array (
    'name' => 'opportunities_abcde_jobs_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'opportunities_abcde_jobs_1' => 
      array (
        'lhs_module' => 'Opportunities',
        'lhs_table' => 'opportunities',
        'lhs_key' => 'id',
        'rhs_module' => 'abcde_Jobs',
        'rhs_table' => 'abcde_jobs',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'opportunities_abcde_jobs_1_c',
        'join_key_lhs' => 'opportunities_abcde_jobs_1opportunities_ida',
        'join_key_rhs' => 'opportunities_abcde_jobs_1abcde_jobs_idb',
      ),
    ),
    'table' => 'opportunities_abcde_jobs_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'opportunities_abcde_jobs_1opportunities_ida' => 
      array (
        'name' => 'opportunities_abcde_jobs_1opportunities_ida',
        'type' => 'id',
      ),
      'opportunities_abcde_jobs_1abcde_jobs_idb' => 
      array (
        'name' => 'opportunities_abcde_jobs_1abcde_jobs_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'opportunities_abcde_jobs_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'opportunities_abcde_jobs_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'opportunities_abcde_jobs_1opportunities_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'opportunities_abcde_jobs_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'opportunities_abcde_jobs_1abcde_jobs_idb',
        ),
      ),
    ),
    'lhs_module' => 'Opportunities',
    'lhs_table' => 'opportunities',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'opportunities_abcde_jobs_1_c',
    'join_key_lhs' => 'opportunities_abcde_jobs_1opportunities_ida',
    'join_key_rhs' => 'opportunities_abcde_jobs_1abcde_jobs_idb',
    'readonly' => true,
    'relationship_name' => 'opportunities_abcde_jobs_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'accounts_abcde_jobs_1' => 
  array (
    'name' => 'accounts_abcde_jobs_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'accounts_abcde_jobs_1' => 
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'abcde_Jobs',
        'rhs_table' => 'abcde_jobs',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_abcde_jobs_1_c',
        'join_key_lhs' => 'accounts_abcde_jobs_1accounts_ida',
        'join_key_rhs' => 'accounts_abcde_jobs_1abcde_jobs_idb',
      ),
    ),
    'table' => 'accounts_abcde_jobs_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'accounts_abcde_jobs_1accounts_ida' => 
      array (
        'name' => 'accounts_abcde_jobs_1accounts_ida',
        'type' => 'id',
      ),
      'accounts_abcde_jobs_1abcde_jobs_idb' => 
      array (
        'name' => 'accounts_abcde_jobs_1abcde_jobs_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'accounts_abcde_jobs_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'accounts_abcde_jobs_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'accounts_abcde_jobs_1accounts_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'accounts_abcde_jobs_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'accounts_abcde_jobs_1abcde_jobs_idb',
        ),
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'accounts_abcde_jobs_1_c',
    'join_key_lhs' => 'accounts_abcde_jobs_1accounts_ida',
    'join_key_rhs' => 'accounts_abcde_jobs_1abcde_jobs_idb',
    'readonly' => true,
    'relationship_name' => 'accounts_abcde_jobs_1',
    'rhs_subpanel' => 'ForAccountsAccounts_abcde_jobs_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'abcde_jobs_notes_1' => 
  array (
    'name' => 'abcde_jobs_notes_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'abcde_jobs_notes_1' => 
      array (
        'lhs_module' => 'abcde_Jobs',
        'lhs_table' => 'abcde_jobs',
        'lhs_key' => 'id',
        'rhs_module' => 'Notes',
        'rhs_table' => 'notes',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'abcde_jobs_notes_1_c',
        'join_key_lhs' => 'abcde_jobs_notes_1abcde_jobs_ida',
        'join_key_rhs' => 'abcde_jobs_notes_1notes_idb',
      ),
    ),
    'table' => 'abcde_jobs_notes_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'abcde_jobs_notes_1abcde_jobs_ida' => 
      array (
        'name' => 'abcde_jobs_notes_1abcde_jobs_ida',
        'type' => 'id',
      ),
      'abcde_jobs_notes_1notes_idb' => 
      array (
        'name' => 'abcde_jobs_notes_1notes_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'abcde_jobs_notes_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'abcde_jobs_notes_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'abcde_jobs_notes_1abcde_jobs_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'abcde_jobs_notes_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'abcde_jobs_notes_1notes_idb',
        ),
      ),
    ),
    'lhs_module' => 'abcde_Jobs',
    'lhs_table' => 'abcde_jobs',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'abcde_jobs_notes_1_c',
    'join_key_lhs' => 'abcde_jobs_notes_1abcde_jobs_ida',
    'join_key_rhs' => 'abcde_jobs_notes_1notes_idb',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_notes_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'supkg_systemusers_abcde_jobs_1' => 
  array (
    'name' => 'supkg_systemusers_abcde_jobs_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'supkg_systemusers_abcde_jobs_1' => 
      array (
        'lhs_module' => 'supkg_SystemUsers',
        'lhs_table' => 'supkg_systemusers',
        'lhs_key' => 'id',
        'rhs_module' => 'abcde_Jobs',
        'rhs_table' => 'abcde_jobs',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'supkg_systemusers_abcde_jobs_1_c',
        'join_key_lhs' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
        'join_key_rhs' => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
      ),
    ),
    'table' => 'supkg_systemusers_abcde_jobs_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida' => 
      array (
        'name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
        'type' => 'id',
      ),
      'supkg_systemusers_abcde_jobs_1abcde_jobs_idb' => 
      array (
        'name' => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'supkg_systemusers_abcde_jobs_1spk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'supkg_systemusers_abcde_jobs_1_ida1',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
        ),
      ),
      2 => 
      array (
        'name' => 'supkg_systemusers_abcde_jobs_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
        ),
      ),
    ),
    'lhs_module' => 'supkg_SystemUsers',
    'lhs_table' => 'supkg_systemusers',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'supkg_systemusers_abcde_jobs_1_c',
    'join_key_lhs' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
    'join_key_rhs' => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
    'readonly' => true,
    'relationship_name' => 'supkg_systemusers_abcde_jobs_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'abcde_jobs_modified_user' => 
  array (
    'name' => 'abcde_jobs_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_created_by' => 
  array (
    'name' => 'abcde_jobs_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_activities' => 
  array (
    'name' => 'abcde_jobs_activities',
    'lhs_module' => 'abcde_Jobs',
    'lhs_table' => 'abcde_jobs',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'abcde_Jobs',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_following' => 
  array (
    'name' => 'abcde_jobs_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'abcde_Jobs',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_favorite' => 
  array (
    'name' => 'abcde_jobs_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'sugarfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'abcde_Jobs',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_assigned_user' => 
  array (
    'name' => 'abcde_jobs_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'abcde_Jobs',
    'rhs_table' => 'abcde_jobs',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'abcde_jobs_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'abcde_jobs_revenuelineitems_1' => 
  array (
    'rhs_label' => 'Revenue Line Items',
    'lhs_label' => 'Jobs',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'abcde_Jobs',
    'rhs_module' => 'RevenueLineItems',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'abcde_jobs_revenuelineitems_1',
  ),
);