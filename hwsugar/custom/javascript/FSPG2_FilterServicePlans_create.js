(function (app) {
    app.events.on("router:init", function () {
        app.router.route("FSPG2_FilterServicePlans/create", "FSPG2_FilterServicePlans_create", function () {
            window.open("/hwsugar/ext_popup/FSPG2_FilterServicePlans/create.html", "Create Filter Service Plan", "width=800,height=600");
            window.history.back();
        });
    });
})(SUGAR.App);