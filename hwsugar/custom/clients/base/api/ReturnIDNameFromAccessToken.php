<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-07-26
 * Time: 오후 4:10
 */

class ReturnIDNameFromAccessToken extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'retIDNameFromAccessToken' => array(
                'reqType' => 'POST',
                'path' => array('getuseridname'),
                'pathVars' => array('', ''),
                'method' => 'retIDNameFromAccessToken',
                'shortHelp' => 'Provide given Access token owners ID and Full name',
                'longHelp' => '',
            )
        );
    }

    function retIDNameFromAccessToken($api, $args)
    {
        $user_id = $_SESSION['user_id'];
        $user_bean = BeanFactory::getBean('Users', $user_id);

        return array(
            'result' => 'ok',
            'id' => $user_bean->user_name,
            'name' => $user_bean->name,
        );
    }
}