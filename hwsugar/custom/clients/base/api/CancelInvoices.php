<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-04
 * Time: 오후 5:18
 */

class CancelInvoices
{
    function run()
    {
        $targets = array(
//            array('ext_key' => '10730', 'last_pay_date' => '2017-04-26'),
//            array('ext_key' => '9819', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '7107', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '11554', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '43125', 'last_pay_date' => null),
//            array('ext_key' => '8710', 'last_pay_date' => '2017-03-06'),
//            array('ext_key' => '3922', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '3919', 'last_pay_date' => '2017-03-28'),
//            array('ext_key' => '5211', 'last_pay_date' => '2017-06-09'),
//            //array('ext_key' => '5219', 'last_pay_date' => '2017-10-11'),
//            array('ext_key' => '9739', 'last_pay_date' => '2017-03-07'),
//            array('ext_key' => '9735', 'last_pay_date' => '2017-01-20'),
//            array('ext_key' => '10637', 'last_pay_date' => '2017-02-15'),
//            array('ext_key' => '9680', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '12901', 'last_pay_date' => '2017-02-10'),
//            array('ext_key' => '11501', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '5619', 'last_pay_date' => '2017-03-06'),
//            array('ext_key' => '14610', 'last_pay_date' => '2017-02-08'),
//            array('ext_key' => '10576', 'last_pay_date' => '2017-02-22'),
//            array('ext_key' => '10932', 'last_pay_date' => '2017-02-02'),
//            array('ext_key' => '13502', 'last_pay_date' => '2017-02-06'),
//            array('ext_key' => '10071', 'last_pay_date' => '2017-02-02'),
//            array('ext_key' => '9860', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '8569', 'last_pay_date' => '2017-02-07'),
//            array('ext_key' => '6305', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '9365', 'last_pay_date' => '2017-02-22'),
//            array('ext_key' => '7085', 'last_pay_date' => '2017-02-17'),
//            array('ext_key' => '4752', 'last_pay_date' => '2017-01-09'),
//            array('ext_key' => '4199', 'last_pay_date' => '2017-02-13'),
//            //array('ext_key' => '3051', 'last_pay_date' => '2017-01-30'),
//            array('ext_key' => '4195', 'last_pay_date' => '2017-02-10'),
//            array('ext_key' => '4654', 'last_pay_date' => '2017-02-07'),
//            array('ext_key' => '2818', 'last_pay_date' => '2017-02-10'),
//            array('ext_key' => '9355', 'last_pay_date' => '2017-02-03'),
//            array('ext_key' => '10409', 'last_pay_date' => '2017-03-01'),
//            array('ext_key' => '4543', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '7076', 'last_pay_date' => '2017-03-22'),
//            array('ext_key' => '8977', 'last_pay_date' => '2017-01-24'),
//            array('ext_key' => '8974', 'last_pay_date' => '2017-01-24'),
//            array('ext_key' => '7952', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '7068', 'last_pay_date' => '2017-02-14'),
//            array('ext_key' => '5559', 'last_pay_date' => '2017-03-10'),
//            array('ext_key' => '10396', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '7809', 'last_pay_date' => '2017-02-07'),
//            array('ext_key' => '7753', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '6081', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '8907', 'last_pay_date' => '2017-02-08'),
//            array('ext_key' => '6919', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '5551', 'last_pay_date' => '2017-02-08'),
//            array('ext_key' => '6073', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '8764', 'last_pay_date' => '2017-02-08'),
//            array('ext_key' => '8759', 'last_pay_date' => '2017-03-07'),
//            array('ext_key' => '8804', 'last_pay_date' => '2017-02-13'),
//            array('ext_key' => '5443', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '5496', 'last_pay_date' => '2017-02-17'),
//            array('ext_key' => '4352', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '5052', 'last_pay_date' => '2017-02-03'),
//            array('ext_key' => '5491', 'last_pay_date' => '2017-03-17'),
//            array('ext_key' => '6353', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '5047', 'last_pay_date' => '2017-02-28'),
//            array('ext_key' => '5041', 'last_pay_date' => '2017-03-24'),
//            array('ext_key' => '2959', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '6678', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '4872', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '3147', 'last_pay_date' => '2017-02-17'),
//            array('ext_key' => '3318', 'last_pay_date' => '2017-05-31'),
//            array('ext_key' => '4857', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '4780', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '3467', 'last_pay_date' => '2017-03-24'),
//            array('ext_key' => '1071', 'last_pay_date' => '2017-03-10'),
//            array('ext_key' => '1592', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '6406', 'last_pay_date' => '2017-03-06'),
//            array('ext_key' => '5465', 'last_pay_date' => '2017-02-08'),
//            array('ext_key' => '3145', 'last_pay_date' => '2017-01-20'),
//            array('ext_key' => '5013', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '4674', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '2109', 'last_pay_date' => '2017-02-21'),
//            array('ext_key' => '1619', 'last_pay_date' => '2017-02-02'),
//            array('ext_key' => '4272', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '1722', 'last_pay_date' => '2017-03-20'),
//            array('ext_key' => '2253', 'last_pay_date' => '2017-02-28'),
//            array('ext_key' => '3470', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '485', 'last_pay_date' => '2017-03-01'),
//            array('ext_key' => '369', 'last_pay_date' => '2017-03-06'),
//            array('ext_key' => '893', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '9094', 'last_pay_date' => '2017-03-06'),
//            array('ext_key' => '8478', 'last_pay_date' => '2017-02-28'),
//            array('ext_key' => '6835', 'last_pay_date' => '2017-03-03'),
//            array('ext_key' => '9407', 'last_pay_date' => '2017-02-15'),
//            array('ext_key' => '9141', 'last_pay_date' => '2017-04-11'),
//            array('ext_key' => '9143', 'last_pay_date' => '2017-06-01'),
//            array('ext_key' => '9567', 'last_pay_date' => '2017-02-02'),
//            array('ext_key' => '9571', 'last_pay_date' => '2017-03-03'),
//            array('ext_key' => '7505', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '12738', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '12098', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '12815', 'last_pay_date' => '2017-03-09'),
//            array('ext_key' => '14278', 'last_pay_date' => '2017-02-01'),
//            array('ext_key' => '11887', 'last_pay_date' => '2017-05-31'),
//            array('ext_key' => '11829', 'last_pay_date' => '2017-04-10'),
//            array('ext_key' => '13848', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '16985', 'last_pay_date' => '2017-03-01'),
//            array('ext_key' => '15946', 'last_pay_date' => '2017-02-16'),
//            array('ext_key' => '20693', 'last_pay_date' => '2017-03-21'),
//            array('ext_key' => '17516', 'last_pay_date' => '2017-02-16'),
//            array('ext_key' => '17517', 'last_pay_date' => '2017-04-13'),
//            array('ext_key' => '19602', 'last_pay_date' => '2017-01-12'),
//            array('ext_key' => '18748', 'last_pay_date' => '2017-02-07'),
//            array('ext_key' => '21873', 'last_pay_date' => '2017-03-28'),
//            array('ext_key' => '23503', 'last_pay_date' => '2017-03-03'),
//            array('ext_key' => '23575', 'last_pay_date' => '2017-03-17'),
//            array('ext_key' => '23719', 'last_pay_date' => '2017-03-24'),
//            array('ext_key' => '23784', 'last_pay_date' => '2017-02-07'),
//            array('ext_key' => '21826', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '22712', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '23205', 'last_pay_date' => '2017-02-21'),
//            array('ext_key' => '26306', 'last_pay_date' => '2017-01-20'),
//            array('ext_key' => '26313', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '30094', 'last_pay_date' => '2017-03-21'),
//            array('ext_key' => '30097', 'last_pay_date' => '2017-09-07'),
//            array('ext_key' => '32919', 'last_pay_date' => '2017-03-08'),
//            array('ext_key' => '29097', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '26001', 'last_pay_date' => '2017-03-28'),
//            array('ext_key' => '26255', 'last_pay_date' => '2017-03-22'),
//            array('ext_key' => '37054', 'last_pay_date' => '2017-02-06'),
//            array('ext_key' => '36251', 'last_pay_date' => '2017-02-21'),
//            array('ext_key' => '28479', 'last_pay_date' => '2017-02-16'),
//            array('ext_key' => '27829', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '25811', 'last_pay_date' => '2017-07-13'),
//            array('ext_key' => '31840', 'last_pay_date' => '2017-03-03'),
//            array('ext_key' => '31549', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '30398', 'last_pay_date' => '2017-03-01'),
//            array('ext_key' => '28074', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '32799', 'last_pay_date' => '2017-03-10'),
//            array('ext_key' => '33043', 'last_pay_date' => '2017-01-12'),
//            array('ext_key' => '39047', 'last_pay_date' => '2017-03-01'),
//            array('ext_key' => '37143', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '31128', 'last_pay_date' => '2017-02-21'),
//            array('ext_key' => '34957', 'last_pay_date' => '2017-03-07'),
//            array('ext_key' => '33989', 'last_pay_date' => '2017-02-17'),
//            array('ext_key' => '32522', 'last_pay_date' => '2017-03-02'),
//            array('ext_key' => '35397', 'last_pay_date' => '2017-03-16'),
//            array('ext_key' => '35939', 'last_pay_date' => '2017-06-12'),
//            array('ext_key' => '35907', 'last_pay_date' => '2017-02-16'),
//            array('ext_key' => '35374', 'last_pay_date' => '2017-03-15'),
//            array('ext_key' => '35450', 'last_pay_date' => '2017-05-30'),
//            array('ext_key' => '36779', 'last_pay_date' => '2017-01-24'),
//            array('ext_key' => '38248', 'last_pay_date' => '2017-02-09'),
//            array('ext_key' => '38123', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '39073', 'last_pay_date' => '2017-03-20'),
//            //array('ext_key' => '38296', 'last_pay_date' => '2017-09-27'),
//            array('ext_key' => '39883', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '36379', 'last_pay_date' => '2017-03-10'),
//            array('ext_key' => '36538', 'last_pay_date' => '2017-02-17'),
//            array('ext_key' => '36361', 'last_pay_date' => '2017-02-27'),
//            array('ext_key' => '35442', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '40312', 'last_pay_date' => '2017-01-27'),
//            array('ext_key' => '38351', 'last_pay_date' => '2017-06-02'),
//            array('ext_key' => '38943', 'last_pay_date' => '2017-02-23'),
//            array('ext_key' => '41546', 'last_pay_date' => '2017-02-10'),
//            array('ext_key' => '41396', 'last_pay_date' => '2017-03-03'),
//            array('ext_key' => '41348', 'last_pay_date' => '2017-02-16'),
//            array('ext_key' => '42473', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '41363', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '41869', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '42622', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '43248', 'last_pay_date' => '2017-02-01'),
//            array('ext_key' => '43253', 'last_pay_date' => '2016-12-31'),
//            array('ext_key' => '43222', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '43232', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '42089', 'last_pay_date' => '2017-02-21'),
//            array('ext_key' => '42544', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '43435', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '43409', 'last_pay_date' => '2017-05-21'),
//            array('ext_key' => '43184', 'last_pay_date' => '2017-05-17'),
//            array('ext_key' => '43546', 'last_pay_date' => '2017-05-17'),
//            array('ext_key' => '43664', 'last_pay_date' => '2017-08-03'),
//            array('ext_key' => '49503', 'last_pay_date' => '2017-08-24'),
//            //array('ext_key' => '46654', 'last_pay_date' => '2017-09-17'),
//            //array('ext_key' => '50213', 'last_pay_date' => '2017-09-24'),
//
//            array('ext_key' => '43932', 'last_pay_date' => null),
//            array('ext_key' => '43412', 'last_pay_date' => null),
//            array('ext_key' => '43938', 'last_pay_date' => null),
//            array('ext_key' => '43431', 'last_pay_date' => null),
//            array('ext_key' => '43473', 'last_pay_date' => null),

            array('ext_key' => '9817', 'last_pay_date' => '2016-12-21'),
            array('ext_key' => '5796', 'last_pay_date' => '2017-02-08'),
            array('ext_key' => '5217', 'last_pay_date' => '2016-12-21'),
            array('ext_key' => '5938', 'last_pay_date' => '2017-01-16'),
            array('ext_key' => '10987', 'last_pay_date' => '2017-01-17'),
            array('ext_key' => '10005', 'last_pay_date' => '2017-01-10'),
            array('ext_key' => '6578', 'last_pay_date' => '2017-01-06'),
            array('ext_key' => '18587', 'last_pay_date' => '2017-01-18'),
            array('ext_key' => '10565', 'last_pay_date' => '2017-01-16'),
            array('ext_key' => '42054', 'last_pay_date' => '2016-12-27'),
            array('ext_key' => '6250', 'last_pay_date' => '2017-01-26'),
            array('ext_key' => '4190', 'last_pay_date' => '2017-02-03'),
            array('ext_key' => '6591', 'last_pay_date' => '2017-01-16'),
            array('ext_key' => '4281', 'last_pay_date' => '2017-01-20'),
            array('ext_key' => '6436', 'last_pay_date' => '2017-02-04'),
            array('ext_key' => '4708', 'last_pay_date' => '2017-01-05'),
            array('ext_key' => '2871', 'last_pay_date' => '2017-01-16'),
            array('ext_key' => '4367', 'last_pay_date' => '2017-02-07'),
            array('ext_key' => '393', 'last_pay_date' => '2017-01-10'),
            array('ext_key' => '29258', 'last_pay_date' => '2017-01-05'),
            array('ext_key' => '20977', 'last_pay_date' => '2017-02-05'),
            array('ext_key' => '20976', 'last_pay_date' => '2017-02-05'),
            array('ext_key' => '21745', 'last_pay_date' => '2016-12-27'),
            array('ext_key' => '21744', 'last_pay_date' => '2016-12-27'),
            array('ext_key' => '27174', 'last_pay_date' => '2017-02-03'),
            array('ext_key' => '23790', 'last_pay_date' => '2017-02-03'),
            array('ext_key' => '29261', 'last_pay_date' => '2017-01-27'),
            array('ext_key' => '25130', 'last_pay_date' => '2017-01-27'),
            array('ext_key' => '29867', 'last_pay_date' => '2017-01-09'),
            array('ext_key' => '26758', 'last_pay_date' => '2016-12-29'),
            array('ext_key' => '24757', 'last_pay_date' => '2016-12-29'),
            array('ext_key' => '44172', 'last_pay_date' => '2017-01-13'),
            array('ext_key' => '44129', 'last_pay_date' => '2017-01-13'),
            array('ext_key' => '33962', 'last_pay_date' => '2017-01-25'),
            array('ext_key' => '31299', 'last_pay_date' => '2017-01-25'),
            array('ext_key' => '39194', 'last_pay_date' => '2017-01-09'),
            array('ext_key' => '39193', 'last_pay_date' => '2017-01-09'),
        );

        $co_api = $this->createCoApi('canada');
        foreach ($targets as $target) {
            $condition = array();
            $condition[] = 'package_id:EQUALS:' . $target['ext_key'];
            if ($target['last_pay_date'] !== null) {
                $condition[] = 'date:GT:' . $target['last_pay_date'];
            }

            $response = $co_api->find(
                ChargeOverAPI_Object::TYPE_INVOICE,
                $condition
            );
            if (200 <= $response->code && $response->code < 300) {
                $invoices = $response->response;
                $GLOBALS['log']->fatal("ext_key => {$target['ext_key']}, last_pay_date => {$target['last_pay_date']}");
                $customer_id = '';
                $note = '';
                foreach ($invoices as $invoice) {
                    $customer_id = $invoice->customer_id;

                    $void_response = $co_api->action(ChargeOverAPI_Object::TYPE_INVOICE, $invoice->invoice_id, 'void');
                    if (200 <= $void_response->code && $void_response->code < 300) {
                        $GLOBALS['log']->fatal("invoice_id => {$invoice->invoice_id} - Void");
                        $note .= "- Invoice #{$invoice->invoice_id}\n";
                    } else {
                        $GLOBALS['log']->fatal("invoice_id => {$invoice->invoice_id} - Void error");
                    }
                }

                $note = "[Auto] Invoices became void\n" . ($note === '' ? 'None' : $note);
                $this->createCoNote($co_api, $customer_id, $note);

                $cancel_response = $co_api->action(ChargeOverAPI_Object::TYPE_PACKAGE, $target['ext_key'], 'cancel');
                if (200 <= $cancel_response->code && $cancel_response->code < 300) {
                    $note = "[Auto] Subscription #{$target['ext_key']} cancelled";
                    $this->createCoNote($co_api, $customer_id, $note);
                } else {
                    $GLOBALS['log']->fatal('Pakcage cancel error');
                }
            }
        }
    }

    function createCoApi($country_code)
    {
        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$country_code]['username'],
            $sugar_config['chargeover'][$country_code]['password']
        );

        return $api;
    }

    function createCoNote($co_api, $customer_id, $note_content)
    {
        $note = new ChargeOverAPI_Object_Note(array(
            'note' => $note_content,
            'obj_type' => ChargeOverAPI_Object::TYPE_CUSTOMER,
            'obj_id' => $customer_id
        ));

        $response = $co_api->create($note);
        if (200 <= $response->code && $response->code < 300) {
            $GLOBALS['log']->fatal('Note created');
        } else {
            $GLOBALS['log']->fatal('Note created error');
        }
    }
}