<?php
/**
 * Created by PhpStorm.
 * User: bpark
 * Date: 7/20/2017
 * Time: 11:56 AM
 */

class ValidateOAuthToken extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'returnTestString' => array(
                'reqType' => 'POST',
                'path' => array('validatetoken'),
                'pathVars' => array('', ''),
                'method' => 'validateToken',
                'shortHelp' => 'Validate current oauth-token',
                'longHelp' => '',
            )
        );
    }

    function validateToken($api, $args)
    {
        return array(
            'result' => 'ok'
        );
    }
}