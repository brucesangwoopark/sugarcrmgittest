<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-04
 * Time: 오후 5:20
 */

class RemoveCOCustomers
{
    function run()
    {
        $targets = array(
            '629',
            '630',
            '631',
            '633',
            '641',
            '646',
            '648',
            '649',
            '681',
            '690',
            '691',
            '693',
            '711',
            '737',
            '821',
            '1306',
            '647',
            '674',
            '675',
            '736',
            '759',
            '868',
        );

        $co_api = $this->createCoApi('us');
        foreach ($targets as $customer_id) {
            $response = $co_api->delete(ChargeOverAPI_Object::TYPE_CUSTOMER, $customer_id);
            if (200 <= $response->code && $response->code < 300) {
                $GLOBALS['log']->fatal("{$customer_id} deleted");
            } else {
                $GLOBALS['log']->fatal("{$customer_id} error => " . print_r($response, true));
            }
        }
    }

    function createCoApi($country_code)
    {
        require_once 'chargeover/ChargeOverAPI.php';
        global $sugar_config;
        $api = new ChargeOverAPI(
            $sugar_config['chargeover'][$country_code]['url'],
            ChargeOverAPI::AUTHMODE_HTTP_BASIC,
            $sugar_config['chargeover'][$country_code]['username'],
            $sugar_config['chargeover'][$country_code]['password']
        );

        return $api;
    }

    function createCoNote($co_api, $customer_id, $note_content)
    {
        $note = new ChargeOverAPI_Object_Note(array(
            'note' => $note_content,
            'obj_type' => ChargeOverAPI_Object::TYPE_CUSTOMER,
            'obj_id' => $customer_id
        ));

        $response = $co_api->create($note);
        if (200 <= $response->code && $response->code < 300) {
            $GLOBALS['log']->fatal('Note created');
        } else {
            $GLOBALS['log']->fatal('Note created error');
        }
    }
}