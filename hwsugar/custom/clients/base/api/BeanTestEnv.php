<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-08-22
 * Time: 오후 4:11
 */

class BeanTestEnv extends SugarApi
{
    public function registerApiRest()
    {
        return array(
            'beantest' => array(
                'reqType' => 'POST',
                'path' => array('beantest'),
                'pathVars' => array('', ''),
                'method' => 'beantestenva',
                'shortHelp' => 'beantestenva',
                'longHelp' => '',
                //'noLoginRequired' => true,
            )
        );
    }

    function beantestenva($api, $args)
    {
        require_once 'custom/clients/base/api/RemoveCOCustomers.php';

        $obj = new RemoveCOCustomers();
        $obj->run();
    }
}