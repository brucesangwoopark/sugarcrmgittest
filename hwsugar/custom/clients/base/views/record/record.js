({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.on('render', this.resetAjax, this);
    },

    resetAjax: function () {
        $(document).ajaxComplete(function (event, request, settings) {
            var reg_visibleuntil_modules = /rest\/v10\/(Accounts|Contacts|Leads|Opportunities|RevenueLineItems|abcde_Jobs)\/.*\?view=record/;
            if (reg_visibleuntil_modules.exec(settings.url) !== null && request.status === 602) {
                console.log(settings.url + ' ' + request.status);
                App.alert.show('over_visible_until_date_error', {
                    level: 'error',
                    title: 'Data not visible',
                    messages: 'This record is no longer visible due to expired Visible Until date.'
                });
            }
        });
    }
})