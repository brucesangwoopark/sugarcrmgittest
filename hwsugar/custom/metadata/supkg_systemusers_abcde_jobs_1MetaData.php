<?php
// created: 2016-11-07 20:33:18
$dictionary["supkg_systemusers_abcde_jobs_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'supkg_systemusers_abcde_jobs_1' => 
    array (
      'lhs_module' => 'supkg_SystemUsers',
      'lhs_table' => 'supkg_systemusers',
      'lhs_key' => 'id',
      'rhs_module' => 'abcde_Jobs',
      'rhs_table' => 'abcde_jobs',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'supkg_systemusers_abcde_jobs_1_c',
      'join_key_lhs' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
      'join_key_rhs' => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
    ),
  ),
  'table' => 'supkg_systemusers_abcde_jobs_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida' => 
    array (
      'name' => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
      'type' => 'id',
    ),
    'supkg_systemusers_abcde_jobs_1abcde_jobs_idb' => 
    array (
      'name' => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'supkg_systemusers_abcde_jobs_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'supkg_systemusers_abcde_jobs_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'supkg_systemusers_abcde_jobs_1supkg_systemusers_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'supkg_systemusers_abcde_jobs_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'supkg_systemusers_abcde_jobs_1abcde_jobs_idb',
      ),
    ),
  ),
);