<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_tickets_revenuelineitems"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cases_tickets_revenuelineitems' => 
    array (
      'lhs_module' => 'RevenueLineItems',
      'lhs_table' => 'revenue_line_items',
      'lhs_key' => 'id',
      'rhs_module' => 'cases_Tickets',
      'rhs_table' => 'cases_tickets',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_tickets_revenuelineitems_c',
      'join_key_lhs' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
      'join_key_rhs' => 'cases_tickets_revenuelineitemscases_tickets_idb',
    ),
  ),
  'table' => 'cases_tickets_revenuelineitems_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cases_tickets_revenuelineitemsrevenuelineitems_ida' => 
    array (
      'name' => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
      'type' => 'id',
    ),
    'cases_tickets_revenuelineitemscases_tickets_idb' => 
    array (
      'name' => 'cases_tickets_revenuelineitemscases_tickets_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cases_tickets_revenuelineitems_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cases_tickets_revenuelineitems_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_revenuelineitemsrevenuelineitems_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cases_tickets_revenuelineitems_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_revenuelineitemscases_tickets_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'cases_tickets_revenuelineitems_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_tickets_revenuelineitemscases_tickets_idb',
      ),
    ),
  ),
);