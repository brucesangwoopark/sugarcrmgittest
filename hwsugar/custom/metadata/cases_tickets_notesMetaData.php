<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_tickets_notes"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cases_tickets_notes' => 
    array (
      'lhs_module' => 'cases_Tickets',
      'lhs_table' => 'cases_tickets',
      'lhs_key' => 'id',
      'rhs_module' => 'Notes',
      'rhs_table' => 'notes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_tickets_notes_c',
      'join_key_lhs' => 'cases_tickets_notescases_tickets_ida',
      'join_key_rhs' => 'cases_tickets_notesnotes_idb',
    ),
  ),
  'table' => 'cases_tickets_notes_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cases_tickets_notescases_tickets_ida' => 
    array (
      'name' => 'cases_tickets_notescases_tickets_ida',
      'type' => 'id',
    ),
    'cases_tickets_notesnotes_idb' => 
    array (
      'name' => 'cases_tickets_notesnotes_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cases_tickets_notes_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cases_tickets_notes_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_notescases_tickets_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cases_tickets_notes_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_notesnotes_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'cases_tickets_notes_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_tickets_notesnotes_idb',
      ),
    ),
  ),
);