<?php
// created: 2017-10-26 16:56:34
$dictionary["cases_tickets_documents"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cases_tickets_documents' => 
    array (
      'lhs_module' => 'cases_Tickets',
      'lhs_table' => 'cases_tickets',
      'lhs_key' => 'id',
      'rhs_module' => 'Documents',
      'rhs_table' => 'documents',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_tickets_documents_c',
      'join_key_lhs' => 'cases_tickets_documentscases_tickets_ida',
      'join_key_rhs' => 'cases_tickets_documentsdocuments_idb',
    ),
  ),
  'table' => 'cases_tickets_documents_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'cases_tickets_documentscases_tickets_ida' => 
    array (
      'name' => 'cases_tickets_documentscases_tickets_ida',
      'type' => 'id',
    ),
    'cases_tickets_documentsdocuments_idb' => 
    array (
      'name' => 'cases_tickets_documentsdocuments_idb',
      'type' => 'id',
    ),
    'document_revision_id' => 
    array (
      'name' => 'document_revision_id',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_cases_tickets_documents_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_cases_tickets_documents_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_documentscases_tickets_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_cases_tickets_documents_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_tickets_documentsdocuments_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'cases_tickets_documents_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_tickets_documentsdocuments_idb',
      ),
    ),
  ),
);