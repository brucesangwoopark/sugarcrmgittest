<?php
// created: 2016-09-23 15:14:29
$dictionary["leads_lipkg_lead_line_items_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'leads_lipkg_lead_line_items_1' => 
    array (
      'lhs_module' => 'Leads',
      'lhs_table' => 'leads',
      'lhs_key' => 'id',
      'rhs_module' => 'lipkg_lead_line_items',
      'rhs_table' => 'lipkg_lead_line_items',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'leads_lipkg_lead_line_items_1_c',
      'join_key_lhs' => 'leads_lipkg_lead_line_items_1leads_ida',
      'join_key_rhs' => 'leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb',
    ),
  ),
  'table' => 'leads_lipkg_lead_line_items_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'leads_lipkg_lead_line_items_1leads_ida' => 
    array (
      'name' => 'leads_lipkg_lead_line_items_1leads_ida',
      'type' => 'id',
    ),
    'leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb' => 
    array (
      'name' => 'leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'leads_lipkg_lead_line_items_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'leads_lipkg_lead_line_items_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'leads_lipkg_lead_line_items_1leads_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'leads_lipkg_lead_line_items_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'leads_lipkg_lead_line_items_1lipkg_lead_line_items_idb',
      ),
    ),
  ),
);