<?php
// created: 2016-12-28 12:31:37
$dictionary["trans_transfers_accounts"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'trans_transfers_accounts' => 
    array (
      'lhs_module' => 'trans_Transfers',
      'lhs_table' => 'trans_transfers',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'trans_transfers_accounts_c',
      'join_key_lhs' => 'trans_transfers_accountstrans_transfers_ida',
      'join_key_rhs' => 'trans_transfers_accountsaccounts_idb',
    ),
  ),
  'table' => 'trans_transfers_accounts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'trans_transfers_accountstrans_transfers_ida' => 
    array (
      'name' => 'trans_transfers_accountstrans_transfers_ida',
      'type' => 'id',
    ),
    'trans_transfers_accountsaccounts_idb' => 
    array (
      'name' => 'trans_transfers_accountsaccounts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_trans_transfers_accounts_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_trans_transfers_accounts_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'trans_transfers_accountstrans_transfers_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_trans_transfers_accounts_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'trans_transfers_accountsaccounts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'trans_transfers_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'trans_transfers_accountstrans_transfers_ida',
        1 => 'trans_transfers_accountsaccounts_idb',
      ),
    ),
  ),
);