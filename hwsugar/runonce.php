<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-11
 * Time: 오후 2:24
 */

if(!defined('sugarEntry'))define('sugarEntry', true);

chdir(dirname(__FILE__));
define('ENTRY_POINT_TYPE', 'api');
require_once('include/entryPoint.php');

$sapi_type = php_sapi_name();
if (substr($sapi_type, 0, 3) != 'cli') {
    sugar_die("cron.php is CLI only.");
}

SugarMetric_Manager::getInstance()->setMetricClass('background')->setTransactionName('cron');

if(empty($current_language)) {
    $current_language = $sugar_config['default_language'];
}

$app_list_strings = return_app_list_strings_language($current_language);
$app_strings = return_application_language($current_language);

global $current_user;
$current_user = BeanFactory::getBean('Users');
$current_user->getSystemUser();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



$agent_name = '';
$ra_num = '';
$job_bean = BeanFactory::getBean('abcde_Jobs', '6c5876c4-b6ba-11e7-be93-0e75c7a95c08');
$opp_beans = $job_bean->opportunities_abcde_jobs_1->getBeans();
foreach ($opp_beans as $opp_bean) {
    $agent_name = $opp_bean->agent_c;
    $ra_num = $opp_bean->ra_num_c;
}

/*
$agent_name = '';
$ra_num = '';
if ($job_bean->load_relationship('opportunities_abcde_jobs_1')) {
    $opp_beans = $job_bean->opportunities_abcde_jobs_1->getBeans();
    foreach ($opp_beans as $opp_bean) {
        $agent_name = $opp_bean->agent_c;
        $ra_num = $opp_bean->ra_num_c;
    }
}
*/


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
sugar_cleanup(false);
// some jobs have annoying habit of calling sugar_cleanup(), and it can be called only once
// but job results can be written to DB after job is finished, so we have to disconnect here again
// just in case we couldn't call cleanup
if(class_exists('DBManagerFactory')) {
    $db = DBManagerFactory::getInstance();
    $db->disconnect();
}

// If we have a session left over, destroy it
if(session_id()) {
    session_destroy();
}
