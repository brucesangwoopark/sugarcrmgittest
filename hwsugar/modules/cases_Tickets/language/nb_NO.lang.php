<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_NUMBER' => 'Nummer:',
  'LBL_STATUS' => 'Status:',
  'LBL_PRIORITY' => 'Prioritet:',
  'LBL_RESOLUTION' => 'Løsning',
  'LBL_LAST_MODIFIED' => 'Sist Endret',
  'LBL_WORK_LOG' => 'Arbieds Log:',
  'LBL_CREATED_BY' => 'Opprettet av:',
  'LBL_DATE_CREATED' => 'Opprettet dato:',
  'LBL_MODIFIED_BY' => 'Sist Endret av:',
  'LBL_ASSIGNED_USER' => 'Tildelt bruker:',
  'LBL_ASSIGNED_USER_NAME' => 'Tildelt til',
  'LBL_SYSTEM_ID' => 'System-ID:',
  'LBL_TEAM_NAME' => 'Team Navn:',
  'LBL_TYPE' => 'Type:',
  'LBL_SUBJECT' => 'Emne:',
  'LBL_LIST_FORM_TITLE' => 'Tickets Liste',
  'LBL_MODULE_NAME' => 'Tickets',
  'LBL_MODULE_TITLE' => 'Tickets',
  'LBL_MODULE_NAME_SINGULAR' => 'Ticket',
  'LBL_HOMEPAGE_TITLE' => 'Min Tickets',
  'LNK_NEW_RECORD' => 'Opprett Ticket',
  'LNK_LIST' => 'Vis Tickets',
  'LNK_IMPORT_CASES_TICKETS' => 'Import Tickets',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Ticket',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_CASES_TICKETS_SUBPANEL_TITLE' => 'Tickets',
  'LBL_NEW_FORM_TITLE' => 'Ny Ticket',
  'LNK_IMPORT_VCARD' => 'Import Ticket vCard',
  'LBL_IMPORT' => 'Import Tickets',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Ticket record by importing a vCard from your file system.',
  'LBL_CCWHENRESOLVED' => 'Call Customer When Resolved',
  'LBL_ASSIGNEDTO_USER_ID' => 'Assigned to (related User ID)',
  'LBL_ASSIGNEDTO' => 'Assigned to',
  'LBL_TSOURCE' => 'Source',
  'LBL_CLOSEDDATE' => 'Resolved Date',
  'LBL_CCREATEDBY' => 'ccreatedby',
  'LBL_CUSTASSIGNEDTO' => 'custassignedto',
  'LBL_MODIFIEDBYC' => 'modifiedbyc',
  'LBL_CACCOUNTNAME' => 'caccountname',
  'LBL_CATEGORY' => 'Category',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_SUBCATEGORY' => 'Sub Category',
);