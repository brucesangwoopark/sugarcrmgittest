<?php
$module_name = 'cases_Tickets';
$_module_name = 'cases_tickets';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_SUBJECT',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
              ),
              2 => 
              array (
                'name' => 'priority',
                'label' => 'LBL_PRIORITY',
                'default' => true,
                'enabled' => true,
              ),
              3 => 
              array (
                'name' => 'resolution',
                'label' => 'LBL_RESOLUTION',
                'default' => true,
                'enabled' => true,
              ),
              4 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              5 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_USER_NAME',
                'default' => true,
                'enabled' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
