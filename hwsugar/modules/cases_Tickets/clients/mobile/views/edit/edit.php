<?php
$module_name = 'cases_Tickets';
$_module_name = 'cases_tickets';
$viewdefs[$module_name] = 
array (
  'mobile' => 
  array (
    'view' => 
    array (
      'edit' => 
      array (
        'templateMeta' => 
        array (
          'maxColumns' => '1',
          'widths' => 
          array (
            0 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
            1 => 
            array (
              'label' => '10',
              'field' => '30',
            ),
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_DEFAULT',
            'name' => 'LBL_PANEL_DEFAULT',
            'columns' => '1',
            'labelsOnTop' => 1,
            'placeholders' => 1,
            'fields' => 
            array (
              0 => 'priority',
              1 => 'status',
              2 => 
              array (
                'name' => 'name',
                'label' => 'LBL_SUBJECT',
              ),
              3 => 'assigned_user_name',
              4 => 'team_name',
            ),
          ),
        ),
      ),
    ),
  ),
);
