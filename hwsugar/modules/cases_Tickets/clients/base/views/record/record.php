<?php
$module_name = 'cases_Tickets';
$_module_name = 'cases_tickets';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'cases_Tickets',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'priority',
                'span' => 12,
              ),
              1 => 'status',
              2 => 'type',
              3 => 'resolution',
              4 => 'assigned_user_name',
              5 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              6 => 
              array (
                'name' => 'tag',
              ),
              7 => 
              array (
                'name' => 'cases_tickets_accounts_name',
              ),
              8 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              17 => 
              array (
              ),
              18 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              19 => 
              array (
              ),
              20 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              21 => 
              array (
              ),
              22 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              23 => 
              array (
              ),
              24 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              25 => 
              array (
              ),
              26 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_name',
              ),
              27 => 
              array (
              ),
              28 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              29 => 
              array (
              ),
              30 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              31 => 
              array (
              ),
              32 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              33 => 
              array (
              ),
              34 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              35 => 
              array (
              ),
              36 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              37 => 
              array (
              ),
              38 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              39 => 
              array (
              ),
              40 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              41 => 
              array (
              ),
              42 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              43 => 
              array (
              ),
              44 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              45 => 
              array (
              ),
              46 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              47 => 
              array (
              ),
              48 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              49 => 
              array (
              ),
              50 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              51 => 
              array (
              ),
              52 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              53 => 
              array (
              ),
              54 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              55 => 
              array (
              ),
              56 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              57 => 
              array (
              ),
              58 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              59 => 
              array (
              ),
              60 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              61 => 
              array (
              ),
              62 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              63 => 
              array (
              ),
              64 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              65 => 
              array (
              ),
              66 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              67 => 
              array (
              ),
              68 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              69 => 
              array (
              ),
              70 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              71 => 
              array (
              ),
              72 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              73 => 
              array (
              ),
              74 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              75 => 
              array (
              ),
              76 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              77 => 
              array (
              ),
              78 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              79 => 
              array (
              ),
              80 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              81 => 
              array (
              ),
              82 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              83 => 
              array (
              ),
              84 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              85 => 
              array (
              ),
              86 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              87 => 
              array (
              ),
              88 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              89 => 
              array (
              ),
              90 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              91 => 
              array (
              ),
              92 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              93 => 
              array (
              ),
              94 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              95 => 
              array (
              ),
              96 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              97 => 
              array (
              ),
              98 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              99 => 
              array (
              ),
              100 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              101 => 
              array (
              ),
              102 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              103 => 
              array (
              ),
              104 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              105 => 
              array (
              ),
              106 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              107 => 
              array (
              ),
              108 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              109 => 
              array (
              ),
              110 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
              111 => 
              array (
              ),
              112 => 
              array (
                'name' => 'cases_tickets_accounts_1_name',
              ),
              113 => 
              array (
              ),
              114 => 
              array (
                'name' => 'cases_tickets_revenuelineitems_1_name',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'team_name',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              3 => 
              array (
                'name' => 'work_log',
                'span' => 12,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
