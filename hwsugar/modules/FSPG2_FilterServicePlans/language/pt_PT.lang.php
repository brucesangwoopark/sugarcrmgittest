<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_ASSIGNED_TO_ID' => 'ID de Utilizador Atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'ID do Utilizador que Criou',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Plans Lista',
  'LBL_MODULE_NAME' => 'Filter Service Plans',
  'LBL_MODULE_TITLE' => 'Filter Service Plans',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Plan',
  'LBL_HOMEPAGE_TITLE' => 'Minha Filter Service Plans',
  'LNK_NEW_RECORD' => 'Criar Filter Service Plan',
  'LNK_LIST' => 'Vista Filter Service Plans',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPLANS' => 'Import Filter Service Plans',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Filter Service Plan',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Fluxo de Atividades',
  'LBL_FSPG2_FILTERSERVICEPLANS_SUBPANEL_TITLE' => 'Filter Service Plans',
  'LBL_NEW_FORM_TITLE' => 'Novo Filter Service Plan',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Plan vCard',
  'LBL_IMPORT' => 'Import Filter Service Plans',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Plan record by importing a vCard from your file system.',
  'LBL_AIRFILTERSERVICEID' => 'Air Filter Service ID',
  'LBL_ISACTIVE' => 'Active?',
  'LBL_ISENROLLED' => 'Enrolled?',
  'LBL_COMMISSIONPAID' => 'Commission Paid?',
  'LBL_LASTSERVICEDATE' => 'Last Filter Changed',
  'LBL_NEXTSERVICEDATE' => 'Next Service Date',
);