<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Plans Saraksts',
  'LBL_MODULE_NAME' => 'Filter Service Plans',
  'LBL_MODULE_TITLE' => 'Filter Service Plans',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Plan',
  'LBL_HOMEPAGE_TITLE' => 'Mans Filter Service Plans',
  'LNK_NEW_RECORD' => 'Izveidot Filter Service Plan',
  'LNK_LIST' => 'Skats Filter Service Plans',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPLANS' => 'Import Filter Service Plans',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Filter Service Plan',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_FSPG2_FILTERSERVICEPLANS_SUBPANEL_TITLE' => 'Filter Service Plans',
  'LBL_NEW_FORM_TITLE' => 'Jauns Filter Service Plan',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Plan vCard',
  'LBL_IMPORT' => 'Import Filter Service Plans',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Plan record by importing a vCard from your file system.',
  'LBL_AIRFILTERSERVICEID' => 'Air Filter Service ID',
  'LBL_ISACTIVE' => 'Active?',
  'LBL_ISENROLLED' => 'Enrolled?',
  'LBL_COMMISSIONPAID' => 'Commission Paid?',
  'LBL_LASTSERVICEDATE' => 'Last Filter Changed',
  'LBL_NEXTSERVICEDATE' => 'Next Service Date',
);