<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Lag-ID',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelat användar-ID',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_TAGS_LINK' => 'Taggar',
  'LBL_TAGS' => 'Taggar',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum skapat',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Ägare av dokument',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Plans List',
  'LBL_MODULE_NAME' => 'Filter Service Plans',
  'LBL_MODULE_TITLE' => 'Filter Service Plans',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Plan',
  'LBL_HOMEPAGE_TITLE' => 'Min Filter Service Plans',
  'LNK_NEW_RECORD' => 'Create Filter Service Plan',
  'LNK_LIST' => 'Visa Filter Service Plans',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPLANS' => 'Import Filter Service Plans',
  'LBL_SEARCH_FORM_TITLE' => 'Search Filter Service Plan',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_FSPG2_FILTERSERVICEPLANS_SUBPANEL_TITLE' => 'Filter Service Plans',
  'LBL_NEW_FORM_TITLE' => 'Ny Filter Service Plan',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Plan vCard',
  'LBL_IMPORT' => 'Import Filter Service Plans',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Plan record by importing a vCard from your file system.',
  'LBL_AIRFILTERSERVICEID' => 'Air Filter Service ID',
  'LBL_ISACTIVE' => 'Active?',
  'LBL_ISENROLLED' => 'Enrolled?',
  'LBL_COMMISSIONPAID' => 'Commission Paid?',
  'LBL_LASTSERVICEDATE' => 'Last Filter Changed',
  'LBL_NEXTSERVICEDATE' => 'Next Service Date',
);