<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Details Lista',
  'LBL_MODULE_NAME' => 'Filter Service Details',
  'LBL_MODULE_TITLE' => 'Filter Service Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Detail',
  'LBL_HOMEPAGE_TITLE' => 'Saját Filter Service Details',
  'LNK_NEW_RECORD' => 'Új létrehozása Filter Service Detail',
  'LNK_LIST' => 'Megtekintés Filter Service Details',
  'LNK_IMPORT_FSPG2_FILTERSERVICEDETAILS' => 'Import Filter Service Details',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Filter Service Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_FSPG2_FILTERSERVICEDETAILS_SUBPANEL_TITLE' => 'Filter Service Details',
  'LBL_NEW_FORM_TITLE' => 'Új Filter Service Detail',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Detail vCard',
  'LBL_IMPORT' => 'Import Filter Service Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Detail record by importing a vCard from your file system.',
  'LBL_FILTERCHANGEDATE' => 'Filter Change Date',
  'LBL_WATERMETERREAD' => 'Water Meter Read',
  'LBL_UNITS' => 'Units',
  'LBL_STATE' => 'State',
  'LBL_CHANGETYPE' => 'Change Type',
  'LBL_TRACKINGNUMBER' => 'Tracking Number',
  'LBL_APPOINMENTID' => 'Appoinment ID',
);