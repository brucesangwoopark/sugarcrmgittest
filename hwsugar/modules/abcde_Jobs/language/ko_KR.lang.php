<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => '팀',
  'LBL_TEAMS' => '팀',
  'LBL_TEAM_ID' => '팀 ID',
  'LBL_ASSIGNED_TO_ID' => '지정 사용자 ID',
  'LBL_ASSIGNED_TO_NAME' => '지정자',
  'LBL_TAGS_LINK' => '태그',
  'LBL_TAGS' => '태그',
  'LBL_ID' => 'ID:',
  'LBL_DATE_ENTERED' => '생성일자:',
  'LBL_DATE_MODIFIED' => '수정일자:',
  'LBL_MODIFIED' => '수정자:',
  'LBL_MODIFIED_ID' => '수정자 ID',
  'LBL_MODIFIED_NAME' => '사용자명에 의해 수정',
  'LBL_CREATED' => '생성자',
  'LBL_CREATED_ID' => '생성자 ID',
  'LBL_DOC_OWNER' => '문서소유자',
  'LBL_USER_FAVORITES' => '사용자 즐겨 찾기',
  'LBL_DESCRIPTION' => '설명',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '사용자에 의해 생성',
  'LBL_MODIFIED_USER' => '사용자에 의해 수정',
  'LBL_LIST_NAME' => '성명',
  'LBL_EDIT_BUTTON' => '수정하기',
  'LBL_REMOVE' => '제거하기',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '이름으로 수정',
  'LBL_LIST_FORM_TITLE' => 'Jobs 목록',
  'LBL_MODULE_NAME' => 'Jobs',
  'LBL_MODULE_TITLE' => 'Jobs',
  'LBL_MODULE_NAME_SINGULAR' => 'Job',
  'LBL_HOMEPAGE_TITLE' => '나의 Jobs',
  'LNK_NEW_RECORD' => '새로 만들기 Job',
  'LNK_LIST' => '보기 Jobs',
  'LNK_IMPORT_ABCDE_JOBS' => 'Import Job',
  'LBL_SEARCH_FORM_TITLE' => '검색 Job',
  'LBL_HISTORY_SUBPANEL_TITLE' => '연혁보기',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '활동내역',
  'LBL_ABCDE_JOBS_SUBPANEL_TITLE' => 'Jobs',
  'LBL_NEW_FORM_TITLE' => '신규 Job',
  'LNK_IMPORT_VCARD' => 'Import Job vCard',
  'LBL_IMPORT' => 'Import Jobs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Job record by importing a vCard from your file system.',
  'LBL_TECH_SUPKG_SYSTEMUSERS_ID' => 'tech (related System User ID)',
  'LBL_TECH' => 'tech',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'opportunity (related Opportunity ID)',
  'LBL_OPPORTUNITY' => 'opportunity',
  'LBL_LOCATION' => 'location',
  'LBL_COMMENTS' => 'comments',
  'LBL_JOB_PRIORITY' => 'job priority',
  'LBL_JOB_DATE' => 'job date',
  'LBL_REAL_START_TIME' => 'real start time',
  'LBL_REAL_END_TIME' => 'real end time',
  'LBL_SCHEDULED_START_TIME' => 'scheduled start time',
  'LBL_SCHEDULED_END_TIME' => 'scheduled end time',
);