<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID:',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Kasutaja',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev:',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja Id',
  'LBL_MODIFIED_NAME' => 'Muutja nime järgi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Looja',
  'LBL_MODIFIED_USER' => 'Muutja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nime järgi',
  'LBL_LIST_FORM_TITLE' => 'Jobs Loend',
  'LBL_MODULE_NAME' => 'Jobs',
  'LBL_MODULE_TITLE' => 'Jobs',
  'LBL_MODULE_NAME_SINGULAR' => 'Job',
  'LBL_HOMEPAGE_TITLE' => 'Minu Jobs',
  'LNK_NEW_RECORD' => 'Loo Job',
  'LNK_LIST' => 'Vaade Jobs',
  'LNK_IMPORT_ABCDE_JOBS' => 'Import Job',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Job',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevused',
  'LBL_ABCDE_JOBS_SUBPANEL_TITLE' => 'Jobs',
  'LBL_NEW_FORM_TITLE' => 'Uus Job',
  'LNK_IMPORT_VCARD' => 'Import Job vCard',
  'LBL_IMPORT' => 'Import Jobs',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Job record by importing a vCard from your file system.',
  'LBL_TECH_SUPKG_SYSTEMUSERS_ID' => 'tech (related System User ID)',
  'LBL_TECH' => 'tech',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'opportunity (related Opportunity ID)',
  'LBL_OPPORTUNITY' => 'opportunity',
  'LBL_LOCATION' => 'location',
  'LBL_COMMENTS' => 'comments',
  'LBL_JOB_PRIORITY' => 'job priority',
  'LBL_JOB_DATE' => 'job date',
  'LBL_REAL_START_TIME' => 'real start time',
  'LBL_REAL_END_TIME' => 'real end time',
  'LBL_SCHEDULED_START_TIME' => 'scheduled start time',
  'LBL_SCHEDULED_END_TIME' => 'scheduled end time',
);