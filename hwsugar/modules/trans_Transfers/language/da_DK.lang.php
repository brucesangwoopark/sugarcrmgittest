<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'Transfers Liste',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Min Transfers',
  'LNK_NEW_RECORD' => 'Opret Transfer',
  'LNK_LIST' => 'Vis Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Ny Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);