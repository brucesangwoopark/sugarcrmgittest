<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'ID de l&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'ID d&#39;usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat per',
  'LBL_MODIFIED_ID' => 'Modificat per ID',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat per ID',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Suprimit',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Edita',
  'LBL_REMOVE' => 'Suprimir',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Transfers Llista',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Meu Transfers',
  'LNK_NEW_RECORD' => 'Crea Transfer',
  'LNK_LIST' => 'Vista Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Nou Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);