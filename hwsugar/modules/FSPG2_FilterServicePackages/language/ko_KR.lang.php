<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => '팀',
  'LBL_TEAMS' => '팀',
  'LBL_TEAM_ID' => '팀 ID',
  'LBL_ASSIGNED_TO_ID' => '지정 사용자 ID',
  'LBL_ASSIGNED_TO_NAME' => '지정자',
  'LBL_TAGS_LINK' => '태그',
  'LBL_TAGS' => '태그',
  'LBL_ID' => 'ID:',
  'LBL_DATE_ENTERED' => '생성일자:',
  'LBL_DATE_MODIFIED' => '수정일자:',
  'LBL_MODIFIED' => '수정자:',
  'LBL_MODIFIED_ID' => '수정자 ID',
  'LBL_MODIFIED_NAME' => '사용자명에 의해 수정',
  'LBL_CREATED' => '생성자',
  'LBL_CREATED_ID' => '생성자 ID',
  'LBL_DOC_OWNER' => '문서소유자',
  'LBL_USER_FAVORITES' => '사용자 즐겨 찾기',
  'LBL_DESCRIPTION' => '설명',
  'LBL_DELETED' => '삭제',
  'LBL_NAME' => '이름',
  'LBL_CREATED_USER' => '사용자에 의해 생성',
  'LBL_MODIFIED_USER' => '사용자에 의해 수정',
  'LBL_LIST_NAME' => '성명',
  'LBL_EDIT_BUTTON' => '수정하기',
  'LBL_REMOVE' => '제거하기',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '이름으로 수정',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Packages 목록',
  'LBL_MODULE_NAME' => 'Filter Service Packages',
  'LBL_MODULE_TITLE' => 'Filter Service Packages',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Package',
  'LBL_HOMEPAGE_TITLE' => '나의 Filter Service Packages',
  'LNK_NEW_RECORD' => '새로 만들기 Filter Service Package',
  'LNK_LIST' => '보기 Filter Service Packages',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPACKAGES' => 'Import Filter Service Packages',
  'LBL_SEARCH_FORM_TITLE' => '검색 Filter Service Package',
  'LBL_HISTORY_SUBPANEL_TITLE' => '연혁보기',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '활동내역',
  'LBL_FSPG2_FILTERSERVICEPACKAGES_SUBPANEL_TITLE' => 'Filter Service Packages',
  'LBL_NEW_FORM_TITLE' => '신규 Filter Service Package',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Package vCard',
  'LBL_IMPORT' => 'Import Filter Service Packages',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Package record by importing a vCard from your file system.',
  'LBL_PRODUCTTYPE' => 'Product Type',
  'LBL_PRODUCTCOUNT' => 'Product Count',
  'LBL_BILLTYPECODE' => 'Bill Type Code',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_STANDINGREQUEST' => 'Standing Request',
  'LBL_SECONDPRICE' => 'Second Price',
  'LBL_INSTALLATION' => 'Installation',
  'LBL_FILTERMAINTENANCE' => 'Filter Maintenance',
  'LBL_FREQUENCY' => 'Frequency',
  'LBL_ISVIP' => 'VIP?',
  'LBL_EFFECTIVEFROM' => 'Effective From',
  'LBL_EFFECTIVETO' => 'Effective To',
  'LBL_ISACTIVE' => 'Active?',
);