<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => '小組',
  'LBL_TEAMS' => '小組',
  'LBL_TEAM_ID' => '小組 ID',
  'LBL_ASSIGNED_TO_ID' => '指派的使用者 ID',
  'LBL_ASSIGNED_TO_NAME' => '已指派至',
  'LBL_TAGS_LINK' => '標籤',
  'LBL_TAGS' => '標籤',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '建立日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '按 ID 修改',
  'LBL_MODIFIED_NAME' => '按名稱修改',
  'LBL_CREATED' => '建立人',
  'LBL_CREATED_ID' => '按 ID 建立',
  'LBL_DOC_OWNER' => '文件擁有者',
  'LBL_USER_FAVORITES' => '最愛的使用者',
  'LBL_DESCRIPTION' => '描述',
  'LBL_DELETED' => '已刪除',
  'LBL_NAME' => '名稱',
  'LBL_CREATED_USER' => '由使用者建立',
  'LBL_MODIFIED_USER' => '由使用者修改',
  'LBL_LIST_NAME' => '名稱',
  'LBL_EDIT_BUTTON' => '編輯',
  'LBL_REMOVE' => '移除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '按名稱修改',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Packages 清單',
  'LBL_MODULE_NAME' => 'Filter Service Packages',
  'LBL_MODULE_TITLE' => 'Filter Service Packages',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Package',
  'LBL_HOMEPAGE_TITLE' => '我的 Filter Service Packages',
  'LNK_NEW_RECORD' => '建立 Filter Service Package',
  'LNK_LIST' => '檢視 Filter Service Packages',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPACKAGES' => 'Import Filter Service Packages',
  'LBL_SEARCH_FORM_TITLE' => '搜尋 Filter Service Package',
  'LBL_HISTORY_SUBPANEL_TITLE' => '檢視歷史',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活動流',
  'LBL_FSPG2_FILTERSERVICEPACKAGES_SUBPANEL_TITLE' => 'Filter Service Packages',
  'LBL_NEW_FORM_TITLE' => '新 Filter Service Package',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Package vCard',
  'LBL_IMPORT' => 'Import Filter Service Packages',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Package record by importing a vCard from your file system.',
  'LBL_PRODUCTTYPE' => 'Product Type',
  'LBL_PRODUCTCOUNT' => 'Product Count',
  'LBL_BILLTYPECODE' => 'Bill Type Code',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_STANDINGREQUEST' => 'Standing Request',
  'LBL_SECONDPRICE' => 'Second Price',
  'LBL_INSTALLATION' => 'Installation',
  'LBL_FILTERMAINTENANCE' => 'Filter Maintenance',
  'LBL_FREQUENCY' => 'Frequency',
  'LBL_ISVIP' => 'VIP?',
  'LBL_EFFECTIVEFROM' => 'Effective From',
  'LBL_EFFECTIVETO' => 'Effective To',
  'LBL_ISACTIVE' => 'Active?',
);