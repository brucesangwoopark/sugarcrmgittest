<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Filter Service Packages Liste',
  'LBL_MODULE_NAME' => 'Filter Service Packages',
  'LBL_MODULE_TITLE' => 'Filter Service Packages',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Service Package',
  'LBL_HOMEPAGE_TITLE' => 'Min Filter Service Packages',
  'LNK_NEW_RECORD' => 'Opprett Filter Service Package',
  'LNK_LIST' => 'Vis Filter Service Packages',
  'LNK_IMPORT_FSPG2_FILTERSERVICEPACKAGES' => 'Import Filter Service Packages',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Filter Service Package',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_FSPG2_FILTERSERVICEPACKAGES_SUBPANEL_TITLE' => 'Filter Service Packages',
  'LBL_NEW_FORM_TITLE' => 'Ny Filter Service Package',
  'LNK_IMPORT_VCARD' => 'Import Filter Service Package vCard',
  'LBL_IMPORT' => 'Import Filter Service Packages',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Service Package record by importing a vCard from your file system.',
  'LBL_PRODUCTTYPE' => 'Product Type',
  'LBL_PRODUCTCOUNT' => 'Product Count',
  'LBL_BILLTYPECODE' => 'Bill Type Code',
  'LBL_CURRENCY' => 'Currency',
  'LBL_PRICE' => 'Price',
  'LBL_STANDINGREQUEST' => 'Standing Request',
  'LBL_SECONDPRICE' => 'Second Price',
  'LBL_INSTALLATION' => 'Installation',
  'LBL_FILTERMAINTENANCE' => 'Filter Maintenance',
  'LBL_FREQUENCY' => 'Frequency',
  'LBL_ISVIP' => 'VIP?',
  'LBL_EFFECTIVEFROM' => 'Effective From',
  'LBL_EFFECTIVETO' => 'Effective To',
  'LBL_ISACTIVE' => 'Active?',
);