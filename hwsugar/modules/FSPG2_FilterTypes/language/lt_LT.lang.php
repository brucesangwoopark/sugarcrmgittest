<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Priskirto vartotojo ID',
  'LBL_ASSIGNED_TO_NAME' => 'Kam priskirta',
  'LBL_TAGS_LINK' => 'Žymės',
  'LBL_TAGS' => 'Žymės',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukūrimo data',
  'LBL_DATE_MODIFIED' => 'Modifikavimo data',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo ID',
  'LBL_DOC_OWNER' => 'Dokumento savininkas',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašas',
  'LBL_DELETED' => 'Panaikintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūręs vartotojas',
  'LBL_MODIFIED_USER' => 'Modifikavęs vartotojas.',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Pašalinti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Filter Types Sąrašas',
  'LBL_MODULE_NAME' => 'Filter Types',
  'LBL_MODULE_TITLE' => 'Filter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Type',
  'LBL_HOMEPAGE_TITLE' => 'Mano Filter Types',
  'LNK_NEW_RECORD' => 'Sukurti Filter Type',
  'LNK_LIST' => 'View Filter Types',
  'LNK_IMPORT_FSPG2_FILTERTYPES' => 'Import Filter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Filter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_FSPG2_FILTERTYPES_SUBPANEL_TITLE' => 'Filter Types',
  'LBL_NEW_FORM_TITLE' => 'Naujas Filter Type',
  'LNK_IMPORT_VCARD' => 'Import Filter Type vCard',
  'LBL_IMPORT' => 'Import Filter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Type record by importing a vCard from your file system.',
  'LBL_LENGTH' => 'Length',
  'LBL_WIDTH' => 'Width',
  'LBL_HEIGHT' => 'Height',
  'LBL_WEIGHT' => 'Weight',
);