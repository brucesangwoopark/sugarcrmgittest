<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'ID de l&#39;equip',
  'LBL_ASSIGNED_TO_ID' => 'ID d&#39;usuari assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_TAGS_LINK' => 'Etiquetes',
  'LBL_TAGS' => 'Etiquetes',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat per',
  'LBL_MODIFIED_ID' => 'Modificat per ID',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat per ID',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Suprimit',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Edita',
  'LBL_REMOVE' => 'Suprimir',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Filter Types Llista',
  'LBL_MODULE_NAME' => 'Filter Types',
  'LBL_MODULE_TITLE' => 'Filter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Type',
  'LBL_HOMEPAGE_TITLE' => 'Meu Filter Types',
  'LNK_NEW_RECORD' => 'Crea Filter Type',
  'LNK_LIST' => 'Vista Filter Types',
  'LNK_IMPORT_FSPG2_FILTERTYPES' => 'Import Filter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Filter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_FSPG2_FILTERTYPES_SUBPANEL_TITLE' => 'Filter Types',
  'LBL_NEW_FORM_TITLE' => 'Nou Filter Type',
  'LNK_IMPORT_VCARD' => 'Import Filter Type vCard',
  'LBL_IMPORT' => 'Import Filter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Type record by importing a vCard from your file system.',
  'LBL_LENGTH' => 'Length',
  'LBL_WIDTH' => 'Width',
  'LBL_HEIGHT' => 'Height',
  'LBL_WEIGHT' => 'Weight',
);