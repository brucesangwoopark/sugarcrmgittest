<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja ID',
  'LBL_ASSIGNED_TO_NAME' => 'Määratud kasutajale',
  'LBL_TAGS_LINK' => 'Sildid',
  'LBL_TAGS' => 'Sildid',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja ID',
  'LBL_MODIFIED_NAME' => 'Muutja nimi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja ID',
  'LBL_DOC_OWNER' => 'Dokumendi omanik',
  'LBL_USER_FAVORITES' => 'Lemmikkasutajad',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Loonud kasutaja',
  'LBL_MODIFIED_USER' => 'Muutnud kasutaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nimi',
  'LBL_LIST_FORM_TITLE' => 'Filter Types Loend',
  'LBL_MODULE_NAME' => 'Filter Types',
  'LBL_MODULE_TITLE' => 'Filter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Type',
  'LBL_HOMEPAGE_TITLE' => 'Minu Filter Types',
  'LNK_NEW_RECORD' => 'Loo Filter Type',
  'LNK_LIST' => 'Vaade Filter Types',
  'LNK_IMPORT_FSPG2_FILTERTYPES' => 'Import Filter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Filter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevuste voog',
  'LBL_FSPG2_FILTERTYPES_SUBPANEL_TITLE' => 'Filter Types',
  'LBL_NEW_FORM_TITLE' => 'Uus Filter Type',
  'LNK_IMPORT_VCARD' => 'Import Filter Type vCard',
  'LBL_IMPORT' => 'Import Filter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Type record by importing a vCard from your file system.',
  'LBL_LENGTH' => 'Length',
  'LBL_WIDTH' => 'Width',
  'LBL_HEIGHT' => 'Height',
  'LBL_WEIGHT' => 'Weight',
);