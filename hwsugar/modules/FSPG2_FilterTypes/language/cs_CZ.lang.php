<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_ASSIGNED_TO_ID' => 'Přiřazené uživateli s ID',
  'LBL_ASSIGNED_TO_NAME' => 'Přiřazeno komu',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum zahájení',
  'LBL_DATE_MODIFIED' => 'Datum poslední úpravy',
  'LBL_MODIFIED' => 'Modifikováno kym',
  'LBL_MODIFIED_ID' => 'Upraveno podle ID',
  'LBL_MODIFIED_NAME' => 'Upraveno uživatelem',
  'LBL_CREATED' => 'Vytvořil:',
  'LBL_CREATED_ID' => 'Vytvořeno podle ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Uživatelé, jejichž je oblíbeným',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Odstranit',
  'LBL_NAME' => 'Název',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_LIST_NAME' => 'Název',
  'LBL_EDIT_BUTTON' => 'Editace',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upraveno uživatelem',
  'LBL_LIST_FORM_TITLE' => 'Filter Types Celk. cena',
  'LBL_MODULE_NAME' => 'Filter Types',
  'LBL_MODULE_TITLE' => 'Filter Types',
  'LBL_MODULE_NAME_SINGULAR' => 'Filter Type',
  'LBL_HOMEPAGE_TITLE' => 'Moje Filter Types',
  'LNK_NEW_RECORD' => 'Přidat Filter Type',
  'LNK_LIST' => 'Zobrazit Filter Types',
  'LNK_IMPORT_FSPG2_FILTERTYPES' => 'Import Filter Types',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Filter Type',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobrazit historii',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_FSPG2_FILTERTYPES_SUBPANEL_TITLE' => 'Filter Types',
  'LBL_NEW_FORM_TITLE' => 'Nový Filter Type',
  'LNK_IMPORT_VCARD' => 'Import Filter Type vCard',
  'LBL_IMPORT' => 'Import Filter Types',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Filter Type record by importing a vCard from your file system.',
  'LBL_LENGTH' => 'Length',
  'LBL_WIDTH' => 'Width',
  'LBL_HEIGHT' => 'Height',
  'LBL_WEIGHT' => 'Weight',
);