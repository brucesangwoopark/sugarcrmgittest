<?php
/**
 * Created by PhpStorm.
 * User: bruce
 * Date: 2017-10-18
 * Time: 오전 11:21
 */

class VerifyRLISalesStage
{
    function run()
    {
        $rli_ids = array(
            '41395',
        );

        foreach ($rli_ids as $rli_id) {
            $query = "
                select a.sales_stage 'sales_stage', a.id 'id'
                from revenue_line_items a
                left join revenue_line_items_cstm b on a.id = b.id_c
                where id = '{$rli_id}';
            ";
            $result = $this->run_query($query);

            $query = "
                select c.job_type_c, c.status_c, b.date_modified
                from abcde_jobs_revenuelineitems_1_c a
                join abcde_jobs b on a.abcde_jobs_revenuelineitems_1abcde_jobs_ida = b.id
                join abcde_jobs_cstm c on b.id = c.id_c
                where c.status_c = 'completed' and abcde_jobs_revenuelineitems_1revenuelineitems_idb = '{$rli_id}'
                order by b.date_modified desc;
            ";
            $result = $this->run_query($query);
        }


    }

    function run_query($query)
    {
        $ret = array();

        $result = $GLOBALS['db']->query($query);
        if ($result === false) {
            return false;
        }

        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $row_elmnt = array();
            foreach ($row as $key => $value) {
                $row_elmnt[$key] = $value;
            }

            $ret[] = $row_elmnt;
        }

        return $ret;
    }
}