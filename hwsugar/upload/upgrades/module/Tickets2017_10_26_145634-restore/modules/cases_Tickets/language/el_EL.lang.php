<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_NUMBER' => 'Αριθμός:',
  'LBL_STATUS' => 'Κατάσταση:',
  'LBL_PRIORITY' => 'Προτεραιότητα:',
  'LBL_RESOLUTION' => 'Επίλυση',
  'LBL_LAST_MODIFIED' => 'Τελευταία Τροποποίηση',
  'LBL_WORK_LOG' => 'Εργασία:',
  'LBL_CREATED_BY' => 'Δημιουργήθηκε Από:',
  'LBL_DATE_CREATED' => 'Ημερομηνία Δημιουργίας:',
  'LBL_MODIFIED_BY' => 'Τροποποιήθηκε από:',
  'LBL_ASSIGNED_USER' => 'Ανατεθειμένος Χειριστής:',
  'LBL_ASSIGNED_USER_NAME' => 'Ανατέθηκε σε',
  'LBL_SYSTEM_ID' => 'Ταυτότητα Συστήματος:',
  'LBL_TEAM_NAME' => 'Όνομα Ομάδας:',
  'LBL_TYPE' => 'Τύπος:',
  'LBL_SUBJECT' => 'Θέμα:',
  'LBL_LIST_FORM_TITLE' => 'Tickets Λίστα',
  'LBL_MODULE_NAME' => 'Tickets',
  'LBL_MODULE_TITLE' => 'Tickets',
  'LBL_MODULE_NAME_SINGULAR' => 'Tickets',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Tickets',
  'LNK_NEW_RECORD' => 'Δημιουργία Tickets',
  'LNK_LIST' => 'Προβολή Tickets',
  'LNK_IMPORT_CASES_TICKETS' => 'Import Tickets',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Tickets',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_CASES_TICKETS_SUBPANEL_TITLE' => 'Tickets',
  'LBL_NEW_FORM_TITLE' => 'Νέα Tickets',
  'LNK_IMPORT_VCARD' => 'Import Tickets vCard',
  'LBL_IMPORT' => 'Import Tickets',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tickets record by importing a vCard from your file system.',
  'LBL_CCWHENRESOLVED' => 'Call Customer When Resolved',
  'LBL_ASSIGNEDTO_USER_ID' => 'Assigned to (related User ID)',
  'LBL_ASSIGNEDTO' => 'Assigned to',
  'LBL_TSOURCE' => 'Source',
  'LBL_CLOSEDDATE' => 'Resolved Date',
  'LBL_CCREATEDBY' => 'ccreatedby',
  'LBL_CUSTASSIGNEDTO' => 'custassignedto',
  'LBL_MODIFIEDBYC' => 'modifiedbyc',
  'LBL_CACCOUNTNAME' => 'caccountname',
  'LBL_CATEGORY' => 'Category',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_SUBCATEGORY' => 'Sub Category',
);