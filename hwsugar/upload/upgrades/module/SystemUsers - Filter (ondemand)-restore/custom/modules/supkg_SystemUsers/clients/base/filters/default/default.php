<?php
// created: 2016-10-18 13:53:14
$viewdefs['supkg_SystemUsers']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'first_name' => 
    array (
    ),
    'last_name' => 
    array (
    ),
    'address_city' => 
    array (
      'dbFields' => 
      array (
        0 => 'primary_address_city',
        1 => 'alt_address_city',
      ),
      'vname' => 'LBL_CITY',
      'type' => 'text',
    ),
    'created_by_name' => 
    array (
    ),
    'do_not_call' => 
    array (
    ),
    'email' => 
    array (
    ),
    'tag' => 
    array (
    ),
    '$owner' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);