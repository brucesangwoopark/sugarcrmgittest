<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array (
  'built_in_version' => '7.7.1.2',
  'acceptable_sugar_versions' => 
  array (
    0 => '',
  ),
  'acceptable_sugar_flavors' => 
  array (
    0 => 'ENT',
    1 => 'ULT',
  ),
  'readme' => '',
  'key' => 'abcde',
  'author' => 'DL',
  'description' => '',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'JobsPackage',
  'published_date' => '2016-10-19 16:04:29',
  'type' => 'module',
  'version' => 1476893069,
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'JobsPackage',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'abcde_Jobs',
      'class' => 'abcde_Jobs',
      'path' => 'modules/abcde_Jobs/abcde_Jobs.php',
      'tab' => true,
    ),
  ),
  'layoutdefs' => 
  array (
  ),
  'relationships' => 
  array (
  ),
  'image_dir' => '<basepath>/icons',
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/abcde_Jobs',
      'to' => 'modules/abcde_Jobs',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
  ),
);