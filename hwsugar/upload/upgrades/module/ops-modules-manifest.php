<?php

$manifest = array (
    'acceptable_sugar_versions' =>
        array (
            'regex_matches' => array(
                '^7.5\\..+',
                '^7.6\\..+',
                '^7.7\\..+',
                '^7.8\\..+',
                '^7.9\\..+',
            ),
        ),
    'acceptable_sugar_flavors' =>
        array (
            'PRO',
            'ENT',
            'CORP',
            'ULT',
        ),
    'readme' => '',
    'key' => 'ops',
    'author' => 'SugarCRM',
    'description' => 'A set of modules providing additional functionality for Sugar On-Demand.',
    'icon' => '',
    'is_uninstallable' => true,
    'name' => 'Ops-Modules',
    'published_date' => '2016-03-15 12:00:00',
    'type' => 'module',
    'version' => '2.0.2',
    'remove_tables' => 'prompt',
);

$installdefs = array (
    'id' => 'ops-modules',
    'administration' => array(
        array(
            'from' => '<basepath>/Files/custom/Extension/modules/Administration/Ext/Administration/ops_modules.php',
            'to_module' => 'Administration'
        )
    ),
    'beans' => array (
        array (
            'module' => 'ops_Backups',
            'class' => 'ops_Backups',
            'path' => 'modules/ops_Backups/ops_Backups.php',
            'tab' => true,
        ),
    ),
    'language' => array (
        array (
            'from' => '<basepath>/Files/custom/Extension/application/Ext/Language/en_us.ops_modules.php',
            'to_module' => 'application',
            'language' => 'en_us',
        ),
        array(
            'from' => '<basepath>/Files/custom/Extension/modules/Schedulers/Ext/Language/en_us.ops_backups.php',
            'to_module' => 'Schedulers',
            'language' => 'en_us'
        ),
        array(
            'from' => '<basepath>/Files/custom/Extension/modules/Administration/Ext/Language/en_us.ops_modules.php',
            'to_module' => 'Administration',
            'language' => 'en_us'
        )
    ),
    'scheduledefs' => array(
        array(
            'from' => '<basepath>/Files/custom/Extension/modules/Schedulers/Ext/ScheduledTasks/ops_backups_fetch_exports.php',
            'to_module' => 'Schedulers'
        ),
    ),
    'copy' => array(
        array (
            'from' => '<basepath>/Files/modules/ops_Backups',
            'to' => 'modules/ops_Backups',
        ),
    ),
    'post_execute' => array (
        '<basepath>/scripts/post_execute/0.php',
    ),
    'pre_uninstall' => array (
        '<basepath>/scripts/pre_uninstall/0.php',
    ),
);
