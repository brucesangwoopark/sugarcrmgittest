<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_NUMBER' => 'Numéro :',
  'LBL_STATUS' => 'Statut :',
  'LBL_PRIORITY' => 'Priorité :',
  'LBL_RESOLUTION' => 'Résolution',
  'LBL_LAST_MODIFIED' => 'Dernière modification',
  'LBL_WORK_LOG' => 'Réflexion menée :',
  'LBL_CREATED_BY' => 'Créé par :',
  'LBL_DATE_CREATED' => 'Date de création :',
  'LBL_MODIFIED_BY' => 'Modifié par :',
  'LBL_ASSIGNED_USER' => 'Assigné à l&#39;utilisateur:',
  'LBL_ASSIGNED_USER_NAME' => 'Assigné à',
  'LBL_SYSTEM_ID' => 'Identifiant système :',
  'LBL_TEAM_NAME' => 'Nom de l&#39;équipe :',
  'LBL_TYPE' => 'Type :',
  'LBL_SUBJECT' => 'Sujet :',
  'LBL_LIST_FORM_TITLE' => 'Tickets Catalogue',
  'LBL_MODULE_NAME' => 'Tickets',
  'LBL_MODULE_TITLE' => 'Tickets',
  'LBL_MODULE_NAME_SINGULAR' => 'Tickets',
  'LBL_HOMEPAGE_TITLE' => 'Mes Tickets',
  'LNK_NEW_RECORD' => 'Créer Tickets',
  'LNK_LIST' => 'Afficher Tickets',
  'LNK_IMPORT_CASES_TICKETS' => 'Import Tickets',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Tickets',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_CASES_TICKETS_SUBPANEL_TITLE' => 'Tickets',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Tickets',
  'LNK_IMPORT_VCARD' => 'Import Tickets vCard',
  'LBL_IMPORT' => 'Import Tickets',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tickets record by importing a vCard from your file system.',
  'LBL_CCWHENRESOLVED' => 'Call Customer When Resolved',
  'LBL_ASSIGNEDTO_USER_ID' => 'Assigned to (related User ID)',
  'LBL_ASSIGNEDTO' => 'Assigned to',
  'LBL_TSOURCE' => 'Source',
  'LBL_CLOSEDDATE' => 'Resolved Date',
  'LBL_CCREATEDBY' => 'ccreatedby',
  'LBL_CUSTASSIGNEDTO' => 'custassignedto',
  'LBL_MODIFIEDBYC' => 'modifiedbyc',
  'LBL_CACCOUNTNAME' => 'caccountname',
  'LBL_CATEGORY' => 'Category',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_SUBCATEGORY' => 'Sub Category',
);