<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Zotëruesi i dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Hiqe',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_NUMBER' => 'numri',
  'LBL_STATUS' => 'Statusi',
  'LBL_PRIORITY' => 'priorieti',
  'LBL_RESOLUTION' => 'Rezolucioni',
  'LBL_LAST_MODIFIED' => 'Ndryshimi i fundit',
  'LBL_WORK_LOG' => 'identifikimi i punës',
  'LBL_CREATED_BY' => 'Krijuar nga',
  'LBL_DATE_CREATED' => 'të dhëna të krijuara',
  'LBL_MODIFIED_BY' => 'modifikim i fundit nga',
  'LBL_ASSIGNED_USER' => 'drejtuar:',
  'LBL_ASSIGNED_USER_NAME' => 'Drejtuar për',
  'LBL_SYSTEM_ID' => 'ID e sistemit',
  'LBL_TEAM_NAME' => 'Emri i grupit',
  'LBL_TYPE' => 'Lloji',
  'LBL_SUBJECT' => 'Subjekti',
  'LBL_LIST_FORM_TITLE' => 'Tickets lista',
  'LBL_MODULE_NAME' => 'Tickets',
  'LBL_MODULE_TITLE' => 'Tickets',
  'LBL_MODULE_NAME_SINGULAR' => 'Tickets',
  'LBL_HOMEPAGE_TITLE' => 'Mia Tickets',
  'LNK_NEW_RECORD' => 'Krijo Tickets',
  'LNK_LIST' => 'Pamje Tickets',
  'LNK_IMPORT_CASES_TICKETS' => 'Import Tickets',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Tickets',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_CASES_TICKETS_SUBPANEL_TITLE' => 'Tickets',
  'LBL_NEW_FORM_TITLE' => 'E re Tickets',
  'LNK_IMPORT_VCARD' => 'Import Tickets vCard',
  'LBL_IMPORT' => 'Import Tickets',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Tickets record by importing a vCard from your file system.',
  'LBL_CCWHENRESOLVED' => 'Call Customer When Resolved',
  'LBL_ASSIGNEDTO_USER_ID' => 'Assigned to (related User ID)',
  'LBL_ASSIGNEDTO' => 'Assigned to',
  'LBL_TSOURCE' => 'Source',
  'LBL_CLOSEDDATE' => 'Resolved Date',
  'LBL_CCREATEDBY' => 'ccreatedby',
  'LBL_CUSTASSIGNEDTO' => 'custassignedto',
  'LBL_MODIFIEDBYC' => 'modifiedbyc',
  'LBL_CACCOUNTNAME' => 'caccountname',
  'LBL_CATEGORY' => 'Category',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_SUBCATEGORY' => 'Sub Category',
);