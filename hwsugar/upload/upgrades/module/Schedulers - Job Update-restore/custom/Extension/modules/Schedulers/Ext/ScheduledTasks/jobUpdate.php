<?php

array_push($job_strings, 'jobUpdate');

    function jobUpdate()
    {
      
      $job_id = array();		
      $query = 'SELECT j.id as id
               FROM abcde_jobs j
               JOIN abcde_jobs_cstm jc ON jc.id_c = j.id
               WHERE ((jc.status_c <> "completed"
               AND jc.status_c <> "validated"
               AND jc.status_c <> "cancelled") OR jc.status_c IS NULL)
               AND j.deleted <> "1"';
      $result = $GLOBALS['db']->query($query);
      
      while($row = $GLOBALS['db']->fetchByAssoc($result)){
         $job_id[] = $row ['id'];
         $counter++;
      }
      
      for ($i = 0; $i < sizeof($job_id); $i++){
         $job = BeanFactory::getBean('abcde_Jobs', $job_id[$i]);
         $job->save();
         //$GLOBALS['log']->fatal('Job id: '.$job_id[$i].', '.date("Y-m-d H:i:s"));
      }

     return true;
    }

?>