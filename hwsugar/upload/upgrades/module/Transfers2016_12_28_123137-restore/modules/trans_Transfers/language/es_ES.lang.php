<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id Equipo',
  'LBL_ASSIGNED_TO_ID' => 'Id del Usuario Asignado',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'Id.',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_MODIFIED' => 'Modificado por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nombre',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_DOC_OWNER' => 'Propietario del documento',
  'LBL_USER_FAVORITES' => 'Usuarios Favoritos',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nombre',
  'LBL_LIST_FORM_TITLE' => 'Transfers Lista',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Mi Transfers',
  'LNK_NEW_RECORD' => 'Crear Transfer',
  'LNK_LIST' => 'Vista Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Flujo de actividad',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);