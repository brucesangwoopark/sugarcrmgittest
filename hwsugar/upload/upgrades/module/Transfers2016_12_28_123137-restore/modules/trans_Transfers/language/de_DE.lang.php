<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_LIST_FORM_TITLE' => 'Transfers Liste',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Mein Transfers',
  'LNK_NEW_RECORD' => 'Erstellen Transfer',
  'LNK_LIST' => 'Ansicht Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Neu Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);