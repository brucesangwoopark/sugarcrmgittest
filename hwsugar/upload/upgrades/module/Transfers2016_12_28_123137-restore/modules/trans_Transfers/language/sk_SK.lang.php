<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_ASSIGNED_TO_ID' => 'Priradené používateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Priradené k',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenené používateľom',
  'LBL_MODIFIED_ID' => 'Upravené používateľom s ID',
  'LBL_MODIFIED_NAME' => 'Upravené používateľom s menom',
  'LBL_CREATED' => 'Vytvoril',
  'LBL_CREATED_ID' => 'Vytvorené používateľom s ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Obľúbení používatelia',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upravené používateľom s menom',
  'LBL_LIST_FORM_TITLE' => 'Transfers Zoznam',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Moje Transfers',
  'LNK_NEW_RECORD' => 'Vytvoriť Transfer',
  'LNK_LIST' => 'Zobraziť Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Nové Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);