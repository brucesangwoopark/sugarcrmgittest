<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID tima',
  'LBL_ASSIGNED_TO_ID' => 'ID dodijeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Dodijeljeno',
  'LBL_TAGS_LINK' => 'Oznake',
  'LBL_TAGS' => 'Oznake',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum stvaranja',
  'LBL_DATE_MODIFIED' => 'Datum izmjene',
  'LBL_MODIFIED' => 'Izmijenio/la',
  'LBL_MODIFIED_ID' => 'Izmijenio ID',
  'LBL_MODIFIED_NAME' => 'Izmijenilo ime',
  'LBL_CREATED' => 'Stvorio/la',
  'LBL_CREATED_ID' => 'Stvorio ID',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su dodali u Favorite',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Izbrisano',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Stvorio korisnik',
  'LBL_MODIFIED_USER' => 'Izmijenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Uredi',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Izmijenilo ime',
  'LBL_LIST_FORM_TITLE' => 'Transfers Popis',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Moja Transfers',
  'LNK_NEW_RECORD' => 'Stvori Transfer',
  'LNK_LIST' => 'Prikaži Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraži Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Prikaži povijest',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Pregled aktivnosti',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Novo Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);