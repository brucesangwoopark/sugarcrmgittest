<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/Resources/Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_TAGS_LINK' => 'Címkék',
  'LBL_TAGS' => 'Címkék',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_LIST_FORM_TITLE' => 'Transfers Lista',
  'LBL_MODULE_NAME' => 'Transfers',
  'LBL_MODULE_TITLE' => 'Transfers',
  'LBL_MODULE_NAME_SINGULAR' => 'Transfer',
  'LBL_HOMEPAGE_TITLE' => 'Saját Transfers',
  'LNK_NEW_RECORD' => 'Új létrehozása Transfer',
  'LNK_LIST' => 'Megtekintés Transfers',
  'LNK_IMPORT_TRANS_TRANSFERS' => 'Import Transfers',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Transfer',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_TRANS_TRANSFERS_SUBPANEL_TITLE' => 'Transfers',
  'LBL_NEW_FORM_TITLE' => 'Új Transfer',
  'LNK_IMPORT_VCARD' => 'Import Transfer vCard',
  'LBL_IMPORT' => 'Import Transfers',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transfer record by importing a vCard from your file system.',
  'LBL_OLD_ACCOUNT_ID' => 'old account id',
  'LBL_NEW_ACCOUNT_ID' => 'new account id',
  'LBL_BILLING_ADDRESS_STREET' => 'billing address street',
  'LBL_BILLING_ADDRESS_CITY' => 'billing address city',
  'LBL_BILLING_ADDRESS_STATE' => 'billing address state',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'billing address postalcode',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'billing address country',
  'LBL_TRANSFER_DATE' => 'transfer date',
);